(function() {
  'use strict';

    angular.module('lera').factory('Dialogs', DialogsFactory);

    /**
     * @desc The idea of putting all the dialogs aside is to make them easily accessible from
     * anywhere in the app, and from keyboard shortcuts
     * @param {}
     * @return {}
     */
    function DialogsFactory( $mdDialog, toastr, $log ){

      var Dialogs = function(){

      };

      Dialogs.prototype.showAddReferenceDialog = function(){

        $mdDialog.show({
            controller: function($scope, $log, $http, $interpolate, StateManager) {

                $scope.StateManager = StateManager;
                $scope.availableRefTypes = StateManager.registry.getReferenceTypes();
                $scope.refType = $scope.availableRefTypes &&
                  ! _.isEmpty($scope.availableRefTypes) &&
                    _.keys($scope.availableRefTypes)[0];

                /**
                 * @desc Stores objects retrieved from autosuggest. It will need to be transformed
                 * to get only identifiers from these objects.
                 */
                $scope.intervalParams = {};

                /**
                 * @return {object} data fields necessary to start resolving the reference.
                 # The user must input values for this fields to be able to create a Reference.
                 */
                $scope.getReferenceDataFields = function(refType){
                  var refConf = StateManager.registry.getReferenceConf(refType);
                  return refConf && refConf.fields;
                };

                /**
                 * @desc Adds the given reference to the current browser window.
                 */
                $scope.addReference = function() {

                    var valid = true;

                   _.each( $scope.getReferenceDataFields( $scope.refType ), function(field){

                      if( !$scope.intervalParams[field.name] ){
                        toastr.warning("You must fill the "+field.name+" field");
                        valid = false;
                      }
                   });

                   if( !valid ){ return; }

                    //toastr.info("Adding and resolving new reference...", "New Reference");
                    $mdDialog.cancel();

                    StateManager.addReferenceWithDefaults( $scope.refType, $scope.intervalParams, function( ref ){
                      ref.scrollToRef();
                      toastr.success("Reference "+ref.name+" added.", "New Reference");
                    }, function(err){

                    }, function( track ){
                      // TODO use searchable field declared in registry
                      return track && track.autoselectEntity( $scope.intervalParams.query );
                    });

                };

                $scope.getSelectedConfProperty = function( key ){
                  if( $scope.refType  ){
                    var conf = StateManager.registry.getReferenceConf( $scope.refType );
                    return conf && conf[key];
                  }
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

            },
            templateUrl: 'dialogs/add_reference.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
      };

      /**
       * @desc Shows a dialog allowing to add a track to a given reference
       * @param {object} Reference to which we want to add a track
       */
      Dialogs.prototype.showAddTrackDialog = function( ref ){

        $mdDialog.show({
            controller: function($scope, StateManager) {

                $scope.ref = ref;
                $scope.StateManager = StateManager;

                // TODO Preselect first track, same strategy as with selecting first reference

                /**
                 * Adds the given track to the current browser window.
                 */
                $scope.addTrack = function() {

                    //toastr.info("Adding and updating new track...", "New Track");
                    $mdDialog.cancel();
                    StateManager.addTrack(ref, $scope.trackType, {}, function( track ){
                      toastr.success("Track "+track.name+" added.", "New Track");
                    });
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

                $scope.getSelectedConfProperty = function( key ){
                  if( $scope.trackType  ){
                    var conf = StateManager.registry.getTrackConf( $scope.trackType );
                    return conf && conf[key];
                  }
                };

            },
            templateUrl: 'dialogs/add_track.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });

      };

      /**
       * @desc Allows to jump on a new location of a given reference
       * @param {object} ref
       */
      Dialogs.prototype.showSearchDialog = function( ref ){

        $mdDialog.show({
            controller: function($scope) {

              $scope.searchable = ref.searchable;
              $scope.searchableConf =
                ref.searchable &&
                ref.fields &&
                _.find(ref.fields, ['name', ref.searchable]);

              $scope.dataToEdit = _.clone(ref.getIntervalParams()); //we don't want to modyfy the data directly

              $scope.updateReferenceData = function() {

                $mdDialog.cancel(); // close dialog
                ref.replaceInterval( $scope.dataToEdit, function(){
                  toastr.success("Jumped to new location on "+ref.name,"Jump successful");
                }, function( res ){

                });
              };

              $scope.close = function() {
                  $mdDialog.cancel();
              };

            },
            templateUrl: 'dialogs/search.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
      };


      Dialogs.prototype.editEntityFiltersDialog = function( track ) {

          $mdDialog.show({
              controller: function($scope) {

                // We don't want the actual entity filters to be updated until the users clicks UPDATE
                $scope.filtersToEdit = _.clone(track.filter.getEntityFilters());

                $scope.track = track;
                $scope.intervalParams = track.getIntervalParams();

                $scope.updateEntityFilters = function() {

                  $mdDialog.cancel(); // close the dialog

                  track.updateEntityFilters( $scope.filtersToEdit, function(){

                      toastr.success("Updated Entity Filters",
                      { allowHtml: true, timeout: 10000, closeButton: true });

                  });
                };

                $scope.getTitle = function(){
                  var num = $scope.filtersToEdit.length;
                  var items_name = track.filtersAutocomplete && track.filtersAutocomplete.name || "filters";
                  return num + " " + items_name + " selected";
                };

                $scope.removeTag = function(index){
                  _.pullAt($scope.filtersToEdit, index);
                };

                $scope.hasTags = function(){
                  return ! _.isEmpty( $scope.filtersToEdit );
                };

                $scope.hasMultipleTags = function(){
                  return _.size( $scope.filtersToEdit ) > 1;
                };

                $scope.removeAllTags = function(){
                  $scope.filtersToEdit = [];
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

              },
              templateUrl: 'dialogs/edit_filters.template.html',
              parent: angular.element(document.body),
              clickOutsideToClose: true
          });
      };

      /**
       * @desc Shows a dialog allowing to select the fork target.
       * Forking creates another reference+track based on a selected entity id and
       * transformation rules in forkTargetConf
       * @param {object} track
       * @param {string} entityId
       * @param {object} forkTargetConf, configuration of the forlking, ex:
           {
             "name": "orthologs",
             "legend": "'epiro'",
             "urlTemplate": "api/autocomplete/ortholog/gene?entityId={{params.entityId}}&species={{params.species}}&query={{text}}"
           }
       * @todo check that parameters not null
       */
      Dialogs.prototype.showForkToEntityDialog = function( track, entityId, forkTargetConf ){

        $mdDialog.show({
            controller: function( $scope, StateManager, toastr ) {

              $scope.forkTargetConf = forkTargetConf;
              // Used to configure autosuggest
              // TODO remove explicit reference to species
              $scope.params = { entityId: entityId, species: track.getIntervalParam('species') };

              $scope.fork = function() {

                $mdDialog.cancel(); // close the dialog
                StateManager.fork( track, $scope.forkValue, forkTargetConf );
              };

              $scope.close = function() {
                  $mdDialog.cancel();
              };

            },
            templateUrl: 'dialogs/fork.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            onComplete: function(scope, element){
                var input = element.find("input")[0];
                return input && input.focus();
            }
        });
      };

      return new Dialogs();
    }
})();
