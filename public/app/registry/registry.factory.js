(function() {
  'use strict';

  angular.module('lera').factory('Registry', Registry);


  function Registry( $http, toastr, $log ){

      /**
       * @class
       * @memberOf lera.factories
       * @param registerURL {string} URL to retrieve all available references and tracks from the registry
       */
      var Registry = function(registryURL){

        this.registryURL = registryURL;
        this.data = {};
      };

      /**
       * @desc Retrieves from the registry server all declared tracks and references
       * @param {function} callback Function called when all data was retrieved from the registry
       */
      Registry.prototype.load = function( callback ){

        var self = this;

        // We add a t parameter to make sure to get a non cached version of the registry
        $http.get(this.registryURL+"?t="+new Date()).then(
          function(res){
            self.data = res.data;
            $log.debug('Registry data loaded. '+_.size(self.data.tracks)+' tracks and '+_.size(self.data.references)+' references are declared.');
            if( callback ){ callback(); }
          },
          function(res){
            $log.error("Can not load registry : " + JSON.stringify(res.data) + " : " + res.status);
            var message = res.data ?  res.data.message : "Can not retrieve registry data";
            toastr.error(message, 'Registry');
        });

      };

      /**
       * @desc Finds track definition from the registry given the trackType
       * @param {string} trackType : ex: ensTranscript
       * @return {object} trackConfig
       */
      Registry.prototype.getTrackConf = function( trackType ){
        return this.data.tracks && this.data.tracks[trackType];
      };

      /**
       * @param {string} trackType
       * @return {boolean} True if the given trackType is declared in the registry
       */
      Registry.prototype.isTrackTypeAvailable = function( trackType ){
        return !! trackType && this.getTrackConf( trackType );
      };

      /**
       * @param {string} trackType
       * @returns {array} references Array of supported references by the track
       */
      Registry.prototype.getSupportedReferences = function( trackType ){
        return trackType && this.data.tracks && this.data.tracks[trackType] && this.data.tracks[trackType].supportedReferences;
      };

      /**
       * @desc Finds reference definition from the registry given the refType
       * @param {string} refType ex: ensGenome
       * @return {object} refConfig
       */
      Registry.prototype.getReferenceConf = function( refType ){
        return this.data.references && this.data.references[refType];
      };


      /**
       * @return {list} List of all declared types of tracks in the registry
       * @deprecated
       */
      Registry.prototype.getTrackTypes = function(){
        return _.keys(this.data.tracks);
      };

      /**
      * @param {string} refType
      * @return {object} Dictionary of trackConfs compatibles with given refType
       */
      Registry.prototype.getTrackTypesForRefType = function(refType){
        return _.transform( this.data.tracks, function( result, trackConf, trackType ){
          if(_.some( trackConf.supportedReferences, function(rt){return rt == refType;} )){
            result[trackType] = trackConf;
          }
        }, {});
      };

      /**
       * @return {object} Dictionary of refConfs
       */
      Registry.prototype.getReferenceTypes = function(){
        return this.data.references;
      };

      /**
       * @param {object} trackConf
       * @param {string} refType
       * @returns {boolean} True if this track supports this reference type
       */
      Registry.prototype.trackSupportsReference = function( trackConf, refType ){
        // The list is very short and this function is rarely invoked, so no need for optimisations such as transforming the list into a hash
        return refType && _.some( trackConf.supportedReferences, function(rt){return rt == refType;} );
      };

      return Registry;
  }


})();
