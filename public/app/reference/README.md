A reference allows to define a uni-dimensional reference system

Multiple tracks of same or different type can be added to a reference, if they support it.

All references must be declared in the registry.

## The minimum reference declaration in the registry is :

```javascript
"ensGenome": {
  "name": "Genome",
  "url":"api/reference/genome"
}
```

## Additional optional parameters are :

Parameter | Type | Definition | Example value
-- | -- | -- | --
"unit" | string | Shown in the legend | bp
"fields" | list | REST endpoints used for autosuggest for fields used to define the reference | [{ "name":"species","urlTemplate":"api/autocomplete/species?query={{text}}"}]
"searchable" | string |  | "query"
"defaultTracks" | list | Tracks automatically added when a new reference is created | ["ensTranscript", "vcftoolsGenotype"]
