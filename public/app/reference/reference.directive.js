(function() {
    'use strict';

    angular.module('lera').directive('referenceCard', ReferenceCard);

    function ReferenceCard() {
        return {
            restrict: 'EA',
            templateUrl: 'reference/reference.template.html',
            scope: {
              ref:'='
            },
            controller: function($scope, $mdDialog, toastr, $location, $log, $sce, Dialogs) {

              $scope.Dialogs = Dialogs;

              $scope.isReferenceEmpty = function(){
                return $scope.ref && $scope.ref.isEmpty();
              };

              $scope.removeReference = function(refId){
                $scope.ref.removeFromState();
              };

            }
          };
        }
})();
