(function() {
  'use strict';

    angular.module('lera').factory('Reference', ReferenceFactory);

    function ReferenceFactory( $http, $q, toastr, $log, $interpolate, Track, StatusEnum, UpdateModeEnum ){

      var colors = [ '#006064', '#01579B', '#E65100', '#004D40', '#4CAF50', '#2962FF', '#6200EA', '#795548' ];

      /**
       * @class
       * @memberOf lera.factories
       *
       * @desc A reference is basically a one-dimensional Coordinate System.
       * The first job of a reference is to retrieve all the parameters (chromosome, start, end, etc..)
       * for a (genomic, protein, etc...) interval based on incomplete data provided by the user
       * ( a species and transcript id for example).
       * The second job of a reference is to manage changes to this interval made by the user, when browsing a track,
       * and sending the updated interval data to all depending tracks to update them.
       *
       * @param {string} refId Id allowing to refer to this reference
       * @param {string} refType type of this reference. ex: ensGenome.
       * @param {object} intervalParams Contains all the parameters which will be given to the tracks
       * ( and sent to track rest apis tu update them).
       * The rest api will be called to update the interval parameters with missing values
       * @param {object} refConf  JSON object from the registry describing a reference.
       * It must contain an URL to a REST API allowing to resolve missing parameters for this reference.
       * @param {object} stateManager Object representing the current state of the app, containing all the references.
       */
    	var Reference = function( refId, refType, intervalParams, refConf, stateManager ){

        this.refType = refType;

        /**
         * @desc The refId allows to directly access a reference in the project
         */
        this.refId = refId;

        /**
         * @desc Number used to determine in which order we should display the references.
         * This value can be changed to change the order.
         * Using this number avoids having to manage two data structues (list and hash)
         * to access references by keys and manage their order
         */
        this.order = refId;

        this.stateManager = stateManager;

        /**
         * @desc Data resolving status for this reference.
         */
        this.status = StatusEnum.PENDING;

        /**
         * @desc By default we assume the reference starts from 1. This can be overrided with the parameter data.min
         */
        this.DEFAULT_MIN = 1;

        _.defaultsDeep(this, refConf);

        if( !this.url ){
          throw "A REST API URL is mandatory for each reference, but was not declared in the registy for reference type "+refType;
        }

        /**
         * @desc Parameters describing the coordinate system used to draw the tracks
         */
        this.intervalParams = intervalParams;

        /**
         * @desc Color used to draw the references, tracks, and used as default color for entities and components
         */
        this.color = Reference._getColor( stateManager && stateManager.getReferences() );

        /**
         * @desc Tracks which depend on this reference
         */
        this.tracks = {};
      };

      Reference.trackIdCounter = 0;

      /**
       * @desc Number of references currently being resolved
       */
      Reference.resolvingCount = 0;

      /**
       * @desc Default dimensions of the track visualisation.
       * All tracks depending on the same reference have the same width.
       */
      Reference.width = 900;

      /**
       * @desc Number of ticks (segments of alternating color) that will be drawn under the track
       * to help the user compare the size of entities inside the track.
       */
      Reference.ticksCount = 10;

      /**
       * @desc Calls the server to retrieve missing properties for the current interval parameters.
       * @param {function} success The callback function that will be called after resolving of the
       * interval parameters for this reference is successfully completed.
       * We can not know in advance what reference information each depending track needs,
       * so we always try to update the reference.
       * @param {function} error If server has responded with an error.
       */
      Reference.prototype.resolve = function( success, error ){

        this._resolve( this.getIntervalParams(), success, error );
      };

      /**
       * @desc We use the REST API URL declared for this reference to update missing data about the intervalParams for this reference.
       * Bascially we send to the server all the interval data that we have (ex: start, end, chromosome or a gene and species )
       * and we ask it to complete the missing data (min and max for the chromosome, start and end for the gene, etc.)
       * @private
       * @param {object} intervalParams sent to the server to resolve the reference
       * @param {function} success The callback function that will be called after processing is successfully completed
       * We can not know in advance what reference information each depending track needs.
       * So we always try to update the reference when possible.
       * @param {function} error If server has responded with an error, or the resolved reference is still incomplete
       */
      Reference.prototype._resolve = function( intervalParams, success, error ){

        var self = this;

        self.status = StatusEnum.LOADING;
        Reference.resolvingCount++;

        $http.get( self.url, {params: intervalParams} ).then(

            function(res){
              self.status = StatusEnum.OK;
              Reference.resolvingCount--;
              if( !_.isEmpty(res.data) ){

                self.intervalParams = res.data;
                Reference._ensureNumbers( self.intervalParams, ['start', 'end', 'min', 'max']);
                var fieldNotFound = Reference._assessInterval( self.intervalParams, ['start', 'end']);
                if( fieldNotFound ){ return error && error( self, "The mandatory parameter "+fieldNotFound+" could not be resolved.");}
                return success && success( self );

              } else {
                return error && error( self, 'Could not resolve reference. Some mandatory fields are probably missing.');
              }
            },
            function(res){
              self.status = StatusEnum.ERROR;
              Reference.resolvingCount--;
              var msg = ( res.data && res.data.message ) || res.statusText || 'Can not connect to server';
              return error && error(self, "Could not resolve "+self.name+" reference : "+msg);
            }
          );
      };

      /**
       * @desc Returns the first requested field that was not found in interval
       * @param {object} interval
       * @param {list} fields : fields to look for in the interval
       * @example _assessInterval( {start: 5, chr: '2L'}, ['start'] )
       * @return {string} f : first field that was not found in the interval
       */
      Reference._assessInterval = function( interval, fields ){

        return _.find(fields, function(f){ return _.isUndefined( interval[f] ); });
      };

      /**
       * @desc Makes sure that all requested fields are numbers (transforms them into numbers)
       * Ignores fields that are not found in interval
       * WARNING: Does not manage the case where a field can not be transformed to number
       * @param {object} interval
       * @param {list} fields : fields which values should be transformed to numbers
       * @example _ensureNumbers( {start: 5, chr: '2L'}, ['start'] )
       */
      Reference._ensureNumbers = function( interval, fields ){

        _.each(fields, function(f){ if( interval[f] ){ interval[f] = +interval[f]; } });
      };

      /**
       * @desc Tries to resolve the reference with the new provided (incomplete) interval parameters.
       * If the resolve is successful, replace current interval with the new one and update depending tracks.
       * @param {object} intervalParams New interval parameters (chromosome, start, etc...) that will reaplce the previous ones
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       */
      Reference.prototype.replaceInterval = function( intervalParams, success, error ){

        var self = this;
        self._resolve( intervalParams, function(){
          self.updateTracks( success, error, UpdateModeEnum.JUMP );
        }, function(ref, msg){
          toastr.error(msg, "Could not Jump");
          return error && error( msg );
        });
      };

      /**
       * @desc This function is typically called after scrolling/zooming the view top update the reference data.
       * This triggers the update of all tracks that depend on that reference
       * The difference with replaceInterval is that this function only changes the start/stop
       * @param {integer} start Start of the new interval
       * @param {integer} end End of the new interval
       * @param {string} mode Update mode. JUMP forces to clean current pool of entities and make a server update.
       * MOVE will allow the data engine to decide if a server update is necessary.
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       * the reference first.
       */
      Reference.prototype.updateInterval = function( start, end, mode, success, error ){

        if( _.isUndefined(start) || _.isUndefined(end) ){
          $log.warn('Could not update reference '+this.refId+' because start/end undefined');
          return;
        }

        if( start >= end ){
          $log.warn('Could not update reference '+this.refId+' because start >= end');
          return;
        }

        if( start == this.getIntervalParam('start') && end == this.getIntervalParam('end') ){
          $log.info("Reference already set to interval. No update needed.");
          return;
        }

        this.setIntervalParam('start', start);
        this.setIntervalParam('end', end);
        this.updateTracks( success, error, mode );
      };

      /**
       * @desc Updates all dependant tracks with current interval parameters
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       * @param {string} mode : JUMP or MOVE. Used by data.engine to decide if a server call is necessary
       */
      Reference.prototype.updateTracks = function( success, error, mode ){

        var self = this;
        var trackCallback = function(){};

        var tracksPromise = $q.all( _.map( this.getTracks(), function(track){
          return track.update(self.getIntervalParams(), trackCallback,  mode);
        }) );

        tracksPromise.then( function(res){
          return success && success( res && res.data );
        }, function(res){
          return error && error( res && res.data );
        });
      };


      /**
       * @desc Adds a new track and synchronises it (retrieves corresponding entities/components)
       * with the current interval parameters.
       *
       * @param {string} trackType Type of the track to add, declared in the registry file
       * @param {object} trackConf Data retrieved from the registry characterising this track
       * @param {object} filters Filters that will select entities that will be displayed on the track (ex: specific samples)
       * and categories of entities/components that will be displayed (ex: only transcripts on forward strand)
       * @param {function} callback Function called when the new track has been added and updated. The new Track is passed as parameter
       * (synchronised with current reference interval)
       *
       * @return {object} track The created track (not updated yet)
       */
      Reference.prototype.addTrack = function( trackType, trackConf, filters, callback ){

        var self = this;
        var trackId = Reference.getNextTrackId( trackType );
        var track = new Track( trackId, trackType, trackConf, this, filters );
        this.tracks[trackId] = track;

        track.update( this.getIntervalParams(), function(){
          $log.debug("Track "+trackId+" added and updated.");
          if( self.stateManager ){ self.stateManager.setActiveTrack( track ); }
          if( callback ){ callback( track ); }
        }, UpdateModeEnum.JUMP);

        return track;
      };

      /**
       * @desc Deletes the given track
       * @param {string} trackId
       */
      Reference.prototype.removeTrack = function( trackId ){
        if( this.tracks[trackId] ){
          if( this.stateManager ){ this.stateManager.stackState(); }
          delete this.tracks[trackId];

          if( self.stateManager ){ self.stateManager.resetActiveTrack(); }
          $log.debug("Track "+trackId+" removed.");
        }
      };

      /**
       * @desc Returns all tracks from this reference
       * @return {list} tracks Unordered list of tracks
       */
      Reference.prototype.getTracks = function(){
        return _.values( this.tracks );
      };

      /**
       * @desc Returns an ordered list of tracks
       * @return {list} tracks Ordered by trackId
       * @todo while this is quite fast for the very low number of tracks we have,
       * we may still want to store the ordered list of tracks instead of generating it each time.
       */
      Reference.prototype.getOrderedTracks = function(){
        return _.sortBy( this.tracks, 'order' );
      };

      /**
       * @desc Returns true if track is the first track in the ordered tracks list
       * @param {object} track
       * @return {boolean} true if track is the first track in the ordered tracks list
       * @todo implement without regenerating an ordered list each time
       */
      Reference.prototype.isFirstTrack = function( track ){
        var orderedTracks = this.getOrderedTracks();
        var idx = _.indexOf( orderedTracks, track );
        return idx === 0;
      };

      /**
       * @desc Returns true if track is the last track in the ordered tracks list
       * @param {object} track
       * @return {boolean} true if track is the last track in the ordered tracks list
       * @todo implement without regenerating an ordered list each time
       */
      Reference.prototype.isLastTrack = function( track ){
        var orderedTracks = this.getOrderedTracks();
        var idx = _.indexOf( orderedTracks, track );
        return idx > 0 && idx === orderedTracks.length - 1;
      };

      /**
       * @desc Changes the order of display of a given track inside the reference
       * @param {object} track
       * @param {integer} direction The track will jump into the position given by the direction,
       * while the other track at this position will jump to the position occupied by this track
       * @return {boolean}
       */
      Reference.prototype.shiftTrack = function( track, direction ){
        var orderedTracks = this.getOrderedTracks();
        return Reference.shiftElement( orderedTracks, track, direction );
      };

      /**
       * @desc Returns true if the reference contains no tracks
       * @return {boolean} true if the reference contains no tracks
       */
      Reference.prototype.isEmpty = function(){
        return _.isEmpty( this.tracks );
      };

      /**
       * @desc Removes all tracks from this reference
       * @todo If one of them is the active track, reset the active track
       */
      Reference.prototype.removeAllTracks = function(){
        var self = this;
        _.each(self.tracks, function(track, k){
          delete self.tracks[k];
        });
        if( self.stateManager ){ self.stateManager.resetActiveTrack(); }
      };

      /**
      * @desc
       * When a track is dragged, it modifies the reference interval, so that ALL tracks depending on that reference are dragged
       * This function if applied only if this track has a start/end declared.
       * If max and min are declared for this reference, start and end will not be allowed to exceed these limits.
       * @param {integer} factor Generally the distance the mouse mouved when dragging the track, or a fixed value when
       * browsing the track woth keyboard keys. Positive to browse right, negative to browse left.
       */
      Reference.prototype.browse = function( factor ){

        if( _.isUndefined(this.getIntervalParam('start')) || _.isUndefined(this.getIntervalParam('end')) ){
          $log.warn('Can not browse a track depending on the reference '+this.refId+' with no start/stop declared');
          return;
        }

        // browsing limits.
        var min = this.getIntervalParam('min') || this.DEFAULT_MIN;
        var max = this.getIntervalParam('max');

        /**
         * We transforn the view factor into the reference factor
         * The factor divided by width gives us the fraction of the reference by which we would like to move the view
         * If the view width is 900 and factor is 30, this means than (30/900 = 0.03) => We want to move the reference by 0.03 of the ref interval
         * If the ref bounds are (5000;5050) then the ref interval width is 50. We want to move that width by 0.03 of it, or 0.03*50 = 1.5
         */
        var refFactor = Reference._getRefFactor( factor, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end') );

        var start = this.getIntervalParam('start') + refFactor;
        start = (start < min) ? min : start;

        var end = this.getIntervalParam('end') + refFactor;
        end = (max && end > max) ? max : end;

        if(start != this.getIntervalParam('start') && end != this.getIntervalParam('end')){
          if( this.stateManager ){ this.stateManager.stackState(); }
          this.updateInterval( start, end, UpdateModeEnum.MOVE );
        }
      };


      /**
      * @desc Zooms in/out of the reference interval. Note that the minimum interval is [x;x+1]
      * @param {integer} factor Positive to zoom in and negative to zoom out
      * @param {float} mouseX Position of the mouse inside the track
      */
      Reference.prototype.zoom = function( factor, mouseX ){

        if( _.isUndefined(this.getIntervalParam('start')) || _.isUndefined(this.getIntervalParam('end')) ){
            $log.warn("Can not zoom a track depending on a reference with no start/stop declared");
            return;
        }

        var refFactor = Reference._getRefFactor( factor, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end') );

        // browsing limits. default min is 0.
        var min   = this.getIntervalParam('min') || this.DEFAULT_MIN;
        var max   = this.getIntervalParam('max');

        var start = this.getIntervalParam('start') + refFactor;
        var end   = this.getIntervalParam('end') - refFactor;

        // SHIFT to zoom into mouse position and not the center of the track
        if ( mouseX ){
          var dx = Reference._getShiftFactor( this.fromView( mouseX ), this.getIntervalParam('start'), this.getIntervalParam('end'), start, end );
          start += dx;
          end += dx;
        }

        // Avoiding exceeding reference limits
        start = (start < min) ? min : start;
        end = (max && end > max) ? max : end;

        if( start < end && ( start != this.getIntervalParam('start') || end != this.getIntervalParam('end') ) ){
          if( this.stateManager ){ this.stateManager.stackState(); }
          this.updateInterval( start, end, UpdateModeEnum.MOVE );
        }
      };

      /**
       * @desc Returns the width for all references
       * @return {integer} width
       */
      Reference.prototype.getWidth = function(){
        return Reference.width;
      };

      /**
       * @desc Returns the number of tics to draw under the track
       * @return {integer} ticksCount
       */
      Reference.prototype.getTicksCount = function(){
        return Reference.ticksCount;
      };

      /**
       * @desc This factor determines by how much the real reference must move,
       * given a move of the view
       * We transforn the view factor into the reference factor
       * The factor divided by width gives us the fraction of the reference by which we would like to move the view
       * @example
       * If the view width is 900 and factor is 30, this means than (30/900 = 0.03) =>
       * We want to move the reference by 0.03 of the ref interval
       * If the ref bounds are (5000;5050) then the ref interval width is 50.
       * We want to move that width by 0.03 of it, or 0.03*50 = 1.5
       * @private
       * @param {float} factor Distance on track view
       * @param {float} with of the view
       * @param {integer} start of the reference interval
       * @param {integer} end of the reference interval
       *
       * @return {integer} factor Corresponding to the real reference interval.
       * It is allways rounded to the number further from 0, that way it is never 0.
       */
      Reference._getRefFactor = function( factor, width, start, end){
        var f = (factor / width) * ( end - start ) ;
        return f > 0 ? _.ceil(f) : _.floor(f);
      };

      /**
       * @desc Calculates the necessary reference coordinate shift so that zoom center on the mouse position
       * instead of zooming into the center of the displayed reference.
       * @param {integer} mouseRefX : coordinates of the mouse inside the reference coordinates
       * @param {integer} oldStart : start before zoom
       * @param {integer} oldEnd : end before zoom
       * @param {integer} newStart : start after zoom
       * @param {integer} newEnd : end after zoom
       * @return {integer} dx Necessary coordinate shift so that a feature under the mouse cursor before the zoom,
       * stays under mouse cursor after the zoom
       */
      Reference._getShiftFactor = function( mouseRefX, oldStart, oldEnd, newStart, newEnd ){

        if( !mouseRefX ){ return 0; }

        var oldDx = mouseRefX - oldStart;
        var newInterval = newEnd -  newStart;
        var oldInterval = oldEnd - oldStart;
        oldInterval = oldInterval ? oldInterval : 1; // never divide by 0
        var refFactor = newStart - oldStart;
        var zoomFactor = newInterval / oldInterval;
        var dx = oldDx * ( 1 - zoomFactor ) - refFactor;
        return _.round( dx );
      };

      /**
       * @desc Transforms a coordinate from the VIEW interval into the real REFERENCE interval
       * @param {float} x coordinate to transform
       * @return {integer} The coordinate inside the view transformed into reference position and rounded to closest integer
       */
      Reference.prototype.fromView = function( x ){

        if( _.isUndefined(x) ){
          throw("fromView needs valid x parameter");
        }

        return _.round( Track.transform( 0, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end'), x ) );
      };


      /**
       * @param {integer} start on the reference interval
       * @param {integer} end on the reference interval
       * @return {boolean} True if given start and end match current reference interval
       */
      Reference.prototype.intervalMatch = function( start, end ){
        return start == this.getIntervalParam('start') && end == this.getIntervalParam('end');
      };

      /**
       * @return {object} parameters describing the current reference interval
       */
      Reference.prototype.getIntervalParams = function(){
        return this.intervalParams;
      };

      /**
       * @desc Retrieves a given interval parameter
       * @example getIntervalParam('start') retrieves the current start of the reference
       * @param {string} param
       * @return {variable} paramValue
       */
      Reference.prototype.getIntervalParam = function(param){
        return this.intervalParams && this.intervalParams[param];
      };

      Reference.prototype.setIntervalParam = function(param, value){
        this.intervalParams[param] = value;
      };

      /**
       * @desc Updates the position inside the reference corresponding to the cursor position over the drawn representation
       * of a track.
       * @param {float} viewX Position of the mouse on the view of a track
       */
      Reference.prototype.setCursorPosition = function( viewX ){
        this.cursorPosition = this.fromView( viewX );
      };

      /**
       * @return {integer} Mouse position over a track translated into the position in the current reference interval.
       */
      Reference.prototype.getCursorPosition = function(){
        return this.cursorPosition;
      };

      /**
       * @desc Removes this reference from the state in which it was declared
       */
      Reference.prototype.removeFromState = function(){
        return this.stateManager.removeReference( this.refId );
      };

      /**
       * @desc Scrolls the browser window to the current position of this reference in the webpage
       */
      Reference.prototype.scrollToRef = function(){

        var refEl = $("#"+this.refId);
        if( refEl ){
          var refElBottomPos = refEl.offset() && refEl.offset().top - 65; // taking into account toolbar height
          if( refElBottomPos ){
            $("body,html").animate({ scrollTop: refElBottomPos }, 1000);
          }
        }
      };


       /**
        * @desc Generates a track id
        * @param {string} trackType
        * @return {number} id
        */
      Reference.getNextTrackId = function( trackType ){
         return ++Reference.trackIdCounter;
      };

       /**
        * @param {object} references
        * @return {string} Returns the color matching the position of the current reference in the state
        */
      Reference._getColor = function( references ){
         var self = this;
         var index = 0;
         _.each(references, function(ref, rId){
           if(rId === self.refId){
             return;
           }
           index++;
         });
         index = index > colors.length - 1 ? colors.length - 1 : index;
         return colors[index];
      };

      /**
       * @desc Exchanges the order of two elements in the list
       * @param {list} list
       * @param {object} el : object having the 'order' property
       * @param {integer} direction Number of steps to walk to the start or to the end of the list
       * @return {boolean} true if the element was successfully moved inside the list
       */
      Reference.shiftElement = function( list, el, direction ){
        if( !list || !el || !direction ){ return false; }
        var idx = _.indexOf( list, el );
        if( _.isUndefined( idx ) ){ return false; }

        var newIdx = idx + direction;
        if( newIdx < 0 || newIdx >= _.size( list ) ){ return false; }
        var otherEl = list[newIdx];
        if( _.isUndefined( otherEl ) ){ return false; }

        // Shifting order values
        var tmp = el.order;
        el.order = otherEl.order;
        otherEl.order = tmp;

        return true;
      };

      return Reference;
    }

})();
