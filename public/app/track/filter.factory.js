(function() {
  'use strict';

    angular.module('lera').factory('Filter', FilterFactory);

    function FilterFactory( $http, $q, $sce, toastr, $log, $interpolate ){

      /**
       * @constructor
       * @memberOf lera.factories
       * @desc Filters decide which entities and components should be displayed.
       *
       * ENTITY FILTERS : are a list of entities ids sent to the server, to retrieve only requested entities.
       *
       * CATEGORY FILTERS : Are applied after the date has been retrieved from the server, and before it is displayed.
       * The category filters store key/value pairs characterising the entities or components that should not be displayed.
       *
       * How this works :

       1. The server send entities and components, each containing a data object with key/value pairs, ex : { biotype: protein_coding }

       2. The server also sends a hash of categories which indicates which keys from data should be displayed on the overview.
       For example an overview categories hash { biotype => "entity" } means that we should build an overview chart using biotype keys
       in data objects from entities. The resulting overview object will look like
        { biotype: [{ name: protein_coding, y: 6, visible: true, color: red}]}

       3. Finally, when the user click on the overview legend, this adds a category filters, meaning that entities or components
       with a given key/values in their data object should not be displayed. The resulting object is like :
       { biotype: [{ name: protein_coding, y: 6, visible: true, color: red}]} ---(click)--> { biotype: { protein_coding: true } }

       To decide if an entity or component is visible, we sill compare

       { biotype: protein_coding } AND { biotype: { protein_coding: true } }

       *
       */
      var Filter = function( filters ){

        /**
         * @desc For categories the order does not matter but we want
         * to be able to rapidly check if a filter exists in a given category +> hash
         * Note that we put in categories the categories we want to HIDE, not to show !
         * ex: this.categories = {"AARA007746-RA":{"missense_variant":true}};
         */
        this.categories = {};

        /**
         * @desc For entities order matters, as we want to display entities
         * in the order they were asked for => list

         * @todo For each sample found by autosuggest, store the keywords that allowd to find it
         * @todo add properties : found/not found. entities become a list of objects instead of strings, can complicate save/load
         */
        this.entities = [];


        if( filters ){
          _.merge(this, filters);
        }

      };

      Filter.prototype.hasCategoryFilters = function(){
        return _.some( this.categories, function(cat){ return !_.isEmpty(cat);} );
      };

      Filter.prototype.hasEntityFilters = function(){
        return ! _.isEmpty( this.entities );
      };

      /**
       * @return {object} Returns ALL declared entity filters. (ex: sample_id:[sam1, sam2, sam3]})
       */
      Filter.prototype.getEntityFilters = function(){
        return this.entities;
      };


      /**
       * @desc An entity should not be drawn only if one of its data fields is in the filters and the value for this data field corresponds
       * to the filter value
       * @return {boolean} visible
       */
      Filter.prototype.isEntityVisible = function( entity ){
        return Filter.isVisible( this.categories, entity.data );
      };

      /**
       * @desc A component should not be drawn only if one of its data fields is in the filters and the value for this data field corresponds
       * to the filter value
       * @return {boolean} visible
       */
      Filter.prototype.isComponentVisible = function( component ){
        return Filter.isVisible( this.categories, component.data );
      };


      /**
       * @static
       * @desc An entity/component is usually hidden if a value from data exists in the filter
       * or if it has no value for a given entity
       * @param catFilter {object} Ex : {biotype:{protein_coding:1}}
       * @param data {object} Ex : {biotype:'protein_coding'}
       * @returns {boolean} True only if no category filter was declared for any data key,
       * or if the corresponding data value corresponds to filter values
       * @todo return ! _.some(categories, function(cat, catName){ return _.isUndefined(data && data[catName]) && cat['NONE'] === level });
       * @todo use the level (component/entity) instead of 1 in categories, ex: {biotype:{protein_coding:'entity'}}
       */
      Filter.isVisible = function(catFilter, data){
        return ! _.some(data, function(value,category){ return catFilter[category] && catFilter[category][value]; });
      };

      /**
       * @desc Updates the filters that should be shown for a category
       * @param {string} category ex: biotype
       * @param {object} filters ex: {"protein_coding":1}
       */
      Filter.prototype.setCategoryFilters = function(category, filters){
        if(category && filters){
          if(_.isEmpty(filters)){
            filters = {};
          }
          this.categories[category] = filters;
        }
      };

      /**
       * @desc Returns a hash of filters corresponding to active legend items after user click
       * @static
       * @param {object} point Clicked point
       * @param {list} points List of point objects used by highcharts to model chart data
       * @todo category[point.name] = point.level (entity/component) instead of category[point.name] = true;
       */
      Filter.legendAsFilters = function( clickedPoint, points ){

        return _.transform( points, function( category, point ){
          // visibility of current legend item will be trigerred AFTER the call to this function
          var hidden = (clickedPoint === point) ? point.visible : !point.visible;
          if( hidden ){
            category[point.name] = true;
          } else{
            delete category[point.name];
          }

        }, {});
      };

      /**
       * @desc Removes all category filters. Categories will still be displayed in the overview,
       * but no entities and components will be hidden
       */
      Filter.prototype.resetCategoryFilters = function(){
        var self = this;
        _.each( self.categories, function(cat, catName){
          self.categories[catName] = {};
        });
      };

      /**
       * @desc Wheter we should hide entities and components corresponding to this category
       * @return {boolean} true if no filters are declared for this category,
       * or if filters are declared and meta_name is NOT in declared filters
       */
      Filter.prototype.isCategoryFiltered = function( category, meta_name ){

        return !!this.categories[category] && !!this.categories[category][meta_name];
      };

      /**
       * @desc Updates the entity filters with the new value and updates the track, but only if new filters are different form the old ones
       * @param {list} entities
       * @todo return true if new filters different from old ones
       */
      Filter.prototype.updateEntityFilters = function(entities){
        this.entities = entities;
      };

      /**
       * @desc Stores a list of entities ids that were requested but not found during last server request.
       * This list in only updated when the server returns information about notFound entities (it can be an empty list)
       * @param {object} filtersStatus Object received from the server listing found and not found entities
       */
      Filter.prototype.setNotFoundFilters = function( filtersStatus ){

        if( filtersStatus && filtersStatus.notFound ){
          this.entitiesNotFound = filtersStatus.notFound;
        }
      };

      /**
       * @desc Returns the a string of requested entities that were not found, separated by commas,
       * or undefined if all requested entities were found.
       * @return {string} notFoundEntities
       */
      Filter.prototype.getNotFoundEntities = function(){
        if( this.entitiesNotFound && _.size(this.entitiesNotFound) ){
          return _.join(this.entitiesNotFound, ", ");
        }
      };

      return Filter;
    }

})();
