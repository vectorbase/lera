(function() {
  'use strict';

    angular.module('lera').factory('Renderer', EaseJsRendererFactory);

    function EaseJsRendererFactory($log, Track, Reference, ContextMenuState){


      var MAX_FONT_SIZE = 20;

      /**
       * @class
       * @memberOf lera.factories
       * @desc using easeJS library to render tracks

       This renderer bascially draw a table with rows and columns. Each lane/entity corresponds
       to a row, then components are drawn on each column.
       The difference with a real table is that columns (their position, number and width)
       is different/independant for each row.

       @todo Show a cursor:pointer when cursor over clickable element, on mouse-move event.
       Can not use directly EaselJS functionality for this as it is too exact and too expensive.
       Some alternative solutions : https://stackoverflow.com/questions/20350199/easeljs-best-way-to-detect-collision
                                    http://www.createjs.com/tutorials/HitTest/

      @todo EaseJs may add non negligeable weight in terms of memory (lots of Shape objects for genotypes)
      The problem is that to allow interactions we must store the bounding rectangle of each genotype...


       * @param browserElement {object} DOM node into which we will draw the 'genome' browser svg
       * @param browserElement {object} DOM node into which we will draw the entities names
       * @param track {object} Track from which we will draw the entities
       */
      var EaseJsRenderer = function( browserElement, entitiesElement, track){

        this.track = track;
        this.fontColor = 'black';

        $(browserElement).html("<canvas></canvas>");
        $(entitiesElement).html("<canvas></canvas>");

        this.browserStage = new createjs.Stage( $(browserElement).find('canvas')[0] );

        this.entitiesStage = new createjs.Stage( $(entitiesElement).find('canvas')[0] );
        this.entitiesStage.enableMouseOver();

      };

      /**
       * @desc Number of currently executed renderings
       */
      EaseJsRenderer.renderingsCount = 0;

      /**
       * @desc Links this renderer with a new track
       */
      EaseJsRenderer.prototype.switchTrack =function( track ){
        this.track = track;
      };

      /**
       * @desc Creates and adds the HTML for the browser and enames divs
       */
    	EaseJsRenderer.prototype.update = function(){
    		// Creates levels of non overlapping entities (calculated on real positions), then sends the y position to each entity
        // If there is only one entity, it is shown in the vertical center and takes all the height/width of the this.view
        var self = this;
        EaseJsRenderer.renderingsCount++;

        var laneGroups = self.track.getLaneGroups();
        var nbLanes = Track.countLanes( laneGroups );
        var trackHeight = this.track.getHeight( nbLanes );
        var verticalInterval = Track.getVerticalInterval( trackHeight, nbLanes );

        this.browserStage.removeAllChildren();
        this.browserStage.update();
        this.entitiesStage.removeAllChildren();
        this.entitiesStage.update();

        this.browserStage.canvas.width = Reference.width;
        this.browserStage.canvas.height = trackHeight;

        this.entitiesStage.canvas.width = 200; // TODO put all constant sized in lera conf
        this.entitiesStage.canvas.height = trackHeight;

        // Note that lanes have already been filtered to keep only entities/components corresponding
        // to the current reference interval

        var y = verticalInterval.separationHeight / 2;

        // DRAWING GROUPS
        _.each( laneGroups, function( group ){

          self.drawGroupBackground( group, y, verticalInterval.separationHeight );

          var lanes = group && group.lanes || [];

          // DRAWING LANES
      		_.each(lanes, function(lane, i){

              // DRAWING ENTITIES
              _.each(lane, function( o ){
                  self._renderEntity( o.entityId, o.entity, y, verticalInterval.entityHeight );

                  // Entity name is rendered in the legend only if there is one entity per lane
                  if(_.size(lane) === 1){
                    self._renderEntityName( o.entityId, o.entity, y, verticalInterval.entityHeight );
                  }
              });

              y += verticalInterval.separationHeight;
          });

        });

        this.browserStage.update();
        this.entitiesStage.update();
        EaseJsRenderer.renderingsCount--;
    	};

      /**
       * @todo If number of characters > limit, elipse the entity name (or use both lines ? or change lineHeight ?)
       */
      EaseJsRenderer.prototype._renderEntityName = function( entityId, entity, y, lineHeight ){

        // TODO Displaying entity ID only if one entity by lane !!

        // return if entity.name is empty
         if( !entity || !entity.name ){ return; }

         var self = this;
         var fontSize = lineHeight < MAX_FONT_SIZE ?_.round( lineHeight ) : MAX_FONT_SIZE;
         var legendEntityName = new createjs.Text( entity.name, fontSize+"px Arial", this.fontColor );

         legendEntityName.x = 10;
         legendEntityName.y = y+(fontSize/2);
         legendEntityName.textBaseline = "alphabetic";
         legendEntityName.cursor = "pointer";

         var hit = new createjs.Shape();
         var hw = legendEntityName.getMeasuredWidth();
         var hh = legendEntityName.getMeasuredHeight();
  			 hit.graphics.beginFill("#000").drawRect( 0, -hh, hw, hh );
  			 legendEntityName.hitArea = hit;

         legendEntityName.on("click", function(ejsEvent) {
           self.track.openLink( entityId );
         });

         this.entitiesStage.addChild(legendEntityName);
      };


      /**
       * @desc renders the components of the entity. If the entity has no components (ex: gene) then treats the
       * entity as a component.
       * @param {string} entityId
       * @param {object} entity
       * @param {integer} y : Height at which the components for this entity will be drawn
       * @param {integer} entityHeight : height of the entity to draw and all included components
       * @todo Display name next to entity ?
       */
      EaseJsRenderer.prototype._renderEntity = function( entityId, entity, y, entityHeight ){

        var self = this;
        // TODO Warning if entity has no components and no start
        var components = _.isEmpty(entity.components) ? [entity] : entity.components;

        _.each(components, function(component, componentId){
          if( !self.track.isOutsideInterval( component ) && self.track.filter.isComponentVisible(component)){
            var color = self.track.getColor( component, entityId, entity );
            self._renderComponent( entityId, componentId, component, y, entityHeight, color );
          }
        });
      };



       /**
        * @desc This function decides of the shape of each component, and transforms real positions of components into view coordinates
        * @param {string} entityId
        * @param {string} componentId
        * @param {object} component
        * @param {float} y
        * @param {float} entityHeight : maximum height allowed for this entity
        * @param {string} color
        */
      EaseJsRenderer.prototype._renderComponent = function( entityId, componentId, component, y, entityHeight, color ){

        var refStart = component.start;

        // Start is the only mandatory parameter
        if( _.isUndefined( refStart ) ){
          throw("start is mandatory for all components. It is missing from component "+componentId+" of entity "+entityId);
        }

        var refEnd =  component.end || refStart; // If not end declared for component, we use start as end

        // TODO Maybe special simple calculations for a unique point ? (start == end) Would be faster ??
        var pos   = this.track.toViewAdjusted( refStart, refEnd );
        var start = pos.start;
        var end   = pos.end;
        var shape = component.shape || this.track.defaultComponentShape;

        // TODO don't render component if something already drawn at the exact same position(s)

        if( !shape ){
          $log.warn("No shape declared for component");
          return;
        }

        var canvasComponent = new createjs.Shape();

        canvasComponent.on("click", function(ejsEvent) {
          var e = ejsEvent && ejsEvent.nativeEvent;
          ContextMenuState.showInfoMenu(e, entityId, componentId);
        });

        // Height can be dynamic, to create histograms for example
        if( ! _.isUndefined( component.heightFactor ) ){
          entityHeight *= component.heightFactor;
        }

        // We can set max height for entities
        if( this.track.maxEntityHeight && entityHeight > this.track.maxEntityHeight ){
          entityHeight = this.track.maxEntityHeight;
        }

        var canvasComponentGraphics = canvasComponent.graphics.beginFill(color);

        switch( shape ){

          case "thick-full" : canvasComponentGraphics.s(color).ss(entityHeight).mt(start, y).lt(end, y);break;
          case "thin"       : canvasComponentGraphics.s(color).mt(start, y).lt(end, y);break;
          default           : $log.warn("Invalid shape "+shape);
        }

        this.browserStage.addChild(canvasComponent);
      };


      /**
       * @desc Draws the ticks directly on the canvas to separate the groups
       * @param {float} y Vertical position where to draw the line
       */
      EaseJsRenderer.prototype.drawSeparationLine = function( y ){

        var separationLine = new createjs.Shape();
        var tickCount = this.track.getReference().getTicksCount();
        var tickWidth = Reference.width / tickCount;
        var ticks = _.range( 0, Reference.width, tickWidth );

        _.each( ticks, function( w, i ){
          var color = i % 2 ? '#0097A7' : '#80DEEA';
          var separationLineGraphics =
            separationLine.graphics
            .ss(2)
            .s(color)
            .mt(w, y)
            .lt(w+tickWidth, y);
        });

        this.browserStage.addChild( separationLine );
      };

      /**
       * @desc Draws a background rectangle for a group
       * @param {object} group Configuration object for this group
       * @param {float} y Horizontal position on which entities are drawn
       * @param {float} separationHeight Maximum height that an entity can occupy
       */
      EaseJsRenderer.prototype.drawGroupBackground = function( group, y, separationHeight){
        // Draw background ONLY if more than 1 non empty groups AND if color declared for value colors: {'+':'red', '-':'blue'}
        var groupRect = EaseJsRenderer.calculateGroupRect( group, y, separationHeight, Reference.width );
        if( groupRect ){
          var background = new createjs.Shape();
          background.graphics
            .beginFill( groupRect.c )//group.conf.color )
            .drawRect( groupRect.x, groupRect.y, groupRect.w, groupRect.h );
          this.browserStage.addChild( background );

          this.drawSeparationLine( groupRect.y +1 );
        }
      };

      /**
       * @desc Creates the parameters for a background rectangle for a group
       * @param {}
       * @return {object} A {x,y,w,h,c} object describing the background rectangle to draw, or undefined if
       * no configuration available to draw a background for this group
       */
      EaseJsRenderer.calculateGroupRect = function( group, y, separationHeight, canvasWidth ){
        if( _.isEmpty(group && group.lanes)){ return; }
        var color = group.name && group.conf && group.conf.colors && group.conf.colors[group.name];
        if( color ){
          var backgroundStart = y - separationHeight / 2;
          var backgroundHeight = _.size( group.lanes ) * separationHeight;
          return { x:0, y: backgroundStart, w: canvasWidth, h: backgroundHeight, c: color };
        }
      };

      return EaseJsRenderer;
    }

})();
