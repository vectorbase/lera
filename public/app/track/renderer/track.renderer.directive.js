(function() {
  'use strict';

  angular.module('lera').directive('trackRenderer', TrackRenderer);

  function TrackRenderer( $sce, Renderer, $rootScope, $log ){

      return {
          restrict: 'EA',
          templateUrl: 'track/renderer/track.renderer.template.html',

          link: function(scope, element){

            /**
             * @desc The renderer is expected to implemet following method :
             * - update() : recreate/update the DOM
             */
            scope.renderer = createNewRenderer();

            scope.ticks = _.range(scope.track.getReference().getTicksCount() );

            scope.$watch("track.lastUpdate", updateView, true);

            // If a track replaces another track at the same position (id),
            // this directive becomes associated with a different track.
            // We need to recreate a renderer with a pointer to this news track.
            scope.$watch("track", function(newValue, oldValue){
              if(newValue !== oldValue){
                scope.renderer = createNewRenderer();
              }
            });

            /**
             * @desc Rerenders the canvas
             */
            function updateView(newValue, oldValue){

              if(newValue!==oldValue){
                  var startTime = performance.now();

                  scope.renderer.update();

                  var renderingTime = performance.now() - startTime;
                  if( renderingTime > 100 ){ $log.warn("Rendering calculations done in "+renderingTime+" ms."); }
              }
            }


            /**
             * @return {object} A new Renderer instance with the current scope view and track.
             */
            function createNewRenderer(){
              return new Renderer(
                $(element).find(".browser")[0],
                $(element).find(".enames")[0],
                scope.track
              );
            }

          }

        };
      }
})();
