// 1. Decide wheter data should be retrieved from server
// track api provider sends data about thresholds (list of integers) with entities and meta
// if user zooms in/out then see if one of the thresholds is in [oldIntervalSize;newIntervalSize]

// if filters or master data has changed, must get data from server again

// Contains a pool of entities for that track, larger than the entities shown in the track for an interval
// This pool of entities can never contain holes (entities can only be removed by the edges)
// The DataEngine maintains the pool of entities to keep the following equilibrium :


// WHEN NOT TO MAKE SERVER REQUESTS :
// 1. TODO REMOVED entity filters
// 2. New zoom/browse interval inside current virtual interval


(function() {
  'use strict';

    angular.module('lera').constant('StatusEnum',
      {
        PENDING: 'pending', // Track just created, no update performed
        OK: 'ok', // Last update successfully performed
        ERROR: 'error', // Last update was a failure
        WARNING: 'warn' // Incorrect parameters or need user action
      });

    angular.module('lera').constant('UpdateModeEnum',
      {
        /**
         * Moving to another genomic location, chromosome genome...
         * 1. Force clean entities pool
         * 2. Force server update
         */
        JUMP: 'jump',

        /**
         * Sliding the window when dragging / zooming, adjacent entities
         * Updates the virtual pool of entities with result of server request
         */
        MOVE: 'move',

        /**
         * Returns immediately after recalculating lanes and overview
         */
        VIRT: 'virtual_update'

      });


    angular.module('lera').factory('DataEngine', DataEngineFactory);

    function DataEngineFactory( $http, $q, toastr, $log, StatusEnum, UpdateModeEnum ){

      /**
      * @desc Each track is associated with a Data Engine whcih will retrive entities for given parameters.
      * making server calls when necessary.
       * @class
       * @memberOf lera.factories
       */
      var DataEngine = function( url ){

        /**
         * @desc Last stable data status for this data engine. This is
         * the status obtained after the last update.
         */
        this.status = StatusEnum.PENDING;

        this.lastUpdateMode = undefined;

        /**
         * @desc Latest parameters received from the track to do an update.
         * Not necesserally parameters sent to the server.
         */
        this.lastRequestedParams = undefined;

        /**
         * @desc Allows to cancel the current server request
         */
        this.canceller = $q.defer();

        /**
         * @desc hash of entities. Entities are the highest level objects displayed on a track.
         * Usually entities are displayed one per lane.
         * registry->reference->track->entity->component is just a tree of relationships. Biological nature has no importance.
         *
         * Entities are generally composed of lower level objects called components. Components can be genotypes of a sample entity or
         * exons and introns from a transcript. Components are allways drawn on the same lane.
         *
         * This is a virtual pool of entities. It contains all entities retrieved with server updates.
         * It should contain >= of entities displayed in the browser.
         *
         * NOTE: Currently this pool of entities is reseted only on JUMP. So if the user keeps MOVEing and MOVEing,
         * the pool will grow bigger and bigger. We may consider a threshold based on number of entities or interval size
         * and remove some entities/components from the virtual pool when it becomes too big.
         *
         */
        this.virtualEntities = {};

        /**
         * @desc URL of the REST API endpoint to retrieve data for this track
         */
        this.url = url;

        this.virtualParams = undefined;

        /**
         * @desc Number of server calls this data engine is currently running
         * This does not count the preload server calls.
         */
        this.serverCalls = 0;
      };

      /**
       * @desc Number of updating requests being currently run for all tracks by all
       * created data engines
       */
      DataEngine.updatingCount = 0;

      /**
       * @desc HTTP error code used when there is no real server/client error,
       * but the server wants to communicate
       * some information to the user (generally asking to specify some data)
       */
      DataEngine.USER_ERROR_CODE = 418;

      /**
       * @desc Updates the pool of entities if necessary
       * @param {object} intervalParams : new parameters for the track, sent by the reference
       * @param {object} trackParams : parameters specific to the track, including entity filters
       * @param {function} success : called if server responded with 200
       * @param {function} error : called if server responded with something else than 200
       * @param {string} rawMode : JUMP or MOVE. Helps the system to determine whether a server update is mandatory
       */
      DataEngine.prototype.update = function( intervalParams, trackParams, success, error, rawMode ){

        var self = this;
        var mode = self.getCorrectedMode( intervalParams, rawMode );
        this.lastUpdateMode = mode;

        switch( mode ){

          case UpdateModeEnum.VIRT: self._virt( intervalParams, trackParams, success, error ); break;
          case UpdateModeEnum.MOVE: self._move( intervalParams, trackParams, success, error ); break;
          case UpdateModeEnum.JUMP: self._jump( intervalParams, trackParams, success, error ); break;
        }

      };

      /**
       * @desc This update is executed when the user navigate inside the pool of virtual entities,
       * and no new entities/components must be retrieved from the server.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._virt = function( intervalParams, trackParams, success, error ){

          var self = this;
          $log.debug("Performing VIRT update for "+self.url+"("+self.status+")");
          self.setLastRequestedParams(intervalParams);

          self.cancelCurrentRequest();

          self.status = StatusEnum.OK;
          DataEngine._logParams(UpdateModeEnum.VIRT, self.virtualParams, intervalParams);

          return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories );
      };

      /**
       * @desc Appends new entities to the existing virtual pool of entities,
       * by making a server request.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._move = function( intervalParams, trackParams, success, error ){

        var self = this;

        $log.debug("Performing MOVE server update for "+self.url);
        self.setLastRequestedParams(intervalParams);

        // Optimising size of interval to query for browsing left/right
        // Not yet adapted for the two queries required to zoom-out
        var adaptedIntervals = DataEngine.adaptQueryInterval( self.lastRequestedParams, self.virtualParams );
        if( adaptedIntervals.length === 1){
          intervalParams = adaptedIntervals[0];
        }

        return self.serverUpdate( intervalParams, trackParams, function( responseParams, entities, overviewCategories ){

          // When a MOVE request is finished, the big quest is how relevant is the retrieved data to the current
          // state of the virtual pool, as many others VIRT, MOVE or JUMP requests may have finished before this one.
          // The simpliest would be to ignore all data retrieved for all but the latest request.

            // The callback is called only if the server request returns data for the last asked parameters,

              // use _.merge instead to give priority to newly retrieved objects on existing objects
              _.defaultsDeep( self.virtualEntities, entities );
              _.defaultsDeep( self.overviewCategories, overviewCategories ); // Merging with previous categories
              self.virtualParams = {
                start: (self.virtualParams.start < responseParams.start) ? self.virtualParams.start : responseParams.start,
                end: (self.virtualParams.end > responseParams.end) ? self.virtualParams.end : responseParams.end
              };

              self.status = StatusEnum.OK;
              DataEngine._logParams(UpdateModeEnum.MOVE, self.virtualParams, responseParams);
              return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories );

          }, function( responseParams, errorMessage, status ){

                self.status = status;
                return error && error( self.lastRequestedParams, errorMessage );
          }  );
      };

      /**
       * @desc This update replaces th current virtual pool of entities with data retrieved from the server.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._jump = function( intervalParams, trackParams, success, error ){

        var self = this;
        $log.debug("Performing JUMP server update for "+self.url);
        self.setLastRequestedParams(intervalParams);

        return self.serverUpdate( intervalParams, trackParams, function( responseParams, entities, overviewCategories, filtersStatus ){

              self.status = StatusEnum.OK;
              self.virtualEntities = entities;
              self.overviewCategories = overviewCategories; // Replacing all previous categories
              self.virtualParams = {
                start: responseParams.start,
                end: responseParams.end
              };

              return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories, filtersStatus );

          }, function( responseParams, errorMessage, status ){

                self.status = status;
                self.virtualParams = undefined;
                $log.debug("Invalidating virtual interval after unsuccessful jump");
                return error && error( self.lastRequestedParams, errorMessage );
          } );

      };

      /**
       * @desc Decides which king of update should be performed
       * -  By default we JUMP, which means that the current virtual pool of entities is completely renewed with
       *    entities retrieved from the server.
       * -  If the new intervalParams intersect with the current pool of entities, we add new entities from
       *    the server to the current pool.
       * -  If the new intervalParams are inside the current virtual pool of entities, no server call is necessary,
       *    so we perforam a VIRTUAL update
       *
       * @param {string} mode Requested MODE. This function will determine which
       * is the best mode to use, given the requested mode.
       */
      DataEngine.prototype.getCorrectedMode = function( intervalParams, mode ){
        var self = this;

        if( mode == UpdateModeEnum.MOVE && self.isIntersect( intervalParams ) ){

          if( self.isInside( intervalParams ) ){
            return UpdateModeEnum.VIRT;
          }
          return UpdateModeEnum.MOVE;

        } else {
          return UpdateModeEnum.JUMP;
        }
        return UpdateModeEnum.JUMP;
      };


       /**
        * @desc We retrieve from the rest api endpoint declared for this track in the registry, alle entities corresponding
        * to the current intervalParams from the reference, and entity filters declared for this track.
        *
        * The data engine associated to a track should run only one server request at a given time.
        *
        *
        * @param {object} newIntervalParams new intervalParams describing the interval for which the track must be updated
        * @param {object} newTrackParams object containing the filter with the filtered entities
        * that must be displayed in this track.
        * @param {function} success Function executed if the server call is successfull.
        *    This function will be called with following parameters : requestParams, requestEntities, requestCategories, filtersStatus
        * @param {function} error Function executed if the server call resulted in an error.
        *    This can be a server error or a user error (missing information)
             Called with parameters : requestParams, errorMessage, status
        * @returns {object} promise The promise used to query the data from the server
        * @todo Server can decide to send a different interval from the interval asked.
        *       Ex: peptype api will send all the data for a protein (as its simplier for VCF retrieval)
        *       So it needs a way to say to the data engine that it returns more data than requested to avoid
        *       unecessary MOVE queries
        * @todo It could have a parallel mechanism for automatic PREL requests.
        */
      DataEngine.prototype.serverUpdate = function( newIntervalParams, newTrackParams, success, error ){

        var self = this;

        // https://stackoverflow.com/questions/35375120/cancelling-ongoing-angular-http-requests-when-theres-a-new-request
        // http://www.frontcoded.com/angular-cancel-$http-request.html
        self.cancelCurrentRequest();

        var promise = DataEngine.getPromise( self.url, newIntervalParams, newTrackParams, self.canceller );

        DataEngine.updatingCount++;
        self.serverCalls++;

        promise.then(
          function(res){
            //console.log("OK: "+res.config.url+" : "+JSON.stringify(res.config.data)+" : "+JSON.stringify(_.keys(res.data.entities)));

            // The reference data that was used to retrieve this track
            var intervalParams = res && res.config && res.config.data && res.config.data.intervalParams;
            var requestEntities = res.data.entities;
            var requestCategories = res.data.categories;
            var filtersStatus = res && res.data && res.data.about && res.data.about.filtersStatus;

            DataEngine.updatingCount--;
            self.serverCalls--;
            return success && success( intervalParams, requestEntities, requestCategories, filtersStatus );
          },
          function(res){

            var status = ( res.status == DataEngine.USER_ERROR_CODE ) ? StatusEnum.WARNING : StatusEnum.ERROR;
            var intervalParams = res && res.config && res.config.data && res.config.data.intervalParams;
            var errorMessage = ( res.data && res.data.message ) || res.statusText || 'Can not connect to server';

            DataEngine.updatingCount--;
            self.serverCalls--;

            // Differentiate between canceled by us or server not accessible
            if( res && res.config && res.config.timeout && res.config.timeout.leraDataEngineCancel ){
               $log.debug("Cancelled unfinished request.");
               return;
            }

            return error && error( intervalParams, errorMessage, status );
          }
        );

        return promise;
      };

      /**
       * @desc Cancels the current server request. Should be executed when a new request is executed,
       * to cancel any previous requests.
       * Each canceller promise is associated to a single request.
       * @private
       */
      DataEngine.prototype.cancelCurrentRequest = function(){
        if(this.canceller){
          // To differentiate between when we cancel a request and when the server does not responds
          this.canceller.promise.leraDataEngineCancel = true;
          this.canceller.resolve("cancelled");
          this.canceller = $q.defer();
        }
      };


      /**
       * @desc Stores a copy of the parameters used for the last update
       * @param {object} params
       */
      DataEngine.prototype.setLastRequestedParams = function( params ){
        this.lastRequestedParams = _.cloneDeep( params );
      };

      /**
       * @desc Returns all entities from the current virtual pool.
       * Those are all entities retrieved from the server since the last JUMP,
       * and correspond to an interval which is smaller or equal to the current interval
       * @return {object} entities
       */
      DataEngine.prototype.getVirtualEntities = function(){
        return this.virtualEntities;
      };

      /**
       * @param {object} requestInterval Interval (start,end) for which entities must be displayed on the track
       * @return {boolean} true if the requested interval intersects with current virtual interval
       */
      DataEngine.prototype.isIntersect = function( requestInterval ){
        return requestInterval && this.virtualParams && this.virtualParams.start && this.virtualParams.end &&
          ! ( requestInterval.end < this.virtualParams.start || requestInterval.start > this.virtualParams.end );
      };

      /**
       * @param {object} requestInterval Interval (start,end) for which entities must be displayed on the track
       * @return {boolean} true if the requested interval is inside the current virtual interval
       */
      DataEngine.prototype.isInside = function( requestInterval ){
        return requestInterval && this.virtualParams && this.virtualParams.start && this.virtualParams.end &&
          requestInterval.start >= this.virtualParams.start && requestInterval.end <= this.virtualParams.end;
      };

      /**
       * @desc Returns the intervalParams defining the current virtual pool of entities
       * @return {object} virtualParams
       */
      DataEngine.prototype.getVirtualParams = function(){
        return this.virtualParams;
      };

      /**
       * @return {object} Last parameters requested by the track
       */
      DataEngine.prototype.getRealParams = function(){
        return this.lastRequestedParams;
      };

      /**
       * @desc Returns true if data for this track is currently retrieved from the server
       * @return {boolean} isLoading
       */
      DataEngine.prototype.isLoading = function(){
        return this.serverCalls !== 0;
      };



      /**
       * @desc prints to the console log the virtual and real intervals
       * @param {}
       */
      DataEngine._logParams = function(mode, virt, real, prefix){
        // $log.debug( "\t"+(prefix || "")+" AFTER "+mode+" VIRT("+virt.start+"-"+virt.end+") REAL("+real.start+"-"+real.end+")");
      };

      /**
       * @param {object} position : the object for which we want to know if it is outside the given interval
       * @param {object} interval
       */
      DataEngine.isOutsideInterval = function( position, interval ){

        if( !interval || _.isUndefined(interval.start) || _.isUndefined(interval.end) ||
                 !position || _.isUndefined(position.start) ){ return false; }

        return (
          ( position.start < interval.start && ( _.isUndefined(position.end) || position.end < interval.start )) ||
          ( position.start > interval.end && ( _.isUndefined(position.end) || position.end > interval.end ))
        );
      };

      /**
       * @desc When possible reduces the interval sent to the server to retrieve only missing entities/components
       * To use only for a MOVE update.
       * @param {object} queryInterval Parameters (start, end, etc) for the new interval to retrieve
       * @return {list} virtualInterval Interval (start, end, etc) corresponding to the current virualInterval
       */
      DataEngine.adaptQueryInterval = function( queryInterval, virtualInterval ){

        // We do not need to check if both intervals intersect, because the MOVE update is only called if the do.
        var adaptedIntervals = [];

        if( queryInterval.start < virtualInterval.start ){
          var newLeftInterval = _.cloneDeep( queryInterval );
          newLeftInterval.end = virtualInterval.start;
          adaptedIntervals.push( newLeftInterval );
        }

        if( queryInterval.end > virtualInterval.end ){
          var newRightInterval = _.cloneDeep( queryInterval );
          newRightInterval.start = virtualInterval.end;
          adaptedIntervals.push( newRightInterval );
        }

        return adaptedIntervals;
      };

      /**
       * @desc Created a $http promise, allowing to the intervalParams from the reference and filters from the track
       * @param {string} url URL to which send the reference and track params
       * @param {object} intervalParams data object
       * @param {object} trackParams object
       * @param {object} canceller (optional) Used to cancel a previous request
       * @return {object} The promise to the server call to retrieve track data
       */
      DataEngine.getPromise = function( url, intervalParams, trackParams, canceller ){

        var options = {
          method: 'POST',
          url   : url,
          data  : _.cloneDeep({
            intervalParams: intervalParams,
            trackParams: trackParams // ex: samples, ignore homo_ref, experience...
          })
        };

        if( canceller ){ options.timeout = canceller.promise; }

        return $http( options );
      };


      /**
       * @desc Returns the extreme bounds of visible entities in the current view
       * @param {object} entities
       * @param {object} filter The filter contains category filters, used to decide wheter considered objects
       * are visible or not. If they are not visible, their bounds are not taken into consideration.
       * @param {object} interval (optional) Objects outside the interval are not taken into consideration
       * @return {object} (start, end) of bounds of submitted entities (min start and max end from all entities)
       */
      DataEngine.getEntitiesBounds = function( entities, filter, interval ){
        var self = this;
        var starts = _.map( _.filter( entities, function(e){
          return filter.isEntityVisible(e) && ! DataEngine.isOutsideInterval( e, interval);
        } ), 'start');
        var ends = _.map( _.filter( entities, function(e){
          return filter.isEntityVisible(e) && ! DataEngine.isOutsideInterval( e, interval);
        } ), 'end');
        return {
          start: _.min( starts ),
          end  : _.max( ends )
        };
      };

      /**
       * @desc Returns true if no track is corrently using his dataEngine to update its data.
       * @return {boolean} allTracksStandby
       */
      DataEngine.allTracksStandby = function(){
        return DataEngine.updatingCount === 0;
      };


      return DataEngine;
    }

})();
