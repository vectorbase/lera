(function() {
    'use strict';

    angular.module('lera').directive('trackCard', TrackCard);

    function TrackCard($mdDialog, toastr, $log, $sce, StateManager, Overview, Dialogs) {
        return {
            restrict: 'EA',
            templateUrl: 'track/track.template.html',
            scope: {
              track:'='
            },
            link: function(scope, element){

              $(element).on('click', '.message .fa-edit', function(){
                Dialogs.editEntityFiltersDialog( scope.track );
              });

            },
            controller: function($scope) {

                $scope.StateManager = StateManager;
                $scope.Overview = Overview;
                $scope.Dialogs = Dialogs;

                // TODO watch for reference.cursorPosition and change coordinated of the red line div accordingly
                // absolute coordinated for the page, given track start and end height, and width = track.start + cursorPosition
                // TODO line draw in very transparent red, to see what is behind

                $scope.setView = function() {
                    $scope.track.optimiseView();
                };

                $scope.remove = function() {
                    $scope.track.removeFromReference();
                };

                $scope.changeDisplayOrder = function( direction ){
                    $scope.track.shiftTrack( direction );
                };

                $scope.canMoveUp = function(){
                    return ! $scope.track.getReference().isFirstTrack( $scope.track );
                };

                $scope.canMoveDown = function(){
                    return ! $scope.track.getReference().isLastTrack( $scope.track );
                };

                $scope.export = function() {
                    $scope.track.exportTrack();
                };

                $scope.getHeader = function() {
                  var header = '';

                  var headerTemp = $scope.track.headerTemplate;
                  var params = $scope.track.getIntervalParams();
                  if( headerTemp && ! _.isEmpty( params ) ){
                    header += headerTemp({ ref: params, track: $scope.track });
                  }

                  var cp = $scope.track.getReference().getCursorPosition();
                  if( !_.isUndefined(cp)){ header += " : " + cp; }

                  return header;
                };

                $scope.getLegend = function(){
                  if( $scope.track.legendTemplate ){
                    return $sce.trustAsHtml( $scope.track.legendTemplate({ ref: $scope.track.getIntervalParams(), track: $scope.track }) );
                  }
                };

            }
          };
        }
})();
