
All tracks must be declared in the registry.

## The minimum track declaration in the registry is :

```javascript
"ensProtein": {
  "name": "Protein features",
  "supportedReferences":["ensProtein"],
  "url":"api/track/protein"
}
```

## Additional optional parameters are :

Parameter | Type | Definition | Example value
-- | -- | -- | --
"description" | string | Shown in the "add track" dialog |
"name" | string | | "Transcript"
"image" | string | Shown in the 'add track' menu | "images/data_trans.png"
"morphing" | object | |
"forksTo" | object | |
"headerTemplate" | string | | "{{ref.species}}, {{ref.seq_region_name}}"
"links" | object | Links to external ressources shown for each entity | {"VectorBase":"https://www.vectorbase.org/{{ref.species}}/Transcript/Summary?db=core;t={{entity_id}}"}
