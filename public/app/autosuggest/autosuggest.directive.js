(function() {
  'use strict';

  angular.module('lera').directive('autosuggest', Autosuggest);

  /**
   * @param {object} field Declared in registry, contains all necessary information to retrieve data from the user
   * for a specific params field
   * @param {string} value Value for the params field that we want to autocomplete
   * @param {list} values (optional) Ensembl of values selected by the user. If this list is declared, when a user selects
   * a value, it is added to values
   * @param {object} params: already filled parameters for this interval that can be used as context/filter to
   * retrieve autocomplete suggestions. For example 'species' can be used to filter transcript or sample suggestions
   * @param {boolean} forceObjectValue Instead of setting the value to the object identifier, it is set to the whole object.
   * The object identifier is still displayed in the input
   * @param {boolean} showExample Display some example hits on focus, by setting typeahead-min-length to 0
   * @todo 'unselect/reset' button to remove selected value
   */
  function Autosuggest(){
    return {
        restrict: 'E',
        templateUrl: 'autosuggest/autosuggest.template.html',
        scope: {
          field: '=',
          value: '=',
          values: '=',
          params: '=',
          forceObjectValue: '@',
          showExample: '@'
        },
        controller:function($scope, toastr, $interpolate, $http, $log){

          /**
           * @desc Suggestions are shown when at least one character is typed by the user, except if
           * showExample is set to true, then some example hits are shown on input focus
           */
          $scope.triggerLength = $scope.showExample ? 0 : 1;

          /**
           * @desc If an url is declared for the autocomplete of this data attribute, we use it for autocomplete.
           * If not we return an empty list.
           * @param {string} text
           * @return {array} suggestions
           */
          $scope.getSuggestions = function( text ){

            if( $scope.field && $scope.field.urlTemplate ){
              $scope.url = $interpolate( $scope.field.urlTemplate );
              text = text && encodeURIComponent(text);

              return $http.post(
                $scope.url({text:text, params: $scope.params }),
                { exclude: $scope.values }
              ).then(
                function( res ){

                  // In case of multivalued, we store the suggestions to have access outside typeahead directive scope
                  if ( $scope.values ){
                    $scope.suggestions = _.map(res.data, function($item){return $scope.valueFromItem($item);});
                    if( res.data ){ res.data.unshift({command: "add_all", commandName: "Add top 10 hits"}); }
                  }

                  return res.data;
                },
                function( res ){
                  var message = (res.data && res.data.message) || "Can not reach server";
                  $log.error("Could not reach autocomplete endpoint for "+$scope.field.name+" : "+message);
                  toastr.warning(message, "Autocomplete for "+$scope.field.name);
                  return [];
                }
              );

            } else{
              $log.debug("Will not autocomplete field "+($scope.field && $scope.field.name)+" as it has no urlTemplate declared");
              return [];
            }
          };

          /**
           * @return {boolean} True only if there is an autocomplete URL declared for this field
           */
          $scope.hasAutocomplete = function(){
            return !!($scope.url);
          };


          /**
           * @return {string} The text message explaining the currnt status of the autosuggest
           * @todo Allways show selected ? Even with no resultsw ??
           */
          $scope.getHint = function(){
            if( $scope.isLoading ){
              return 'Searching...';
            } else if( $scope.field && $scope.noResults ){
              return '<div class="hint-bad">No results...</div>';
            } else if( $scope.value ){
              return '<div class="hint-selected">Selected: '+ ($scope.value.identifier || $scope.value)+'</div>';
            } else if( $scope.field && $scope.field.legend ){
              return $scope.field.legend;
            } else {
              return 'Suggestions not available for this field';
            }
          };


          /**
           * @desc When a suggestion is selected, 'value' is set to suggestion identifier
           * if multivalued, add value to values and clean selected
           * @todo If multivalued (values defined), rest value of scope at the end
           */
          $scope.onSelect = function( $item, $model, $label, $event ){

            // Removing user input
            $scope.rawValue = "";
            $scope.triggerLength = 1; // triggerLength back to default value when an element selected

            // Some autosuggest suggestions are clickable commands
            if( $item.command ){
              // Adds all (10) suggestions to values
              if( $item.command === 'add_all' ){
                $scope.rawValue = "";
                $scope.addAllToValues( $scope.suggestions );
              }
              return;
            }

            // Setting value to selected
            $scope.value = $scope.valueFromItem( $item );

            // reseting/invalidating specified fields
            if( $scope.params && $scope.field && $scope.field.reset ){
              $scope.params[ $scope.field.reset ] = undefined;
            }

            // setting additional fields if requested. Setting will refuse to set an
            // empty or undefined value. Use reset for that.
            if( $scope.params && $scope.field && $scope.field.set ){
              _.each( $scope.field.set, function( valueToSet, fieldToSet){
                var val = $item && valueToSet && $item[ valueToSet ];
                if ( !_.isUndefined(val) ){ $scope.params[ fieldToSet ] = val; }
              });
            }


            if(! $scope.existsValue( $scope.value )){
              return $scope.addToValues( $scope.value );
            }

          };

          /**
           * @return The identifier of the selected item, if it has an identifier and
           * we do not explicitely ask to have an object, or the item otherwise.
           */
          $scope.valueFromItem = function( $item ){
            return ( !$scope.forceObjectValue && $item && $item.identifier ) || $item;
          };


        /**
           * @desc adds/removes a specific value from values, if multivalued
           * @param {object} item The sample to add/remove from project cart
           */
          $scope.toggleValue = function( value ) {

            if ( $scope.existsValue( value ) ){
              return $scope.removeFromValues( value );
            }
            else{
              return $scope.addToValues( value );
            }
          };

          /**
           * @return true if the value exists in values, and values exists
           */
          $scope.existsValue = function( value ) {
            return !_.isUndefined(value) && $scope.values && _.includes($scope.values, value);
          };

          $scope.addToValues = function( value ){
            return !_.isUndefined(value) &&
                   ! $scope.existsValue( value ) &&
                   $scope.values &&
                   $scope.values.push( value );
          };

          $scope.addAllToValues = function( values ){
            return _.each( values, function( value ){ $scope.addToValues( value ); });
          };

          $scope.removeFromValues = function( value ){
            return !_.isUndefined(value) && $scope.values && _.pull($scope.values, value);
          };


        }
    };
  }

})();
