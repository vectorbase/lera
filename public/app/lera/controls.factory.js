(function() {
  'use strict';

  angular.module('lera').factory('ZoomController', ZoomControllerFactory);
  angular.module('lera').factory('DragController', DragControllerFactory);
  angular.module('lera').factory('ContextMenuController', ContextMenuControllerFactory);
  angular.module('lera').factory('KeyboardController', KeyboardControllerFactory);
  angular.module('lera').factory('CursorPositionController', CursorPositionControllerFactory);
  angular.module('lera').factory('WindowController', WindowControllerFactory);

  function getMouseX( e ){
    var e_target = $(e.target);
    var e_browser = e_target && e_target.closest(".browser");
    // We can not use directly e.offsetX because of a bug with firefox: https://github.com/jquery/jquery/issues/3080
    var browserX = e_browser.offset() && e_browser.offset().left;
    var x = e.pageX - browserX;
    return x;
  }

  function ZoomControllerFactory( $log, $timeout, StateManager ){

    var MAX_LEVEL = 8;
    var ZOOM_AMPLIFIER = 80;
    var ZOOM_WAIT_MS = 100;
    var ZOOM_LOCK_MS = 200;

    /**
     * @class
     * @desc Zooming tracks using mouse scroll
     * @memberOf lera.factories
     */
    var ZoomController = function( element ){

      this.zoomTimer = undefined;
      this.zoomLevel = 0;

      // is set to false when previous zoom is too recent
      this.lockTimer = undefined;
      this.locked = false;
      // TODO force a wait time between zooms. Do not execute zoom if the wait time from previous zoom is not finished.


      var self = this;
      $(element).on('mousewheel', '.browsable', function( e ) {
        e.preventDefault();

        if( !self.locked && StateManager.getActiveTrack() && StateManager.isStandBy() ){
         self.zoomLevel++;
         // Inspired by http://stackoverflow.com/questions/29654288/how-to-stop-scroll-triggering-events-multiple-times
         $timeout.cancel( this.zoomTimer );
         // zoom executed only after scrolling has stopped for x ms.
          self.zoomTimer = $timeout(function() {
            if( self.zoomLevel ){
              var level = self.zoomLevel <= MAX_LEVEL ? self.zoomLevel : MAX_LEVEL;
              var zoomDirection = ( e.deltaY > 0 ) ? 1 : -1;
              var zoomFactor = zoomDirection * level * ZOOM_AMPLIFIER;
              var mouseX = getMouseX( e );

              // Restraining max zoom factor to one third of the width
              var maxZoomFactor = _.ceil( StateManager.getActiveTrack().getReference().getWidth() / 3 );
              if( zoomFactor > maxZoomFactor){
                zoomFactor = maxZoomFactor;
              }

              StateManager.getActiveTrack().getReference().zoom( zoomFactor, mouseX );
              self.zoomLevel = 0;

              self.lockZoom();
            }
          }, ZOOM_WAIT_MS);
        }
      });

      /**
       * @desc Prevents the zoom to be trigerred just after the previous one. Locks
       * the zoom for ZOOM_LOCK_MS after the last zoom, before a new zoom can be made.
       * TODO Keep locked while receiving singals. Unlock only when no more signals for a given time
       */
      ZoomController.prototype.lockZoom = function(){
        var self = this;
        self.locked = true;
        $timeout.cancel( this.lockTimer );
        self.lockTimer = $timeout( function(){
          self.locked = false;
          console.log("Zoom UNLOCKED");
        }, ZOOM_LOCK_MS );
      };

    };
    return ZoomController;
  }


  function DragControllerFactory( $timeout, StateManager, ContextMenuController ){

    /**
     * @class
     * @desc Dragging tracks using mouse down/mouve/up
     * @memberOf lera.factories
     */
    var DragController = function( element ){

       this.dragStart = undefined;
       this.dragFactor = undefined;
       this.isDragging = false;

       var self = this;

       // TODO Only if in stand by ??

       $(element).on('mousedown', '.browser', function( e ) {

         self.isDragging = true;
         self.dragStart = e.pageX;

       });

       $(element).on('mouseup', '.browser', function( e ) {

         self.dragFactor = self.dragStart - e.pageX;
         self.isDragging = false;

         // if drag factor > 0 browse, else show menu
         $timeout(function(){
           if( self.dragFactor ){

             e.stopPropagation(); // preventing easelJS from getting the click event
             var activeRef = StateManager.getActiveReference();
             return activeRef && activeRef.browse( self.dragFactor );

           }

         });
       });
    };

    return DragController;
  }


  function ContextMenuControllerFactory( $timeout, StateManager, ContextMenuState ){

    /**
     * @class
     * @desc Showing / hiding context menu
     * @memberOf lera.factories
     */
    var ContextMenuController = function(){

      /**
       * @desc Hiding the context menu IF :
       * - The context menu is displayed
       * - We are not inside menu
       * - We are inside menu but on a button that is not a link
       */
      $(document).on('mouseup', function( e ){

        var notInsideMenu = $(e.target).closest("md-card").length === 0;
        var isButton = $(e.target).closest(".md-button").length !== 0;
        var notLinkButton = isButton && $(e.target).closest(".link").length === 0;

        if( ContextMenuState.isDisplayable() && ( notInsideMenu || notLinkButton ) ){
          $timeout(function(){ ContextMenuState.resetContextMenu(); });
        }
      });
    };

    return ContextMenuController;
  }

  function KeyboardControllerFactory( $timeout, $log, StateManager, Dialogs ){

    /**
     * @class
     * @desc Zooming/dragging track using keyboards keys
     * @memberOf lera.factories
     */
    var KeyboardController = function(){

      $(document).on('keydown', function( e ) {

        // Ignoring keys pressed inside an input
        if( $(e.target ).is("input") ){ return; }

        // Only listen to keys inside body
        if( ! $(e.target ).is("body") ){ return; }

        // Ignoring keys pressed with Cntrl
        if( e.ctrlKey ){ return; }

        $timeout(function(){

          var ar = StateManager.getActiveReference();
          // Controls related to a given reference
          if( ar ){
            switch(e.which){
              case 65 : ar.browse(-50); break; // A
              case 68 : ar.browse(50); break; // D
              case 87 : ar.zoom(200); break; // W
              case 83 : ar.zoom(-200); break; // S
              case 74 : Dialogs.showSearchDialog( ar ); break; // J
              case 84 : Dialogs.showAddTrackDialog( ar );break; // T
            }
          }

          switch(e.which){
            case 82 : Dialogs.showAddReferenceDialog(); break; // R
            case 85 : StateManager.undo();break; // U
            case 67 : StateManager.removeAllReferences();break; // C
            //case 76 : $timeout(function(){ $('#load-session').click(); },0);break; // L TODO Does not work
          }

        });
      });

    };
    return KeyboardController;
  }

  function TouchControllerFactory( $timeout, $log, StateManager, Reference ){

    /**
     * @class
     * @desc Touch controls on tablets
     * @memberOf lera.factories
     * @todo Implement
     */
    var TouchController = function(){

      // TODO Use http://hammerjs.github.io/getting-started/
      // Mobile Genomics :
      // Controls :
      // Swipe left/right to move track
      // Pinch in-out to zoom
      // Double-tap on entity to MORPH
      // Rotate to show orthologs

    };
    return TouchController;
  }

  function CursorPositionControllerFactory( $timeout, StateManager ){

    /**
     * @class
     * @desc Zooming/dragging track using keyboards keys
     * @memberOf lera.factories
     */
    var CursorPositionController = function( element ){

      /**
       * @desc Position of mouse cursor translated inside the reference
       */
      $(element).on('mousemove', '.browser', function( e ) {

        var x = getMouseX( e );

        $timeout(function(){
          var ref = StateManager.getActiveReference();
          return ref && ref.setCursorPosition( x );
        });
      });
    };
    return CursorPositionController;
  }


  function WindowControllerFactory( $timeout, $log, StateManager, Reference ){

    /**
     * @class
     * @desc Resizing the browser window
     * @memberOf lera.factories
     */
    var WindowController = function(){

      function getAvailableRefWidth(){
          var rw = $(".lera").width();
          var bw = _.round(rw - 300); // Place reserved for controls and enames legend
          return bw;
      }

      // Before adding any reference, or loading conf, we set the Reference.width variable to the given width...
      Reference.width = getAvailableRefWidth();

      // TODO Deal with case of a browser resize while a track update is running. Only if isStandby is true ?
      // TODO Make sure no 0 size is asked
      // TODO Wait 100ms before trigerring update ??
      $( window ).resize(function() {
        var newWidth = getAvailableRefWidth();
        if( newWidth != Reference.width ){
          $timeout(function(){
            Reference.width = newWidth;
            //$log.debug("Window was resized: updating reference width to "+Reference.width+" and rerendering all tracks.");
            StateManager.rerenderAllTracks();
          });
        }
      });

    };
    return WindowController;
  }


})();
