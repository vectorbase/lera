(function() {
    'use strict';

    angular.module('lera').directive('lera', Lera);

    function Lera( ZoomController, DragController, ContextMenuController, KeyboardController, WindowController, CursorPositionController ) {
        return {
            restrict: 'EA',
            templateUrl: 'lera/lera.template.html',
            scope: {
                postConfig: '@' // Config can be sent in the URL or by POST if too big for get
            },

            link: function(scope, element){

              var zoomController           = new ZoomController( element );
              var dragController           = new DragController( element );
              var contextMenuController    = new ContextMenuController();
              var keyboardController       = new KeyboardController();
              var cursorPositionController = new CursorPositionController( element );
              var windowController         = new WindowController();
            },

            controller: function($scope, $timeout, toastr, $location, $log, StateManager, Reference) {

                var self = this;

                $scope.StateManager = StateManager;

                // When lera initialises the first thing it does is load the most recent version of the registry
                $scope.StateManager.initialize(function() {

                    $scope.loadFromContext();
                });

                /**
                 * @desc Parse configuration from the URL to add and initialise references and tracks
                 */
                $scope.loadFromContext = function() {

                    // Load whole config from URL or from lera.data
                    var conf = $scope.getConf();

                    if (conf) {
                        try{

                            toastr.info('Loading references and tracks submitted by URL', 'URL Configuration');
                            $log.info("Using supplied configuration.");

                            $scope.StateManager.deserialize( conf );

                            $location.search('conf', undefined);
                            $("lera").data("conf", "");

                        } catch (e){
                            toastr.error('Invalid configuration in URL: ' + e, 'URL Configuration');
                            $log.error('Invalid configuration in URL: ' + e);
                        }
                    }
                };

                $scope.getConf = function(){
                  var serverConf = $("lera").data("conf");
                  // The server now provides GET and POST parameters as data-conf
                  // This is deprecated, we keep it only for back compatibility
                  var angularGET = $location.search().conf && decodeURIComponent($location.search().conf);
                  return angularGET || serverConf;
                };

                // TODO Do not show tutorial if has URL conf !!

                $scope.isAppEmpty = function(){
                  return ! $location.search().conf && $scope.StateManager.isEmpty();
                };

            }
        };
    }


})();
