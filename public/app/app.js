(function() {
  'use strict';

  // The gulp task gulp-angular-templatecache will concatenate and add to this module all html templates from templates folder
  angular.module('templates', []);

  var lera = angular.module('lera', ['templates', 'toastr', 'ngMaterial', 'ngSanitize', 'ngFileUpload', 'ui.bootstrap']);

  // Configuration
  lera.value('config', {

    track : {
      view: {}
    }

  });

  lera.config(function( $httpProvider, $logProvider, toastrConfig, $provide, $locationProvider, $animateProvider) {

    // Disable debug console messagef for production
    $logProvider.debugEnabled(true);

    //$httpProvider.useApplyAsync(true);

    $locationProvider.hashPrefix('');

    //$provide.constant('$MD_THEME_CSS', '/**/');

    angular.extend(toastrConfig, { positionClass: 'toast-bottom-right' });

    /**
     * Removing animations by default to improve performance
     */
    $animateProvider.classNameFilter(/ng-animate-enabled/);

    $provide.decorator("$exceptionHandler", function( $delegate ) {
      return function(exception, cause) {

        // Original behaviour, logging to console
        $delegate(exception, cause);

        // TODO Send eror to server LOG
      };
    });



  });

})();


/**
* @namespace lera
*/

/**
 * @namespace factories
 * @memberOf lera
 */

/**
* @namespace directives
* @memberOf lera
*/

/**
 * @namespace controllers
 * @memberOf lera
 */

 /**
  * @namespace services
  * @memberOf lera
  */
