(function() {
  'use strict';

  angular.module('lera').factory('ContextMenuState', ContextMenuStateFactory);


  function ContextMenuStateFactory(StateManager){

    /**
     * @class
     * @memberOf lera.factories
     */
    var ContextMenuState = function(){

    };


    ContextMenuState.prototype.resetContextMenu = function(){
      this.menu = {};
    };

    /**
     * @desc Displays the context menu if entityId or componentId is not undefined
     * @param {object} e Click event containing the x,y coordinated of the mouse pointer
     * @param {string} entityId Id of the clicked entity
     * @param {string} componentId Id of the clicked component
    * FIXME : synchronisation bug. Active track can sometimes be different than the track
    * from which entity and component are from. Use refId and trackId instead ??
    * data-track-id, data-ref-id
     */
    ContextMenuState.prototype.showInfoMenu = function(e, entityId, componentId){

      // TODO remove as deprecated
      // var track = StateManager.getActiveTrack();

      var track = this.getTrackFromEvent( e );

      if( track && ( entityId || componentId ) ){
          this._setMenu(e, track, entityId, componentId);
      }
    };

    /**
     * @desc Sets a menu content to be displayed
     * @param {object} e Click event containing the x,y coordinated of the mouse pointer
     * @param {object} track on which the click occured
     * @param {string} entityId Id of the clicked entity
     * @param {string} componentId Id of the clicked component
     */
    ContextMenuState.prototype._setMenu = function(e, track, entityId, componentId){

      this.menu = {
        top         : e.pageY,
        left        : e.pageX,

        entityId    : entityId,
        entityProperties: this._getProcessedProperties( track.getEntityProperties( entityId ), track.contextmenu ),

        morphTargets: this._getMorphTargets( track.getAllMorphTargets(), track.getEntityProperties( entityId ) ),

        componentId : componentId,
        componentProperties: this._getProcessedProperties( track.getComponentProperties( entityId, componentId ), track.contextmenu ),

        track       : track
      };

    };

    ContextMenuState.prototype.isDisplayable = function(){
      return this.menu && this.menu.top && this.menu.left;
    };

    ContextMenuState.prototype.getEntity = function(){
      return this.menu && this.menu.entityId && this.menu.track.getEntity( this.menu.entityId );
    };

    ContextMenuState.prototype.getComponent = function(){
      return this.menu && this.menu.componentId && this.menu.track.getComponent( this.menu.entityId , this.menu.componentId );
    };

    ContextMenuState.prototype.getTrack = function(){
      return this.menu && this.menu.track;
    };

    ContextMenuState.prototype.getEntityId = function(){
      return this.menu && this.menu.entityId;
    };

    ContextMenuState.prototype.isEntitySelected = function(){
      var track = this.getTrack();
      var entityId = this.getEntityId();
      return track && entityId && track.isSelected( entityId );
    };

    ContextMenuState.prototype.triggerSelectedEntity = function(){
      var track = this.getTrack();
      var entityId = this.getEntityId();
      return track && entityId && track.triggerSelectedEntity( entityId );
    };

    ContextMenuState.prototype.getEntityProperties = function(){
      return this.menu && this.menu.entityProperties;
    };

    ContextMenuState.prototype.getMorphTargets = function(){
      return this.menu && this.menu.morphTargets;
    };

    ContextMenuState.prototype.getComponentId = function(){
      return this.menu && this.menu.componentId;
    };

    ContextMenuState.prototype.getComponentProperties = function(){
      return this.menu && this.menu.componentProperties;
    };

    /**
     * @desc Returns the title of a context menu using either a component title,
     * or by default the entity name
     * @return {string} Title of the context menu
     */
    ContextMenuState.prototype.getTitle = function(){
      var c = this.getComponent();
      var e = this.getEntity();
      return (c && c.title) || (e && e.name);
    };

    ContextMenuState.prototype.getLeftPos = function(){
      return this.menu && this.menu.left;
    };

    ContextMenuState.prototype.getTopPos = function(){
      return this.menu && this.menu.top;
    };

    ContextMenuState.prototype.correctPosition = function(){

      var menuWidth = $(".context-menu").width();
      var menuHeight = $(".context-menu").height();
      var windowWidth = $(window).width();
      var windowHeight = $(window).scrollTop() + $(window).height();
      this._correctPosition( menuWidth, menuHeight, windowWidth, windowHeight );
    };

    /**
     * @desc Returns the track object from which an event originated
     * @param {object} e
     * @return {object} track
     */
    ContextMenuState.prototype.getTrackFromEvent = function( e ){
      var target = e && $(e.target);
      var refCard = target && target.closest("reference-card");
      var refId =  refCard && refCard.data("ref-id");

      var trackCard = target && target.closest("track-card");
      var trackId = trackCard && trackCard.data("track-id");

      var track = refId && trackId && StateManager.getTrack( refId, trackId );
      return track;
    };

    /**
     * @desc  Correcting menu position to avoid conflict with right and bottom borders of the window
     * @param {integer} menuWidth
     * @param {integer} menuHeight
     * @param {integer} windowWidth
     * @param {integer} windowHeight
     */
    ContextMenuState.prototype._correctPosition = function( menuWidth, menuHeight, windowWidth, windowHeight ){

      var leftEdge = menuWidth + this.getLeftPos() - windowWidth;
      if( this.menu && leftEdge > 0 ){ this.menu.left-= (leftEdge + 20); }

      var topEdge = menuHeight + this.getTopPos() - windowHeight;
      if( this.menu && topEdge > 0 ){ this.menu.top-= (topEdge + 20); }
    };


    /**
     * @desc If a contextmenu field is declared in the configuration for this track,
     * we order the fields given the given order, and use supplied 'names' for fields.
     * Fields in data not declared in contextmenu are ignored.
     * If no contextmenu field is present in track configuration, all fields from data are displayed
     * in alphabetical order.
     * @param {object} data Data object from an entity or component
     * @param {object} contextmenu (optional)
     * @return {list}
     */
    ContextMenuState.prototype._getProcessedProperties = function( data, contextmenu ){

      var self = this;
      if( data && contextmenu && contextmenu.fields){

        return _.transform( contextmenu.fields, function(acc, field){
          var v = field.key && data[field.key];
          if( ! _.isUndefined(v) ){
            acc.push( {
              field : field.name || field.key,
              value : self._processValue(v)
            } );
          }
        }, [] );

      } else {

        var unsorted =  _.map( data, function(v,k){
          return {
            field : _.replace( k, /_/g, ' '),
            value : self._processValue(v)
          };
        });
        return _.sortBy( unsorted, function(o){return o.field;});
      }
    };

    /**
     * @desc Transforms a string, array or object into a string suitable to be displayed in the menu
     * @param {variable} v
     * @return {string} processedValue
     */
    ContextMenuState.prototype._processValue = function( v ){

      if( _.isString(v) ){
        return _.replace(v, /_/g, ' ');
      }

      if( _.isPlainObject(v) ){
        return _.join(_.map(v, function(vv,vk){return vk+" : "+vv;}), ', ');
      }

      if( _.isArray(v) ){
        return _.join(v, ', ');
      }

      return v;
    };

    /**
     * @desc Returns the morphing targets compatible with this track and this entity
     * @param {list} targets list of morphing targets. Empty list if no morphTargets
     * @param {object} data Data describing the entity
     */
    ContextMenuState.prototype._getMorphTargets = function( targets, data ){

      return _.filter( targets, function( target ){
          var conditionsMet = _.isUndefined( target.conditions ) ||
           _.every( target.conditions, function( v, k ){
            return data && data[k] === v;
          });

          return conditionsMet;
      });
    };

    /**
     * @desc Returns if it exists, a comment associated with the entity name, entity/component, or data key/val regex
     * @param {object} comments
     * @return {string} comment
     * @todo Implement
     */
    ContextMenuState.prototype._getComment = function( comments ){

    };

    return new ContextMenuState();
  }

})();
