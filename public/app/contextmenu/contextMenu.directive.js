(function() {
  'use strict';

  angular.module('lera').directive('info', Info);

/**
 * @desc Shows information about entities and components clicked by the user
 * If the component has 'data' key, shows data from component
 * If entity has 'data' key, shows data from entity
 * Shows buttons to interact with component / entity
 */
  function Info(){
      return {
          restrict: 'EA',
          templateUrl: 'contextmenu/contextMenu.template.html',
          scope: {},
          controller: function($scope, StateManager, ContextMenuState, Dialogs, Track){
            $scope.StateManager = StateManager;
            $scope.ContextMenuState = ContextMenuState;

            /**
             * Correcting menu position to avoid conflict with right and bottom borders of the window
             */
            $scope.$watch( function(){ return $(".context-menu").get(0); }, function(contextMenuEl){

              if( contextMenuEl ){ // if added to the DOM
                $scope.ContextMenuState.correctPosition();
              }
            });

            $scope.createUrl = function(url){
              return url({
                ref       : ContextMenuState.getTrack().getIntervalParams(),
                entity_id : encodeURIComponent( ContextMenuState.getEntityId() ),
                component : ContextMenuState.getComponent(),
                entity : ContextMenuState.getEntity()
              });
            };

            /**
             * @desc When the user double-clicks on an entity, the track is replaced (same id, different object) by a track
             * of type morphsTo defined in the current track registry declaration
             * @param {string} morphTarget Type of the track into which this entity should morph
             */
            $scope.morphToEntity = function( morphTarget ){
              $scope.StateManager.morphToEntity( ContextMenuState.getTrack(), ContextMenuState.getEntityId(), morphTarget );
            };

            /**
             * @desc Shows a dialog allowing to select the fork target.
             * Forking creates another reference+track based on a selected entity id and
             * transformation rules in forkTargetConf
             * @param {object} forkTargetConf
             */
            $scope.forkToEntity = function( forkTargetConf ){

              var track = $scope.ContextMenuState.getTrack();
              var entityId = $scope.ContextMenuState.getEntityId();

              Dialogs.showForkToEntityDialog( track, entityId, forkTargetConf );
            };


            $scope.canCenterToEntity = function(){
              var entityId = ContextMenuState.getEntityId();
              var track = ContextMenuState.getTrack();
              return track && entityId && track.canCenterToEntity( entityId );
            };

            $scope.canSelectEntity = function(){
              var entity = ContextMenuState.getEntity();
              return entity && Track.isRealEntity( entity );
            };

            /**
             * @desc When the user clicks on an entity, the reference is updated to this entities bounds
             */
            $scope.centerToEntity = function(){

              var entityId = ContextMenuState.getEntityId();
              var track = ContextMenuState.getTrack();

              if(track && entityId){
                track.centerToEntity( entityId );
              }
            };

            $scope.centerToComponent = function(){
              var entityId = ContextMenuState.getEntityId();
              var componentId = ContextMenuState.getComponentId();
              var track = ContextMenuState.getTrack();
              if(track && entityId && componentId){
                track.centerToComponent( entityId, componentId );
              }
            };

          }
    };
  }
})();
