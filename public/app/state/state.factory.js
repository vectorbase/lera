(function() {
  'use strict';

  angular.module('lera').factory('State', StateFactory);


  /**
   * @desc Represents the current project, containing references which contain tracks
   */
  function StateFactory( $log, toastr, Track, Reference ){

    /**
     * @class
     * @memberOf lera.factories
     */
    var State = function(){

      /**
       * @desc hash of references, associated an object to an id
       */
      this.references = {};

    };

    return State;
  }

})();
