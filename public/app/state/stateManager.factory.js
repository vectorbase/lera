(function() {
  'use strict';

  angular.module('lera').factory('StateManager', StateManagerFactory);


  function StateManagerFactory( $log, toastr, State, Registry, Reference, Track, DataEngine ){

    /**
     * @class
     * @desc Contains information about the current state of the app
     * The StateManager is a Singleton
     * @memberOf lera.factories
     * @todo Merge StateManager and State ? Or save state as a full object ( no need to update tracks again )
     */
    var StateManager = function(){

      this.registry = new Registry('registry.json');
      this.state = new State( this.registry );

      /**
       * @desc Keeps the number of references that are busy resolving
       */
      this.resolvingCount = 0;

      /**
       *  Stack of history used to undo actions
       */
      this.history = [];

    };

    StateManager.refIdCounter = 0;

    /**
     * @desc Executes the callback once the registry has been loaded
     * @param {function} callback Function to call
     */
    StateManager.prototype.initialize = function( callback ){

      this.registry.load(function(){
        if( callback ){ callback(); }
      });
    };

    /**
     * @desc Retrieves the configuration for the given track type, retrieves the declared reference object,
     * and creates a track object and adds it to the state.
     * Shows error message if no configuration was found for the trackType,
     * no reference was found for refId, or the track does not supports the reference.
     * @param {object} reference Reference to which add the track
     * @param {string} trackType unique identifier of the track in the registr, ex: ensTranscript
     * @param {object} filters (optional) ex: { entities:['sample1'] }
     * @param {function} callback (optional) The new Track is passed as parameter
     * @param {object} config (optional) configuration object that will be merged with the trackConfig object
     */
    StateManager.prototype.addTrack = function( reference, trackType, filters, callback, config ){

      var trackConf = this.registry.getTrackConf( trackType );
      if( !trackConf ){
        toastr.error("Can not retrieve track configuration for track type ''"+trackType+"''");
        return;
      }

      if(!reference){
        toastr.error("You must provide a valid reference");
        return;
      }

      if( !this.registry.trackSupportsReference(trackConf, reference.refType) ){
        toastr.error("Tracks of type "+trackType+" does not support references of type "+reference.refType+". (Supported references are "+JSON.stringify(trackConf.supportedReferences)+" )");
        return;
      }

      if( config ){
        trackConf = _.cloneDeep( trackConf ); // we do not want to modify the original config object used to create all tracks
        _.merge( trackConf, config );
      }

      return reference.addTrack( trackType, trackConf, filters, callback );
    };

    /**
     * @returns {array} tracks Ruturns an array of all tracks of all references of the current state
     */
    StateManager.prototype.getAllTracks = function(){
      return _.flatMap( this.state.references, function( ref ){
          return _.values( ref.tracks );
      });
    };

    /**
     * @desc Returns all references
     * @return {hash} references
     */
    StateManager.prototype.getReferences = function(){
      return this.state && this.state.references;
    };

    /**
     * @desc Returns the reference associated with the id
     * @param {string} refId
     * @return {object} reference
     */
    StateManager.prototype.getReference = function( refId ){
      return this.state && this.state.references && this.state.references[refId];
    };

    /**
     * @desc Returns the track associated with the reference id and track id
     * @param {string} refId
     * @param {string} trackId
     * @return {object} track
     */
    StateManager.prototype.getTrack = function( refId, trackId ){
      var ref = this.getReference( refId );
      return ref && ref.tracks && ref.tracks[trackId];
    };

    /**
     * @desc Returns an ordered list of references
     * @return {list} references
     */
    StateManager.prototype.getOrderedReferences = function(){
      return this.state && _.sortBy( this.state.references, 'order' );
    };

    /**
     * @desc Triggers the mechanism of redrawing each track
     */
    StateManager.prototype.rerenderAllTracks = function(){
      var self = this;
      _.each( self.getReferences(), function(ref){
        _.each( ref.getTracks(), function(track){
          track.redraw();
        });
      });
    };

    /**
     * @desc Retrieves the configuration for the refType, creates a reference object and adds it to the current state.
     * @param {string} refType
     * @param {object} intervalParams
     * @param {function} success This function will be executed once the reference is created and resolved.
     * The reference will be passed as first parameter to the callback.
     * All tracks dependant on this reference should be added during this callback.
     * @param {function} error If the server responded with an error, or the server responded with empty data
     * and a resolve was required
     * @return {object} The created reference (not resolved yet)
     */
    StateManager.prototype.addReference = function( refType, intervalParams, success, error ){

      var self = this;
      var refConf = this.registry.getReferenceConf( refType );

      if( !refConf ){
        throw "Can not retrieve reference configuration for reference type ''"+refType+"''";
      }

      var refId = StateManager.getNextRefId( refType );
      var reference = new Reference( refId, refType, intervalParams, refConf, this );
      this.state.references[refId] = reference; // Added before resolving

      reference.resolve(
        function(){
          $log.debug("Reference "+refId+" added and resolved. ");
          return success && success( reference );
        },
        function( ref, msg ){
         delete self.state.references[refId]; // Removed if resolve errored
         toastr.error(msg, 'Could not add reference',{ closeButton: true, timeOut: 20500 });
         $log.debug("Can not add reference for intervalParams "+JSON.stringify(intervalParams)+" : "+msg);
         return error && error( ref, msg );
        }
      );

      return reference;
    };

    /**
     * @desc Creates a new reference and loads all default tracks declared for this reference.
     * @param {string} refType
     * @param {object} intervalParams
     * @param {function} success This function will be executed once the reference is created and resolved.
     * The reference will be passed as first parameter to the callback.
     * All tracks dependant on this reference should be added during this callback.
     * @param {function} error If the server responded with an error, or the server responded with empty data
     * and a resolve was required
     * @param {function} trackCallback Function called when each of the default tracks is resolved
     * @return {object} The newly created reference
     */
    StateManager.prototype.addReferenceWithDefaults = function( refType, intervalParams, success, error, trackCallback ){

      var self = this;
      var ref = self.addReference( refType, intervalParams, function( ref ){

        // Adding default tracks declared for this reference
        if( ref.defaultTracks ){
          _.each( ref.defaultTracks, function( dt ){
            self.addTrack(ref, dt, {}, trackCallback);
          });
        }

        return success && success( ref );

      }, function( ref, msg ){
        return error && error( ref, msg );
      });

      return ref;
    };

    /**
     * @desc Deletes the given reference and all associated tracks
     * @param {string} refId
     */
    StateManager.prototype.removeReference = function( refId ){
      var ref = this.state.references[refId];
      if(!ref){
        toastr.error("Can not remove reference for id "+refId+" because it does not exist", "Reference not found");
        return;
      }

      this.stackState();
      ref.removeAllTracks();
      delete this.state.references[refId];
    };

    /**
     * @desc Removes all references
     */
    StateManager.prototype.removeAllReferences = function(){
      var self = this;
      if( self.notEmpty() ){

        self.stackState();
        _.each(self.getReferences(), function(ref, refId){
          ref.removeAllTracks();
          delete self.state.references[refId];
        });
      }
    };

    /**
     * @desc Returns true if the project has at least one reference
     * @return {boolean}
     */
    StateManager.prototype.notEmpty = function(){
      return !! this.getReferencesCount();
    };

    /**
     * @desc Returns true if the project has no references
     * @return {boolean}
     */
    StateManager.prototype.isEmpty = function(){
      return ! this.getReferencesCount();
    };

    /**
     * @return {object} reference used by the currently active track
     */
    StateManager.prototype.getActiveReference = function(){
      return this.state && this.state.activeTrack && this.state.activeTrack.reference;
    };

    /**
     * @return {object} track The currently active (under mouse cursor) track
     */
    StateManager.prototype.getActiveTrack = function(){
      return this.state && this.state.activeTrack;
    };

    /**
     * @desc Creates a link to a track selected as active
     * @param {object} track
     */
    StateManager.prototype.setActiveTrack = function( track ){
      if( this.state ){ this.state.activeTrack = track; }
    };

    /**
     * @desc Resets the active track if no parameter supplied, or resets the active track
     * if it corresponds to the supplied track
     * @param {object} track (optional) Resets the active track if it corresponds to the supplied track
     * @todo previous track in reference becomes actif, or if no more tracks or reference, last rrack in previous reference ?
     */
    StateManager.prototype.resetActiveTrack = function( track ){
      if( this.state ){
        if( _.isUndefined( track ) ){
          this.state.activeTrack = undefined;
        } else if ( this.state.activeTrack === track){
          this.state.activeTrack = undefined;
        }
      }
    };

    /**
     * @desc Adds to the history the current serialized state.
     * It is advised to use this function to save the current state, BEFORE making any changes to it...
     * @todo Resets the undo array, as a new temporal line is created by a change at this point in time.
     */
    StateManager.prototype.stackState = function(){
      var serialized = this.serialize();
      this.history.push( serialized );
    };

    /**
     * @desc Removes the last saved serialized state from the history and returns it
     * @return {string} serializedState
     */
    StateManager.prototype.unstackState = function(){
      return this.history.pop();
    };

    StateManager.prototype.getLastState = function(){
      return _.last( this.history );
    };


    StateManager.prototype.canUndo = function(){
      return !!this.history.length;
    };

    /**
    * @desc Undoes the last MAJOR browsing action
    * @todo put the current state in redo array
     */
    StateManager.prototype.undo = function(){

      if( this.history.length ){

        var serialised = this.history.pop();
        //toastr.warning("Going back to last state...", "History");
        this.state.references = {};
        this.deserialize( serialised );

      } else {
        toastr.warning("No actions to undo", "History");
      }

    };

    /**
     * @todo Redoes the last action before undo
     */
    StateManager.prototype.redo = function(){

    };

    /**
     * @desc Allows the user to download and save a json representation of the current state of the app
     */
    StateManager.prototype.save = function(){

      var serialized = this.serialize();
      try{

        var blob = new Blob([ serialized ], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "leraSave.json");

      } catch(err){

        toastr.error("Could not export current State : "+err);
      }
    };

    /**
     * @desc Serialises the current active state as a string
     * @return {string} String representation of the current state
     */
    StateManager.prototype.serialize = function(){

      var serializedState = [];

      var self = this;

      _.each(self.getOrderedReferences(), function(sRef){

          var tracks = [];
          _.each(sRef.getOrderedTracks(), function(sTrack){
            tracks.push({
              type: sTrack.trackType,
              filters: sTrack.filter,
              label: sTrack.label
            });
          });

          serializedState.push({
            type: sRef.refType,
            data: sRef.getIntervalParams(),
            color: sRef.color,
            tracks: tracks
          });

      });

      return JSON.stringify(serializedState);
    };

    /**
     * @desc Loads a saved session from a file
     */
    StateManager.prototype.load = function( file ){

      var self = this;
      if( file ){

        var reader = new FileReader();
        reader.onload = function(){
          var conf = reader.result;
          try {
              self.state = new State(); // put previous state in history ? in serialized form ??
              $log.info("Loading saved state...");
              self.deserialize( conf );
          } catch (e) {
              toastr.error('Invalid save file: ' + e, 'Load Session');
              $log.error('Invalid save file: ' + e);
          }

        };
        reader.readAsText( file );
      }

    };

    /**
     * @desc Parses a json string to add references and tracks to the current active state
     * @param {object} A JSON string describing references and tracks, that can be used to initialise the app
     * @todo need a callback called when all the work is done
     */
    StateManager.prototype.deserialize = function( serializedStateStr ){

      var self = this;
      var serializedState = (typeof serializedStateStr === 'string') ? JSON.parse(serializedStateStr) : serializedStateStr;

      _.each(serializedState, function(sRef){
        self.addReference( sRef.type, sRef.data, function( ref ){

          if(sRef.tracks){
            _.each(sRef.tracks, function(sTrack){

              var declaredFilters = sTrack.filters || {};
              var options = { label: sTrack.label };
              self.addTrack( ref, sTrack.type, declaredFilters, undefined, options );
            });
          }
        });
      });
    };

    /**
     * @param {string} trackType Type of a track
     * @return {list} Ids of all existing references in this state whith a type compatible with the trackType
     */
    StateManager.prototype.getAvailableReferencesForTrackType = function( trackType ){

      if( !trackType ){ return []; }

      var conf = this.registry.getTrackConf(trackType);
      if( !conf ){ return []; }

      return _.filter(this.state.references, function(r){
        return _.some( conf.supportedReferences, function(crt){return crt && r.refType && crt == r.refType;});
      });

    };


    /**
     * @desc Morphs an entity into a track.
     * When the user clicks on 'Morph', a new track (and if necessary reference)
     * of type morphsTo defined in the current track registry declaration, is/are added.
     * Note that the morphing may necessitate to change the reference too.
     * By default the new track/reference is added, not replacing the old one
     * @example Morph a gene into a transcript. Morph a transcript into a protein.
     * @param {object} track Current track that will be morphed
     * @param {string} entityId Id of the entity that will define new track/reference
     * @param {string} morphTarget Type of the track into which this entity should morph
     * @todo rename to morph
     */
    StateManager.prototype.morphToEntity = function( track, entityId, morphTarget ){

      var self = this;

      // 1. Get the type of the new track to create
      var trackType = morphTarget;
      if( !trackType ){ $log.warn("This track has no morphing target track declared") ; return; }
      if( !self.registry.isTrackTypeAvailable( trackType ) ){ $log.warn("The morphing target declared for this track ("+trackType+") is not declared in the registry") ; return; }

      // This entity id verification is facultative
      var entity = entityId && track.getEntity( entityId );
      if( !entity ){ $log.warn("Invalid entity id "+entityId) ; return; }

      var previousRef = track.getReference();
      var previousRefType = previousRef && previousRef.refType;
      var refId = previousRef.refId;

      var supportedReferences = self.registry.getSupportedReferences( trackType );
      var newRefType = supportedReferences && !_.isEmpty(supportedReferences) && supportedReferences[0]; // by default we take the first supported reference
      if( !newRefType ){ $log.warn("No supported references declared in the registry for ("+trackType+")") ; return; }

      $log.debug("We will morph to track type "+trackType+" and load it with "+newRefType+" reference declared on "+entityId);
      this.stackState();

      var data = _.cloneDeep( previousRef.getIntervalParams() );
      data.query = entityId;

      if( newRefType !== previousRefType ){
        $log.debug("We must replace the previous reference, morph the track, and bring compatible tracks from previous reference");
        var newRef = self.addReference( newRefType, data,
          function( ref ){
            ref.scrollToRef();
            //toastr.success("Old reference replaced");
            // Replacing morphed track
            self.addTrack( ref, trackType, undefined, function(){ toastr.success("Track morphed"); });

            // Transfering other tracks from reference
            _.each( previousRef.getTracks(), function( trackFromOldRef ){
                if( track !== trackFromOldRef ){ // Ignoring track that initiated the morphing
                 // track type by which this track should be replaced in a new reference
                var morphReplaceTrackType = trackFromOldRef.getMorphReplacement( newRefType );
                if( morphReplaceTrackType ){
                  $log.debug("The track "+trackFromOldRef.trackType+" fill be transfered");

                  // Transfering transferable track. ex: genotype -> peptype
                  self.addTrack( ref,
                                 morphReplaceTrackType,
                                 trackFromOldRef.filter, function(){ toastr.success("Track transfered"); },
                                 {label: trackFromOldRef.label});
                }
              }
            });
          }) ;

          // We make sure the new reference is displayed just after the reference that created it
          self.insertAfter( self.getReferences(), previousRef, newRef );

      } else {
        $log.debug("Juste adding the track");
        self.addTrack( previousRef, trackType, undefined, function(){ toastr.success("Morphing done"); });
      }

    };

    /**
     * @desc Unmorphs a track into an entity.
     * When the user clicks on 'Unmorph' button fo a track,
     * a new track (and if necessary reference)
     * of type unmorphsTo defined in the current track registry declaration, is/are added.
     * Note that the unmorphing may necessitate to change the reference too.
     * By default the new track/reference is added, not replacing the old one.
     * @example Unmorph a protein into a transcript. Unmorph a transcript into a gene.
     * @param {object} track Current track that will be unmorphed
     * @param {string} morphTarget Type of the track into which this track should unmorph
     * @param {}
     * @return {}
     * @todo Implement
     */
    StateManager.prototype.unmorph = function( track, morphTarget ){

      // TODO First we need to retrieve the entity id to which we will unmorph.
      // For a protein, we can declare in the registry : entityId : transcript_id and
      // then retrieve it from the trackParams or refParams
      // Then we just call morph

    };



    /**
     * @desc Forks a track into a new track, using an entity id.
     * The idea of forking is basically to recreate the same reference in a different species
     * The species aspect is to genome/protein specific, and should probably be generalised, using fields
     * declared for the reference in the registry

     Example registry configuration :

     "forksTo":[
       {
         "name": "orthologs",
         "legend": "'epiro'",
         "urlTemplate": "api/autocomplete/ortholog/gene?entityId={{params.entityId}}&species={{params.species}}&query={{text}}"
       }
     ],

     One of the main differences between forking and moprhing, is that forking will ask user for additional
     information.

     * @param {object} track : Track containing the entity that will be forked
     * @param {object} forkValue : object containing all the necessary information (key/value couples)
     * to create the new reference, retrieved from the user autosuggest
     * @param {object} forkTargetConf : forking configuration
     */
    StateManager.prototype.fork = function( track, forkValue, forkTargetConf ){

      var self = this;
      if( !forkTargetConf ){ $log.warn("The forking configuration object is mandatory for forking") ; return; }
      if( !forkValue ){ $log.warn("The forking value object is mandatory for forking") ; return; }
      if( !track ){ $log.warn("The track is mandatory for forking") ; return; }

      toastr.info("Adding and resolving new reference...", "New "+forkTargetConf.name+" Reference");

      self.stackState();

      // TODO this must be generalised using fields declared for the reference, inseatd of explicitely using terms species and query
      // map identifier to query in forkTargetConf
      var query = forkValue && forkValue.identifier;
      var species = forkValue && forkValue.species;
      var intervalParams = { species: species, query: query };
      var previousRef = track.getReference();

      var newRef = self.addReferenceWithDefaults(
        previousRef.refType, // we recreate a new reference of the same type
        intervalParams,
        function( ref ){
          ref.scrollToRef();
          toastr.success("Reference "+ref.name+" added.", "New "+forkTargetConf.name+" Reference");
        }
      );

      // We make sure the new reference is displayed just after the reference that created it
      self.insertAfter( self.getReferences(), previousRef, newRef );
    };

    /**
     * @return {integer} The number of tracks in this state
     * (sums the number of tracks for each reference of this state)
     */
    StateManager.prototype.getTracksCount = function(){
      return _.reduce( this.state.references, function( sum, ref ){
        return sum + _.size( ref.tracks );
      }, 0);
    };


    /**
     * @return {integer} The number of references in this state
     */
    StateManager.prototype.getReferencesCount = function(){
      return this.state && _.size(this.state.references);
    };


    /**
     * @desc Returns true only if no reference is currently being resolved
     * @return {boolean}
     */
    StateManager.prototype.allReferencesResolved = function(){
      return Reference.resolvingCount === 0;
    };

    /**
     * @desc Returns true only if no track is currently being updated
     * @return {boolean}
     */
    StateManager.prototype.allTracksUpdated = function(){
      return DataEngine.updatingCount === 0;
    };

    /**
     * @desc Returns true only if all references and tracks are resolved and updated
     * @return {boolean}
     */
    StateManager.prototype.isStandBy = function(){
      return this.allReferencesResolved() && this.allTracksUpdated();
    };

    /**
     * @desc Returns true only if no export query is currently runiing on none of the tracks
     * @return {boolean}
     */
    StateManager.prototype.noExportsRunning = function(){
      return Track.runningExports === 0;
    };

    StateManager.prototype.getRegistry = function(){
      return this.registry;
    };

    /**
    * @desc Generates a reference id
    * @param {string} refType
    * @return {number} id
    */
    StateManager.getNextRefId = function( refType ){
      return ++StateManager.refIdCounter;
    };

    /**
     * @desc Changes the order of all elements in list such as otherEl is positioned just after el
     * @param {list} list Unordered list
     * @param {object} el Element after which we want to move otherEl
     * @param {object} otherEl Element that we want to place after el
     * @return {boolean} true if the operation succeeded
     */
    StateManager.prototype.insertAfter = function( list, el, otherEl ){
      if( !list || !el || !otherEl || !el.order || !otherEl.order ){ return false; }

      otherEl.order = el.order + 1;
      _.each( list, function(oel){
        // if the list does not already have order duplicates, this will not create duplicates
        if( oel !== otherEl && oel.order >= otherEl.order ){
          if(oel && oel.order){ oel.order++; }
        }
      });
      return true;
    };

    return new StateManager();
  }

})();
