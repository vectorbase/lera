(function() {
  'use strict';

    angular.module('lera').directive('highcharts', highcharts);


        /**
         * @description
         * This directive allows to integrate Highcharts charts in angular way.
         * This custom directive has been created because the existing ng-highcharts directive is still not mature
         * It is inspired by :
         * 1. http://jsfiddle.net/csTzc/
         * 2. https://github.com/pablojim/highcharts-ng/blob/master/src/highcharts-ng.js
         *
         * @param {object} data Model representing the overview of a category. ex: overview of biotypes.
         * @param {string} category Name of the category shown oin the chart. ex: biotype
         */
        function highcharts($log, toastr, Filter) {
            return {
                restrict: 'EA',
                template: '<div class="chart-error">Can not display chart</div>',
                scope: {
                    data: '=',
                    category: '=',
                    track: '='
                },
                link: function(scope, element, attrs) {

                  /**
                   * @desc Keeps the category filters updated with the legend items
                   */
                  scope.showHide = function () {

                    var countOfVisibles = _.sumBy(this.series.points, function(o){
                      return o.visible ? 1 : 0;
                    });

                    if( countOfVisibles === 1 && this.visible ){
                      toastr.warning("Refusing to hide all categories", "Filters");
                      return false;
                    } else {

                      var clickedPoint = this;
                      var points = this.series.points;
                      var filters = Filter.legendAsFilters( clickedPoint, points );

                      scope.$applyAsync(function(){
                        scope.track.setCategoryFilters( scope.category, filters );
                      });
                    }
                  };

                  /**
                   * Highcharts options object, for details see (http://api.highcharts.com/highcharts)
                   */
                  var total = 0;
                  var options = {
                      chart: {
                          renderTo: element[0],
                          height: 300,
                          width: 300,
                          events: {
                            load: function () {

                            }
                          }
                      },

                      title: {
                          text: scope.category
                      },
                      yAxis: {
                          title: {
                              text: scope.yLegend
                          }
                      },
                      xAxis: {
                          type: 'category'
                      },
                      legend: {
                          enabled: true,
                          useHTML: true,
                          labelFormatter: function() {
                            total += this.y;
                            return '<div style="width:200px"><span style="float:left">' + this.name + '</span><span style="float:right">' + this.y + '</span></div>';
                          },
                      },
                      plotOptions: {
                          series: {
                            animation: false,
                            enableMouseTracking: false
                          },
                          pie: {
                              showInLegend: true,
                              dataLabels: {
                                  enabled: false
                              },
                              point: {
                                events: {
                                    legendItemClick: scope.showHide
                                }
                            },
                          }
                      },
                      tooltip: {

                      },
                      series: [{
                          type: 'pie',
                          colorByPoint: true,
                          data: scope.data
                      }, ]
                  };

                  var chart = new Highcharts.Chart(options);

                  /**
                   * @desc Charts are updated when the data changes
                   * We do not need to set the legend visibility of the points as this was
                   * already done by Overview when calculating the 'data'
                   */
                  scope.$watch("data", function ( newValue, oldValue ) {
                    if(newValue !== oldValue){
                      chart.series[0].setData( newValue, true );
                    }
                  }, true);

                  scope.$on('$destroy', function() {
                      if ( chart ) {
                        chart.destroy();
                      }
                  });

                }
            };
        }


})();
