(function() {
  'use strict';

  angular.module('lera').directive('overviewCard', OverviewCard);

  /**
   * @desc Shows statistics from the meta objext inside the track object.
   * Statistics in meta are a hash of key/values
   * They are used as visualisation as a graph, but when clicking on the grah this
   * send selected keys as filters to filter data for the track.
   * For example the track sample has statistics about most severe variant conseauences displayed as a chart.
   * When clicking on 'missence', this updates the track by sending filters:{conseauence:['missence']}
   *
   */
  function OverviewCard(){
      return {
          restrict: 'EA',
          templateUrl: 'overview/overview.template.html',
          controller: function($scope, StateManager, Overview){
            $scope.StateManager = StateManager;
            $scope.Overview = Overview;

            $scope.getStatusOfEntities = function(){
              return StateManager.state.activeTrack && StateManager.state.activeTrack.getStatusOfEntities();
            };

            $scope.showEntitiesStatus = function(){
              return StateManager.state.activeTrack && StateManager.state.activeTrack.filter.hasEntityFilters();
            };

          }
      };
  }

})();
