(function() {
  'use strict';

    angular.module('lera').factory('Overview', OverviewFactory);

    function OverviewFactory( toastr, $log, $interpolate, DataEngine ){

      var overviewSidebarState = 'closed';

      /**
       * @desc Colors used for the chart. if index >= length, index = modulo length
       */
      var COLORS = ['#659355', '#3F51B5', '#2196F3', '#009688', '#4CAF50', '#FF5722', '#795548', '#E91E63',
                    '#F44336', '#9C27B0', '#00BCD4', '#673AB7', '#CDDC39', '#FF9800'];

      /**
       * @desc Associating a color for each unique value of the chart
       */
      var COLORS_HASH = {};

      /**
       * @memberOf lera.factories
       * @constructor
       */
      var Overview = function(){

        this.categories = {};

      };

      /**
       * @desc Returns the list of colors that will be used for tha charts in all overviews
       * @return {list} colors
       */
      Overview.getDeclaredColors = function(){
        return COLORS;
      };

      Overview.triggerSidebar = function(){
        overviewSidebarState = overviewSidebarState === 'in' ? 'out' : 'in';
      };

      Overview.closeSidebar = function(){
        overviewSidebarState = 'out';
      };

      Overview.getSidebarState = function(){
        return overviewSidebarState;
      };

      /**
       * @desc Resets the content of the overview
       */
      Overview.prototype.reset = function(){
        this.categories = {};
      };

      /**
       * @desc Calculates the new overview
       * @param {object} entities and components on which to calculate the overview
       * @param {object} overviewCategories, contain the names of 'data' keys used to calculate the overview.
       * @param {object} filter used to determine if a category is in visible or invisible state,
       * as declared in the filter categories.
       * TODO Calculate overview only if there are less than 1K components,
       * else show warn message : please zoom into the track to see the overview

       Workflow :

       (entity/component).data
            -> ignore outside interval
            -> select only overviewCategories
            -> set as visible:false if filtered

       */
      Overview.prototype.set = function(entities, overviewCategories, filter, interval){
        this.categories = Overview.calculateOverview(entities, overviewCategories, filter, interval);
      };

      /**
       * @desc Changes the visible property of all elements to true
       */
      Overview.prototype.setAllVisible = function(){
        _.each( this.categories, function( c ){ _.each( c, function(point){ point.visible = true; });});
      };

      /**
       * @return {object} For each category (ex: biotype, transcript) we have a list of values with their count.
       */
      Overview.prototype.get = function(){
        return this.categories;
      };

      /**
       * @desc Returns true if there are categories
       * @return {boolean} hasOverview
       */
      Overview.prototype.hasOverview = function(){
        return ! _.isEmpty( this.categories );
      };

      /**
       * @desc Parses the meta object returned by the track rest api
       * @param {object} hash of entities to process. Those can be all entities retrieved from the server,
       * as the overview will consider only entities matching the current intervalParams
       * @param {array} list of categories for which we want to display a chart overview.
       * For each category (name, level) corresponding to a data key,
       * we will calculate the count for each data value
       * @return {object} unsortedMeta A data format compatible with highcharts libary
       */
      Overview.calculateOverview = function( entities, overviewCategories, filter, interval ){

        var self = this;
        var startTime = performance.now();

        var rawOverview = Overview.calculateOverviewHash( entities, overviewCategories, interval );
        var overview = _.transform( rawOverview, function(result, metas, category){
           var unsorted = _.map( metas, function( meta_count, meta_name ){

            return {
              name    : meta_name,
              y       : meta_count,
              visible : ! ( filter && filter.isCategoryFiltered( category, meta_name ) ),
              color : Overview._getColor( meta_name, COLORS_HASH, COLORS )
            };

          });

          result[category] = _.orderBy(unsorted, 'y', 'desc');

        }, {});

        var overviewTime = performance.now() - startTime;
        if( overviewTime > 10 ){ $log.warn("Overview calculations done in "+overviewTime+" ms."); }

        return overview;
      };



      /**
       * @desc Calculates basic statistics about returned entities, using data from entities.
       * If a category is not found in the entity data, the components data is used
       * NONE category is added if an entity/component has matched nothing in the category
       * @param {object} entities Entities which will be displayed on the track in the selected interval
       * @param {array} categories Names of the categories (ex: biotype, consequence) we want to display the statistics for.
       * ex: [{key:'biotype', level:'entity'}]
       * @param {object} interval containing start and stop. Only objects that are not outside this interval are
       * used to calculate the overview.
       * @return {object} ex: {biotype:{prot:2, NONE:3}}
       */
      Overview.calculateOverviewHash = function( entities, categories, interval ){

        var meta = {};
        if(!categories){
          return meta;
        }

        function incrementMeta(cat,catval){
          if(_.isUndefined(catval)){return;}
          meta[cat] = meta[cat] || {};
          if(_.isUndefined(meta[cat][catval])){
            meta[cat][catval] = 1;
          } else {
            meta[cat][catval] += 1;
          }
        }

        _.each(categories, function(catLevel, catName){ // for each meta category declared in registry or submitted by track

          _.each(entities, function(entity){ // for category inside data of each entity

            if( ! DataEngine.isOutsideInterval( entity, interval) ){ // if entity is not outside the current displayed interval

              switch(catLevel){

                case 'entity' :
                  var ecatval = entity && entity.data && entity.data[catName];
                  if( ! _.isUndefined(ecatval)){
                    incrementMeta(catName, ecatval);
                  } else {
                    incrementMeta(catName,'NONE'); // No data key for this category for this entity
                  }
                  break;

                case 'component' :
                  _.each(entity.components, function(component){ // for category inside data of each component
                    if( ! DataEngine.isOutsideInterval( component, interval) ){ // if component is not outside the current displayed interval
                      var ccatval = component && component.data && component.data[catName];
                      if( ! _.isUndefined(ccatval)){
                        incrementMeta(catName,ccatval);
                      } else {
                        incrementMeta(catName,'NONE');
                      }
                    }
                  });
                  break;

                default : $log.warn("Category level "+catLevel+" for overview category "+catName+" is not supported.");
              }
            }
          });
        });

        // Removing overview categories that have only NONE as value
        var filtered_meta = _.transform(meta, function(res, cat, catName){
          if( _.size(cat) > 1 || !cat.NONE ){res[catName] = cat;} // if there are more than one value, or the only value is not NONE
        }, {});

        return filtered_meta;
      };

      /**
       * @desc Associates a unique color for each value between different charts. The same
       * value will have the same color in different charts.
       * For this we associate a color
       * @param {string} value
       * @return {string} hex color
       * @todo There seem to be a rare bug which can associate the same color to the first and third value
       */
      Overview._getColor = function( value, valuesHash, colors ){

        if( _.isUndefined(value) || _.isUndefined(colors) || _.isUndefined(valuesHash) ){ return 'gray';}

        if( !valuesHash[value] ){
          // 1. get number of elements in hash
          var rawIdx = _.size(valuesHash);
          var nbColors = _.size( colors );
          if(!nbColors){ return 'gray';}
          var idx = rawIdx % nbColors;
          valuesHash[value] = colors[idx];
        }

        return valuesHash[value];
      };

      /**
       * @desc Returns the color associated with a property value
       * @param {string} value
       * @return {string} color
       */
      Overview.getColor = function( value ){
        return COLORS_HASH[value];
      };

      return Overview;
    }

})();
