(function() {

    'use strict';

    angular
        .module('lera')
        .directive('editable', editable);

  /**
   * @desc Editable text widget.
   * TODO max displayed length : ellipse of 200characters/pixels.
   * @class
   * @memberOf lera.directives
   */
  function editable($timeout) {
    return {
        restrict: 'EA',
        template: '<span ng-click="editMode = true" ng-hide="editMode" class="editable-value" title="Click to edit">{{getValue()}}</span>'+
                  '<input ng-keypress="keysSupport($event)" autofocus ng-blur="editMode = false" ng-model="value" ng-show="editMode" type="text" autocomplete="off" autocorrect="off" spellcheck="false"/>',
        scope: {
            value: '=',
            defaultValue: '@'
        },
        controller: function($scope){
          $scope.editMode = false;

          $scope.getValue = function(){
            return $scope.value || $scope.defaultValue;
          };

          $scope.keysSupport = function( e ){
            switch(e.keyCode){
              case 13 : $scope.editMode = false; break;
            }
          };

        },
        link: function(scope, e, attrs){
          scope.$watch('editMode', function(value) {
            if(value === true) {
              $timeout(function() {
                e.find('input').focus();
                //e.find('input')[0].select();
              });
            }
          });

        }
    };
  }

})();
