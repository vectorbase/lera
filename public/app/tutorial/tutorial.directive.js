(function() {
    'use strict';

    angular.module('lera').directive('tutorial', Tutorial);

    function Tutorial() {
        return {
            restrict: 'E',
            templateUrl: 'tutorial/tutorial.template.html',
            controller: function( $scope, $timeout ) {

                $scope.clickLoad = function(){
                  $timeout(function(){
                    $('#load-session').click();
                  },0);
                };

                $scope.clickAddRef = function(){
                  $timeout(function(){
                    $('#add-ref').click();
                  },0);
                };

            }
        };
    }

})();
