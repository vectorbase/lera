(function() {
  'use strict';

  angular.module('lera').directive('toolbar', Toolbar);

  function Toolbar(){
    return {
        restrict: 'EA',
        templateUrl: 'toolbar/toolbar.template.html',
        scope: {},
        controller:function($scope, StateManager, Overview, $location, Dialogs){
          $scope.StateManager = StateManager;
          $scope.Overview = Overview;
          $scope.Dialogs = Dialogs;

          /**
           * @desc TEMPOARARY. Should be removed/changed for PROD
           */
          $scope.getMessage = function(){
            // put isDevMode method to app.js ( also used to log errors to server if not on gunpowder dev server )
            var port = $location.port();
            if( port === 10973 ){
              return 'DEV';
            }
          };
        }
    };
  }


})();
