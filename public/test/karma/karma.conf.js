// Karma configuration
// Generated on Mon May 23 2016 11:07:06 GMT+0100 (BST)

module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
      '../../vendor/vendor.js',
      '../../lib/bower_components/angular-mocks/angular-mocks.js',
      '../../app/*.js',
	    '../../app/**/*.js',
      '../jasmine/spec/*.js'
    ],
    reporters: ['spec', 'coverage'],
    colors: true,
    browsers: ['PhantomJS'],
    preprocessors: {
     '../../app/**/*.factory.js': ['coverage']
    },
    coverageReporter: {
      type : 'html',
      dir : '../unit-test-coverage/'
    }
  });
};
