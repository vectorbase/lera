
describe('DataEngine',function(){

  beforeEach(module("lera"));

/*
  var log;
  beforeEach(function() { inject(function (_$log_) { log = _$log_; }); });
  afterEach(function () {  _.each(['debug', 'warn', 'error', 'info'], function(lt){
          _.each(log[lt].logs, function(l){ _.each(l, function(lc){ console.log(lc); }); }); });
  });
*/

  var DataEngine, Filter, UpdateModeEnum, StatusEnum;
  beforeEach(inject(function($injector){
      DataEngine = $injector.get('DataEngine');
      Filter = $injector.get('Filter');
      UpdateModeEnum = $injector.get('UpdateModeEnum');
      StatusEnum = $injector.get('StatusEnum');
  }));

  it('getEntitiesBounds', function(){

    var entities1 = {
            e1:{start:5, end:50, data:{biotype:'protein_coding'}},
            e4:{start:1, end:6, data:{biotype:'mRNA'}}
    };
    expect(DataEngine.getEntitiesBounds( entities1, new Filter() )).toEqual({start:1, end:50});
    // entities matching mRNA are hidden and not take into consideration
    expect(DataEngine.getEntitiesBounds( entities1, new Filter({categories:{biotype:{mRNA:true}}}) )).toEqual({start:5, end:50});
    // entities outside the interval are not taken into consideration
    expect(DataEngine.getEntitiesBounds( entities1, new Filter(), {start:10, end:20} )).toEqual({start:5, end:50});

  });

  it('isOutsideInterval', function(){
    expect(DataEngine.isOutsideInterval()).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:6, end:7})).toBeFalsy();
    expect(DataEngine.isOutsideInterval(undefined, {start:6, end:7})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({}, {start:6, end:7})).toBeFalsy();

    expect(DataEngine.isOutsideInterval({start:6, end:7}, {start:0,end:5})).toBeTruthy();
    expect(DataEngine.isOutsideInterval({start:5, end:7}, {start:0,end:5})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:0, end:5}, {start:0,end:5})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:1, end:4}, {start:5,end:10})).toBeTruthy();
    expect(DataEngine.isOutsideInterval({start:1, end:7}, {start:5,end:10})).toBeFalsy();
  });

  it('isOutsideInterval supports entities/components with no end defined', function(){

    expect(DataEngine.isOutsideInterval({start:6}, {start:0,end:5})).toBeTruthy();
    expect(DataEngine.isOutsideInterval({start:5}, {start:0,end:5})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:0}, {start:0,end:5})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:3}, {start:0,end:5})).toBeFalsy();
    expect(DataEngine.isOutsideInterval({start:1}, {start:5,end:10})).toBeTruthy();

  });

  it('MOVE update appends new entities/components from server to the current pool',function(){

    var da = new DataEngine();

    expect(da.getVirtualParams()).toBeUndefined();

    // Performing JUMP, previous entities and interval should be replaced
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e2:{components:{c21:{}}}}, undefined, true );
    };
    da.update({start:30, end:40}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);
    expect(da.getVirtualEntities()['e1']).toBeUndefined();
    expect(da.getVirtualEntities()['e2']).not.toBeUndefined();

    // Performing MOVE, new entities and components should be merged with previous ones
    // 1. New entity e3 should be added
    // 2. component c22 should be merged with previous entity e2
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e2:{components:{c21:{}, c22:{}}}, e3:{}}, undefined, true );
    };
    da.update({start:40, end:60}, new Filter(), function( queryParams ){
      expect( queryParams ).toEqual( {start:40, end:60} );
    }, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
    expect(da.getVirtualParams()).toEqual({start:30, end:60});
    expect(da.getRealParams()).toEqual({start:40, end:60});
    expect(da.getVirtualEntities()['e2']).not.toBeUndefined();
    expect(da.getVirtualEntities()['e2'].components['c21']).not.toBeUndefined();
    expect(da.getVirtualEntities()['e2'].components['c22']).not.toBeUndefined();
    expect(da.getVirtualEntities()['e3']).not.toBeUndefined();
    expect()

  });

  it('VIRT update makes no server calls, pool of virtual entities and virtual params stays the same', function(){

    var da = new DataEngine();

    expect(da.getVirtualParams()).toBeUndefined();

    // Performing JUMP, previous entities and interval should be replaced
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e2:{components:{c21:{}}}}, undefined, true );
    };
    da.update({start:30, end:40}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);
    expect(da.getVirtualEntities()['e1']).toBeUndefined();
    expect(da.getVirtualEntities()['e2']).not.toBeUndefined();

    // Performing VIRT
    da.serverUpdate = function( newParams, newFilter, success, error ){
      throw "Server update should not be called for a VIRT update";
    };
    da.update({start:35, end:38}, new Filter(), function(p, e){
      expect(p).toEqual({start:35, end:38});
      expect(_.size(e)).toBe(1);
    }, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.VIRT);
    expect(da.getVirtualParams()).toEqual({start:30, end:40});
    expect(da.getRealParams()).toEqual({start:35, end:38});
    expect(da.getVirtualEntities()['e2']).not.toBeUndefined();
    expect(da.getVirtualEntities()['e2'].components['c21']).not.toBeUndefined();

  });

  it('JUMP update replaces current pool with entities from server and resets the virtual interval',function(){

    var da = new DataEngine();

    expect(da.getVirtualParams()).toBeUndefined();

    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e1:{components:{c11:{}}}}, undefined, true );
    };
    da.update({start:10, end:20}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);

    // Performing JUMP, previous entities and interval should be replaced
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e2:{components:{c21:{}}}}, undefined, true );
    };
    da.update({start:30, end:40}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);
    expect(da.getVirtualEntities()['e1']).toBeUndefined();
    expect(da.getVirtualEntities()['e2']).not.toBeUndefined();

  });

  it('unsuccessful JUMP update should reset the virtual interval, to force next update to be JUMP', function(){

    var da = new DataEngine();

    expect(da.getVirtualParams()).toBeUndefined();

    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e1:{components:{c11:{}}}}, undefined, true );
    };
    da.update({start:10, end:20}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);
    expect(da.getVirtualParams()).toEqual({start:10, end:20});

    // Performing unsuccessful JUMP, previous entities and interval should be reseted
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return error && error(newParams, '', '', true);
    };
    da.update({start:30, end:40}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    //expect(_.size(da.getVirtualEntities())).toBe(0);
    expect(da.getVirtualParams()).toBeUndefined();

    // After unsuccessful JUMP, next update is forced to be JUMP
    da.serverUpdate = function( newParams, newFilter, success, error ){
      return success && success( newParams, {e1:{components:{c11:{}}}}, undefined, true );
    };
    da.update({start:12, end:18}, new Filter(), undefined, undefined, UpdateModeEnum.MOVE);
    expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
    expect(_.size(da.getVirtualEntities())).toBe(1);
    expect(da.getVirtualParams()).toEqual({start:12, end:18});
  });

  describe('testing with $http and simulating server connections',function(){

    var $httpBackend, $rootScope;
    beforeEach(inject(function($injector){
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
    }));

    afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
    });


    it('VIRT performed while MOVE is not finished', function(){

      // 1. JUMP (30, 40)
      // 3. VIRT (30, 40)
      // 2. MOVE (40, 60)
      // => VIRTUAL (30, 60), REAL (30, 40)

      var da = new DataEngine( '/mockUrl' );

      // Because VIRT was performed after MOVE, it is the coordinates from VIRT that matter
      // when MOVE finishes, it should check if there was not VIRT in between ??
      expect(da.getVirtualParams()).toBeUndefined();

      // ### FIRST JUMP ###
      da.update({start:30, end:40}, { filters : {entities: []}}, function(){}, function(rp, msg){ console.log(msg); }, UpdateModeEnum.MOVE);

      // During JUMP
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
      expect(da.isLoading()).toBeTruthy();

      $httpBackend
        .expect('POST', '/mockUrl', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({ entities: { e1_30_40: { start:31, end:32}, e2_30_40: {start: 38, end: 39} }} );
      $httpBackend.flush()

      // After JUMP
      expect(da.status).toBe(StatusEnum.OK);
      expect(da.isLoading()).toBeFalsy();
      expect(_.size(da.getVirtualEntities())).toBe(2);
      expect(_.keys(da.getVirtualEntities())).toEqual([ 'e1_30_40', 'e2_30_40' ]);
      expect(da.getVirtualParams()).toEqual({start:30, end:40});

      // ###  FIRST MOVE ###
      var afterMoveParams = undefined;
      var interval = {start:40, end:60};
      da.update( interval, { filters : {entities: []}}, function(p,e){ afterMoveParams = p; }, undefined, UpdateModeEnum.MOVE);

      // During MOVE
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
      expect(da.isLoading()).toBeTruthy();
      expect(_.size(da.getVirtualEntities())).toBe(2);
      expect(_.keys(da.getVirtualEntities())).toEqual([ 'e1_30_40', 'e2_30_40' ]);
      expect(da.getVirtualParams()).toEqual({start:30, end:40});

      expect(da.serverCalls).toBe(1);

      // VIRT while MOVE not finished
      da.update({start:30, end:40}, { filters : {entities: []}}, undefined, function(p,msg){console.log(msg);}, UpdateModeEnum.MOVE);
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.VIRT);

      // finishing MOVE
      $httpBackend
        .expect('POST', '/mockUrl', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":40,"end":60},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({ entities: { e1_40_60: { start:41, end:42}, e2_40_60: {start: 58, end: 59} }} );
      //$httpBackend.flush();
      $rootScope.$digest(); //to take into account the cancelled request

      // Expecting track params to be set to params from VIRT, not from MOVE
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.VIRT);
      expect(da.status).toBe(StatusEnum.OK);
      expect(da.isLoading()).toBeFalsy();
      expect(da.serverCalls).toBe(0);

      expect(_.size(da.getVirtualEntities())).toBe(2);
      expect(_.keys(da.getVirtualEntities())).toEqual([ 'e1_30_40', 'e2_30_40' ]);
      expect(da.getVirtualParams()).toEqual({start:30, end:40});
      expect(da.getRealParams()).toEqual({start:30, end:40});
      expect(afterMoveParams).toBeUndefined() // MOVE success never trigerred

    });


    it('MOVE performed while first MOVE is not finished', function(){

      // 1. JUMP (30, 40)
      // 2. MOVE (40, 60) (retrieved data will be ignored)
      // 3. MOVE (30, 50)
      // => VIRTUAL (30, 50), REAL (30, 50)

      var da = new DataEngine( '/mockUrl' );

      expect(da.getVirtualParams()).toBeUndefined();

      // ### FIRST JUMP ###
      da.update({start:30, end:40}, { filters : {entities: []}}, function(){}, function(rp, msg){ console.log(msg); }, UpdateModeEnum.MOVE);

      // During JUMP
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
      expect(da.isLoading()).toBeTruthy();

      $httpBackend
        .expect('POST', '/mockUrl', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({ entities: { e_31_32: { start:31, end:32}, e_38_39: {start: 38, end: 39} }} );
      $httpBackend.flush()

      // After JUMP
      expect(da.status).toBe(StatusEnum.OK);
      expect(da.isLoading()).toBeFalsy();
      expect(_.size(da.getVirtualEntities())).toBe(2);
      expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39']);
      expect(da.getVirtualParams()).toEqual({start:30, end:40});

      $httpBackend
        .expect('POST', '/mockUrl', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":40,"end":60},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({ entities: { e_41_42: { start:41, end:42}, e_58_59: {start: 58, end: 59} }} );

      // ###  FIRST MOVE (40, 60) ###
      var afterFirstMoveParams = undefined;
      var afterLastMoveParams = undefined;
      da.update({start:40, end:60}, { filters : {entities: []}}, function(p,e){ afterFirstMoveParams = p; }, function(p,msg){console.log(msg);}, UpdateModeEnum.MOVE);

      // During MOVE
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
      expect(da.isLoading()).toBeTruthy();
      expect(_.size(da.getVirtualEntities())).toBe(2);
      expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39']);
      expect(da.getVirtualParams()).toEqual({start:30, end:40});

      // NOTE : Because we already have entities for (30, 40) the server request only retrieves entities for (40, 50)
      $httpBackend
        .expect('POST', '/mockUrl', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":40,"end":50},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({ entities: { e_41_42: { start:41, end:42}, e_31_32: {start: 31, end: 32} }} );


      // Last MOVE (30, 50)
      // Because the first move is not finished, the virt interval is still (30,40) so (30,50) is considered to be another MOVE
      da.update({start:30, end:50}, { filters : {entities: []}}, function(p,e){ afterLastMoveParams = p; }, function(p,msg){console.log(msg);},UpdateModeEnum.MOVE);
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);

      $httpBackend.flush();// No more necessary since cancelling previious promise : $httpBackend.flush(1,1); //  Last MOVE (30, 50) is the first to finish

      expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
      expect(da.status).toBe(StatusEnum.OK); // status changed by the most recently executed update
      expect(da.isLoading()).toBeFalsy();
      expect(_.size(da.getVirtualEntities())).toBe(3);
      expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39', 'e_41_42']);
      expect(da.getVirtualParams()).toEqual({start: 30, end: 50});
      expect(afterLastMoveParams).toEqual({ start: 30, end: 50 });

      // $httpBackend.flush(); // flushing first move request (40, 60)
      $rootScope.$digest(); //to take into account the cancelled request

      // Expecting track params to be set to params from VIRT, not from MOVE
      expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
      expect(da.status).toBe(StatusEnum.OK);
      expect(da.isLoading()).toBeFalsy();
      expect(_.size(da.getVirtualEntities())).toBe(3);
      expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39', 'e_41_42']);
      expect(da.getVirtualParams()).toEqual({start:30, end:50});
      expect(da.getRealParams()).toEqual({start:30, end:50});
      expect(afterFirstMoveParams).toBeUndefined(); //success callback not called

    });

    it('JUMP requested before MOVE finished', function(){

      // 1. JUMP (30, 40)
      // 3. JUMP (60, 70)
      // 2. MOVE (40, 60)
      // => VIRTUAL (40, 70) Because when the MOVE (requested before JUMP but completed after) finishes,
      // it extends the virtual interval of the second jump

        var da = new DataEngine( '/mockUrl' );

        expect(da.getVirtualParams()).toBeUndefined();

        // ### FIRST JUMP ###
        da.update({start:30, end:40}, { filters : {entities: []}}, function(){}, function(rp, msg){ console.log(msg); }, UpdateModeEnum.MOVE);

        // During JUMP
        expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
        expect(da.isLoading()).toBeTruthy();

        $httpBackend
          .expect('POST', '/mockUrl', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({ entities: { e_31_32: { start:31, end:32}, e_38_39: {start: 38, end: 39} }} );
        $httpBackend.flush()

        // After JUMP
        expect(da.status).toBe(StatusEnum.OK);
        expect(da.isLoading()).toBeFalsy();
        expect(_.size(da.getVirtualEntities())).toBe(2);
        expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39']);
        expect(da.getVirtualParams()).toEqual({start:30, end:40});

        $httpBackend
          .expect('POST', '/mockUrl', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":40,"end":60},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({ entities: { e_41_42: { start:41, end:42}, e_58_59: {start: 58, end: 59} }} );

        // ###  FIRST MOVE (40, 60) ###
        var afterFirstMoveParams = undefined;
        var afterLastMoveParams = undefined;
        da.update({start:40, end:60}, { filters : {entities: []}}, function(p,e){ afterFirstMoveParams = p; }, function(p,msg){console.log(msg);}, UpdateModeEnum.MOVE);

        // During MOVE
        expect(da.lastUpdateMode).toBe(UpdateModeEnum.MOVE);
        expect(da.isLoading()).toBeTruthy();
        expect(_.size(da.getVirtualEntities())).toBe(2);
        expect(_.keys(da.getVirtualEntities())).toEqual(['e_31_32', 'e_38_39']);
        expect(da.getVirtualParams()).toEqual({start:30, end:40});

        $httpBackend
          .expect('POST', '/mockUrl', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":60,"end":70},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({ entities: { e_61_62: { start:61, end:62}, e_69_70: {start: 69, end: 70} }} );


        // Last JUMP (60, 70)
        //
        da.update({start:60, end:70}, { filters : {entities: []}}, function(p,e){ afterLastMoveParams = p; }, function(p,msg){console.log(msg);},UpdateModeEnum.JUMP);
        expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);

        $httpBackend.flush(); //  No more necessary since cancelling previious promise : $httpBackend.flush(1,1); //  Last JUMP (60, 70) is the first to finish

        expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
        expect(da.status).toBe(StatusEnum.OK); // status changed by the most recently executed update
        expect(da.isLoading()).toBeFalsy();
        expect(_.size(da.getVirtualEntities())).toBe(2);
        expect(_.keys(da.getVirtualEntities())).toEqual(['e_61_62', 'e_69_70']);
        expect(da.getVirtualParams()).toEqual({start:60, end:70});
        expect(afterLastMoveParams).toEqual({ start: 60, end: 70 });

        // No more necessary since cancelling previious promise : $httpBackend.flush(); // flushing first move request (40, 60)

        // Expecting track params to be set to params from JUMP, not from MOVE
        expect(da.lastUpdateMode).toBe(UpdateModeEnum.JUMP);
        expect(da.status).toBe(StatusEnum.OK);
        expect(da.isLoading()).toBeFalsy();
        expect(_.size(da.getVirtualEntities())).toBe(2);
        expect(_.keys(da.getVirtualEntities())).toEqual(['e_61_62', 'e_69_70']);
        expect(da.getVirtualParams()).toEqual({start:60, end:70});
        expect(afterFirstMoveParams).toBeUndefined(); //success callback not called


    });


  });


  it('isIntersect()', function(){
    var de = new DataEngine();
    expect(de.isIntersect()).toBeFalsy();
    de.virtualParams = {start:10, end:20};
    expect(de.isIntersect()).toBeFalsy();
    expect(de.isIntersect({start:5, end:9})).toBeFalsy(); // TODO should be adjacent, so true
    expect(de.isIntersect({start:5, end:10})).toBeTruthy();
    expect(de.isIntersect({start:5, end:15})).toBeTruthy();
    expect(de.isIntersect({start:10, end:20})).toBeTruthy();
    expect(de.isIntersect({start:15, end:16})).toBeTruthy();
    expect(de.isIntersect({start:15, end:25})).toBeTruthy();
    expect(de.isIntersect({start:20, end:30})).toBeTruthy();
    expect(de.isIntersect({start:21, end:25})).toBeFalsy(); // TODO should be adjacent, so true
    expect(de.isIntersect({start:25, end:30})).toBeFalsy();
  });

  it('isInside()', function(){
    var de = new DataEngine();
    expect(de.isInside()).toBeFalsy();
    de.virtualParams = {start:10, end:20};
    expect(de.isInside()).toBeFalsy();
    expect(de.isInside({start:5, end:9})).toBeFalsy();
    expect(de.isInside({start:5, end:10})).toBeFalsy();
    expect(de.isInside({start:5, end:15})).toBeFalsy();
    expect(de.isInside({start:10, end:20})).toBeTruthy();
    expect(de.isInside({start:15, end:16})).toBeTruthy();
    expect(de.isInside({start:15, end:25})).toBeFalsy();
    expect(de.isInside({start:20, end:25})).toBeFalsy();
    expect(de.isInside({start:21, end:25})).toBeFalsy();
    expect(de.isInside({start:25, end:30})).toBeFalsy();
  });

  it('getCorrectedMode()', function(){

    var de = new DataEngine();
    de.virtualParams = {start:10, end:20};
    expect(de.getCorrectedMode()).toBe(UpdateModeEnum.JUMP);
    // TODO Should be move infact because adjacent ??
    expect(de.getCorrectedMode({start:21, end:30}, UpdateModeEnum.MOVE)).toBe(UpdateModeEnum.JUMP);
    expect(de.getCorrectedMode({start:20, end:30}, UpdateModeEnum.MOVE)).toBe(UpdateModeEnum.MOVE);
    expect(de.getCorrectedMode({start:15, end:18}, UpdateModeEnum.MOVE)).toBe(UpdateModeEnum.VIRT);

  });

  it('Check that StatusEnum is set to the right value depending on the request status code', function(){
    var de = new DataEngine();
    expect(de.status).toBe(StatusEnum.PENDING);
  });

  it('adaptQueryInterval changes the queryInterval to retrieve only the missing data', function(){

    var virtInterval = {start:10, end:20};

    // Browse left
    var p1 = {start: 5, end: 15};
    expect( DataEngine.adaptQueryInterval( p1 , virtInterval ).length ).toBe(1);
    expect( DataEngine.adaptQueryInterval( p1 , virtInterval )).toEqual([{start:5, end: 10}]);
    expect( p1 ).toEqual({start: 5, end: 15});

    // Browse right
    var p2 = {start: 15, end: 25};
    expect( DataEngine.adaptQueryInterval( p2 , virtInterval ).length ).toBe(1);
    expect( DataEngine.adaptQueryInterval( p2 , virtInterval )).toEqual([{start:20, end: 25}]);
    expect( p2 ).toEqual({start: 15, end: 25});

    // Zoom out
    var p3 = {start: 5, end: 25};
    expect( DataEngine.adaptQueryInterval( p3 , virtInterval ).length ).toBe(2);
    expect( DataEngine.adaptQueryInterval( p3 , virtInterval ))
      .toEqual([ {start: 5, end: 10} , {start:20, end: 25} ]);
    expect( p3 ).toEqual({start: 5, end: 25});

    // JUMP
    var p4 = {start: 25, end: 30};
    expect( DataEngine.adaptQueryInterval( p4 , virtInterval ).length ).toBe(1);
    expect( DataEngine.adaptQueryInterval( p4 , virtInterval ) )
      .toEqual([ { start: 20, end: 30 } ]);
    expect( p4 ).toEqual({start: 25, end: 30});

  });

});
