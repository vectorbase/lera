describe('Overview',function(){

  beforeEach(module("lera"));

  var Overview, Filter;
  beforeEach(inject(function($injector){
      Overview = $injector.get('Overview');
      Filter = $injector.get('Filter');
  }));



  it('calculateOverview', function(){

    expect(Overview.calculateOverviewHash({1:{data:{biotype:"a"}}},
    {biotype :'entity'} )).toEqual({biotype:{a:1}});


    expect(Overview.calculateOverviewHash({
      1:{data:{biotype:"a", strand:0}},
      2:{data:{biotype:"a", strand:1}},
      3:{data:{biotype:"a", strand:1}},
      4:{data:{biotype:"b", strand:1}},
      5:{data:{biotype:"b", strand:1}}
    }, {biotype:"entity", strand:"entity"}))
    .toEqual({biotype:{a:3,b:2},strand:{0:1,1:4}});

    // category not found, if only 'NONE' in category, category not shown
    expect(Overview.calculateOverviewHash({1:{data:{biotype:"a"}}}, {badCat:"entity"})).toEqual({});

    // can process categories with 0 values
    expect(Overview.calculateOverviewHash({1:{data:{quality:0}}}, {quality:"entity"})).toEqual({quality:{0:1}});

    // if category not in entity data, lokk into comonents data
    expect(Overview.calculateOverviewHash({
      1:{data:{biotype:"a", strand:0}},
      2:{data:{biotype:"a", strand:1}},
      3:{components:{
        1:{data:{effect:"missence"}},
        2:{data:{effect:"missence"}},
        3:{data:{effect:"missence"}},
        4:{data:{effect:"synonymous"}},
      }},
      4:{data:{biotype:"b", strand:1}},
      5:{data:{biotype:"b", strand:1}, components:{
        1:{data:{effect:"missence"}},
        2:{data:{effect:"5utr"}},
        3:{data:{}}
      }}
    }, {biotype:"entity",strand:"entity", effect:"component"}))
    .toEqual({biotype:{a:2,b:2,NONE:1}, strand:{0:1,1:3,NONE:1}, effect:{missence:4, synonymous:1, "5utr":1, NONE:1}});

  });

  it('calculateOverview retuns an ORDER by count DESC list', function(){

    var entities = {
      0: {data:{effect:"very-bad"}},
      1: {data:{effect:"bad"}},
      2: {data:{effect:"good"}},
      3: {data:{effect:"very-good"}},
      4: {data:{effect:"very-good"}},
      5: {data:{effect:"very-good"}},
      6: {data:{effect:"good"}}
    };
    var overview = Overview.calculateOverview( entities, {effect:"entity"}, new Filter(), {start:1, end:100} );
    expect(overview).toEqual({
      effect: [
      {name:"very-good", y:3, visible: true, color: Overview.getDeclaredColors()[3]},
      {name:"good", y:2, visible: true, color: Overview.getDeclaredColors()[2]},
      {name:"very-bad", y:1, visible: true, color: Overview.getDeclaredColors()[0]},
      {name:"bad", y:1, visible: true, color: Overview.getDeclaredColors()[1]}
    ]
    });
  });

  it('calculateOverview ignores entities and components outside the current interval', function(){

    var entities = {
      0: {data:{effect:"very-bad"}}, // if no start/end, by default considered as IN
      1: {start: 10, end: 90,data:{effect:"bad"}},
      2: {start: 10, end: 90,data:{effect:"good"}},
      3: {start: 200, end: 210, data:{effect:"very-good"}}, // should be ignored
      4: {start: 200, end: 210, data:{effect:"very-good"}}, // shoud be ignored
      5: {start: 10, end: 90, data:{effect:"very-good"}},
      6: {start: 10, end: 90,data:{effect:"good"}}
    };
    var overview = Overview.calculateOverview( entities, {effect:"entity"}, new Filter(), {start:1, end:100} );
    expect(overview).toEqual({
      effect: [
      {name:"good", y:2, visible: true, color: Overview.getDeclaredColors()[2]},
      {name:"very-bad", y:1, visible: true, color: Overview.getDeclaredColors()[0]},
      {name:"bad", y:1, visible: true, color: Overview.getDeclaredColors()[1]},
      {name:"very-good", y:1, visible: true, color: Overview.getDeclaredColors()[3]}
    ]
    });

  });

  it('__getColor', function(){

    var colors = ['red', 'green', 'blue'];
    var hashValues = {};
    expect( Overview._getColor() ).toBe('gray');
    expect( Overview._getColor('test1') ).toBe('gray');
    expect( Overview._getColor('test1', {}, []) ).toBe('gray');

    // independent
    expect( Overview._getColor('test1', {}, colors) ).toBe('red');
    expect( Overview._getColor('test2', {}, colors) ).toBe('red');

    expect( Overview._getColor('test1', hashValues, colors) ).toBe('red');
    expect( Overview._getColor('test2', hashValues, colors) ).toBe('green');
    expect( Overview._getColor('test1', hashValues, colors) ).toBe('red'); // color conserved from hash
    expect( Overview._getColor('test3', hashValues, colors) ).toBe('blue');
    expect( Overview._getColor('test3', hashValues, colors) ).toBe('blue');
    expect( Overview._getColor('test4', hashValues, colors) ).toBe('red'); // no more new colors, starting from beginning

  });

it('calculateOverview sets filtered values as visible:false', function(){

  var entities = {
    0: {data:{effect:"very-bad"}}, // if no start/end, by default considered as IN
    1: {start: 10, end: 90,data:{effect:"bad"}},
    2: {start: 10, end: 90,data:{effect:"good"}},
  };

  var f = new Filter();
  f.setCategoryFilters('effect', {'good': true});

  var overview = Overview.calculateOverview( entities, {effect:"entity"}, f, {start:1, end:100} );
  expect(overview).toEqual({
    effect: [
    {name:"very-bad", y:1, visible: true, color: Overview.getDeclaredColors()[0]},
    {name:"bad", y:1, visible: true, color: Overview.getDeclaredColors()[1]},
    {name:"good", y:1, visible: false, color: Overview.getDeclaredColors()[2]}
  ]
  });

});

it('setAllVisible', function(){

  var overview = new Overview();

  var entities = {
    0: {data:{effect:"very-bad"}}, // if no start/end, by default considered as IN
    1: {start: 10, end: 90,data:{effect:"bad"}},
    2: {start: 10, end: 90,data:{effect:"good"}},
  };

  var f = new Filter();
  f.setCategoryFilters('effect', {'good': true});

  overview.set( entities, { effect:'entity' }, f );

  expect(overview.get()).toEqual({
    effect: [
    {name:"very-bad", y:1, visible: true, color: Overview.getDeclaredColors()[0]},
    {name:"bad", y:1, visible: true, color: Overview.getDeclaredColors()[1]},
    {name:"good", y:1, visible: false, color: Overview.getDeclaredColors()[2]}
  ]
  });

  overview.setAllVisible();

  expect(overview.get()).toEqual({
    effect: [
    {name:"very-bad", y:1, visible: true, color: Overview.getDeclaredColors()[0]},
    {name:"bad", y:1, visible: true, color: Overview.getDeclaredColors()[1]},
    {name:"good", y:1, visible: true, color: Overview.getDeclaredColors()[2]}
  ]
  });

});

});
