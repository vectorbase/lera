
describe('Reference',function(){

  beforeEach(module("lera"));

  var Reference, $httpBackend, StateManager;
  beforeEach(inject(function($injector){
      Reference = $injector.get('Reference');
      StateManager = $injector.get('StateManager');
      $httpBackend = $injector.get('$httpBackend');
  }));


  describe('resolve()',function(){

    refConf = {
      "url":"/api/reference/genome"
    };

    it('if can not resolve aditional data with the server, fires error callback', function(){
      $httpBackend
        .expect('GET', '/api/reference/genome?end=20&start=10')
        .respond({}); //server does not resolve the track, initial data is maintained

      var msg1;
      var ref = new Reference('id1', '', {start:10, end:20}, refConf);

      ref.resolve(function(){}, function(ref, msg){msg1=msg;});

      $httpBackend.flush(); // reference resolve
      expect(msg1).toBe("Could not resolve reference. Some mandatory fields are probably missing.");
    });

    it('replaces initial reference data by data from server', function(){
      $httpBackend
        .expect('GET', '/api/reference/genome?transcript=t1')
        .respond({start:1, end:2});

      var ref = new Reference('id1', '', {transcript:'t1'}, refConf);
      ref.resolve(function(){
        expect( _.size(ref.getIntervalParams()) ).toBe(2);
        expect(ref.getIntervalParam('start')).toBe(1);
        expect(ref.getIntervalParam('end')).toBe(2);
        expect(ref.getIntervalParam('transcript')).toBeUndefined();
      });
      $httpBackend.flush(); // reference resolve
    });

    it('_assessInterval returns first requested field not found in interval', function(){

      expect(Reference._assessInterval({start:10}, ['start', 'end'])).toBe('end');
      expect(Reference._assessInterval({end:20}, ['start', 'end'])).toBe("start");

    });

    it('_ensureNumbers transforms requested fields to numbers', function(){

      var interval = { start: 10, min: '1' };
      Reference._ensureNumbers(interval, ['start', 'min', 'inexisting']);
      expect(interval).toEqual({ start: 10, min: 1 });
    });

  });


  it('updateInterval() updates all tracks with new start and end', function(){



  });

  describe('browse',function(){

    it('simple base pair level browse', function(){
      var r = new Reference('myRefId', 'ensGenome', { start:7, end:8 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.browse(50);

      expect(r.getIntervalParam('start')).toBe(8);
      expect(r.getIntervalParam('end')).toBe(9);

      r.browse(-50);

      expect(r.getIntervalParam('start')).toBe(7);
      expect(r.getIntervalParam('end')).toBe(8);

    });

  });

  describe('zoom interaction',function(){

    it('simple zoom', function(){
      var r = new Reference('myRefId', 'myRefType', { start:10, end:20 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.zoom(10); // ref factor will be 1 here
      expect(r.getIntervalParam('start')).toBe(11);
      expect(r.getIntervalParam('end')).toBe(19);
    });

    it('zoom with min and max borders', function(){
      var r = new Reference('myRefId', 'myRefType', { start:10, end:20, min:5, max:30 }, {"url":"/api/reference/genome"});
      Reference.width = 100;

      // when dezooming, first we reach the min, then the max
      r.zoom(-50);

      expect(r.getIntervalParam('start')).toBe(5);
      expect(r.getIntervalParam('end')).toBe(25);

      // again to reach max
      r.zoom(-50);

      expect(r.getIntervalParam('start')).toBe(5);
      expect(r.getIntervalParam('end')).toBe(30);

      // again, should not change this time
      r.zoom(-50);

      expect(r.getIntervalParam('start')).toBe(5);
      expect(r.getIntervalParam('end')).toBe(30);

    });

    it('zoom from base-pair level interval', function(){
      var r = new Reference('myRefId', 'myRefType', { start:7, end:8 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.zoom(-50);

      expect(r.getIntervalParam('start')).toBe(6);
      expect(r.getIntervalParam('end')).toBe(9);

    });

    it('zoom + limit', function(){
      var r = new Reference('myRefId', 'myRefType', { start:7, end:8 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.zoom(50);
      expect(r.getIntervalParam('start')).toBe(7);
      expect(r.getIntervalParam('end')).toBe(8);

    });

    it('_getShiftFactor', function(){

      expect(Reference._getShiftFactor( 180, 100, 200, 55, 245 )).toBe(-27);
      expect(Reference._getShiftFactor( 0, 100, 200, 55, 245 )).toBe(0);
      expect(Reference._getShiftFactor( 150, 100, 200, 150, 150 )).toBe(0); // new interval has 0 length
      expect(Reference._getShiftFactor( 100, 100, 100, 50, 150 )).toBe(50); // old interval has 0 length
      expect(Reference._getShiftFactor( 0, 0, 0, 0, 0 )).toBe(0);
      expect(Reference._getShiftFactor()).toBe(0);
      expect(Reference._getShiftFactor( 1412004, 1412003, 1412006, 1412003, 1412004 )).toBe(1); // real case
    });

    it('zoom centering on mouse position', function(){
      var r = new Reference('myRefId', 'myRefType', { start:10, end:20 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.zoom(10, 100); // ref factor will be 1 here
      expect(r.getIntervalParam('start')).toBe(12);
      expect(r.getIntervalParam('end')).toBe(20);

      var r = new Reference('myRefId', 'myRefType', { start:10, end:20 }, {"url":"/api/reference/genome"});
      Reference.width = 100;
      r.zoom(10, 1); // ref factor will be 1 here
      expect(r.getIntervalParam('start')).toBe(10);
      expect(r.getIntervalParam('end')).toBe(18);
    });

  });


  it('fromView() should transform screen coordinates into reference coordinates', function(){
    var r = new Reference('myRefId', 'myRefType', {start:10, end:20}, {"url":"/api/reference/genome"});
    Reference.width = 100;
    expect(r.fromView(10)).toBe(11);
    expect(r.fromView(0)).toBe(10);
    expect(r.fromView(100)).toBe(20);
    expect(r.fromView(30)).toBe(13); // rounded
    expect(r.fromView(110)).toBe(20);

    expect(function(){ r.fromView() }).toThrow("fromView needs valid x parameter");
  });

  it('getTicksCount', function(){

    var r = new Reference('myRefId', 'myRefType', {start:10, end:20}, {"url":"/api/reference/genome"});
    expect(r.getTicksCount()).toBe(10);

  });

  it('shiftElement', function(){

    expect(Reference.shiftElement()).toBeFalsy();
    expect(Reference.shiftElement( [] )).toBeFalsy();
    expect(Reference.shiftElement( [], {} )).toBeFalsy();
    expect(Reference.shiftElement( [], {}, 1 )).toBeFalsy();
    expect(Reference.shiftElement( [], undefined, 1 )).toBeFalsy();
    expect(Reference.shiftElement( undefined, {}, 1 )).toBeFalsy();

    var a = { name: 'a', order: 1};
    var b = { name: 'b', order: 2};
    var c = { name: 'c', order: 3};

    var data = {a:a, b:b, c:c};

    var getOrdered = function(){
      return _.sortBy( data, 'order');
    };

    expect(Reference.shiftElement( getOrdered(), undefined, 1 )).toBeFalsy()
    expect( getOrdered() ).toEqual([a,b,c]);

    expect(Reference.shiftElement( getOrdered(), a, -1 )).toBeFalsy();
    expect( getOrdered() ).toEqual([a,b,c]);

    expect(Reference.shiftElement( getOrdered(), c, 1 )).toBeFalsy();
    expect( getOrdered() ).toEqual([a,b,c]);

    expect(Reference.shiftElement( getOrdered(), a, 10 )).toBeFalsy();
    expect( getOrdered() ).toEqual([a,b,c]);

    expect(Reference.shiftElement( getOrdered(), a, 1 )).toBeTruthy();
    expect( getOrdered() ).toEqual([b,a,c]);

    expect(Reference.shiftElement( getOrdered(), c, -2 )).toBeTruthy();
    expect( getOrdered() ).toEqual([c, a, b]); // c and b change places

    expect(Reference.shiftElement( getOrdered(), a, 1.2 )).toBeFalsy();
    expect( getOrdered() ).toEqual([c,a,b]);

  });

  it('removeFromState', function(){


          StateManager.registry.data = {
            "references": {
              "ensGenome": {
                "url":"/api/reference/genome",
                "name":"genome",
                "defaultTracks":["ensTranscript"]
              }
            }
          }

    $httpBackend
      .expect('GET', '/api/reference/genome?end=20&start=10')
      .respond({start:10, end:20}); //server does not resolve the track, initial data is maintained

    expect(StateManager.isStandBy()).toBeTruthy();

    var ref = StateManager.addReference('ensGenome', {start:10, end:20});
    $httpBackend.flush(); // reference resolve

    expect(StateManager.getReferencesCount()).toBe(1);

    ref.removeFromState();

    expect(StateManager.getReferencesCount()).toBe(0);

  });

  it('setCursorPosition getCursorPosition', function(){

    var r = new Reference('myRefId', 'ensGenome', { start:10, end:20 }, {"url":"/api/reference/genome"});
    expect(r.getCursorPosition()).toBeUndefined();

    Reference.width = 100;
    r.setCursorPosition(10);
    expect(r.getCursorPosition()).toBe(11); // 10 in 100 is like 1 in 10

    Reference.width = 100;
    r.setCursorPosition(90);
    expect(r.getCursorPosition()).toBe(19); // 90 in 100 is like 9 in 10

  });

  describe('shift tracks',function(){

    beforeEach(inject(function($injector){

    }));

    it('getOrderedTracks isFirstTrack isLastTrack shiftTrack', function(){

      var r = new Reference('myRefId', 'ensGenome', { start:10, end:20 }, {"url":"/api/reference/genome"});
      var t1 = { order: 1 };
      var t2 = { order: 2 };
      var t3 = { order: 3 };

      expect(r.getOrderedTracks()).toEqual([]);

      expect(r.isFirstTrack()).toBeFalsy();
      expect(r.isLastTrack()).toBeFalsy();

      expect(r.isFirstTrack({})).toBeFalsy();
      expect(r.isLastTrack({})).toBeFalsy();

      r.tracks = { t2: t2, t1: t1, t3: t3 };

      expect(r.getTracks()).toEqual([t2, t1, t3]);
      expect(r.getOrderedTracks()).toEqual([t1, t2, t3]);

      expect(r.isFirstTrack(t1)).toBeTruthy();
      expect(r.isFirstTrack(t2)).toBeFalsy();
      expect(r.isFirstTrack(t3)).toBeFalsy();

      expect(r.isLastTrack(t1)).toBeFalsy();
      expect(r.isLastTrack(t2)).toBeFalsy();
      expect(r.isLastTrack(t3)).toBeTruthy();

      r.shiftTrack(t2, 1);
      expect(r.getOrderedTracks()).toEqual([t1, t3, t2]);

    });

  });

});
