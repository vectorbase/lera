describe('Filter',function(){

  beforeEach(module("lera"));

  var Filter;
  beforeEach(inject(function($injector){
      Filter = $injector.get('Filter');
  }));



  it('isVisible returns false if filters are declared for one of the data keys and the data value for this key is not in filters', function(){

    expect(Filter.isVisible({}, {biotype:'protein_coding'})).toBeTruthy();
    expect(Filter.isVisible({biotype:{}}, {biotype:'protein_coding'})).toBeTruthy();

    // WARNING: we put in categories, the categories we want to hide, not the ones we want to show.
    expect(Filter.isVisible({biotype:{mRNA:1}}, {biotype:'protein_coding'})).toBeTruthy();
    expect(Filter.isVisible({biotype:{protein_coding:1}}, {biotype:'protein_coding', strand:'+'})).toBeFalsy();
  });

  it('isCategoryFiltered', function(){
    var f = new Filter( {categories:{cat1: {'filt1':true}}} );
    expect(f.isCategoryFiltered('cat1', 'filt1')).toBe(true);
    expect(f.isCategoryFiltered('cat1', 'filt2')).toBe(false);
    expect(f.isCategoryFiltered('cat2', 'filt1')).toBe(false);
  });

  it('hasCategoryFilters', function(){

    var f1 = new Filter( {categories:{cat1: {'filt1':true}}} );
    var f2 = new Filter( {categories:{cat1: {}}} );
    var f3 = new Filter( {categories:{}} );
    expect(f1.hasCategoryFilters()).toBeTruthy();
    expect(f2.hasCategoryFilters()).toBeFalsy();
    expect(f3.hasCategoryFilters()).toBeFalsy();

  });

  it('legendAsFilters', function(){

    var p1 = { visible: true, name: 'Point 1'};
    var p2 = { visible: true, name: 'Point 2'};
    var p3 = { visible: true, name: 'Point 3'};

    var points = [p1, p2, p3];

    expect(Filter.legendAsFilters()).toEqual({});
    expect(Filter.legendAsFilters( points )).toEqual({});
    expect(Filter.legendAsFilters( undefined, points )).toEqual({});
    expect(Filter.legendAsFilters( p1, points )).toEqual({ "Point 1" : true }); // Point 1 should be hidden
    expect(Filter.legendAsFilters( p1, points )).toEqual({ "Point 1" : true }); // Still hidden
    p1.visible = false; // making p1 hidden
    expect(Filter.legendAsFilters( p1, points )).toEqual({}); // After click, p1 becomes visible, so removed from filters
    expect(Filter.legendAsFilters( p2, points )).toEqual({ "Point 1" : true, "Point 2" : true  }); // both p1 and p2 are hidden now

  });

});
