describe('Track',function(){

  beforeEach(module("lera"));

  /*
  var log;
  beforeEach(function() { inject(function (_$log_) { log = _$log_; }); });
  afterEach(function () {  _.each(['debug', 'warn', 'error', 'info'], function(lt){
          _.each(log[lt].logs, function(l){ _.each(l, function(lc){ console.log(lc); }); }); });
  });
  */

  var $httpBackend, StateManager, Track, Reference, UpdateModeEnum, Filter;
  beforeEach(inject(function($injector){

      StateManager = $injector.get('StateManager');
      $httpBackend = $injector.get('$httpBackend');
      Track = $injector.get('Track');
      Filter = $injector.get('Filter');
      Reference = $injector.get('Reference');
      UpdateModeEnum = $injector.get('UpdateModeEnum');

      StateManager.registry.data = {
        "references": {

          "ensGenome": {
            "url":"/api/reference/genome",
            "data":{
              "species" : {"required":true, "urlTemplate": "/api/reference/genome/autocomplete/species?text={{text}}"},
              "seq_region_name" : {"required":false, "urlTemplate": "/api/reference/genome/autocomplete/seq_region_name?text={{text}}"}
            }
          },
          "ensProtein":{
            "url": "/api/reference/protein"
          }
        },
        "tracks": {

          "ensTranscript": {
            "name": "transcript",
            "description": "Transcripts from vectorbase",
            "supportedReferences":["ensGenome"],
            "url":"/api/track/transcript",
            "entityLabelPosition":"entity",
            "morphsTo":"ensProtein",
            "entity_threshold":40
          },
          "ensGenotype":{
            "url":"/api/track/genotype",
            "export":{
              "url":"/api/track/genotype/export",
              "fileNameTemplate":"{ref.species}.vcf"
            },
            "supportedReferences":["ensGenome"]
          }
        }
      }

      $httpBackend
        .expect('GET', '/api/reference/genome?end=20&start=10')
        .respond({start:10, end:20}); //server does not resolve the track, initial data is maintained

      var ref = StateManager.addReference('ensGenome', {start:10, end:20});
      $httpBackend.flush(); // reference resolve

      $httpBackend
        .expect('POST', '/api/track/transcript')
        .respond({
          entities:{
            e1:{
              name:'entity1',
              start:15,
              end:18,
              data:{
                biotype:'protein_coding'
              },
              components:{
                c1:{
                  name:'comp1',
                  start: 16,
                  end: 17
                },
                c2:{}
              }
            },
            e2:{
              name:'entity2',
              start:10,
              end:16,
              data:{
                biotype:'tRNA'
              }
            }
          }
        });

      StateManager.addTrack(ref, 'ensTranscript');
      $httpBackend.flush(); // update track

      $httpBackend
        .expect('POST', '/api/track/genotype')
        .respond({
          entities:{
            sample1:{
              name:'sample1',
              components:{
                genot1:{
                  name:'genot1'
                },
                genot2:{}
              }
            }
          },
          meta:{

          }
        });

      StateManager.addTrack(ref, 'ensGenotype');
      $httpBackend.flush(); // update track

  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });


  describe('creating new Track instance',function(){

    it('with base parameters', function(){
      var track = new Track( 'id', 'type', {url:"ttt"}, {} );
      expect(track.url).not.toBeUndefined();
    });

    it('with link templates declared in conf', function(){
      var track = new Track( 'id', 'type', {url:"ttt", links:{l1:"link {{name}}"}}, {addTrack:function(){}} );
      expect(track.links.l1({name:"name1"})).toBe("link name1");
    });

    it('with filters getFilters() getEntityFilters() getCategoryFilters()', function(){
      var track = new Track( 'id', 'type', {url:"ttt"}, {addTrack:function(){}}, { entities:['sample1', 'sample2'], categories:{biotype:{protein_coding:1}} } );
      expect( _.size(track.filter.getEntityFilters())).toBe(2);
      expect(track.filter.getEntityFilters()[0]).toBe('sample1');
      expect(track.getCategoryFilters().biotype).not.toBeUndefined();
      expect(track.getCategoryFilters().biotype.protein_coding).toBeTruthy();
    });

    it('without reference', function(){
      expect(function(){ var track = new Track( 'id', 'type', {url:"ttt"} ); })
        .toThrow("Undefined reference for track id");
    });

    it('without url', function(){
      expect(function(){ var track = new Track( 'id', 'type', {}, {} ); })
        .toThrow("A REST API URL is mandatory for each track, but was not declared in the registy for track typetype");
    });

  });



  it('isOutsideInterval() returns true if both start and end of position are outside ref interval', function(){

    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
    t.data = {start:10, end:20};

    expect(t.isOutsideInterval({start:10, end:20})).toBeFalsy();
    expect(t.isOutsideInterval({start:10, end:15})).toBeFalsy();
    expect(t.isOutsideInterval({start:5, end :15})).toBeFalsy();
    expect(t.isOutsideInterval({start:15, end:25})).toBeFalsy();
    expect(t.isOutsideInterval({start:-2, end:6})).toBeTruthy();
    expect(t.isOutsideInterval({start:21, end:26})).toBeTruthy();
  });


  it('centerToEntity()', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];

    t.centerToEntity('e1');

    expect(t.getIntervalParams().start).toBe(15);
    expect(t.getIntervalParams().end).toBe(18);

  });

  it('centerToComponent()', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];

    // VIRT update
    t.centerToComponent('e1', 'c1');

    expect(t.getIntervalParams().start).toBe(16);
    expect(t.getIntervalParams().end).toBe(17);

  });



  it('getEntity returns en entity object corresponding to an entity id', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
    expect(t).not.toBeUndefined();
    expect(t.getEntity('e1')).not.toBeUndefined();
    expect(t.getEntity('e1').name).toBe('entity1');
  });


  it('getComponent to return a component given a component id and an entity id', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
    expect(t).not.toBeUndefined();
    expect(t.getEntity('e1')).not.toBeUndefined();
    expect(t).not.toBeUndefined();
    expect(t.getComponent('e1', 'c1')).not.toBeUndefined();
    expect(t.getComponent('e1', 'c1').name).toBe('comp1');
  });

  describe('update',function(){

    it('simple update', function(){

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":20,"end":30},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({
          entities:{
            e3:{ name:'entity3'},
            e4:{ name:'entity4'}
          },meta:{}
        });

      $httpBackend
        .expect('POST', '/api/track/genotype', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":20,"end":30},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({});

        var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
        t.reference.updateInterval( 20, 30 );
        $httpBackend.flush(); // update track

        expect( _.size(t.getEntities()) ).toBe(2);
        expect(t.getEntity('e3')).not.toBeUndefined();
        expect(t.getEntity('e4')).not.toBeUndefined();


        t.reference.updateInterval( 30, 40 );

        expect(t.getIntervalParams()).toEqual({start: 20, end: 30}); // before server responds, track keeps old params

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({
          entities:{
            e4:{ name:'entity4'},
            e5:{ name:'entity5'}
          },meta:{}
        });

        $httpBackend
          .expect('POST', '/api/track/genotype', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({});

        $httpBackend.flush(); // update track

        expect(t.getIntervalParams()).toEqual({start: 30, end: 40});
        expect( _.size(t.getEntities()) ).toBe(2);
        expect(t.getEntity('e4')).not.toBeUndefined();
        expect(t.getEntity('e5')).not.toBeUndefined();

        expect(t.filter.categories).toEqual({});
    })

    it('new update request before previous update completed', function(){

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":20,"end":30},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({
          entities:{
            e20:{ name:'entity20'},
            e30:{ name:'entity30'}
          },meta:{}
        });

      $httpBackend
        .expect('POST', '/api/track/genotype', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{"start":20,"end":30},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond({});

        var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
        t.reference.updateInterval( 20, 30 );

        $httpBackend
          .expect('POST', '/api/track/transcript', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({
            entities:{
              e30:{ name:'entity30'},
              e40:{ name:'entity40'}
            },meta:{}
          });

        $httpBackend
          .expect('POST', '/api/track/genotype', function(o){
            expect(JSON.parse(o)).toEqual({"intervalParams":{"start":30,"end":40},"trackParams":{"filters":{"entities":[]}}});
            return true;
          })
          .respond({});

        /**
         * Because the virtual interval is still (10,20) and the first request to be completed is (30,10)
         * both intervals are not adjacent and the mode is automatically changed to JUMP
         * Or in JUMP mode, data from not last send requests is ignored.
         */
        t.reference.updateInterval( 30, 40, UpdateModeEnum.MOVE );

        //$httpBackend.flush(2,2); // completed second update request
        $httpBackend.flush(); // completing first update request

        // Data from first update request is ignored, even if it is the last to be completed
        expect( _.size(t.getEntities()) ).toBe(2);
        expect(t.getEntity('e30')).not.toBeUndefined();
        expect(t.getEntity('e40')).not.toBeUndefined();
    })

    it('after unsuccessful update, pool of entities is empty', function(){

      var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
      expect(t.getEntities()).not.toEqual({});

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
          expect(JSON.parse(o)).toEqual({"intervalParams":{},"trackParams":{"filters":{"entities":[]}}});
          return true;
        })
        .respond(418, 'Simulating user error');

      t.update( {}, function(){}, UpdateModeEnum.MOVE );
      $httpBackend.flush();

      expect(t.getEntities()).toEqual({});


      // expect(t.getEntities()).not.toEqual({});
    });

  });

  it('hasExportCapability returns true if the track has an exportUrl declared in the registry declaration', function(){
    expect(_.values(_.values(StateManager.state.references)[0].tracks)[0].hasExportCapability()).toBeFalsy();
    expect(_.values(_.values(StateManager.state.references)[0].tracks)[1].hasExportCapability()).toBeTruthy();
  });

  it('getEntitiesBounds returns the min of all starts and the max of all ends of submitted entnties hash', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
    var entities = {
      e1:{start:5, end:50, data:{biotype:'protein_coding'}},
      e2:{start:10, end:60, data:{biotype:'protein_coding'}},
      e3:{},
      e4:{start:1, end:6, data:{biotype:'mRNA'}}
    }

    t.entities = entities;

    t.intervalParams = {start:1, end:100};
    expect(t.getEntitiesBounds()).toEqual({start:1, end:60});

    t.intervalParams = {start:10, end:20};
    expect(t.getEntitiesBounds()).toEqual({start:5, end:60});

    t.setCategoryFilters('biotype', {mRNA:true});
    expect(t.filter.categories.biotype.mRNA).toBeTruthy()
    expect(t.getEntitiesBounds()).toEqual({start:5, end:60});

  });

  it('getEntitiesBounds returns the min/max of entities declared for the track', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];
    expect(t.getEntitiesBounds()).toEqual({start:10, end:18});
  });



  it('isEntityVisible returns true only if this entity does not corresponds to one of the filters', function(){
    var t = _.values(_.values(StateManager.state.references)[0].tracks)[0];

    expect(t.filter.isEntityVisible(t.getEntities().e1)).toBeTruthy();
    expect(t.filter.isEntityVisible(t.getEntities().e2)).toBeTruthy();

    t.setCategoryFilters('biotype', {'protein_coding':true});

    expect(t.filter.isEntityVisible(t.getEntities().e1)).toBeFalsy();
    expect(t.filter.isEntityVisible(t.getEntities().e2)).toBeTruthy();

  });


  describe('createLanes()',function(){

    it('laneForEntity', function(){

      // Added on the same lane
      expect( Track.laneForEntity( [ [{entity:{start:10, end:19}}] ], {start: 20, end: 30} )).toEqual([{entity:{start:10, end:19}}]);

      // Added on the same lane
      expect( Track.laneForEntity( [ [{entity:{start:10, end:19}}] ], {start: 20, end: 30}, 0 )).toEqual([{entity:{start:10, end:19}}]);

      // Added to a new lane
      expect( Track.laneForEntity( [ [{entity:{start:10, end:19}}] ], {start: 20, end: 30}, 2 )).toEqual([]);
    });

    it('createLanes with only real entities', function(){

      var lanes = Track.createLanes( { 'e1': {start:0, end:10}, 'e2': {start:5, end:15}, 'e3': {start:11, end: 18} } );
      expect(_.size(lanes)).toBe(2);
      expect( lanes ).toEqual([
        [ { entity: { start: 0, end: 10 }, entityId: 'e1' }, { entity: { start: 11, end: 18 }, entityId: 'e3' } ],
        [ { entity: { start: 5, end: 15 }, entityId: 'e2' } ]
      ]);

    });

    it('createLanes inserts based on the order of ends', function(){

      var lanes = Track.createLanes( { 'e1': {start:0, end:10}, 'e2': {start:20, end:25}, 'e3': {start:12, end: 18} } );
      expect(_.size(lanes)).toBe(1);
      expect( lanes ).toEqual([[
        { entity: { start: 0, end: 10 }, entityId: 'e1' },
        { entity: { start: 12, end: 18 }, entityId: 'e3' } ,
        { entity: { start: 20, end: 25 }, entityId: 'e2' }
      ]]);

    });

    it('createLanes with only virtual entities', function(){

      var lanes = Track.createLanes( { 'e1':{}, 'e2':{}, 'e3':{} } );
      expect(_.size(lanes)).toBe(3);
      expect( lanes ).toEqual([
        [ { entity: {}, entityId: 'e1' } ],
        [ { entity: {}, entityId: 'e2' } ],
        [ { entity: {}, entityId: 'e3' } ]
      ]);
    });

    it('createLanes with real and virtual entities', function(){

      var lanes = Track.createLanes( { 'e1': {start:0, end:10}, 'e2': {start:20, end:25}, 'e3v': {} } );
      expect(_.size(lanes)).toBe(2);
      expect( lanes ).toEqual([
        [{ entity: { start: 0, end: 10 }, entityId: 'e1' },{ entity: { start: 20, end: 25 }, entityId: 'e2' }],
        [{ entity: {}, entityId: 'e3v' }]
      ]);
    });

    it('createLanes with entity filter', function(){
      var lanes = Track.createLanes( { 'e1': {start:0, end:10}, 'e2': {start:20, end:25}, 'e3v': {} }, function(e){return e.start > 0;} );
      expect(_.size(lanes)).toBe(1);
      expect( lanes ).toEqual([
        [{ entity: { start: 20, end: 25 }, entityId: 'e2' }]
      ]);
    });


    it('createGroups', function(){

      expect( Track.createGroups() ).toEqual( {} );
      expect( Track.createGroups( {'e1':{}} ) ).toEqual( { undefined: {'e1':{}} } );
      expect( Track.createGroups( {'e1':{}}, {field: 'strand'}) ).toEqual( { undefined: {'e1':{}} } );
      expect( Track.createGroups( {
        'e1': { data: {strand:'+'} },
        'e2': { data: {strand:'+'} },
        'e3': { data: {strand:'-'} },
        'e4': { data: {} },
        'e5': { }
      }, {field: 'strand'})).toEqual({
        '+':{
         'e1': { data: { strand: '+' } },
         'e2': { data: { strand: '+' } }
        },
        '-': {
          'e3': { data: { strand: '-' } }
        },
        undefined: {
          'e4': { data: {  } },
          'e5': {}
        }
      });

    });

    it('createGroupedLanes', function(){

      expect( Track.createGroupedLanes() ).toEqual( [] );
      expect( Track.createGroupedLanes( {'e1':{}} ) ).toEqual( [{ conf: undefined, name:'undefined', lanes: [[{ entity: {}, entityId: 'e1' }]]}] );
      expect( Track.createGroupedLanes( {'e1':{}}, undefined, {field:'strand'}) ).toEqual( [{ conf: {field:'strand'}, name:'undefined', lanes: [[{ entity: {}, entityId: 'e1' }]]}] );
      expect( Track.createGroupedLanes( {
        'e1': { data: {strand:'+'} },
        'e2': { data: {strand:'+'} },
        'e3': { data: {strand:'-'} },
        'e4': { data: {} },
        'e5': { }
      }, undefined, {field:'strand'})).toEqual([
        {
          name: '+',
          conf: {field:'strand'},
          lanes :[
            [{entity: { data: { strand: '+' } }, entityId: 'e1'}],
            [{entity: { data: { strand: '+' } }, entityId: 'e2'}]
          ]
        },
        {
          name: '-',
          conf: {field:'strand'},
          lanes: [
            [{entity: { data: { strand: '-' } }, entityId: 'e3'}]
          ]
        },
        {
          name: 'undefined',
          conf: {field:'strand'},
          lanes: [
            [{entity: { data: {  } }, entityId: 'e4'}],
            [{entity: {}, entityId: 'e5'}]
          ]
        }
      ]);

    });

    it('countLanes', function(){

      expect( Track.countLanes( [{lanes:[[],[]]}] ) ).toBe(2);
      expect( Track.countLanes( [{lanes:[]}] ) ).toBe(0);
      expect( Track.countLanes( [] ) ).toBe(0);
      expect( Track.countLanes() ).toBe(0);
      expect( Track.countLanes( [{lanes:[[],[]]}, {lanes:[[]]}] ) ).toBe(3);

    });

  });

  describe('Select entities',function(){

    it('hasSelectedEntities triggerSelectedEntity selectEntity  isSelected  getColor', function(){

      var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
      var t = new Track(undefined, undefined, {url: "someUrl"}, r);
      expect( t.hasSelectedEntities() ).toBeFalsy();
      expect( t.isSelected( 'e1') ).toBeFalsy();
      expect(t.getColor( {}, 'e1', {})).toBe( t.reference.color );

      t.selectEntity( 'e1' );
      expect( t.hasSelectedEntities() ).toBeFalsy(); // The selected id corresponds to no existing entity

      t.entities = { e1: {name:'e1'}};
      expect( t.hasSelectedEntities() ).toBeTruthy();

      expect( t.isSelected( 'e1') ).toBeTruthy();
      expect(t.getColor( {}, 'e1', {})).toBe( Track.SELECTED_ENTITY_COLOR );

      t.triggerSelectedEntity( 'e1' );
      expect( t.hasSelectedEntities() ).toBeFalsy();
      expect( t.isSelected( 'e1') ).toBeFalsy();
      expect(t.getColor( {}, 'e1', {})).toBe( t.reference.color );

      t.triggerSelectedEntity( 'e1' );
      expect( t.hasSelectedEntities() ).toBeTruthy();
      expect( t.isSelected( 'e1') ).toBeTruthy();

      expect(t.getColor( {}, 'e1', {})).toBe( Track.SELECTED_ENTITY_COLOR );
      expect(t.getColor( {}, 'e2', {})).toBe( t.reference.color );

    });

    it('getSelectedEntities', function(){

      var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
      var t = new Track(undefined, undefined, {url: "someUrl"}, r);
      expect(t.selected.entities).toEqual({});

      expect(t.getSelectedEntities()).toEqual([]);

      t.entities = { 'e1': {name:'e1'}, 'e2': { }};

      expect(t.getSelectedEntities()).toEqual([]);

      t.selectEntity('e3');

      expect(t.getSelectedEntities()).toEqual([]);

      t.selectEntity('e1');

      expect(t.getSelectedEntities()).toEqual([{name:'e1'}]);

    });


  });

  it('isVirtualEntity isRealEntity', function(){

    expect( Track.isRealEntity() ).toBeFalsy();
    expect( Track.isRealEntity( {} ) ).toBeFalsy();
    expect( Track.isRealEntity( { start:1} ) ).toBeFalsy();
    expect( Track.isRealEntity( { end:5} ) ).toBeFalsy();
    expect( Track.isRealEntity( { start:1, end:5} ) ).toBeTruthy();
    expect( Track.isVirtualEntity( { start:1, end:5} ) ).toBeFalsy();

  });


  describe('getVerticalInterval',function(){

    it('calculates vertical interval between lanes of the track', function(){

      expect( Track.getVerticalInterval( 9, 2 ) ).toEqual({entityHeight:3, separationHeight:4.5});
      expect( Track.getVerticalInterval( 9, 1 ) ).toEqual({entityHeight:6, separationHeight:9});

    });

  });

  it('_getAdjustment should calculate how many pixels to add/retrieve from coordinates to keep juxtaposing entities still juxtaposing', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.setIntervalParams({ start:10, end: 20 });
    Reference.width = 100;
    expect(t._getAdjustment()).toBe(5);

    var t2 = new Track(undefined, undefined, {url: "someUrl"}, r);
    t2.setIntervalParams({ start:0, end: 100 });
    Reference.width = 10;
    expect(t2._getAdjustment()).toBe(0);

  });


  it('toView() can deal with basic calculations and limits', function(){
    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.setIntervalParams({ start:10, end: 20});
    Reference.width = 100;
    expect(t.toView(10)).toBe(0);
    expect(t.toView(20)).toBe(100);
    expect(t.toView(30)).toBe(100); // out of bounds, setting to bounds
  });

  it('toView() can display juxtaposing elements on big intervals', function(){
    // real case
    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.setIntervalParams({ start:931776, end: 987882});
    Reference.width = 900;
    expect( _.round(t.toView(956128), 2) ).toBe(390.63); // end exon
    expect( _.round(t.toView(956129), 2) ).toBe(390.65); // start intron
  });

  it('toViewAdjusted() can display juxtaposing elements on big AND small intervals', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.setIntervalParams({ start:0, end: 10});
    Reference.width = 100;
    expect(t.toViewAdjusted( 4, 5)).toEqual({start:35, end:55}); // end exon
    expect(t.toViewAdjusted( 6, 7)).toEqual({start:55, end:75}); // start intron

    // beyond limits
    expect(t.toViewAdjusted( 0, 5)).toEqual({start:0, end:55});
    expect(t.toViewAdjusted( 4, 20)).toEqual({start:35, end:100});

    /**
     * 3 Real cases
     */
    Reference.width = 900;
    var t2 = new Track(undefined, undefined, {url: "someUrl"}, r);
    t2.setIntervalParams({ start:744195, end: 744218 });

    expect( _.round(t2.toViewAdjusted( 743959, 744207).end, 2) ).toBe(489.57); //end exon
    expect( _.round(t2.toViewAdjusted( 744208, 744303).start, 2) ).toBe(488.7); //start intron

    var t3 = new Track(undefined, undefined, {url: "someUrl"}, r);
    t3.setIntervalParams({ start:744161, end: 744252 });
    expect( _.round(t3.toViewAdjusted( 743959, 744207).end, 2) ).toBe(459.95); //end exon
    expect( _.round(t3.toViewAdjusted( 744208, 744303).start, 2) ).toBe(459.84); //start intron

    // Should be the same as toView because interval very big
    var t4 = new Track(undefined, undefined, {url: "someUrl"}, r);
    t4.setIntervalParams({ start:931776, end: 987882 });
    expect( _.round(t4.toViewAdjusted( -1,956128).end, 2) ).toBe(390.63); // end exon
    expect( _.round(t4.toViewAdjusted( 956129, 1).start, 2) ).toBe(389.15); // start intron, applyied artifrical correction

  });


  it('transform() can transform a position from one interval into another', function(){
    expect( Track.transform( 50, 100, 5, 10, 50 ) ).toBe(5);
    expect( Track.transform( 50, 100, 5, 10, 10 ) ).toBe(5);
    expect( Track.transform( 50, 100, 5, 10, 70 ) ).toBe(7);
  });

  it('transform() allways returns a value inside the new interval', function(){
    expect( Track.transform( 50, 100, 5, 10, 10 ) ).toBe(5);
    expect( Track.transform( 50, 100, 5, 10, 120 ) ).toBe(10);
  });

  it('convertSize converts a interval in b.p to Mbp, Kbp, etc...', function(){

    expect(Track.convertSize(0)).toBe('less than 1');
    expect(Track.convertSize(0.3)).toBe('less than 1');
    expect(Track.convertSize(0.7)).toBe('less than 1');
    expect(Track.convertSize(1)).toBe(1);
    expect(Track.convertSize(100)).toBe(100);
    expect(Track.convertSize(1000)).toBe('1K');
    expect(Track.convertSize(20000000)).toBe('20M');
    expect(Track.convertSize(99999999999)).toBe('100G');
    expect(Track.convertSize(9007199254740992)).toBe(9007199254740992);

  });


  it('getHeight', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);

    expect(t.getHeight()).toBe(200);     //default 200

    t.height = 500;
    expect(t.getHeight()).toBe(500);     //height can be set in track config

    t.adaptativeHeight = true;
    expect(t.getHeight()).toBe(0); // if adaptative, height depends on number of lanes
    expect(t.getHeight( 1 )).toBe( 20 ); // if adaptative, height depends on number of lanes

    expect(t.getHeight( 2 )).toBe( 40 ); // two lanes

  });


  it('autoselectEntity', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    expect(t.selected.entities).toEqual({});

    t.autoselectEntity('testEntity');
    expect(t.selected.entities).toEqual({});

    t.entities = { 'e1': { name: 'testEntity-1'},  'e2': { name: 'testEntity-2'},  'e3': { name: 'NontestEntity-1'} }
    expect(t.selected.entities).toEqual({});
    t.autoselectEntity('testEntity');
    expect(t.selected.entities).toEqual({ e1: 1, e2: 1 });

  });

  it('getColor', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.entities = {};

    expect( t.getColor() ).toBe(t.reference.color);

    t.entities = {
      'e1': { color: 'e1Color', components: {'c11': {color: 'c11Color'} } },
      'e2': { components: { 'c21' : {color:'c21Color'}}},
      'e3': { color: 'e3Color', components: {'c31':{}}},
      'e4': { color: 'e4Color'},
      'e5':{},
      'e6':undefined
    };

    expect( t.getColor( t.entities.e1.components.c11, 'e1', t.entities.e1 )).toBe('c11Color');
    expect( t.getColor( t.entities.e2.components.c21, 'e2', t.entities.e2 )).toBe('c21Color');
    expect( t.getColor( t.entities.e1.components.c31, 'e3', t.entities.e3 )).toBe('e3Color');
    expect( t.getColor( undefined, 'e4', t.entities.e4 )).toBe('e4Color');
    expect( t.getColor( undefined, 'e5', t.entities.e5 )).toBe(t.reference.color);
    expect( t.getColor( undefined, 'e6', t.entities.e6 )).toBe(t.reference.color);

  });

  it('getSelectedEntityColor', function(){

    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.entities = {};

    expect(t.getSelectedEntityColor()).toBeUndefined();

    t.entities = { 'e1' : {}, 'e2': {} };

    expect(t.getSelectedEntityColor()).toBeUndefined();

    t.selectEntity('e1');
    expect(t.getSelectedEntityColor()).toBeUndefined();
    expect(t.getSelectedEntityColor('e2')).toBeUndefined();
    expect(t.getSelectedEntityColor('e1')).toBe(Track.SELECTED_ENTITY_COLOR);

  });

  it('getEntityOverviewColor', function(){
    var r = new Reference('myRefId', 'myRefType', {}, {"url":"/api/reference/genome"});
    var t = new Track(undefined, undefined, {url: "someUrl"}, r);
    t.entities = {};

    expect(t.getEntityOverviewColor()).toBeUndefined();
  });



});
