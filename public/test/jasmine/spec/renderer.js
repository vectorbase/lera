
  describe('Renderer',function(){

    beforeEach(module("lera"));

    var Renderer;
    beforeEach(inject(function($injector){
        Renderer = $injector.get('Renderer');

    }));


    it('calculateGroupRect', function(){

      expect( Renderer.calculateGroupRect( {}, 5, 10, 100 ) ).toBeUndefined();
      expect( Renderer.calculateGroupRect( { name: 'v1', conf: { colors:{ 'v1':'color1'}} }, 5, 10, 100 ) ).toBeUndefined();
      expect( Renderer.calculateGroupRect( { name: 'v1', lanes: [[]], conf: { colors:{ 'v1':'color1'}} }, 5, 10, 100 ) )
        .toEqual( {x:0, y:0, w:100, h:10, c:'color1'} );
      expect( Renderer.calculateGroupRect( { name: 'v1', lanes: [[], []], conf: { colors:{ 'v1':'color1'}} }, 5, 10, 100 ) )
        .toEqual( {x:0, y:0, w:100, h:20, c:'color1'} );
      expect( Renderer.calculateGroupRect( { name: 'v1', lanes: [[], []], conf: { colors:{ 'v2':'color2'}} }, 5, 10, 100 ) )
        .toBeUndefined();

    });


  });
