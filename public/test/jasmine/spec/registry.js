
  describe('Registry',function(){

    beforeEach(module("lera"));

    var r, Registry, $httpBackend;
    beforeEach(inject(function($injector){
        Registry = $injector.get('Registry');
        $httpBackend = $injector.get('$httpBackend');

        r = new Registry('registry_url');
        $httpBackend
          .expectGET(/registry_url.*/)
          .respond({
            "references": {

              "ensGenome": {
                "url":"/api/reference/genome",
                "data":{
                  "species" : {"required":true, "urlTemplate": "/api/reference/genome/autocomplete/species?text={{text}}"},
                  "seq_region_name" : {"required":false, "urlTemplate": "/api/reference/genome/autocomplete/seq_region_name?text={{text}}"}
                }
              },
              "superRef":{

              },
              "ensTranscriptome":{

              },
            },
            "tracks": {

              "ensTranscript": {
                "name": "transcript",
                "description": "Transcripts from vectorbase",
                "supportedReferences":["ensGenome", "ensTranscriptome"],
                "url":"/api/track/transcript",
                "entityLabelPosition":"entity",
                "morphsTo":"ensProtein",
                "entity_threshold":40
              }
            }
          });

    }));

    afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
    });

    it('Async create new registry and test getTrackConf() getReferenceConf() getTrackTypes() ', function(){

      r.load(function(){

        expect (r.data ).not.toBeUndefined();
        expect( r.getTrackConf('ensTranscript') ).not.toBeUndefined();
        expect( r.getReferenceConf('ensGenome') ).not.toBeUndefined();

        expect( r.getTrackTypes() ).not.toBeUndefined();
        expect( r.getTrackTypes().length ).toBe(1);
        expect( r.getTrackTypes() ).toContain('ensTranscript');

        expect( r.getReferenceTypes() ).not.toBeUndefined();
        expect( _.size(r.getReferenceTypes())).toBe(3);
        expect( _.keys(r.getReferenceTypes()) ).toContain('ensGenome');
        expect( _.keys(r.getReferenceTypes()) ).toContain('ensTranscriptome');

      });

      $httpBackend.flush();

    });

    it('trackSupportsReference', function(){
      r.load();
      $httpBackend.flush();

      var tc = r.getTrackConf('ensTranscript');
      expect(r.trackSupportsReference(tc, 'ensGenome')).toBeTruthy();
      expect(r.trackSupportsReference(tc, 'invalidRef')).toBeFalsy();
      expect(r.trackSupportsReference(tc, 'superRef')).toBeFalsy();
    });

    it('getTrackTypesForRefType', function(){
      r.load();
      $httpBackend.flush();
      expect(r.getTrackTypesForRefType('ensGenome')['ensTranscript']).not.toBeUndefined();
      expect(r.getTrackTypesForRefType('ensTranscriptome')['ensTranscript']).not.toBeUndefined();
      expect(r.getTrackTypesForRefType('ensGenome')['superRef']).toBeUndefined();
      expect(r.getTrackTypesForRefType('ensGenome')['invalidRef']).toBeUndefined();


    });

  });
