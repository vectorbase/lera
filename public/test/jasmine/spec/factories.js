describe("Factories", function() {

  beforeEach(module("lera"));

  var log;

  beforeEach(function() {
        inject(function (_$log_) {
            log = _$log_;
        });
    });

  afterEach(function () {
        _.each(['debug', 'warn', 'error', 'info'], function(lt){
          _.each(log[lt].logs, function(l){
            _.each(l, function(lc){
                console.log(lc);
            });
          });
        });
  });

});
