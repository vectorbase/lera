
describe('ContextMenuState',function(){

  beforeEach(module("lera"));

  var ContextMenuState, Track;
  beforeEach(inject(function($injector){
      ContextMenuState = $injector.get('ContextMenuState');
      Track = $injector.get('Track');
  }));

  it('_processValue', function(){

    expect(ContextMenuState._processValue('test')).toBe("test");
    expect(ContextMenuState._processValue('test_another')).toBe("test another");
    expect(ContextMenuState._processValue(9)).toBe(9);

  });

  it('processProperties basic transformations', function(){

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" } ) )
    .toEqual( [ { field : "a", value : "va"}, { field : "b", value : "vb"}, { field : "c", value : "vc"} ] );

    expect( ContextMenuState._getProcessedProperties() ).toEqual( [] );
    expect( ContextMenuState._getProcessedProperties( {} ) ).toEqual( [] );

    expect( ContextMenuState._getProcessedProperties( { a : undefined } ) ).toEqual( [{field:"a", value:undefined}] );
    expect( ContextMenuState._getProcessedProperties( { a : "protein_coding" } ) ).toEqual( [{field:"a", value:"protein coding"}] );
    expect( ContextMenuState._getProcessedProperties( { a : "non_protein_coding" } ) ).toEqual( [{field:"a", value:"non protein coding"}] );

    expect( ContextMenuState._getProcessedProperties( { a : ['B', 'C'] } ) ).toEqual( [{field:"a", value:"B, C"}] );
    expect( ContextMenuState._getProcessedProperties( { a : [] } ) ).toEqual( [{field:"a", value:""}] );
    expect( ContextMenuState._getProcessedProperties( { a : [undefined] } ) ).toEqual( [{field:"a", value:""}] );

    expect( ContextMenuState._getProcessedProperties( { a : {b:"bv", c: "cv"} } ) ).toEqual( [{field:"a", value:"b : bv, c : cv"}] );
    expect( ContextMenuState._getProcessedProperties( { a : {} } ) ).toEqual( [{field:"a", value:""}] );
    expect( ContextMenuState._getProcessedProperties( { a : {b: undefined} } ) ).toEqual( [{field:"a", value:"b : undefined"}] );

    expect( ContextMenuState._getProcessedProperties( { a : 15 } ) ).toEqual( [{field:"a", value:15}] );
    expect( ContextMenuState._getProcessedProperties( { a_b : 15 } ) ).toEqual( [{field:"a b", value:15}] );

  });

  it('processProperties using contextmenu declared in track configuration', function(){

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, [] ) )
    .toEqual( [ { field : "a", value : "va"}, { field : "b", value : "vb"}, { field : "c", value : "vc"} ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, {} ) )
    .toEqual( [ { field : "a", value : "va"}, { field : "b", value : "vb"}, { field : "c", value : "vc"} ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, { fields: {} } ) )
    .toEqual( [ ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, { fields: [] } ) )
    .toEqual( [ ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, { fields: [{key:"b"}] } ) )
    .toEqual( [ { field: 'b', value: 'vb' } ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, { fields: [{key:"b", name: "B"}] } ) )
    .toEqual( [ { field: 'B', value: 'vb' } ] );

    expect( ContextMenuState._getProcessedProperties( { b : "vb", a : "va", c : "vc" }, { fields: [{key:"b"}, {key: 'a'}] } ) )
    .toEqual( [ { field: 'b', value: 'vb' }, { field: 'a', value: 'va' } ] );

    expect( ContextMenuState._getProcessedProperties( {}, { fields: [{key:"b"}, {key: 'a'}] } ) )
    .toEqual( [ ] );

    expect( ContextMenuState._getProcessedProperties( undefined, { fields: [{key:"b"}, {key: 'a'}] } ) )
    .toEqual( [ ] );

  });

  it('_getMorphTargets() returns morphing targets corresponsing to specified conditions', function(){

    expect( ContextMenuState._getMorphTargets() ).toEqual([]);
    expect( ContextMenuState._getMorphTargets(undefined, {}) ).toEqual([]);
    expect( ContextMenuState._getMorphTargets({}) ).toEqual([]);

    expect(

      ContextMenuState._getMorphTargets( [
      { name: "Compatible with all", trackType: "universal" },
      { name: "Protein", trackType: "ensProtein", conditions: { biotype: "protein_coding" } },
      { name: "Protein", trackType: "ensProtein", conditions: { biotype: "tRNA", strand: "+" } },
      { name: "TrackTwoConditions", trackType: "twoConditions", conditions: { biotype: "protein_coding", strand: "+" } }
    ], { biotype: "protein_coding", strand: "+", source: "Ensembl" } ) ).toEqual(

      [
        { name: "Compatible with all", trackType: "universal" },
        { name: "Protein", trackType: "ensProtein", conditions: { biotype: "protein_coding" } },
        { name: "TrackTwoConditions", trackType: "twoConditions", conditions: { biotype: "protein_coding", strand: "+" } }
      ]
    );

  });

  it('_correctPosition', function(){

    ContextMenuState.menu = { left: 500, top: 500 };

    ContextMenuState._correctPosition(100, 100, 700, 700);
    expect(ContextMenuState.menu).toEqual({ left: 500, top: 500 });

    ContextMenuState._correctPosition(100, 100, 550, 550);
    expect(ContextMenuState.menu).toEqual({ left: 430, top: 430 });

    ContextMenuState.menu = { left: 500, top: 500 };

    ContextMenuState._correctPosition(100, 100, 550, 700);
    expect(ContextMenuState.menu).toEqual({ left: 430, top: 500 });

    ContextMenuState.menu = { left: 500, top: 500 };

    ContextMenuState._correctPosition(100, 100, 700, 550);
    expect(ContextMenuState.menu).toEqual({ left: 500, top: 430 });

  });

  it('getters', function(){

    var track = new Track( 'id', 'type', {url:"ttt"}, {} );
    var c1 = {};
    var e1 = { components: { c1:c1, c2:{} } };
    track.entities = { e1: e1 , e2:{} };

    expect(ContextMenuState.isDisplayable()).toBeFalsy();
    expect(ContextMenuState.getEntity()).toBeUndefined();
    expect(ContextMenuState.getComponent()).toBeUndefined();
    expect(ContextMenuState.getTrack()).toBeUndefined();
    expect(ContextMenuState.getEntityId()).toBeUndefined();
    expect(ContextMenuState.getEntityProperties()).toBeUndefined();
    expect(ContextMenuState.getMorphTargets()).toBeUndefined();
    expect(ContextMenuState.getComponentId()).toBeUndefined();
    expect(ContextMenuState.getComponentProperties()).toBeUndefined();
    expect(ContextMenuState.getTitle()).toBeUndefined();
    expect(ContextMenuState.getLeftPos()).toBeUndefined();
    expect(ContextMenuState.getTopPos()).toBeUndefined();

    ContextMenuState._setMenu( { pageX:10, pageY:20 }, track, 'e1', 'c1' );
    expect(ContextMenuState.isDisplayable()).toBeTruthy();
    expect(ContextMenuState.getEntity()).toBe(e1);
    expect(ContextMenuState.getComponent()).toBe(c1);
    expect(ContextMenuState.getTrack()).toBe(track);
    expect(ContextMenuState.getEntityId()).toBe('e1');
    expect(ContextMenuState.getEntityProperties()).toEqual([]);
    expect(ContextMenuState.getMorphTargets()).toEqual([]);
    expect(ContextMenuState.getComponentId()).toBe('c1');
    expect(ContextMenuState.getTitle()).toBeUndefined();
    expect(ContextMenuState.getComponentProperties()).toEqual([]);
    expect(ContextMenuState.getLeftPos()).toBe(10);
    expect(ContextMenuState.getTopPos()).toBe(20);

    ContextMenuState.resetContextMenu();

    expect(ContextMenuState.isDisplayable()).toBeFalsy();
  });

});
