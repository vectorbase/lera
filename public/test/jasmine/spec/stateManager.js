describe('StateManager',function(){

  beforeEach(module("lera"));

  /*
  var log;
  beforeEach(function() { inject(function (_$log_) { log = _$log_; }); });
  afterEach(function () {  _.each(['debug', 'warn', 'error', 'info'], function(lt){
          _.each(log[lt].logs, function(l){ _.each(l, function(lc){ console.log(lc); }); }); });
  });
  */

  var $httpBackend, StateManager, Track;
  beforeEach(inject(function($injector){
      StateManager = $injector.get('StateManager');
      $httpBackend = $injector.get('$httpBackend');
      Track = $injector.get('Track');

      StateManager.registry.data = {
        "references": {

          "ensGenome": {
            "url":"/api/reference/genome",
            "name":"genome",
            "defaultTracks":["ensTranscript"]
          },
          "ensProtein":{
            "name":"protein",
            "url": "/api/reference/protein"
          }
        },
        "tracks": {

          "ensTranscript": {
            "name": "transcript",
            "description": "Transcripts from vectorbase",
            "supportedReferences":["ensGenome"],
            "url":"/api/track/transcript"
          },

          "ensProtein": {
            "name": "protein",
            "supportedReferences":["ensProtein"],
            "url":"/api/track/protein",
          },

          "ensGene": {
            "name": "gene",
            "supportedReferences":["ensGenome"],
            "url":"/api/track/gene"
          },

          "genotype": {
            "name": "genotype",
            "supportedReferences":["ensGenome"],
            "url":"/api/track/genotype",
            "morphing": {
              "replacement" : {
                "ensProtein" : "peptype"
              }
            }
          },

          "peptype": {
            "name": "peptype",
            "supportedReferences":["ensProtein"],
            "url":"/api/track/peptype"
          }
        }
      }

  }));

  it('initialize', function(){

    $httpBackend
      .expectGET(/registry\.json.*/)
      .respond({});

    var callbackExecuted = false;
    StateManager.initialize( function(){ callbackExecuted = true; });
    $httpBackend.flush();

    expect(callbackExecuted).toBeTruthy();

  });

  it('addTrack() and getAllTracks() and removeTrack()', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?end=20&start=10')
      .respond({start:10, end:20}); //server does not resolve the track, initial data is maintained

    expect(StateManager.isStandBy()).toBeTruthy();

    var ref = StateManager.addReference('ensGenome', {start:10, end:20});

    expect(StateManager.isStandBy()).toBeFalsy();
    expect(StateManager.allReferencesResolved()).toBeFalsy();

    $httpBackend.flush(); // reference resolve

    expect(StateManager.isStandBy()).toBeTruthy();
    expect(StateManager.allReferencesResolved()).toBeTruthy();
    expect(ref.isEmpty()).toBeTruthy();

    $httpBackend
      .expect('POST', '/api/track/transcript', function(o){
                expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                return true;
              })
      .respond({entities:{e1:{c1:{}, c2:{}}}, meta:{}});

    expect(StateManager.isStandBy()).toBeTruthy();

    StateManager.addTrack(ref, 'ensTranscript');

    expect(StateManager.isStandBy()).toBeFalsy();
    expect(StateManager.allTracksUpdated()).toBeFalsy();
    $httpBackend.flush(); // update track
    expect(StateManager.isStandBy()).toBeTruthy();
    expect(StateManager.allTracksUpdated()).toBeTruthy();

    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(1);

    expect(StateManager.getAllTracks().length).toBe(1);
    expect(StateManager.getAllTracks()[0].name).toBe('transcript');
    expect(ref.isEmpty()).toBeFalsy();

    expect(StateManager.addTrack()).toBeFalsy();

    var trackId = ref.getTracks()[0].trackId;
    ref.removeTrack('wrongId');
    expect(StateManager.getTracksCount()).toBe(1);
    ref.removeTrack(trackId);
    expect(StateManager.getTracksCount()).toBe(0);

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();

  });

  it('addTrack() with additional config object', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?end=20&start=10')
      .respond({start:10, end:20}); //server does not resolve the track, initial data is maintained
    var ref = StateManager.addReference('ensGenome', {start:10, end:20});
    $httpBackend.flush(); // reference resolve
    $httpBackend
      .expect('POST', '/api/track/transcript', function(o){
                expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                return true;
              })
      .respond({entities:{e1:{c1:{}, c2:{}}}, meta:{}});

    var myTrack;
    StateManager.addTrack(ref, 'ensTranscript', undefined, function(track){ myTrack = track; }, { customOption: true });
    $httpBackend.flush(); // update track

    expect( myTrack ).not.toBeUndefined();
    expect( myTrack.customOption ).toBeTruthy();

    // The track configuration in the registry is not affected
    expect( StateManager.getRegistry().getTrackConf('ensTranscript').customOption ).toBeUndefined();


  });

  it('rerenderAllTracks', function(){

  });

  it('addReference() removeReference() notEmpty() isEmpty()', function(){
    $httpBackend
      .expect('GET', '/api/reference/genome?gene_symbol=superGene')
      .respond({start:10, end:20});

    expect(StateManager.getReferencesCount()).toBe(0);
    expect(StateManager.getTracksCount()).toBe(0);

    var ref = StateManager.addReference('ensGenome', {'gene_symbol':'superGene'});

    // BEFORE RESOLVE
    expect( ref.getIntervalParam('gene_symbol') ).toBe('superGene');
    expect(StateManager.getReferencesCount()).toBe(1);

    $httpBackend.flush(); // reference resolve

    // AFTER RESOLVE
    expect(StateManager.getReferencesCount()).toBe(1);
    var myRef = _.values(StateManager.state.references)[0];
    expect(myRef.getIntervalParam('gene_symbol')).toBeUndefined();
    expect(myRef.getIntervalParam('start')).toBe(10);
    expect(myRef.getIntervalParam('end')).toBe(20);

    expect(StateManager.notEmpty()).toBeTruthy();
    expect(StateManager.isEmpty()).toBeFalsy();

    // REMOVING

    StateManager.removeReference( myRef.refId );
    expect(StateManager.getReferencesCount()).toBe(0);
    expect(StateManager.getTracksCount()).toBe(0);

    expect(StateManager.notEmpty()).toBeFalsy();
    expect(StateManager.isEmpty()).toBeTruthy();

    StateManager.removeReference( myRef.refId );

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('addReferenceWithDefaults()', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?gene_symbol=superGene')
      .respond({start:10, end:20});

    expect(StateManager.getReferencesCount()).toBe(0);
    expect(StateManager.getTracksCount()).toBe(0);

    var ref = StateManager.addReferenceWithDefaults('ensGenome', {'gene_symbol':'superGene'});

    // BEFORE RESOLVE
    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(0);
    $httpBackend.flush(); // reference resolve

    // AFTER RESOLVE
    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(1);

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();

  });

  it('getActiveReference() getActiveTrack() setActiveTrack() resetActiveTrack()', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?gene_symbol=superGene')
      .respond({start:10, end:20});

    expect(StateManager.getActiveTrack()).toBeUndefined();
    expect(StateManager.getActiveReference()).toBeUndefined();

    var ref = StateManager.addReferenceWithDefaults('ensGenome', {'gene_symbol':'superGene'});
    $httpBackend.flush(); // reference resolve

    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(1);

    var ref1 = _.values(StateManager.state.references)[0];
    var track1 = _.values(ref1.tracks)[0];

    // When track added it becomes actif automatically
    expect(StateManager.getActiveTrack()).toBe(track1);
    expect(StateManager.getActiveReference()).toBe(ref1);

    StateManager.resetActiveTrack({});
    expect(StateManager.getActiveTrack()).toBe(track1);

    StateManager.resetActiveTrack(track1);
    expect(StateManager.getActiveTrack()).toBeUndefined();
    expect(StateManager.getActiveReference()).toBeUndefined();

    StateManager.setActiveTrack( track1 );
    expect(StateManager.getActiveTrack()).toBe(track1);
    expect(StateManager.getActiveReference()).toBe(ref1);

    StateManager.resetActiveTrack();
    expect(StateManager.getActiveTrack()).toBeUndefined();
    expect(StateManager.getActiveReference()).toBeUndefined();

  });

  it('addReference() is added then removed if server error', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?gene_symbol=superGene')
      .respond(404);

    expect(StateManager.getReferencesCount()).toBe(0);
    expect(StateManager.getTracksCount()).toBe(0);

    var ref = StateManager.addReference('ensGenome', {'gene_symbol':'superGene'});

    // BEFORE RESOLVE
    expect( ref.getIntervalParam('gene_symbol') ).toBe('superGene');
    expect(StateManager.getReferencesCount()).toBe(1);

    $httpBackend.flush(); // reference resolve

    // AFTER RESOLVE
    expect(StateManager.getReferencesCount()).toBe(0); // reference removed after server error
    var myRef = _.values(StateManager.state.references)[0];

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();

  });

  it('getReferencesCount()', function(){

    expect( StateManager.getReference(1) ).toBeUndefined();
    expect( StateManager.getTrack(1) ).toBeUndefined();
    expect( StateManager.getTrack(1, 1) ).toBeUndefined();

    StateManager.state.references['ref1'] = {};
    StateManager.state.references['ref2'] = {};
    expect(StateManager.getReferencesCount()).toBe(2);

    expect( StateManager.getReference('ref1') ).not.toBeUndefined();
    expect( StateManager.getTrack(1, 1) ).toBeUndefined();
  });


  it('stackState() and undo()', function(){

    // PREPARATION...
    $httpBackend
      .expect('GET', '/api/reference/genome?gene_symbol=superGene')
      .respond({start:10, end:20});

    var ref = StateManager.addReference('ensGenome', {'gene_symbol':'superGene'});
    $httpBackend.flush(); // reference resolve

    $httpBackend
      .expect('POST', '/api/track/transcript', function(o){
                expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                return true;
              })
      .respond({entities:{e1:{c1:{}, c2:{}}}, meta:{}});

    StateManager.addTrack(ref, 'ensTranscript');
    $httpBackend.flush(); // update track

    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(1);

    // TESTING STACK STATE

    expect(StateManager.canUndo()).toBeFalsy();
    expect(StateManager.history.length).toBe(0);

    StateManager.undo(); // nothing should happen, as we can not undo yet

    expect(StateManager.canUndo()).toBeFalsy();
    expect(StateManager.history.length).toBe(0);

    StateManager.stackState();
    expect(StateManager.canUndo()).toBeTruthy();
    expect(StateManager.history.length).toBe(1);
    expect(StateManager.getLastState()).toBe('[{"type":"ensGenome","data":{"start":10,"end":20},"color":"#006064","tracks":[{"type":"ensTranscript","filters":{"categories":{},"entities":[]}}]}]');

    StateManager.state.references = {};

    expect(StateManager.history.length).toBe(1);

    expect(StateManager.getReferencesCount()).toBe(0);
    expect(StateManager.getTracksCount()).toBe(0);

    console.log(JSON.parse(StateManager.history[0]));

    $httpBackend
      .expect('GET', '/api/reference/genome?end=20&start=10')
      .respond({start:10, end:20});

    // TESTING UNDO
    StateManager.undo();
    $httpBackend.flush(); // resolve ref and update track

    expect(StateManager.canUndo()).toBeFalsy();
    expect(StateManager.history.length).toBe(0);
    expect(StateManager.getReferencesCount()).toBe(1);
    expect(StateManager.getTracksCount()).toBe(1);

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });



  it('serialize()', function(){

    $httpBackend
      .expect('GET', '/api/reference/genome?g=1')
      .respond({start:10, end:20});

    StateManager.addReference('ensGenome', {g:1}, function( ref ){
      StateManager.addTrack(ref, 'ensTranscript', {categories:{'filter_cat1':{'f1':1}}}, function(){
      });
    });

    $httpBackend.flush();

    var serialized = StateManager.serialize();
    expect(serialized).toBe('[{"type":"ensGenome","data":{"start":10,"end":20},"color":"#006064","tracks":[{"type":"ensTranscript","filters":{"categories":{"filter_cat1":{"f1":1}},"entities":[]}}]}]');

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  describe('deserialize()',function(){

    it('Deserialize one ref with one track', function(){

      $httpBackend
        .expect('GET', '/api/reference/genome')
        .respond({start:10, end:20});

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                  return true;
                })
                .respond({ entities: { e_41_42: { start:41, end:42}, e_31_32: {start: 31, end: 32} }} );


      var string = '[{"id":"myRef","type":"ensGenome","data":{},"tracks":[{"id":"myTrack","type":"ensTranscript","filters":{"categories":{"filter_cat1":["f1"]}}}]}]';

      expect(StateManager.getReferencesCount()).toBe(0);
      expect(StateManager.getTracksCount()).toBe(0);

      StateManager.deserialize(string);
      $httpBackend.flush();

      expect(StateManager.getReferencesCount()).toBe(1);
      expect(StateManager.getTracksCount()).toBe(1);

      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });


  });

  it('getAvailableReferencesForTrackType() returns all references that are compatible with the given track type', function(){

    StateManager.addReference('ensGenome', {});
    StateManager.addReference('ensProtein', {});

    expect(_.size(StateManager.state.references)).toBe(2);
    var compRef = StateManager.getAvailableReferencesForTrackType('ensTranscript');
    expect( compRef ).not.toBeUndefined();
    expect( compRef.length ).toBe(1);
    expect( compRef[0].refType ).toBe('ensGenome');

    var badCompRef = StateManager.getAvailableReferencesForTrackType('test');
    expect( badCompRef ).toEqual([]);
    var badCompRef2 = StateManager.getAvailableReferencesForTrackType();
    expect( badCompRef2 ).toEqual([]);

  });

  describe('morphToEntity()',function(){

    beforeEach(function(){

      // PREPARATION...
      $httpBackend
        .expect('GET', '/api/reference/genome?gene_symbol=superGene')
        .respond({start:10, end:20});

      var ref = StateManager.addReference('ensGenome', {'gene_symbol':'superGene'});
      $httpBackend.flush(); // reference resolve

      $httpBackend
        .expect('POST', '/api/track/transcript', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                  return true;
                })
        .respond({entities:{e1:{c1:{}, c2:{}}}, meta:{}});

      StateManager.addTrack(ref, 'ensTranscript');
      $httpBackend.flush(); // update track

    });

    it('just adding a track', function(){

      $httpBackend
        .expect('POST', '/api/track/gene', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                  return true;
                })
        .respond({entities:{eg1:{cg1:{}, cg2:{}}}, meta:{}});

      expect(StateManager.getReferencesCount()).toBe(1);
      expect(StateManager.getTracksCount()).toBe(1);
      var track = StateManager.getAllTracks()[0];

      // Gene has same reference as transcript, so no need to add new ref
      StateManager.morphToEntity( track, 'e1', 'ensGene' );

      $httpBackend.flush();

      expect(StateManager.getReferencesCount()).toBe(1);
      expect(StateManager.getTracksCount()).toBe(2);

      expect( _.map( StateManager.getReferences(), 'name' ) ).toEqual(['genome']);
      expect( _.map( StateManager.getAllTracks(), 'name' ) ).toEqual(['transcript', 'gene']);

      expect( StateManager.getReference(1) ).not.toBeUndefined();
      expect( StateManager.getReference(1).refType ).toBe("ensGenome");

      expect( StateManager.getTrack(1, 1) ).not.toBeUndefined();
      expect( StateManager.getTrack(1, 1).trackType ).toBe("ensTranscript");

    });

    it('adding a new reference, morphing the selected entity', function(){

      $httpBackend
        .expect('GET', '/api/reference/protein?end=20&start=10&query=e1')
        .respond({start:10, end:20});

      $httpBackend
        .expect('POST', '/api/track/protein', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                  return true;
                })
        .respond({entities:{ep1:{cp1:{}, cp2:{}}}, meta:{}});

      expect(StateManager.getReferencesCount()).toBe(1);
      expect(StateManager.getTracksCount()).toBe(1);
      var track = StateManager.getAllTracks()[0];

      StateManager.morphToEntity( track, 'e1', 'ensProtein' );

      $httpBackend.flush();

      expect(StateManager.getReferencesCount()).toBe(2);
      expect(StateManager.getTracksCount()).toBe(2);

      expect( _.map( StateManager.getReferences(), 'name' ) ).toEqual(['genome', 'protein']);
      expect( _.map( StateManager.getAllTracks(), 'name' ) ).toEqual(['transcript', 'protein']);

    });

    it('also tranfering compatible tracks', function(){

      // First adding a second track to the genome reference
      $httpBackend
        .expect('POST', '/api/track/genotype', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":['sample1']}}});
                  return true;
                })
        .respond({entities:{sample1:{g1:{}, g2:{}}}, meta:{}});

      StateManager.addTrack( _.values(StateManager.getReferences())[0], 'genotype', { entities:['sample1']});
      $httpBackend.flush(); // update track

      expect(StateManager.getReferencesCount()).toBe(1);
      expect(StateManager.getTracksCount()).toBe(2);


      // Anf now the actual test...

      $httpBackend
        .expect('GET', '/api/reference/protein?end=20&start=10&query=e1')
        .respond({start:10, end:20});

      $httpBackend
        .expect('POST', '/api/track/protein', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":[]}}});
                  return true;
                })
        .respond({entities:{ep1:{cp1:{}, cp2:{}}}, meta:{}});

      $httpBackend
        .expect('POST', '/api/track/peptype', function(o){
                  expect(JSON.parse(o)).toEqual({"intervalParams":{"start":10,"end":20},"trackParams":{"filters":{"entities":['sample1']}}});
                  return true;
                })
        .respond({entities:{sample1:{pep1:{}, pep2:{}}}, meta:{}});

      var track = StateManager.getAllTracks()[0];

      StateManager.morphToEntity( track, 'e1', 'ensProtein' );

      $httpBackend.flush();

      expect(StateManager.getReferencesCount()).toBe(2);
      expect(StateManager.getTracksCount()).toBe(4);

      expect( _.map( StateManager.getReferences(), 'name' ) ).toEqual(['genome', 'protein']);
      expect( _.map( StateManager.getAllTracks(), 'name' ) ).toEqual(['transcript', 'genotype', 'protein', 'peptype']);

      StateManager.removeAllReferences();

      expect(StateManager.getReferencesCount()).toBe(0);
      expect(StateManager.getTracksCount()).toBe(0);

    });

  });


  it('insertAfter', function(){

        expect(StateManager.insertAfter()).toBeFalsy();
        expect(StateManager.insertAfter( [] )).toBeFalsy();
        expect(StateManager.insertAfter( [], {} )).toBeFalsy();
        expect(StateManager.insertAfter( [], undefined)).toBeFalsy();
        expect(StateManager.insertAfter( [], undefined, undefined)).toBeFalsy();
        expect(StateManager.insertAfter( undefined)).toBeFalsy();

        var a = { name: 'a', order: 1};
        var b = { name: 'b', order: 2};
        var c = { name: 'c', order: 3};
        var d = { name: 'd', order: 4};
        var f = { name: 'f', order: 5};

        var data = {a:a, f:f, b:b, d:d, c:c};

        var getOrdered = function(){
          return _.sortBy( data, 'order');
        };

        expect( getOrdered() ).toEqual([a,b,c,d,f]);

        expect(StateManager.insertAfter( _.values(data), a, f)).toBeTruthy() // end to middle
        expect( getOrdered() ).toEqual([a,f,b,c,d]);

        expect(StateManager.insertAfter( _.values(data), d, a)).toBeTruthy() // start to end
        expect( getOrdered() ).toEqual([f,b,c,d,a]);

        expect(StateManager.insertAfter( _.values(data), a, c)).toBeTruthy() // middle to end
        expect( getOrdered() ).toEqual([f,b,d,a,c]);

  });

  it('fork', function(){

  });

  it('noExportsRunning', function(){

    expect ( StateManager.noExportsRunning() ).toBeTruthy();
    Track.runningExports = 1;
    expect ( StateManager.noExportsRunning() ).toBeFalsy();
    Track.runningExports = 0;
    expect ( StateManager.noExportsRunning() ).toBeTruthy();
  });

});
