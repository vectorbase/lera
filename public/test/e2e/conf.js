
var SpecReporter = require('../../../gulp/node_modules/jasmine-spec-reporter').SpecReporter;
var reporter = new SpecReporter({
    displayStacktrace: 'none'
});


exports.config = {
    rootElement: '#ng-app',
    verbose: false,
    stackTrace: false,
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    localSeleniumStandaloneOpts: {
      args: ["--no-stackTrace", "-silent", "-log log.log"]
    },
    specs: ['spec.js'],
    multiCapabilities: [
    //  {
    //    browserName: 'firefox'
    //  },
      {
        browserName: 'chrome',
        prefs: {
            download: {
                prompt_for_download: false,
                default_directory: '/e2e/downloads/'
            },
        },
      }
  ],
  jasmineNodeOpts: {
   print: function() {},
   showTiming: true,
   showColors: true,
   isVerbose: false,
   includeStackTrace: false
  },
  onPrepare: function() {
      jasmine.getEnv().addReporter( reporter );
  }
};
