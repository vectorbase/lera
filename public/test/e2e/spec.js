var _ = require('../../../gulp/node_modules/lodash');
var request = require('../../../gulp/node_modules/sync-request');
var fs = require('fs');

var speciesSpecs = JSON.parse(fs.readFileSync(require('path').resolve(__dirname, '../../../tests.json'), 'utf8'))

var dev_url = "http://gunpowder.ebi.ac.uk:10973/lera";
var prod_url = "https://pre.vectorbase.org/lera";

var baseUrl = dev_url;
var speciesRes = request('GET', baseUrl+'/api/stats/species');
var speciesToTest = JSON.parse(speciesRes.getBody('utf8'));

console.log("Following species will be tested :");
console.log(speciesToTest);

describe('LERA', function() {

  _.each( speciesToTest, function(speciesId){

    var params = speciesSpecs[speciesId] && speciesSpecs[speciesId].e2e;

    if( !params ){
      console.warn("Ignoring species "+speciesId+" because no test data defined in tests.json for e2e tests");
      return;
    }

    // We distinguish the two as in the future we may alter the way the speciesId is displayed, by removing
    // the underscores for example
    var species_name = speciesId;
    var species_autosuggest = _.last( _.split( speciesId, '_' ) );

    describe('Full test for '+species_name, function() {

      it('0. displays wellcome message and tutorial', function(){

        browser.get(baseUrl);
        browser.driver.manage().window().maximize();

        expect(element(by.css('.wellcome-message'))).not.toBeUndefined();
      });

      it('1.1 Add new genomic reference', function() {

        element(by.css('#add-ref')).click();
        expect(element(by.css('.add-ref-dialog'))).not.toBeUndefined();

        // GENOME ref selected by default

        // species
        element.all(by.css('.add-ref-dialog input')).get(0).sendKeys( species_autosuggest );
        var firstSpeciesFound = element.all(by.css('.uib-typeahead-match .sa-identifier')).get(0);
        expect( firstSpeciesFound ).not.toBeUndefined();
        expect(firstSpeciesFound.getText()).toMatch('\s*'+species_name);
        firstSpeciesFound.click();
        expect(element.all(by.css('autosuggest .hint')).get(0).getText()).toBe("Selected: "+species_name);

        // gene/transcript
        element.all(by.css('.add-ref-dialog input')).get(1).sendKeys(params.gene_autosuggest);
        var firstGeneFound = element.all(by.css('.uib-typeahead-match .sa-identifier')).get(0);
        expect( firstGeneFound ).not.toBeUndefined();
        expect(firstGeneFound.getText()).toMatch('\s*'+params.gene_match);
        firstGeneFound.click();
        expect(element.all(by.css('autosuggest .hint')).get(1).getText()).toBe("Selected: "+params.gene_match);

        element(by.css('.confirm-add-ref')).click();
        expect(element.all(by.css('reference-card')).count()).toBe(1);
      });


      it('1.2 Browse track', function(){

        element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .browser')).click();

        expect(element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .track-start')).getText())
        .toBe(params.genome_landing.start);
        expect(element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .track-end')).getText())
        .toBe(params.genome_landing.end);

        // zoom in
        browser.actions().sendKeys("W").perform();
        browser.actions().sendKeys("W").perform();
        browser.actions().sendKeys("W").perform();
        browser.actions().sendKeys("W").perform();
        browser.actions().sendKeys("W").perform();

        // browse left
        browser.actions().sendKeys("A").perform();
        browser.actions().sendKeys("A").perform();
        browser.actions().sendKeys("A").perform();

        expect(element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .track-start')).getText())
        .not.toBe(params.genome_landing.start);
        expect(element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .track-end')).getText())
        .not.toBe(params.genome_landing.end);

      });

      it('1.3 Add samples', function() {

        // Track has status WARN now
        expect(hasClass( element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(2)')), 'warn' )).toBe(true);
        expect(hasClass( element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(2)')), 'ok' )).toBe(false);

        var addFiltersButton = element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(2) .controls .add-filters'));
        addFiltersButton.click();

        element(by.css('.edit-filters-dialog input')).sendKeys( params.samples_autosuggest );

        expect( element.all(by.css('.edit-filters-dialog .tags .tag')).count() ).toBe(0);
        // add all
        element.all(by.css('.uib-typeahead-match .sa-command')).get(0).click();
        expect( element.all(by.css('.edit-filters-dialog .tags .tag')).count() ).toBe(params.expected_samples_add_all);

        // remove a sample
        element.all(by.css('.edit-filters-dialog .tags .tag .tag-remove')).get(0).click();
        expect( element.all(by.css('.edit-filters-dialog .tags .tag')).count() ).toBe(params.expected_samples_add_all-1);

        element(by.css('.confirm-edit-filters')).click();

        // Track should have status OK now
        expect(hasClass( element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(2)')), 'ok' )).toBe(true);
        expect(hasClass( element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(2)')), 'warn' )).toBe(false);

      });

      it('1.4 Morph to protein', function() {

        element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .browser')).click();
        expect( by.css('info .morph-button') ).not.toBeUndefined();
        element(by.css('info .morph-button')).click();
        expect(element.all(by.css('reference-card')).count()).toBe(2);

      });

      it('1.5 Fork to ortholog', function() {

        browser.executeScript('window.scrollTo(0,0);');
        browser.sleep( 1000 );

        element(by.css('lera reference-card:nth-of-type(1) track-card:nth-of-type(1) .browser')).click();
        expect( by.css('info .fork-button') ).not.toBeUndefined();
        element(by.css('info .fork-button')).click();

        element(by.css('.add-fork-dialog input')).sendKeys( params.ortholog_autosuggest );
        var foundOrtholog = element.all(by.css('.uib-typeahead-match .sa-identifier')).get(0);
        foundOrtholog.click();

        element(by.css('.confirm-add-fork')).click();
        expect(element.all(by.css('reference-card')).count()).toBe(3);

      });

/*
      it('1.6 Overview', function(){

        element(by.css('lera reference-card:nth-of-type(2) track-card:nth-of-type(1) .show-overview')).click();

        var visibleOverviews = $$("overview-card .track-overview").filter(function(link) {
            return link.isDisplayed();
        });

        expect(visibleOverviews.count()).toBe(1); // Only one overview shown for acrive track

        //  expect(element.all(by.css('overview-card div highcharts')).count()).toBe( params.e2e.genotype_overview_charts_count );
        element(by.css('.close-overview')).click();

      });
*/
/** TODO
      it('1.7 Filters', function(){

      });
*/

      it('2.1 Add new proteic reference', function() {

        element(by.css('#add-ref')).click();
        expect(element(by.css('.add-ref-dialog'))).not.toBeUndefined();

        // Selecting PROTEIN ref
        element.all(by.css('.select-ref-type')).get(1).click();

        // species
        element.all(by.css('.add-ref-dialog input')).get(0).sendKeys( species_autosuggest );
        var firstSpeciesFound = element.all(by.css('.uib-typeahead-match .sa-identifier')).get(0);
        expect( firstSpeciesFound ).not.toBeUndefined();
        expect(firstSpeciesFound.getText()).toMatch('\s*'+species_name);
        firstSpeciesFound.click();
        expect(element.all(by.css('autosuggest .hint')).get(0).getText()).toBe("Selected: "+species_name);

        // transcript/protein
        element.all(by.css('.add-ref-dialog input')).get(1).sendKeys(params.protein_autosuggest);
        var firstGeneFound = element.all(by.css('.uib-typeahead-match .sa-identifier')).get(0);
        expect( firstGeneFound ).not.toBeUndefined();
        expect(firstGeneFound.getText()).toMatch('\s*'+params.protein_match);
        firstGeneFound.click();
        expect(element.all(by.css('autosuggest .hint')).get(1).getText()).toBe("Selected: "+params.protein_match);

        element(by.css('.confirm-add-ref')).click();
        expect(element.all(by.css('reference-card')).count()).toBe(4);
      });

      it('3.1 URL config : adding a reference with transcripts track', function(){

        browser.get(baseUrl+'#?conf=[{"type":"ensGenome", "data":{"species":"'+speciesId+'", "query":"'+params.gene_match+'"}, "tracks":[{"type":"ensTranscript"}]}]');
        browser.driver.manage().window().maximize();
        expect(element.all(by.css('reference-card')).count()).toBe(1);
        expect(element.all(by.css('reference-card track-card')).count()).toBe(1);

      });

      describe('Export VCF', function() {


      });

      describe('UNDO', function() {


      });


      describe('LOAD / SAVE', function() {


      });



    });
  });
});


// HELPERS

// From https://stackoverflow.com/questions/20268128/how-to-test-if-an-element-has-class-using-protractor
var hasClass = function (element, cls) {
    return element.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
    });
};
