(function() {
  'use strict';

  // The gulp task gulp-angular-templatecache will concatenate and add to this module all html templates from templates folder
  angular.module('templates', []);

  var lera = angular.module('lera', ['templates', 'toastr', 'ngMaterial', 'ngSanitize', 'ngFileUpload', 'ui.bootstrap']);

  // Configuration
  lera.value('config', {

    track : {
      view: {}
    }

  });

  lera.config(["$httpProvider", "$logProvider", "toastrConfig", "$provide", "$locationProvider", "$animateProvider", function( $httpProvider, $logProvider, toastrConfig, $provide, $locationProvider, $animateProvider) {

    // Disable debug console messagef for production
    $logProvider.debugEnabled(true);

    //$httpProvider.useApplyAsync(true);

    $locationProvider.hashPrefix('');

    //$provide.constant('$MD_THEME_CSS', '/**/');

    angular.extend(toastrConfig, { positionClass: 'toast-bottom-right' });

    /**
     * Removing animations by default to improve performance
     */
    $animateProvider.classNameFilter(/ng-animate-enabled/);

    $provide.decorator("$exceptionHandler", ["$delegate", function( $delegate ) {
      return function(exception, cause) {

        // Original behaviour, logging to console
        $delegate(exception, cause);

        // TODO Send eror to server LOG
      };
    }]);



  }]);

})();


/**
* @namespace lera
*/

/**
 * @namespace factories
 * @memberOf lera
 */

/**
* @namespace directives
* @memberOf lera
*/

/**
 * @namespace controllers
 * @memberOf lera
 */

 /**
  * @namespace services
  * @memberOf lera
  */

(function() {
  'use strict';

  angular.module('lera').directive('autosuggest', Autosuggest);

  /**
   * @param {object} field Declared in registry, contains all necessary information to retrieve data from the user
   * for a specific params field
   * @param {string} value Value for the params field that we want to autocomplete
   * @param {list} values (optional) Ensembl of values selected by the user. If this list is declared, when a user selects
   * a value, it is added to values
   * @param {object} params: already filled parameters for this interval that can be used as context/filter to
   * retrieve autocomplete suggestions. For example 'species' can be used to filter transcript or sample suggestions
   * @param {boolean} forceObjectValue Instead of setting the value to the object identifier, it is set to the whole object.
   * The object identifier is still displayed in the input
   * @param {boolean} showExample Display some example hits on focus, by setting typeahead-min-length to 0
   * @todo 'unselect/reset' button to remove selected value
   */
  function Autosuggest(){
    return {
        restrict: 'E',
        templateUrl: 'autosuggest/autosuggest.template.html',
        scope: {
          field: '=',
          value: '=',
          values: '=',
          params: '=',
          forceObjectValue: '@',
          showExample: '@'
        },
        controller:["$scope", "toastr", "$interpolate", "$http", "$log", function($scope, toastr, $interpolate, $http, $log){

          /**
           * @desc Suggestions are shown when at least one character is typed by the user, except if
           * showExample is set to true, then some example hits are shown on input focus
           */
          $scope.triggerLength = $scope.showExample ? 0 : 1;

          /**
           * @desc If an url is declared for the autocomplete of this data attribute, we use it for autocomplete.
           * If not we return an empty list.
           * @param {string} text
           * @return {array} suggestions
           */
          $scope.getSuggestions = function( text ){

            if( $scope.field && $scope.field.urlTemplate ){
              $scope.url = $interpolate( $scope.field.urlTemplate );
              text = text && encodeURIComponent(text);

              return $http.post(
                $scope.url({text:text, params: $scope.params }),
                { exclude: $scope.values }
              ).then(
                function( res ){

                  // In case of multivalued, we store the suggestions to have access outside typeahead directive scope
                  if ( $scope.values ){
                    $scope.suggestions = _.map(res.data, function($item){return $scope.valueFromItem($item);});
                    if( res.data ){ res.data.unshift({command: "add_all", commandName: "Add top 10 hits"}); }
                  }

                  return res.data;
                },
                function( res ){
                  var message = (res.data && res.data.message) || "Can not reach server";
                  $log.error("Could not reach autocomplete endpoint for "+$scope.field.name+" : "+message);
                  toastr.warning(message, "Autocomplete for "+$scope.field.name);
                  return [];
                }
              );

            } else{
              $log.debug("Will not autocomplete field "+($scope.field && $scope.field.name)+" as it has no urlTemplate declared");
              return [];
            }
          };

          /**
           * @return {boolean} True only if there is an autocomplete URL declared for this field
           */
          $scope.hasAutocomplete = function(){
            return !!($scope.url);
          };


          /**
           * @return {string} The text message explaining the currnt status of the autosuggest
           * @todo Allways show selected ? Even with no resultsw ??
           */
          $scope.getHint = function(){
            if( $scope.isLoading ){
              return 'Searching...';
            } else if( $scope.field && $scope.noResults ){
              return '<div class="hint-bad">No results...</div>';
            } else if( $scope.value ){
              return '<div class="hint-selected">Selected: '+ ($scope.value.identifier || $scope.value)+'</div>';
            } else if( $scope.field && $scope.field.legend ){
              return $scope.field.legend;
            } else {
              return 'Suggestions not available for this field';
            }
          };


          /**
           * @desc When a suggestion is selected, 'value' is set to suggestion identifier
           * if multivalued, add value to values and clean selected
           * @todo If multivalued (values defined), rest value of scope at the end
           */
          $scope.onSelect = function( $item, $model, $label, $event ){

            // Removing user input
            $scope.rawValue = "";
            $scope.triggerLength = 1; // triggerLength back to default value when an element selected

            // Some autosuggest suggestions are clickable commands
            if( $item.command ){
              // Adds all (10) suggestions to values
              if( $item.command === 'add_all' ){
                $scope.rawValue = "";
                $scope.addAllToValues( $scope.suggestions );
              }
              return;
            }

            // Setting value to selected
            $scope.value = $scope.valueFromItem( $item );

            // reseting/invalidating specified fields
            if( $scope.params && $scope.field && $scope.field.reset ){
              $scope.params[ $scope.field.reset ] = undefined;
            }

            // setting additional fields if requested. Setting will refuse to set an
            // empty or undefined value. Use reset for that.
            if( $scope.params && $scope.field && $scope.field.set ){
              _.each( $scope.field.set, function( valueToSet, fieldToSet){
                var val = $item && valueToSet && $item[ valueToSet ];
                if ( !_.isUndefined(val) ){ $scope.params[ fieldToSet ] = val; }
              });
            }


            if(! $scope.existsValue( $scope.value )){
              return $scope.addToValues( $scope.value );
            }

          };

          /**
           * @return The identifier of the selected item, if it has an identifier and
           * we do not explicitely ask to have an object, or the item otherwise.
           */
          $scope.valueFromItem = function( $item ){
            return ( !$scope.forceObjectValue && $item && $item.identifier ) || $item;
          };


        /**
           * @desc adds/removes a specific value from values, if multivalued
           * @param {object} item The sample to add/remove from project cart
           */
          $scope.toggleValue = function( value ) {

            if ( $scope.existsValue( value ) ){
              return $scope.removeFromValues( value );
            }
            else{
              return $scope.addToValues( value );
            }
          };

          /**
           * @return true if the value exists in values, and values exists
           */
          $scope.existsValue = function( value ) {
            return !_.isUndefined(value) && $scope.values && _.includes($scope.values, value);
          };

          $scope.addToValues = function( value ){
            return !_.isUndefined(value) &&
                   ! $scope.existsValue( value ) &&
                   $scope.values &&
                   $scope.values.push( value );
          };

          $scope.addAllToValues = function( values ){
            return _.each( values, function( value ){ $scope.addToValues( value ); });
          };

          $scope.removeFromValues = function( value ){
            return !_.isUndefined(value) && $scope.values && _.pull($scope.values, value);
          };


        }]
    };
  }

})();

(function() {
  'use strict';

    DialogsFactory.$inject = ["$mdDialog", "toastr", "$log"];
    angular.module('lera').factory('Dialogs', DialogsFactory);

    /**
     * @desc The idea of putting all the dialogs aside is to make them easily accessible from
     * anywhere in the app, and from keyboard shortcuts
     * @param {}
     * @return {}
     */
    function DialogsFactory( $mdDialog, toastr, $log ){

      var Dialogs = function(){

      };

      Dialogs.prototype.showAddReferenceDialog = function(){

        $mdDialog.show({
            controller: ["$scope", "$log", "$http", "$interpolate", "StateManager", function($scope, $log, $http, $interpolate, StateManager) {

                $scope.StateManager = StateManager;
                $scope.availableRefTypes = StateManager.registry.getReferenceTypes();
                $scope.refType = $scope.availableRefTypes &&
                  ! _.isEmpty($scope.availableRefTypes) &&
                    _.keys($scope.availableRefTypes)[0];

                /**
                 * @desc Stores objects retrieved from autosuggest. It will need to be transformed
                 * to get only identifiers from these objects.
                 */
                $scope.intervalParams = {};

                /**
                 * @return {object} data fields necessary to start resolving the reference.
                 # The user must input values for this fields to be able to create a Reference.
                 */
                $scope.getReferenceDataFields = function(refType){
                  var refConf = StateManager.registry.getReferenceConf(refType);
                  return refConf && refConf.fields;
                };

                /**
                 * @desc Adds the given reference to the current browser window.
                 */
                $scope.addReference = function() {

                    var valid = true;

                   _.each( $scope.getReferenceDataFields( $scope.refType ), function(field){

                      if( !$scope.intervalParams[field.name] ){
                        toastr.warning("You must fill the "+field.name+" field");
                        valid = false;
                      }
                   });

                   if( !valid ){ return; }

                    //toastr.info("Adding and resolving new reference...", "New Reference");
                    $mdDialog.cancel();

                    StateManager.addReferenceWithDefaults( $scope.refType, $scope.intervalParams, function( ref ){
                      ref.scrollToRef();
                      toastr.success("Reference "+ref.name+" added.", "New Reference");
                    }, function(err){

                    }, function( track ){
                      // TODO use searchable field declared in registry
                      return track && track.autoselectEntity( $scope.intervalParams.query );
                    });

                };

                $scope.getSelectedConfProperty = function( key ){
                  if( $scope.refType  ){
                    var conf = StateManager.registry.getReferenceConf( $scope.refType );
                    return conf && conf[key];
                  }
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

            }],
            templateUrl: 'dialogs/add_reference.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
      };

      /**
       * @desc Shows a dialog allowing to add a track to a given reference
       * @param {object} Reference to which we want to add a track
       */
      Dialogs.prototype.showAddTrackDialog = function( ref ){

        $mdDialog.show({
            controller: ["$scope", "StateManager", function($scope, StateManager) {

                $scope.ref = ref;
                $scope.StateManager = StateManager;

                // TODO Preselect first track, same strategy as with selecting first reference

                /**
                 * Adds the given track to the current browser window.
                 */
                $scope.addTrack = function() {

                    //toastr.info("Adding and updating new track...", "New Track");
                    $mdDialog.cancel();
                    StateManager.addTrack(ref, $scope.trackType, {}, function( track ){
                      toastr.success("Track "+track.name+" added.", "New Track");
                    });
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

                $scope.getSelectedConfProperty = function( key ){
                  if( $scope.trackType  ){
                    var conf = StateManager.registry.getTrackConf( $scope.trackType );
                    return conf && conf[key];
                  }
                };

            }],
            templateUrl: 'dialogs/add_track.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });

      };

      /**
       * @desc Allows to jump on a new location of a given reference
       * @param {object} ref
       */
      Dialogs.prototype.showSearchDialog = function( ref ){

        $mdDialog.show({
            controller: ["$scope", function($scope) {

              $scope.searchable = ref.searchable;
              $scope.searchableConf =
                ref.searchable &&
                ref.fields &&
                _.find(ref.fields, ['name', ref.searchable]);

              $scope.dataToEdit = _.clone(ref.getIntervalParams()); //we don't want to modyfy the data directly

              $scope.updateReferenceData = function() {

                $mdDialog.cancel(); // close dialog
                ref.replaceInterval( $scope.dataToEdit, function(){
                  toastr.success("Jumped to new location on "+ref.name,"Jump successful");
                }, function( res ){

                });
              };

              $scope.close = function() {
                  $mdDialog.cancel();
              };

            }],
            templateUrl: 'dialogs/search.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
      };


      Dialogs.prototype.editEntityFiltersDialog = function( track ) {

          $mdDialog.show({
              controller: ["$scope", function($scope) {

                // We don't want the actual entity filters to be updated until the users clicks UPDATE
                $scope.filtersToEdit = _.clone(track.filter.getEntityFilters());

                $scope.track = track;
                $scope.intervalParams = track.getIntervalParams();

                $scope.updateEntityFilters = function() {

                  $mdDialog.cancel(); // close the dialog

                  track.updateEntityFilters( $scope.filtersToEdit, function(){

                      toastr.success("Updated Entity Filters",
                      { allowHtml: true, timeout: 10000, closeButton: true });

                  });
                };

                $scope.getTitle = function(){
                  return "Add "+(track.filtersAutocomplete && track.filtersAutocomplete.name || "filters");
                };

                $scope.removeTag = function(index){
                  _.pullAt($scope.filtersToEdit, index);
                };

                $scope.hasTags = function(){
                  return ! _.isEmpty( $scope.filtersToEdit );
                };

                $scope.hasMultipleTags = function(){
                  return _.size( $scope.filtersToEdit ) > 1;
                };

                $scope.removeAllTags = function(){
                  $scope.filtersToEdit = [];
                };

                $scope.close = function() {
                    $mdDialog.cancel();
                };

              }],
              templateUrl: 'dialogs/edit_filters.template.html',
              parent: angular.element(document.body),
              clickOutsideToClose: true
          });
      };

      /**
       * @desc Shows a dialog allowing to select the fork target.
       * Forking creates another reference+track based on a selected entity id and
       * transformation rules in forkTargetConf
       * @param {object} track
       * @param {string} entityId
       * @param {object} forkTargetConf, configuration of the forlking, ex:
           {
             "name": "orthologs",
             "legend": "'epiro'",
             "urlTemplate": "api/autocomplete/ortholog/gene?entityId={{params.entityId}}&species={{params.species}}&query={{text}}"
           }
       * @todo check that parameters not null
       */
      Dialogs.prototype.showForkToEntityDialog = function( track, entityId, forkTargetConf ){

        $mdDialog.show({
            controller: ["$scope", "StateManager", "toastr", function( $scope, StateManager, toastr ) {

              $scope.forkTargetConf = forkTargetConf;
              // Used to configure autosuggest
              // TODO remove explicit reference to species
              $scope.params = { entityId: entityId, species: track.getIntervalParam('species') };

              $scope.fork = function() {

                $mdDialog.cancel(); // close the dialog
                StateManager.fork( track, $scope.forkValue, forkTargetConf );
              };

              $scope.close = function() {
                  $mdDialog.cancel();
              };

            }],
            templateUrl: 'dialogs/fork.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            onComplete: function(scope, element){
                var input = element.find("input")[0];
                return input && input.focus();
            }
        });
      };

      return new Dialogs();
    }
})();

(function() {

    'use strict';

  editable.$inject = ["$timeout"];
    angular
        .module('lera')
        .directive('editable', editable);

  /**
   * @desc Editable text widget.
   * TODO max displayed length : ellipse of 200characters/pixels.
   * @class
   * @memberOf lera.directives
   */
  function editable($timeout) {
    return {
        restrict: 'EA',
        template: '<span ng-click="editMode = true" ng-hide="editMode" class="editable-value" title="Click to edit">{{getValue()}}</span>'+
                  '<input ng-keypress="keysSupport($event)" autofocus ng-blur="editMode = false" ng-model="value" ng-show="editMode" type="text" autocomplete="off" autocorrect="off" spellcheck="false"/>',
        scope: {
            value: '=',
            defaultValue: '@'
        },
        controller: ["$scope", function($scope){
          $scope.editMode = false;

          $scope.getValue = function(){
            return $scope.value || $scope.defaultValue;
          };

          $scope.keysSupport = function( e ){
            switch(e.keyCode){
              case 13 : $scope.editMode = false; break;
            }
          };

        }],
        link: function(scope, e, attrs){
          scope.$watch('editMode', function(value) {
            if(value === true) {
              $timeout(function() {
                e.find('input').focus();
                //e.find('input')[0].select();
              });
            }
          });

        }
    };
  }

})();

(function() {
  'use strict';

  angular.module('lera').directive('info', Info);

/**
 * @desc Shows information about entities and components clicked by the user
 * If the component has 'data' key, shows data from component
 * If entity has 'data' key, shows data from entity
 * Shows buttons to interact with component / entity
 */
  function Info(){
      return {
          restrict: 'EA',
          templateUrl: 'contextmenu/contextMenu.template.html',
          scope: {},
          controller: ["$scope", "StateManager", "ContextMenuState", "Dialogs", "Track", function($scope, StateManager, ContextMenuState, Dialogs, Track){
            $scope.StateManager = StateManager;
            $scope.ContextMenuState = ContextMenuState;

            /**
             * Correcting menu position to avoid conflict with right and bottom borders of the window
             */
            $scope.$watch( function(){ return $(".context-menu").get(0); }, function(contextMenuEl){

              if( contextMenuEl ){ // if added to the DOM
                $scope.ContextMenuState.correctPosition();
              }
            });

            $scope.createUrl = function(url){
              return url({
                ref       : ContextMenuState.getTrack().getIntervalParams(),
                entity_id : encodeURIComponent( ContextMenuState.getEntityId() ),
                component : ContextMenuState.getComponent(),
                entity : ContextMenuState.getEntity()
              });
            };

            /**
             * @desc When the user double-clicks on an entity, the track is replaced (same id, different object) by a track
             * of type morphsTo defined in the current track registry declaration
             * @param {string} morphTarget Type of the track into which this entity should morph
             */
            $scope.morphToEntity = function( morphTarget ){
              $scope.StateManager.morphToEntity( ContextMenuState.getTrack(), ContextMenuState.getEntityId(), morphTarget );
            };

            /**
             * @desc Shows a dialog allowing to select the fork target.
             * Forking creates another reference+track based on a selected entity id and
             * transformation rules in forkTargetConf
             * @param {object} forkTargetConf
             */
            $scope.forkToEntity = function( forkTargetConf ){

              var track = $scope.ContextMenuState.getTrack();
              var entityId = $scope.ContextMenuState.getEntityId();

              Dialogs.showForkToEntityDialog( track, entityId, forkTargetConf );
            };


            $scope.canCenterToEntity = function(){
              var entityId = ContextMenuState.getEntityId();
              var track = ContextMenuState.getTrack();
              return track && entityId && track.canCenterToEntity( entityId );
            };

            $scope.canSelectEntity = function(){
              var entity = ContextMenuState.getEntity();
              return entity && Track.isRealEntity( entity );
            };

            /**
             * @desc When the user clicks on an entity, the reference is updated to this entities bounds
             */
            $scope.centerToEntity = function(){

              var entityId = ContextMenuState.getEntityId();
              var track = ContextMenuState.getTrack();

              if(track && entityId){
                track.centerToEntity( entityId );
              }
            };

            $scope.centerToComponent = function(){
              var entityId = ContextMenuState.getEntityId();
              var componentId = ContextMenuState.getComponentId();
              var track = ContextMenuState.getTrack();
              if(track && entityId && componentId){
                track.centerToComponent( entityId, componentId );
              }
            };

          }]
    };
  }
})();

(function() {
  'use strict';

  ContextMenuStateFactory.$inject = ["StateManager"];
  angular.module('lera').factory('ContextMenuState', ContextMenuStateFactory);


  function ContextMenuStateFactory(StateManager){

    /**
     * @class
     * @memberOf lera.factories
     */
    var ContextMenuState = function(){

    };


    ContextMenuState.prototype.resetContextMenu = function(){
      this.menu = {};
    };

    /**
     * @desc Displays the context menu if entityId or componentId is not undefined
     * @param {object} e Click event containing the x,y coordinated of the mouse pointer
     * @param {string} entityId Id of the clicked entity
     * @param {string} componentId Id of the clicked component
    * FIXME : synchronisation bug. Active track can sometimes be different than the track
    * from which entity and component are from. Use refId and trackId instead ??
    * data-track-id, data-ref-id
     */
    ContextMenuState.prototype.showInfoMenu = function(e, entityId, componentId){

      // TODO remove as deprecated
      // var track = StateManager.getActiveTrack();

      var track = this.getTrackFromEvent( e );

      if( track && ( entityId || componentId ) ){
          this._setMenu(e, track, entityId, componentId);
      }
    };

    /**
     * @desc Sets a menu content to be displayed
     * @param {object} e Click event containing the x,y coordinated of the mouse pointer
     * @param {object} track on which the click occured
     * @param {string} entityId Id of the clicked entity
     * @param {string} componentId Id of the clicked component
     */
    ContextMenuState.prototype._setMenu = function(e, track, entityId, componentId){

      this.menu = {
        top         : e.pageY,
        left        : e.pageX,

        entityId    : entityId,
        entityProperties: this._getProcessedProperties( track.getEntityProperties( entityId ), track.contextmenu ),

        morphTargets: this._getMorphTargets( track.getAllMorphTargets(), track.getEntityProperties( entityId ) ),

        componentId : componentId,
        componentProperties: this._getProcessedProperties( track.getComponentProperties( entityId, componentId ), track.contextmenu ),

        track       : track
      };

    };

    ContextMenuState.prototype.isDisplayable = function(){
      return this.menu && this.menu.top && this.menu.left;
    };

    ContextMenuState.prototype.getEntity = function(){
      return this.menu && this.menu.entityId && this.menu.track.getEntity( this.menu.entityId );
    };

    ContextMenuState.prototype.getComponent = function(){
      return this.menu && this.menu.componentId && this.menu.track.getComponent( this.menu.entityId , this.menu.componentId );
    };

    ContextMenuState.prototype.getTrack = function(){
      return this.menu && this.menu.track;
    };

    ContextMenuState.prototype.getEntityId = function(){
      return this.menu && this.menu.entityId;
    };

    ContextMenuState.prototype.isEntitySelected = function(){
      var track = this.getTrack();
      var entityId = this.getEntityId();
      return track && entityId && track.isSelected( entityId );
    };

    ContextMenuState.prototype.triggerSelectedEntity = function(){
      var track = this.getTrack();
      var entityId = this.getEntityId();
      return track && entityId && track.triggerSelectedEntity( entityId );
    };

    ContextMenuState.prototype.getEntityProperties = function(){
      return this.menu && this.menu.entityProperties;
    };

    ContextMenuState.prototype.getMorphTargets = function(){
      return this.menu && this.menu.morphTargets;
    };

    ContextMenuState.prototype.getComponentId = function(){
      return this.menu && this.menu.componentId;
    };

    ContextMenuState.prototype.getComponentProperties = function(){
      return this.menu && this.menu.componentProperties;
    };

    /**
     * @desc Returns the title of a context menu using either a component title,
     * or by default the entity name
     * @return {string} Title of the context menu
     */
    ContextMenuState.prototype.getTitle = function(){
      var c = this.getComponent();
      var e = this.getEntity();
      return (c && c.title) || (e && e.name);
    };

    ContextMenuState.prototype.getLeftPos = function(){
      return this.menu && this.menu.left;
    };

    ContextMenuState.prototype.getTopPos = function(){
      return this.menu && this.menu.top;
    };

    ContextMenuState.prototype.correctPosition = function(){

      var menuWidth = $(".context-menu").width();
      var menuHeight = $(".context-menu").height();
      var windowWidth = $(window).width();
      var windowHeight = $(window).scrollTop() + $(window).height();
      this._correctPosition( menuWidth, menuHeight, windowWidth, windowHeight );
    };

    /**
     * @desc Returns the track object from which an event originated
     * @param {object} e
     * @return {object} track
     */
    ContextMenuState.prototype.getTrackFromEvent = function( e ){
      var target = e && $(e.target);
      var refCard = target && target.closest("reference-card");
      var refId =  refCard && refCard.data("ref-id");

      var trackCard = target && target.closest("track-card");
      var trackId = trackCard && trackCard.data("track-id");

      var track = refId && trackId && StateManager.getTrack( refId, trackId );
      return track;
    };

    /**
     * @desc  Correcting menu position to avoid conflict with right and bottom borders of the window
     * @param {integer} menuWidth
     * @param {integer} menuHeight
     * @param {integer} windowWidth
     * @param {integer} windowHeight
     */
    ContextMenuState.prototype._correctPosition = function( menuWidth, menuHeight, windowWidth, windowHeight ){

      var leftEdge = menuWidth + this.getLeftPos() - windowWidth;
      if( this.menu && leftEdge > 0 ){ this.menu.left-= (leftEdge + 20); }

      var topEdge = menuHeight + this.getTopPos() - windowHeight;
      if( this.menu && topEdge > 0 ){ this.menu.top-= (topEdge + 20); }
    };


    /**
     * @desc If a contextmenu field is declared in the configuration for this track,
     * we order the fields given the given order, and use supplied 'names' for fields.
     * Fields in data not declared in contextmenu are ignored.
     * If no contextmenu field is present in track configuration, all fields from data are displayed
     * in alphabetical order.
     * @param {object} data Data object from an entity or component
     * @param {object} contextmenu (optional)
     * @return {list}
     */
    ContextMenuState.prototype._getProcessedProperties = function( data, contextmenu ){

      var self = this;
      if( data && contextmenu && contextmenu.fields){

        return _.transform( contextmenu.fields, function(acc, field){
          var v = field.key && data[field.key];
          if( ! _.isUndefined(v) ){
            acc.push( {
              field : field.name || field.key,
              value : self._processValue(v)
            } );
          }
        }, [] );

      } else {

        var unsorted =  _.map( data, function(v,k){
          return {
            field : _.replace( k, /_/g, ' '),
            value : self._processValue(v)
          };
        });
        return _.sortBy( unsorted, function(o){return o.field;});
      }
    };

    /**
     * @desc Transforms a string, array or object into a string suitable to be displayed in the menu
     * @param {variable} v
     * @return {string} processedValue
     */
    ContextMenuState.prototype._processValue = function( v ){

      if( _.isString(v) ){
        return _.replace(v, /_/g, ' ');
      }

      if( _.isPlainObject(v) ){
        return _.join(_.map(v, function(vv,vk){return vk+" : "+vv;}), ', ');
      }

      if( _.isArray(v) ){
        return _.join(v, ', ');
      }

      return v;
    };

    /**
     * @desc Returns the morphing targets compatible with this track and this entity
     * @param {list} targets list of morphing targets. Empty list if no morphTargets
     * @param {object} data Data describing the entity
     */
    ContextMenuState.prototype._getMorphTargets = function( targets, data ){

      return _.filter( targets, function( target ){
          var conditionsMet = _.isUndefined( target.conditions ) ||
           _.every( target.conditions, function( v, k ){
            return data && data[k] === v;
          });

          return conditionsMet;
      });
    };

    /**
     * @desc Returns if it exists, a comment associated with the entity name, entity/component, or data key/val regex
     * @param {object} comments
     * @return {string} comment
     * @todo Implement
     */
    ContextMenuState.prototype._getComment = function( comments ){

    };

    return new ContextMenuState();
  }

})();

(function() {
  'use strict';

  ZoomControllerFactory.$inject = ["$log", "$timeout", "StateManager"];
  DragControllerFactory.$inject = ["$timeout", "StateManager", "ContextMenuController"];
  ContextMenuControllerFactory.$inject = ["$timeout", "StateManager", "ContextMenuState"];
  KeyboardControllerFactory.$inject = ["$timeout", "$log", "StateManager", "Dialogs"];
  CursorPositionControllerFactory.$inject = ["$timeout", "StateManager"];
  WindowControllerFactory.$inject = ["$timeout", "$log", "StateManager", "Reference"];
  angular.module('lera').factory('ZoomController', ZoomControllerFactory);
  angular.module('lera').factory('DragController', DragControllerFactory);
  angular.module('lera').factory('ContextMenuController', ContextMenuControllerFactory);
  angular.module('lera').factory('KeyboardController', KeyboardControllerFactory);
  angular.module('lera').factory('CursorPositionController', CursorPositionControllerFactory);
  angular.module('lera').factory('WindowController', WindowControllerFactory);

  function getMouseX( e ){
    var e_target = $(e.target);
    var e_browser = e_target && e_target.closest(".browser");
    // We can not use directly e.offsetX because of a bug with firefox: https://github.com/jquery/jquery/issues/3080
    var browserX = e_browser.offset() && e_browser.offset().left;
    var x = e.pageX - browserX;
    return x;
  }

  function ZoomControllerFactory( $log, $timeout, StateManager ){

    var MAX_LEVEL = 8;
    var ZOOM_AMPLIFIER = 80;
    var ZOOM_WAIT_MS = 100;
    var ZOOM_LOCK_MS = 200;

    /**
     * @class
     * @desc Zooming tracks using mouse scroll
     * @memberOf lera.factories
     */
    var ZoomController = function( element ){

      this.zoomTimer = undefined;
      this.zoomLevel = 0;

      // is set to false when previous zoom is too recent
      this.lockTimer = undefined;
      this.locked = false;
      // TODO force a wait time between zooms. Do not execute zoom if the wait time from previous zoom is not finished.


      var self = this;
      $(element).on('mousewheel', '.browsable', function( e ) {
        e.preventDefault();

        if( !self.locked && StateManager.getActiveTrack() && StateManager.isStandBy() ){
         self.zoomLevel++;
         // Inspired by http://stackoverflow.com/questions/29654288/how-to-stop-scroll-triggering-events-multiple-times
         $timeout.cancel( this.zoomTimer );
         // zoom executed only after scrolling has stopped for x ms.
          self.zoomTimer = $timeout(function() {
            if( self.zoomLevel ){
              var level = self.zoomLevel <= MAX_LEVEL ? self.zoomLevel : MAX_LEVEL;
              var zoomDirection = ( e.deltaY > 0 ) ? 1 : -1;
              var zoomFactor = zoomDirection * level * ZOOM_AMPLIFIER;
              var mouseX = getMouseX( e );

              // Restraining max zoom factor to one third of the width
              var maxZoomFactor = _.ceil( StateManager.getActiveTrack().getReference().getWidth() / 3 );
              if( zoomFactor > maxZoomFactor){
                zoomFactor = maxZoomFactor;
              }

              StateManager.getActiveTrack().getReference().zoom( zoomFactor, mouseX );
              self.zoomLevel = 0;

              self.lockZoom();
            }
          }, ZOOM_WAIT_MS);
        }
      });

      /**
       * @desc Prevents the zoom to be trigerred just after the previous one. Locks
       * the zoom for ZOOM_LOCK_MS after the last zoom, before a new zoom can be made.
       * TODO Keep locked while receiving singals. Unlock only when no more signals for a given time
       */
      ZoomController.prototype.lockZoom = function(){
        var self = this;
        self.locked = true;
        $timeout.cancel( this.lockTimer );
        self.lockTimer = $timeout( function(){
          self.locked = false;
          console.log("Zoom UNLOCKED");
        }, ZOOM_LOCK_MS );
      };

    };
    return ZoomController;
  }


  function DragControllerFactory( $timeout, StateManager, ContextMenuController ){

    /**
     * @class
     * @desc Dragging tracks using mouse down/mouve/up
     * @memberOf lera.factories
     */
    var DragController = function( element ){

       this.dragStart = undefined;
       this.dragFactor = undefined;
       this.isDragging = false;

       var self = this;

       // TODO Only if in stand by ??

       $(element).on('mousedown', '.browser', function( e ) {

         self.isDragging = true;
         self.dragStart = e.pageX;

       });

       $(element).on('mouseup', '.browser', function( e ) {

         self.dragFactor = self.dragStart - e.pageX;
         self.isDragging = false;

         // if drag factor > 0 browse, else show menu
         $timeout(function(){
           if( self.dragFactor ){

             e.stopPropagation(); // preventing easelJS from getting the click event
             var activeRef = StateManager.getActiveReference();
             return activeRef && activeRef.browse( self.dragFactor );

           }

         });
       });
    };

    return DragController;
  }


  function ContextMenuControllerFactory( $timeout, StateManager, ContextMenuState ){

    /**
     * @class
     * @desc Showing / hiding context menu
     * @memberOf lera.factories
     */
    var ContextMenuController = function(){

      /**
       * @desc Hiding the context menu IF :
       * - The context menu is displayed
       * - We are not inside menu
       * - We are inside menu but on a button that is not a link
       */
      $(document).on('mouseup', function( e ){

        var notInsideMenu = $(e.target).closest("md-card").length === 0;
        var isButton = $(e.target).closest(".md-button").length !== 0;
        var notLinkButton = isButton && $(e.target).closest(".link").length === 0;

        if( ContextMenuState.isDisplayable() && ( notInsideMenu || notLinkButton ) ){
          $timeout(function(){ ContextMenuState.resetContextMenu(); });
        }
      });
    };

    return ContextMenuController;
  }

  function KeyboardControllerFactory( $timeout, $log, StateManager, Dialogs ){

    /**
     * @class
     * @desc Zooming/dragging track using keyboards keys
     * @memberOf lera.factories
     */
    var KeyboardController = function(){

      $(document).on('keydown', function( e ) {

        // Ignoring keys pressed inside an input
        if( $(e.target ).is("input") ){ return; }

        // Only listen to keys inside body
        if( ! $(e.target ).is("body") ){ return; }

        // Ignoring keys pressed with Cntrl
        if( e.ctrlKey ){ return; }

        $timeout(function(){

          var ar = StateManager.getActiveReference();
          // Controls related to a given reference
          if( ar ){
            switch(e.which){
              case 65 : ar.browse(-50); break; // A
              case 68 : ar.browse(50); break; // D
              case 87 : ar.zoom(200); break; // W
              case 83 : ar.zoom(-200); break; // S
              case 74 : Dialogs.showSearchDialog( ar ); break; // J
              case 84 : Dialogs.showAddTrackDialog( ar );break; // T
            }
          }

          switch(e.which){
            case 82 : Dialogs.showAddReferenceDialog(); break; // R
            case 85 : StateManager.undo();break; // U
            case 67 : StateManager.removeAllReferences();break; // C
            //case 76 : $timeout(function(){ $('#load-session').click(); },0);break; // L TODO Does not work
          }

        });
      });

    };
    return KeyboardController;
  }

  function TouchControllerFactory( $timeout, $log, StateManager, Reference ){

    /**
     * @class
     * @desc Touch controls on tablets
     * @memberOf lera.factories
     * @todo Implement
     */
    var TouchController = function(){

      // TODO Use http://hammerjs.github.io/getting-started/
      // Mobile Genomics :
      // Controls :
      // Swipe left/right to move track
      // Pinch in-out to zoom
      // Double-tap on entity to MORPH
      // Rotate to show orthologs

    };
    return TouchController;
  }

  function CursorPositionControllerFactory( $timeout, StateManager ){

    /**
     * @class
     * @desc Zooming/dragging track using keyboards keys
     * @memberOf lera.factories
     */
    var CursorPositionController = function( element ){

      /**
       * @desc Position of mouse cursor translated inside the reference
       */
      $(element).on('mousemove', '.browser', function( e ) {

        var x = getMouseX( e );

        $timeout(function(){
          var ref = StateManager.getActiveReference();
          return ref && ref.setCursorPosition( x );
        });
      });
    };
    return CursorPositionController;
  }


  function WindowControllerFactory( $timeout, $log, StateManager, Reference ){

    /**
     * @class
     * @desc Resizing the browser window
     * @memberOf lera.factories
     */
    var WindowController = function(){

      function getAvailableRefWidth(){
          var rw = $(".lera").width();
          var bw = _.round(rw - 300); // Place reserved for controls and enames legend
          return bw;
      }

      // Before adding any reference, or loading conf, we set the Reference.width variable to the given width...
      Reference.width = getAvailableRefWidth();

      // TODO Deal with case of a browser resize while a track update is running. Only if isStandby is true ?
      // TODO Make sure no 0 size is asked
      // TODO Wait 100ms before trigerring update ??
      $( window ).resize(function() {
        var newWidth = getAvailableRefWidth();
        if( newWidth != Reference.width ){
          $timeout(function(){
            Reference.width = newWidth;
            //$log.debug("Window was resized: updating reference width to "+Reference.width+" and rerendering all tracks.");
            StateManager.rerenderAllTracks();
          });
        }
      });

    };
    return WindowController;
  }


})();

(function() {
    'use strict';

    Lera.$inject = ["ZoomController", "DragController", "ContextMenuController", "KeyboardController", "WindowController", "CursorPositionController"];
    angular.module('lera').directive('lera', Lera);

    function Lera( ZoomController, DragController, ContextMenuController, KeyboardController, WindowController, CursorPositionController ) {
        return {
            restrict: 'EA',
            templateUrl: 'lera/lera.template.html',
            scope: {
                postConfig: '@' // Config can be sent in the URL or by POST if too big for get
            },

            link: function(scope, element){

              var zoomController           = new ZoomController( element );
              var dragController           = new DragController( element );
              var contextMenuController    = new ContextMenuController();
              var keyboardController       = new KeyboardController();
              var cursorPositionController = new CursorPositionController( element );
              var windowController         = new WindowController();
            },

            controller: ["$scope", "$timeout", "toastr", "$location", "$log", "StateManager", "Reference", function($scope, $timeout, toastr, $location, $log, StateManager, Reference) {

                var self = this;

                $scope.StateManager = StateManager;

                // When lera initialises the first thing it does is load the most recent version of the registry
                $scope.StateManager.initialize(function() {

                    $scope.loadFromContext();
                });

                /**
                 * @desc Parse configuration from the URL to add and initialise references and tracks
                 */
                $scope.loadFromContext = function() {

                    // Load whole config from URL or from lera.data
                    var conf = $scope.getConf();

                    if (conf) {
                        try{

                            toastr.info('Loading references and tracks submitted by URL', 'URL Configuration');
                            $log.info("Using supplied configuration.");

                            $scope.StateManager.deserialize( conf );

                            $location.search('conf', undefined);
                            $("lera").data("conf", "");

                        } catch (e){
                            toastr.error('Invalid configuration in URL: ' + e, 'URL Configuration');
                            $log.error('Invalid configuration in URL: ' + e);
                        }
                    }
                };

                $scope.getConf = function(){
                  var serverConf = $("lera").data("conf");
                  // The server now provides GET and POST parameters as data-conf
                  // This is deprecated, we keep it only for back compatibility
                  var angularGET = $location.search().conf && decodeURIComponent($location.search().conf);
                  return angularGET || serverConf;
                };

                // TODO Do not show tutorial if has URL conf !!

                $scope.isAppEmpty = function(){
                  return ! $location.search().conf && $scope.StateManager.isEmpty();
                };

            }]
        };
    }


})();

(function() {
  'use strict';

  Registry.$inject = ["$http", "toastr", "$log"];
  angular.module('lera').factory('Registry', Registry);


  function Registry( $http, toastr, $log ){

      /**
       * @class
       * @memberOf lera.factories
       * @param registerURL {string} URL to retrieve all available references and tracks from the registry
       */
      var Registry = function(registryURL){

        this.registryURL = registryURL;
        this.data = {};
      };

      /**
       * @desc Retrieves from the registry server all declared tracks and references
       * @param {function} callback Function called when all data was retrieved from the registry
       */
      Registry.prototype.load = function( callback ){

        var self = this;

        // We add a t parameter to make sure to get a non cached version of the registry
        $http.get(this.registryURL+"?t="+new Date()).then(
          function(res){
            self.data = res.data;
            $log.debug('Registry data loaded. '+_.size(self.data.tracks)+' tracks and '+_.size(self.data.references)+' references are declared.');
            if( callback ){ callback(); }
          },
          function(res){
            $log.error("Can not load registry : " + JSON.stringify(res.data) + " : " + res.status);
            var message = res.data ?  res.data.message : "Can not retrieve registry data";
            toastr.error(message, 'Registry');
        });

      };

      /**
       * @desc Finds track definition from the registry given the trackType
       * @param {string} trackType : ex: ensTranscript
       * @return {object} trackConfig
       */
      Registry.prototype.getTrackConf = function( trackType ){
        return this.data.tracks && this.data.tracks[trackType];
      };

      /**
       * @param {string} trackType
       * @return {boolean} True if the given trackType is declared in the registry
       */
      Registry.prototype.isTrackTypeAvailable = function( trackType ){
        return !! trackType && this.getTrackConf( trackType );
      };

      /**
       * @param {string} trackType
       * @returns {array} references Array of supported references by the track
       */
      Registry.prototype.getSupportedReferences = function( trackType ){
        return trackType && this.data.tracks && this.data.tracks[trackType] && this.data.tracks[trackType].supportedReferences;
      };

      /**
       * @desc Finds reference definition from the registry given the refType
       * @param {string} refType ex: ensGenome
       * @return {object} refConfig
       */
      Registry.prototype.getReferenceConf = function( refType ){
        return this.data.references && this.data.references[refType];
      };


      /**
       * @return {list} List of all declared types of tracks in the registry
       * @deprecated
       */
      Registry.prototype.getTrackTypes = function(){
        return _.keys(this.data.tracks);
      };

      /**
      * @param {string} refType
      * @return {object} Dictionary of trackConfs compatibles with given refType
       */
      Registry.prototype.getTrackTypesForRefType = function(refType){
        return _.transform( this.data.tracks, function( result, trackConf, trackType ){
          if(_.some( trackConf.supportedReferences, function(rt){return rt == refType;} )){
            result[trackType] = trackConf;
          }
        }, {});
      };

      /**
       * @return {object} Dictionary of refConfs
       */
      Registry.prototype.getReferenceTypes = function(){
        return this.data.references;
      };

      /**
       * @param {object} trackConf
       * @param {string} refType
       * @returns {boolean} True if this track supports this reference type
       */
      Registry.prototype.trackSupportsReference = function( trackConf, refType ){
        // The list is very short and this function is rarely invoked, so no need for optimisations such as transforming the list into a hash
        return refType && _.some( trackConf.supportedReferences, function(rt){return rt == refType;} );
      };

      return Registry;
  }


})();

(function() {
    'use strict';

    angular.module('lera').directive('referenceCard', ReferenceCard);

    function ReferenceCard() {
        return {
            restrict: 'EA',
            templateUrl: 'reference/reference.template.html',
            scope: {
              ref:'='
            },
            controller: ["$scope", "$mdDialog", "toastr", "$location", "$log", "$sce", "Dialogs", function($scope, $mdDialog, toastr, $location, $log, $sce, Dialogs) {

              $scope.Dialogs = Dialogs;

              $scope.isReferenceEmpty = function(){
                return $scope.ref && $scope.ref.isEmpty();
              };

              $scope.removeReference = function(refId){
                $scope.ref.removeFromState();
              };

            }]
          };
        }
})();

(function() {
  'use strict';

    ReferenceFactory.$inject = ["$http", "$q", "toastr", "$log", "$interpolate", "Track", "StatusEnum", "UpdateModeEnum"];
    angular.module('lera').factory('Reference', ReferenceFactory);

    function ReferenceFactory( $http, $q, toastr, $log, $interpolate, Track, StatusEnum, UpdateModeEnum ){

      var colors = [ '#006064', '#01579B', '#E65100', '#004D40', '#4CAF50', '#2962FF', '#6200EA', '#795548' ];

      /**
       * @class
       * @memberOf lera.factories
       *
       * @desc A reference is basically a one-dimensional Coordinate System.
       * The first job of a reference is to retrieve all the parameters (chromosome, start, end, etc..)
       * for a (genomic, protein, etc...) interval based on incomplete data provided by the user
       * ( a species and transcript id for example).
       * The second job of a reference is to manage changes to this interval made by the user, when browsing a track,
       * and sending the updated interval data to all depending tracks to update them.
       *
       * @param {string} refId Id allowing to refer to this reference
       * @param {string} refType type of this reference. ex: ensGenome.
       * @param {object} intervalParams Contains all the parameters which will be given to the tracks
       * ( and sent to track rest apis tu update them).
       * The rest api will be called to update the interval parameters with missing values
       * @param {object} refConf  JSON object from the registry describing a reference.
       * It must contain an URL to a REST API allowing to resolve missing parameters for this reference.
       * @param {object} stateManager Object representing the current state of the app, containing all the references.
       */
    	var Reference = function( refId, refType, intervalParams, refConf, stateManager ){

        this.refType = refType;

        /**
         * @desc The refId allows to directly access a reference in the project
         */
        this.refId = refId;

        /**
         * @desc Number used to determine in which order we should display the references.
         * This value can be changed to change the order.
         * Using this number avoids having to manage two data structues (list and hash)
         * to access references by keys and manage their order
         */
        this.order = refId;

        this.stateManager = stateManager;

        /**
         * @desc Data resolving status for this reference.
         */
        this.status = StatusEnum.PENDING;

        /**
         * @desc By default we assume the reference starts from 1. This can be overrided with the parameter data.min
         */
        this.DEFAULT_MIN = 1;

        _.defaultsDeep(this, refConf);

        if( !this.url ){
          throw "A REST API URL is mandatory for each reference, but was not declared in the registy for reference type "+refType;
        }

        /**
         * @desc Parameters describing the coordinate system used to draw the tracks
         */
        this.intervalParams = intervalParams;

        /**
         * @desc Color used to draw the references, tracks, and used as default color for entities and components
         */
        this.color = Reference._getColor( stateManager && stateManager.getReferences() );

        /**
         * @desc Tracks which depend on this reference
         */
        this.tracks = {};
      };

      Reference.trackIdCounter = 0;

      /**
       * @desc Number of references currently being resolved
       */
      Reference.resolvingCount = 0;

      /**
       * @desc Default dimensions of the track visualisation.
       * All tracks depending on the same reference have the same width.
       */
      Reference.width = 900;

      /**
       * @desc Number of ticks (segments of alternating color) that will be drawn under the track
       * to help the user compare the size of entities inside the track.
       */
      Reference.ticksCount = 10;

      /**
       * @desc Calls the server to retrieve missing properties for the current interval parameters.
       * @param {function} success The callback function that will be called after resolving of the
       * interval parameters for this reference is successfully completed.
       * We can not know in advance what reference information each depending track needs,
       * so we always try to update the reference.
       * @param {function} error If server has responded with an error.
       */
      Reference.prototype.resolve = function( success, error ){

        this._resolve( this.getIntervalParams(), success, error );
      };

      /**
       * @desc We use the REST API URL declared for this reference to update missing data about the intervalParams for this reference.
       * Bascially we send to the server all the interval data that we have (ex: start, end, chromosome or a gene and species )
       * and we ask it to complete the missing data (min and max for the chromosome, start and end for the gene, etc.)
       * @private
       * @param {object} intervalParams sent to the server to resolve the reference
       * @param {function} success The callback function that will be called after processing is successfully completed
       * We can not know in advance what reference information each depending track needs.
       * So we always try to update the reference when possible.
       * @param {function} error If server has responded with an error, or the resolved reference is still incomplete
       */
      Reference.prototype._resolve = function( intervalParams, success, error ){

        var self = this;

        self.status = StatusEnum.LOADING;
        Reference.resolvingCount++;

        $http.get( self.url, {params: intervalParams} ).then(

            function(res){
              self.status = StatusEnum.OK;
              Reference.resolvingCount--;
              if( !_.isEmpty(res.data) ){

                self.intervalParams = res.data;
                Reference._ensureNumbers( self.intervalParams, ['start', 'end', 'min', 'max']);
                var fieldNotFound = Reference._assessInterval( self.intervalParams, ['start', 'end']);
                if( fieldNotFound ){ return error && error( self, "The mandatory parameter "+fieldNotFound+" could not be resolved.");}
                return success && success( self );

              } else {
                return error && error( self, 'Could not resolve reference. Some mendatory fields are probably missing.');
              }
            },
            function(res){
              self.status = StatusEnum.ERROR;
              Reference.resolvingCount--;
              var msg = ( res.data && res.data.message ) || res.statusText || 'Can not connect to server';
              return error && error(self, "Could not resolve "+self.name+" reference : "+msg);
            }
          );
      };

      /**
       * @desc Returns the first requested field that was not found in interval
       * @param {object} interval
       * @param {list} fields : fields to look for in the interval
       * @example _assessInterval( {start: 5, chr: '2L'}, ['start'] )
       * @return {string} f : first field that was not found in the interval
       */
      Reference._assessInterval = function( interval, fields ){

        return _.find(fields, function(f){ return _.isUndefined( interval[f] ); });
      };

      /**
       * @desc Makes sure that all requested fields are numbers (transforms them into numbers)
       * Ignores fields that are not found in interval
       * WARNING: Does not manage the case where a field can not be transformed to number
       * @param {object} interval
       * @param {list} fields : fields which values should be transformed to numbers
       * @example _ensureNumbers( {start: 5, chr: '2L'}, ['start'] )
       */
      Reference._ensureNumbers = function( interval, fields ){

        _.each(fields, function(f){ if( interval[f] ){ interval[f] = +interval[f]; } });
      };

      /**
       * @desc Tries to resolve the reference with the new provided (incomplete) interval parameters.
       * If the resolve is successful, replace current interval with the new one and update depending tracks.
       * @param {object} intervalParams New interval parameters (chromosome, start, etc...) that will reaplce the previous ones
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       */
      Reference.prototype.replaceInterval = function( intervalParams, success, error ){

        var self = this;
        self._resolve( intervalParams, function(){
          self.updateTracks( success, error, UpdateModeEnum.JUMP );
        }, function(ref, msg){
          toastr.error(msg, "Could not Jump");
          return error && error( msg );
        });
      };

      /**
       * @desc This function is typically called after scrolling/zooming the view top update the reference data.
       * This triggers the update of all tracks that depend on that reference
       * The difference with replaceInterval is that this function only changes the start/stop
       * @param {integer} start Start of the new interval
       * @param {integer} end End of the new interval
       * @param {string} mode Update mode. JUMP forces to clean current pool of entities and make a server update.
       * MOVE will allow the data engine to decide if a server update is necessary.
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       * the reference first.
       */
      Reference.prototype.updateInterval = function( start, end, mode, success, error ){

        if( _.isUndefined(start) || _.isUndefined(end) ){
          $log.warn('Could not update reference '+this.refId+' because start/end undefined');
          return;
        }

        if( start >= end ){
          $log.warn('Could not update reference '+this.refId+' because start >= end');
          return;
        }

        if( start == this.getIntervalParam('start') && end == this.getIntervalParam('end') ){
          $log.info("Reference already set to interval. No update needed.");
          return;
        }

        this.setIntervalParam('start', start);
        this.setIntervalParam('end', end);
        this.updateTracks( success, error, mode );
      };

      /**
       * @desc Updates all dependant tracks with current interval parameters
       * @param {function} success Called when all tracks successfully updated
       * @param {function} error Called if one of the tracks could not update
       * @param {string} mode : JUMP or MOVE. Used by data.engine to decide if a server call is necessary
       */
      Reference.prototype.updateTracks = function( success, error, mode ){

        var self = this;
        var trackCallback = function(){};

        var tracksPromise = $q.all( _.map( this.getTracks(), function(track){
          return track.update(self.getIntervalParams(), trackCallback,  mode);
        }) );

        tracksPromise.then( function(res){
          return success && success( res && res.data );
        }, function(res){
          return error && error( res && res.data );
        });
      };


      /**
       * @desc Adds a new track and synchronises it (retrieves corresponding entities/components)
       * with the current interval parameters.
       *
       * @param {string} trackType Type of the track to add, declared in the registry file
       * @param {object} trackConf Data retrieved from the registry characterising this track
       * @param {object} filters Filters that will select entities that will be displayed on the track (ex: specific samples)
       * and categories of entities/components that will be displayed (ex: only transcripts on forward strand)
       * @param {function} callback Function called when the new track has been added and updated. The new Track is passed as parameter
       * (synchronised with current reference interval)
       *
       * @return {object} track The created track (not updated yet)
       */
      Reference.prototype.addTrack = function( trackType, trackConf, filters, callback ){

        var self = this;
        var trackId = Reference.getNextTrackId( trackType );
        var track = new Track( trackId, trackType, trackConf, this, filters );
        this.tracks[trackId] = track;

        track.update( this.getIntervalParams(), function(){
          $log.debug("Track "+trackId+" added and updated.");
          if( self.stateManager ){ self.stateManager.setActiveTrack( track ); }
          if( callback ){ callback( track ); }
        }, UpdateModeEnum.JUMP);

        return track;
      };

      /**
       * @desc Deletes the given track
       * @param {string} trackId
       */
      Reference.prototype.removeTrack = function( trackId ){
        if( this.tracks[trackId] ){
          if( this.stateManager ){ this.stateManager.stackState(); }
          delete this.tracks[trackId];

          if( self.stateManager ){ self.stateManager.resetActiveTrack(); }
          $log.debug("Track "+trackId+" removed.");
        }
      };

      /**
       * @desc Returns all tracks from this reference
       * @return {list} tracks Unordered list of tracks
       */
      Reference.prototype.getTracks = function(){
        return _.values( this.tracks );
      };

      /**
       * @desc Returns an ordered list of tracks
       * @return {list} tracks Ordered by trackId
       * @todo while this is quite fast for the very low number of tracks we have,
       * we may still want to store the ordered list of tracks instead of generating it each time.
       */
      Reference.prototype.getOrderedTracks = function(){
        return _.sortBy( this.tracks, 'order' );
      };

      /**
       * @desc Returns true if track is the first track in the ordered tracks list
       * @param {object} track
       * @return {boolean} true if track is the first track in the ordered tracks list
       * @todo implement without regenerating an ordered list each time
       */
      Reference.prototype.isFirstTrack = function( track ){
        var orderedTracks = this.getOrderedTracks();
        var idx = _.indexOf( orderedTracks, track );
        return idx === 0;
      };

      /**
       * @desc Returns true if track is the last track in the ordered tracks list
       * @param {object} track
       * @return {boolean} true if track is the last track in the ordered tracks list
       * @todo implement without regenerating an ordered list each time
       */
      Reference.prototype.isLastTrack = function( track ){
        var orderedTracks = this.getOrderedTracks();
        var idx = _.indexOf( orderedTracks, track );
        return idx > 0 && idx === orderedTracks.length - 1;
      };

      /**
       * @desc Changes the order of display of a given track inside the reference
       * @param {object} track
       * @param {integer} direction The track will jump into the position given by the direction,
       * while the other track at this position will jump to the position occupied by this track
       * @return {boolean}
       */
      Reference.prototype.shiftTrack = function( track, direction ){
        var orderedTracks = this.getOrderedTracks();
        return Reference.shiftElement( orderedTracks, track, direction );
      };

      /**
       * @desc Returns true if the reference contains no tracks
       * @return {boolean} true if the reference contains no tracks
       */
      Reference.prototype.isEmpty = function(){
        return _.isEmpty( this.tracks );
      };

      /**
       * @desc Removes all tracks from this reference
       * @todo If one of them is the active track, reset the active track
       */
      Reference.prototype.removeAllTracks = function(){
        var self = this;
        _.each(self.tracks, function(track, k){
          delete self.tracks[k];
        });
        if( self.stateManager ){ self.stateManager.resetActiveTrack(); }
      };

      /**
      * @desc
       * When a track is dragged, it modifies the reference interval, so that ALL tracks depending on that reference are dragged
       * This function if applied only if this track has a start/end declared.
       * If max and min are declared for this reference, start and end will not be allowed to exceed these limits.
       * @param {integer} factor Generally the distance the mouse mouved when dragging the track, or a fixed value when
       * browsing the track woth keyboard keys. Positive to browse right, negative to browse left.
       */
      Reference.prototype.browse = function( factor ){

        if( _.isUndefined(this.getIntervalParam('start')) || _.isUndefined(this.getIntervalParam('end')) ){
          $log.warn('Can not browse a track depending on the reference '+this.refId+' with no start/stop declared');
          return;
        }

        // browsing limits.
        var min = this.getIntervalParam('min') || this.DEFAULT_MIN;
        var max = this.getIntervalParam('max');

        /**
         * We transforn the view factor into the reference factor
         * The factor divided by width gives us the fraction of the reference by which we would like to move the view
         * If the view width is 900 and factor is 30, this means than (30/900 = 0.03) => We want to move the reference by 0.03 of the ref interval
         * If the ref bounds are (5000;5050) then the ref interval width is 50. We want to move that width by 0.03 of it, or 0.03*50 = 1.5
         */
        var refFactor = Reference._getRefFactor( factor, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end') );

        var start = this.getIntervalParam('start') + refFactor;
        start = (start < min) ? min : start;

        var end = this.getIntervalParam('end') + refFactor;
        end = (max && end > max) ? max : end;

        if(start != this.getIntervalParam('start') && end != this.getIntervalParam('end')){
          if( this.stateManager ){ this.stateManager.stackState(); }
          this.updateInterval( start, end, UpdateModeEnum.MOVE );
        }
      };


      /**
      * @desc Zooms in/out of the reference interval. Note that the minimum interval is [x;x+1]
      * @param {integer} factor Positive to zoom in and negative to zoom out
      * @param {float} mouseX Position of the mouse inside the track
      */
      Reference.prototype.zoom = function( factor, mouseX ){

        if( _.isUndefined(this.getIntervalParam('start')) || _.isUndefined(this.getIntervalParam('end')) ){
            $log.warn("Can not zoom a track depending on a reference with no start/stop declared");
            return;
        }

        var refFactor = Reference._getRefFactor( factor, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end') );

        // browsing limits. default min is 0.
        var min   = this.getIntervalParam('min') || this.DEFAULT_MIN;
        var max   = this.getIntervalParam('max');

        var start = this.getIntervalParam('start') + refFactor;
        var end   = this.getIntervalParam('end') - refFactor;

        // SHIFT to zoom into mouse position and not the center of the track
        if ( mouseX ){
          var dx = Reference._getShiftFactor( this.fromView( mouseX ), this.getIntervalParam('start'), this.getIntervalParam('end'), start, end );
          start += dx;
          end += dx;
        }

        // Avoiding exceeding reference limits
        start = (start < min) ? min : start;
        end = (max && end > max) ? max : end;

        if( start < end && ( start != this.getIntervalParam('start') || end != this.getIntervalParam('end') ) ){
          if( this.stateManager ){ this.stateManager.stackState(); }
          this.updateInterval( start, end, UpdateModeEnum.MOVE );
        }
      };

      /**
       * @desc Returns the width for all references
       * @return {integer} width
       */
      Reference.prototype.getWidth = function(){
        return Reference.width;
      };

      /**
       * @desc Returns the number of tics to draw under the track
       * @return {integer} ticksCount
       */
      Reference.prototype.getTicksCount = function(){
        return Reference.ticksCount;
      };

      /**
       * @desc This factor determines by how much the real reference must move,
       * given a move of the view
       * We transforn the view factor into the reference factor
       * The factor divided by width gives us the fraction of the reference by which we would like to move the view
       * @example
       * If the view width is 900 and factor is 30, this means than (30/900 = 0.03) =>
       * We want to move the reference by 0.03 of the ref interval
       * If the ref bounds are (5000;5050) then the ref interval width is 50.
       * We want to move that width by 0.03 of it, or 0.03*50 = 1.5
       * @private
       * @param {float} factor Distance on track view
       * @param {float} with of the view
       * @param {integer} start of the reference interval
       * @param {integer} end of the reference interval
       *
       * @return {integer} factor Corresponding to the real reference interval.
       * It is allways rounded to the number further from 0, that way it is never 0.
       */
      Reference._getRefFactor = function( factor, width, start, end){
        var f = (factor / width) * ( end - start ) ;
        return f > 0 ? _.ceil(f) : _.floor(f);
      };

      /**
       * @desc Calculates the necessary reference coordinate shift so that zoom center on the mouse position
       * instead of zooming into the center of the displayed reference.
       * @param {integer} mouseRefX : coordinates of the mouse inside the reference coordinates
       * @param {integer} oldStart : start before zoom
       * @param {integer} oldEnd : end before zoom
       * @param {integer} newStart : start after zoom
       * @param {integer} newEnd : end after zoom
       * @return {integer} dx Necessary coordinate shift so that a feature under the mouse cursor before the zoom,
       * stays under mouse cursor after the zoom
       */
      Reference._getShiftFactor = function( mouseRefX, oldStart, oldEnd, newStart, newEnd ){

        if( !mouseRefX ){ return 0; }

        var oldDx = mouseRefX - oldStart;
        var newInterval = newEnd -  newStart;
        var oldInterval = oldEnd - oldStart;
        oldInterval = oldInterval ? oldInterval : 1; // never divide by 0
        var refFactor = newStart - oldStart;
        var zoomFactor = newInterval / oldInterval;
        var dx = oldDx * ( 1 - zoomFactor ) - refFactor;
        return _.round( dx );
      };

      /**
       * @desc Transforms a coordinate from the VIEW interval into the real REFERENCE interval
       * @param {float} x coordinate to transform
       * @return {integer} The coordinate inside the view transformed into reference position and rounded to closest integer
       */
      Reference.prototype.fromView = function( x ){

        if( _.isUndefined(x) ){
          throw("fromView needs valid x parameter");
        }

        return _.round( Track.transform( 0, Reference.width, this.getIntervalParam('start'), this.getIntervalParam('end'), x ) );
      };


      /**
       * @param {integer} start on the reference interval
       * @param {integer} end on the reference interval
       * @return {boolean} True if given start and end match current reference interval
       */
      Reference.prototype.intervalMatch = function( start, end ){
        return start == this.getIntervalParam('start') && end == this.getIntervalParam('end');
      };

      /**
       * @return {object} parameters describing the current reference interval
       */
      Reference.prototype.getIntervalParams = function(){
        return this.intervalParams;
      };

      /**
       * @desc Retrieves a given interval parameter
       * @example getIntervalParam('start') retrieves the current start of the reference
       * @param {string} param
       * @return {variable} paramValue
       */
      Reference.prototype.getIntervalParam = function(param){
        return this.intervalParams && this.intervalParams[param];
      };

      Reference.prototype.setIntervalParam = function(param, value){
        this.intervalParams[param] = value;
      };

      /**
       * @desc Updates the position inside the reference corresponding to the cursor position over the drawn representation
       * of a track.
       * @param {float} viewX Position of the mouse on the view of a track
       */
      Reference.prototype.setCursorPosition = function( viewX ){
        this.cursorPosition = this.fromView( viewX );
      };

      /**
       * @return {integer} Mouse position over a track translated into the position in the current reference interval.
       */
      Reference.prototype.getCursorPosition = function(){
        return this.cursorPosition;
      };

      /**
       * @desc Removes this reference from the state in which it was declared
       */
      Reference.prototype.removeFromState = function(){
        return this.stateManager.removeReference( this.refId );
      };

      /**
       * @desc Scrolls the browser window to the current position of this reference in the webpage
       */
      Reference.prototype.scrollToRef = function(){

        var refEl = $("#"+this.refId);
        if( refEl ){
          var refElBottomPos = refEl.offset() && refEl.offset().top - 65; // taking into account toolbar height
          if( refElBottomPos ){
            $("body,html").animate({ scrollTop: refElBottomPos }, 1000);
          }
        }
      };


       /**
        * @desc Generates a track id
        * @param {string} trackType
        * @return {number} id
        */
      Reference.getNextTrackId = function( trackType ){
         return ++Reference.trackIdCounter;
      };

       /**
        * @param {object} references
        * @return {string} Returns the color matching the position of the current reference in the state
        */
      Reference._getColor = function( references ){
         var self = this;
         var index = 0;
         _.each(references, function(ref, rId){
           if(rId === self.refId){
             return;
           }
           index++;
         });
         index = index > colors.length - 1 ? colors.length - 1 : index;
         return colors[index];
      };

      /**
       * @desc Exchanges the order of two elements in the list
       * @param {list} list
       * @param {object} el : object having the 'order' property
       * @param {integer} direction Number of steps to walk to the start or to the end of the list
       * @return {boolean} true if the element was successfully moved inside the list
       */
      Reference.shiftElement = function( list, el, direction ){
        if( !list || !el || !direction ){ return false; }
        var idx = _.indexOf( list, el );
        if( _.isUndefined( idx ) ){ return false; }

        var newIdx = idx + direction;
        if( newIdx < 0 || newIdx >= _.size( list ) ){ return false; }
        var otherEl = list[newIdx];
        if( _.isUndefined( otherEl ) ){ return false; }

        // Shifting order values
        var tmp = el.order;
        el.order = otherEl.order;
        otherEl.order = tmp;

        return true;
      };

      return Reference;
    }

})();


(function() {
  'use strict';

        highcharts.$inject = ["$log", "toastr", "Filter"];
    angular.module('lera').directive('highcharts', highcharts);


        /**
         * @description
         * This directive allows to integrate Highcharts charts in angular way.
         * This custom directive has been created because the existing ng-highcharts directive is still not mature
         * It is inspired by :
         * 1. http://jsfiddle.net/csTzc/
         * 2. https://github.com/pablojim/highcharts-ng/blob/master/src/highcharts-ng.js
         *
         * @param {object} data Model representing the overview of a category. ex: overview of biotypes.
         * @param {string} category Name of the category shown oin the chart. ex: biotype
         */
        function highcharts($log, toastr, Filter) {
            return {
                restrict: 'EA',
                template: '<div class="chart-error">Can not display chart</div>',
                scope: {
                    data: '=',
                    category: '=',
                    track: '='
                },
                link: function(scope, element, attrs) {

                  /**
                   * @desc Keeps the category filters updated with the legend items
                   */
                  scope.showHide = function () {

                    var countOfVisibles = _.sumBy(this.series.points, function(o){
                      return o.visible ? 1 : 0;
                    });

                    if( countOfVisibles === 1 && this.visible ){
                      toastr.warning("Refusing to hide all categories", "Filters");
                      return false;
                    } else {

                      var clickedPoint = this;
                      var points = this.series.points;
                      var filters = Filter.legendAsFilters( clickedPoint, points );

                      scope.$applyAsync(function(){
                        scope.track.setCategoryFilters( scope.category, filters );
                      });
                    }
                  };

                  /**
                   * Highcharts options object, for details see (http://api.highcharts.com/highcharts)
                   */
                  var total = 0;
                  var options = {
                      chart: {
                          renderTo: element[0],
                          height: 300,
                          width: 300,
                          events: {
                            load: function () {

                            }
                          }
                      },

                      title: {
                          text: scope.category
                      },
                      yAxis: {
                          title: {
                              text: scope.yLegend
                          }
                      },
                      xAxis: {
                          type: 'category'
                      },
                      legend: {
                          enabled: true,
                          useHTML: true,
                          labelFormatter: function() {
                            total += this.y;
                            return '<div style="width:200px"><span style="float:left">' + this.name + '</span><span style="float:right">' + this.y + '</span></div>';
                          },
                      },
                      plotOptions: {
                          series: {
                            animation: false,
                            enableMouseTracking: false
                          },
                          pie: {
                              showInLegend: true,
                              dataLabels: {
                                  enabled: false
                              },
                              point: {
                                events: {
                                    legendItemClick: scope.showHide
                                }
                            },
                          }
                      },
                      tooltip: {

                      },
                      series: [{
                          type: 'pie',
                          colorByPoint: true,
                          data: scope.data
                      }, ]
                  };

                  var chart = new Highcharts.Chart(options);

                  /**
                   * @desc Charts are updated when the data changes
                   * We do not need to set the legend visibility of the points as this was
                   * already done by Overview when calculating the 'data'
                   */
                  scope.$watch("data", function ( newValue, oldValue ) {
                    if(newValue !== oldValue){
                      chart.series[0].setData( newValue, true );
                    }
                  }, true);

                  scope.$on('$destroy', function() {
                      if ( chart ) {
                        chart.destroy();
                      }
                  });

                }
            };
        }


})();

(function() {
  'use strict';

  angular.module('lera').directive('overviewCard', OverviewCard);

  /**
   * @desc Shows statistics from the meta objext inside the track object.
   * Statistics in meta are a hash of key/values
   * They are used as visualisation as a graph, but when clicking on the grah this
   * send selected keys as filters to filter data for the track.
   * For example the track sample has statistics about most severe variant conseauences displayed as a chart.
   * When clicking on 'missence', this updates the track by sending filters:{conseauence:['missence']}
   *
   */
  function OverviewCard(){
      return {
          restrict: 'EA',
          templateUrl: 'overview/overview.template.html',
          controller: ["$scope", "StateManager", "Overview", function($scope, StateManager, Overview){
            $scope.StateManager = StateManager;
            $scope.Overview = Overview;

            $scope.getStatusOfEntities = function(){
              return StateManager.state.activeTrack && StateManager.state.activeTrack.getStatusOfEntities();
            };

            $scope.showEntitiesStatus = function(){
              return StateManager.state.activeTrack && StateManager.state.activeTrack.filter.hasEntityFilters();
            };

          }]
      };
  }

})();

(function() {
  'use strict';

    OverviewFactory.$inject = ["toastr", "$log", "$interpolate", "DataEngine"];
    angular.module('lera').factory('Overview', OverviewFactory);

    function OverviewFactory( toastr, $log, $interpolate, DataEngine ){

      var overviewSidebarState = 'closed';

      /**
       * @desc Colors used for the chart. if index >= length, index = modulo length
       */
      var COLORS = ['#659355', '#3F51B5', '#2196F3', '#009688', '#4CAF50', '#FF5722', '#795548', '#E91E63',
                    '#F44336', '#9C27B0', '#00BCD4', '#673AB7', '#CDDC39', '#FF9800'];

      /**
       * @desc Associating a color for each unique value of the chart
       */
      var COLORS_HASH = {};

      /**
       * @memberOf lera.factories
       * @constructor
       */
      var Overview = function(){

        this.categories = {};

      };

      /**
       * @desc Returns the list of colors that will be used for tha charts in all overviews
       * @return {list} colors
       */
      Overview.getDeclaredColors = function(){
        return COLORS;
      };

      Overview.triggerSidebar = function(){
        overviewSidebarState = overviewSidebarState === 'in' ? 'out' : 'in';
      };

      Overview.closeSidebar = function(){
        overviewSidebarState = 'out';
      };

      Overview.getSidebarState = function(){
        return overviewSidebarState;
      };

      /**
       * @desc Resets the content of the overview
       */
      Overview.prototype.reset = function(){
        this.categories = {};
      };

      /**
       * @desc Calculates the new overview
       * @param {object} entities and components on which to calculate the overview
       * @param {object} overviewCategories, contain the names of 'data' keys used to calculate the overview.
       * @param {object} filter used to determine if a category is in visible or invisible state,
       * as declared in the filter categories.
       * TODO Calculate overview only if there are less than 1K components,
       * else show warn message : please zoom into the track to see the overview

       Workflow :

       (entity/component).data
            -> ignore outside interval
            -> select only overviewCategories
            -> set as visible:false if filtered

       */
      Overview.prototype.set = function(entities, overviewCategories, filter, interval){
        this.categories = Overview.calculateOverview(entities, overviewCategories, filter, interval);
      };

      /**
       * @desc Changes the visible property of all elements to true
       */
      Overview.prototype.setAllVisible = function(){
        _.each( this.categories, function( c ){ _.each( c, function(point){ point.visible = true; });});
      };

      /**
       * @return {object} For each category (ex: biotype, transcript) we have a list of values with their count.
       */
      Overview.prototype.get = function(){
        return this.categories;
      };

      /**
       * @desc Returns true if there are categories
       * @return {boolean} hasOverview
       */
      Overview.prototype.hasOverview = function(){
        return ! _.isEmpty( this.categories );
      };

      /**
       * @desc Parses the meta object returned by the track rest api
       * @param {object} hash of entities to process. Those can be all entities retrieved from the server,
       * as the overview will consider only entities matching the current intervalParams
       * @param {array} list of categories for which we want to display a chart overview.
       * For each category (name, level) corresponding to a data key,
       * we will calculate the count for each data value
       * @return {object} unsortedMeta A data format compatible with highcharts libary
       */
      Overview.calculateOverview = function( entities, overviewCategories, filter, interval ){

        var self = this;
        var startTime = performance.now();

        var rawOverview = Overview.calculateOverviewHash( entities, overviewCategories, interval );
        var overview = _.transform( rawOverview, function(result, metas, category){
           var unsorted = _.map( metas, function( meta_count, meta_name ){

            return {
              name    : meta_name,
              y       : meta_count,
              visible : ! ( filter && filter.isCategoryFiltered( category, meta_name ) ),
              color : Overview._getColor( meta_name, COLORS_HASH, COLORS )
            };

          });

          result[category] = _.orderBy(unsorted, 'y', 'desc');

        }, {});

        var overviewTime = performance.now() - startTime;
        if( overviewTime > 10 ){ $log.warn("Overview calculations done in "+overviewTime+" ms."); }

        return overview;
      };



      /**
       * @desc Calculates basic statistics about returned entities, using data from entities.
       * If a category is not found in the entity data, the components data is used
       * NONE category is added if an entity/component has matched nothing in the category
       * @param {object} entities Entities which will be displayed on the track in the selected interval
       * @param {array} categories Names of the categories (ex: biotype, consequence) we want to display the statistics for.
       * ex: [{key:'biotype', level:'entity'}]
       * @param {object} interval containing start and stop. Only objects that are not outside this interval are
       * used to calculate the overview.
       * @return {object} ex: {biotype:{prot:2, NONE:3}}
       */
      Overview.calculateOverviewHash = function( entities, categories, interval ){

        var meta = {};
        if(!categories){
          return meta;
        }

        function incrementMeta(cat,catval){
          if(_.isUndefined(catval)){return;}
          meta[cat] = meta[cat] || {};
          if(_.isUndefined(meta[cat][catval])){
            meta[cat][catval] = 1;
          } else {
            meta[cat][catval] += 1;
          }
        }

        _.each(categories, function(catLevel, catName){ // for each meta category declared in registry or submitted by track

          _.each(entities, function(entity){ // for category inside data of each entity

            if( ! DataEngine.isOutsideInterval( entity, interval) ){ // if entity is not outside the current displayed interval

              switch(catLevel){

                case 'entity' :
                  var ecatval = entity && entity.data && entity.data[catName];
                  if( ! _.isUndefined(ecatval)){
                    incrementMeta(catName, ecatval);
                  } else {
                    incrementMeta(catName,'NONE'); // No data key for this category for this entity
                  }
                  break;

                case 'component' :
                  _.each(entity.components, function(component){ // for category inside data of each component
                    if( ! DataEngine.isOutsideInterval( component, interval) ){ // if component is not outside the current displayed interval
                      var ccatval = component && component.data && component.data[catName];
                      if( ! _.isUndefined(ccatval)){
                        incrementMeta(catName,ccatval);
                      } else {
                        incrementMeta(catName,'NONE');
                      }
                    }
                  });
                  break;

                default : $log.warn("Category level "+catLevel+" for overview category "+catName+" is not supported.");
              }
            }
          });
        });

        // Removing overview categories that have only NONE as value
        var filtered_meta = _.transform(meta, function(res, cat, catName){
          if( _.size(cat) > 1 || !cat.NONE ){res[catName] = cat;} // if there are more than one value, or the only value is not NONE
        }, {});

        return filtered_meta;
      };

      /**
       * @desc Associates a unique color for each value between different charts. The same
       * value will have the same color in different charts.
       * For this we associate a color
       * @param {string} value
       * @return {string} hex color
       * @todo There seem to be a rare bug which can associate the same color to the first and third value
       */
      Overview._getColor = function( value, valuesHash, colors ){

        if( _.isUndefined(value) || _.isUndefined(colors) || _.isUndefined(valuesHash) ){ return 'gray';}

        if( !valuesHash[value] ){
          // 1. get number of elements in hash
          var rawIdx = _.size(valuesHash);
          var nbColors = _.size( colors );
          if(!nbColors){ return 'gray';}
          var idx = rawIdx % nbColors;
          valuesHash[value] = colors[idx];
        }

        return valuesHash[value];
      };

      /**
       * @desc Returns the color associated with a property value
       * @param {string} value
       * @return {string} color
       */
      Overview.getColor = function( value ){
        return COLORS_HASH[value];
      };

      return Overview;
    }

})();

(function() {
  'use strict';

  angular.module('lera').directive('toolbar', Toolbar);

  function Toolbar(){
    return {
        restrict: 'EA',
        templateUrl: 'toolbar/toolbar.template.html',
        scope: {},
        controller:["$scope", "StateManager", "Overview", "$location", "Dialogs", function($scope, StateManager, Overview, $location, Dialogs){
          $scope.StateManager = StateManager;
          $scope.Overview = Overview;
          $scope.Dialogs = Dialogs;

          /**
           * @desc TEMPOARARY. Should be removed/changed for PROD
           */
          $scope.getMessage = function(){
            // put isDevMode method to app.js ( also used to log errors to server if not on gunpowder dev server )
            var port = $location.port();
            if( port === 10973 ){
              return 'DEV';
            }
          };
        }]
    };
  }


})();

(function() {
  'use strict';

  StateFactory.$inject = ["$log", "toastr", "Track", "Reference"];
  angular.module('lera').factory('State', StateFactory);


  /**
   * @desc Represents the current project, containing references which contain tracks
   */
  function StateFactory( $log, toastr, Track, Reference ){

    /**
     * @class
     * @memberOf lera.factories
     */
    var State = function(){

      /**
       * @desc hash of references, associated an object to an id
       */
      this.references = {};

    };

    return State;
  }

})();

(function() {
  'use strict';

  StateManagerFactory.$inject = ["$log", "toastr", "State", "Registry", "Reference", "Track", "DataEngine"];
  angular.module('lera').factory('StateManager', StateManagerFactory);


  function StateManagerFactory( $log, toastr, State, Registry, Reference, Track, DataEngine ){

    /**
     * @class
     * @desc Contains information about the current state of the app
     * The StateManager is a Singleton
     * @memberOf lera.factories
     * @todo Merge StateManager and State ? Or save state as a full object ( no need to update tracks again )
     */
    var StateManager = function(){

      this.registry = new Registry('registry.json');
      this.state = new State( this.registry );

      /**
       * @desc Keeps the number of references that are busy resolving
       */
      this.resolvingCount = 0;

      /**
       *  Stack of history used to undo actions
       */
      this.history = [];

    };

    StateManager.refIdCounter = 0;

    /**
     * @desc Executes the callback once the registry has been loaded
     * @param {function} callback Function to call
     */
    StateManager.prototype.initialize = function( callback ){

      this.registry.load(function(){
        if( callback ){ callback(); }
      });
    };

    /**
     * @desc Retrieves the configuration for the given track type, retrieves the declared reference object,
     * and creates a track object and adds it to the state.
     * Shows error message if no configuration was found for the trackType,
     * no reference was found for refId, or the track does not supports the reference.
     * @param {object} reference Reference to which add the track
     * @param {string} trackType unique identifier of the track in the registr, ex: ensTranscript
     * @param {object} filters (optional) ex: { entities:['sample1'] }
     * @param {function} callback (optional) The new Track is passed as parameter
     * @param {object} config (optional) configuration object that will be merged with the trackConfig object
     */
    StateManager.prototype.addTrack = function( reference, trackType, filters, callback, config ){

      var trackConf = this.registry.getTrackConf( trackType );
      if( !trackConf ){
        toastr.error("Can not retrieve track configuration for track type ''"+trackType+"''");
        return;
      }

      if(!reference){
        toastr.error("You must provide a valid reference");
        return;
      }

      if( !this.registry.trackSupportsReference(trackConf, reference.refType) ){
        toastr.error("Tracks of type "+trackType+" does not support references of type "+reference.refType+". (Supported references are "+JSON.stringify(trackConf.supportedReferences)+" )");
        return;
      }

      if( config ){
        trackConf = _.cloneDeep( trackConf ); // we do not want to modify the original config object used to create all tracks
        _.merge( trackConf, config );
      }

      return reference.addTrack( trackType, trackConf, filters, callback );
    };

    /**
     * @returns {array} tracks Ruturns an array of all tracks of all references of the current state
     */
    StateManager.prototype.getAllTracks = function(){
      return _.flatMap( this.state.references, function( ref ){
          return _.values( ref.tracks );
      });
    };

    /**
     * @desc Returns all references
     * @return {hash} references
     */
    StateManager.prototype.getReferences = function(){
      return this.state && this.state.references;
    };

    /**
     * @desc Returns the reference associated with the id
     * @param {string} refId
     * @return {object} reference
     */
    StateManager.prototype.getReference = function( refId ){
      return this.state && this.state.references && this.state.references[refId];
    };

    /**
     * @desc Returns the track associated with the reference id and track id
     * @param {string} refId
     * @param {string} trackId
     * @return {object} track
     */
    StateManager.prototype.getTrack = function( refId, trackId ){
      var ref = this.getReference( refId );
      return ref && ref.tracks && ref.tracks[trackId];
    };

    /**
     * @desc Returns an ordered list of references
     * @return {list} references
     */
    StateManager.prototype.getOrderedReferences = function(){
      return this.state && _.sortBy( this.state.references, 'order' );
    };

    /**
     * @desc Triggers the mechanism of redrawing each track
     */
    StateManager.prototype.rerenderAllTracks = function(){
      var self = this;
      _.each( self.getReferences(), function(ref){
        _.each( ref.getTracks(), function(track){
          track.redraw();
        });
      });
    };

    /**
     * @desc Retrieves the configuration for the refType, creates a reference object and adds it to the current state.
     * @param {string} refType
     * @param {object} intervalParams
     * @param {function} success This function will be executed once the reference is created and resolved.
     * The reference will be passed as first parameter to the callback.
     * All tracks dependant on this reference should be added during this callback.
     * @param {function} error If the server responded with an error, or the server responded with empty data
     * and a resolve was required
     * @return {object} The created reference (not resolved yet)
     */
    StateManager.prototype.addReference = function( refType, intervalParams, success, error ){

      var self = this;
      var refConf = this.registry.getReferenceConf( refType );

      if( !refConf ){
        throw "Can not retrieve reference configuration for reference type ''"+refType+"''";
      }

      var refId = StateManager.getNextRefId( refType );
      var reference = new Reference( refId, refType, intervalParams, refConf, this );
      this.state.references[refId] = reference; // Added before resolving

      reference.resolve(
        function(){
          $log.debug("Reference "+refId+" added and resolved. ");
          return success && success( reference );
        },
        function( ref, msg ){
         delete self.state.references[refId]; // Removed if resolve errored
         toastr.error(msg, 'Could not add reference',{ closeButton: true, timeOut: 20500 });
         $log.debug("Can not add reference for intervalParams "+JSON.stringify(intervalParams)+" : "+msg);
         return error && error( ref, msg );
        }
      );

      return reference;
    };

    /**
     * @desc Creates a new reference and loads all default tracks declared for this reference.
     * @param {string} refType
     * @param {object} intervalParams
     * @param {function} success This function will be executed once the reference is created and resolved.
     * The reference will be passed as first parameter to the callback.
     * All tracks dependant on this reference should be added during this callback.
     * @param {function} error If the server responded with an error, or the server responded with empty data
     * and a resolve was required
     * @param {function} trackCallback Function called when each of the default tracks is resolved
     * @return {object} The newly created reference
     */
    StateManager.prototype.addReferenceWithDefaults = function( refType, intervalParams, success, error, trackCallback ){

      var self = this;
      var ref = self.addReference( refType, intervalParams, function( ref ){

        // Adding default tracks declared for this reference
        if( ref.defaultTracks ){
          _.each( ref.defaultTracks, function( dt ){
            self.addTrack(ref, dt, {}, trackCallback);
          });
        }

        return success && success( ref );

      }, function( ref, msg ){
        return error && error( ref, msg );
      });

      return ref;
    };

    /**
     * @desc Deletes the given reference and all associated tracks
     * @param {string} refId
     */
    StateManager.prototype.removeReference = function( refId ){
      var ref = this.state.references[refId];
      if(!ref){
        toastr.error("Can not remove reference for id "+refId+" because it does not exist", "Reference not found");
        return;
      }

      this.stackState();
      ref.removeAllTracks();
      delete this.state.references[refId];
    };

    /**
     * @desc Removes all references
     */
    StateManager.prototype.removeAllReferences = function(){
      var self = this;
      if( self.notEmpty() ){

        self.stackState();
        _.each(self.getReferences(), function(ref, refId){
          ref.removeAllTracks();
          delete self.state.references[refId];
        });
      }
    };

    /**
     * @desc Returns true if the project has at least one reference
     * @return {boolean}
     */
    StateManager.prototype.notEmpty = function(){
      return !! this.getReferencesCount();
    };

    /**
     * @desc Returns true if the project has no references
     * @return {boolean}
     */
    StateManager.prototype.isEmpty = function(){
      return ! this.getReferencesCount();
    };

    /**
     * @return {object} reference used by the currently active track
     */
    StateManager.prototype.getActiveReference = function(){
      return this.state && this.state.activeTrack && this.state.activeTrack.reference;
    };

    /**
     * @return {object} track The currently active (under mouse cursor) track
     */
    StateManager.prototype.getActiveTrack = function(){
      return this.state && this.state.activeTrack;
    };

    /**
     * @desc Creates a link to a track selected as active
     * @param {object} track
     */
    StateManager.prototype.setActiveTrack = function( track ){
      if( this.state ){ this.state.activeTrack = track; }
    };

    /**
     * @desc Resets the active track if no parameter supplied, or resets the active track
     * if it corresponds to the supplied track
     * @param {object} track (optional) Resets the active track if it corresponds to the supplied track
     * @todo previous track in reference becomes actif, or if no more tracks or reference, last rrack in previous reference ?
     */
    StateManager.prototype.resetActiveTrack = function( track ){
      if( this.state ){
        if( _.isUndefined( track ) ){
          this.state.activeTrack = undefined;
        } else if ( this.state.activeTrack === track){
          this.state.activeTrack = undefined;
        }
      }
    };

    /**
     * @desc Adds to the history the current serialized state.
     * It is advised to use this function to save the current state, BEFORE making any changes to it...
     * @todo Resets the undo array, as a new temporal line is created by a change at this point in time.
     */
    StateManager.prototype.stackState = function(){
      var serialized = this.serialize();
      this.history.push( serialized );
    };

    /**
     * @desc Removes the last saved serialized state from the history and returns it
     * @return {string} serializedState
     */
    StateManager.prototype.unstackState = function(){
      return this.history.pop();
    };

    StateManager.prototype.getLastState = function(){
      return _.last( this.history );
    };


    StateManager.prototype.canUndo = function(){
      return !!this.history.length;
    };

    /**
    * @desc Undoes the last MAJOR browsing action
    * @todo put the current state in redo array
     */
    StateManager.prototype.undo = function(){

      if( this.history.length ){

        var serialised = this.history.pop();
        //toastr.warning("Going back to last state...", "History");
        this.state.references = {};
        this.deserialize( serialised );

      } else {
        toastr.warning("No actions to undo", "History");
      }

    };

    /**
     * @todo Redoes the last action before undo
     */
    StateManager.prototype.redo = function(){

    };

    /**
     * @desc Allows the user to download and save a json representation of the current state of the app
     */
    StateManager.prototype.save = function(){

      var serialized = this.serialize();
      try{

        var blob = new Blob([ serialized ], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "leraSave.json");

      } catch(err){

        toastr.error("Could not export current State : "+err);
      }
    };

    /**
     * @desc Serialises the current active state as a string
     * @return {string} String representation of the current state
     */
    StateManager.prototype.serialize = function(){

      var serializedState = [];

      var self = this;

      _.each(self.getOrderedReferences(), function(sRef){

          var tracks = [];
          _.each(sRef.getOrderedTracks(), function(sTrack){
            tracks.push({
              type: sTrack.trackType,
              filters: sTrack.filter,
              label: sTrack.label
            });
          });

          serializedState.push({
            type: sRef.refType,
            data: sRef.getIntervalParams(),
            color: sRef.color,
            tracks: tracks
          });

      });

      return JSON.stringify(serializedState);
    };

    /**
     * @desc Loads a saved session from a file
     */
    StateManager.prototype.load = function( file ){

      var self = this;
      if( file ){

        var reader = new FileReader();
        reader.onload = function(){
          var conf = reader.result;
          try {
              self.state = new State(); // put previous state in history ? in serialized form ??
              $log.info("Loading saved state...");
              self.deserialize( conf );
          } catch (e) {
              toastr.error('Invalid save file: ' + e, 'Load Session');
              $log.error('Invalid save file: ' + e);
          }

        };
        reader.readAsText( file );
      }

    };

    /**
     * @desc Parses a json string to add references and tracks to the current active state
     * @param {object} A JSON string describing references and tracks, that can be used to initialise the app
     * @todo need a callback called when all the work is done
     */
    StateManager.prototype.deserialize = function( serializedStateStr ){

      var self = this;
      var serializedState = (typeof serializedStateStr === 'string') ? JSON.parse(serializedStateStr) : serializedStateStr;

      _.each(serializedState, function(sRef){
        self.addReference( sRef.type, sRef.data, function( ref ){

          if(sRef.tracks){
            _.each(sRef.tracks, function(sTrack){

              var declaredFilters = sTrack.filters || {};
              var options = { label: sTrack.label };
              self.addTrack( ref, sTrack.type, declaredFilters, undefined, options );
            });
          }
        });
      });
    };

    /**
     * @param {string} trackType Type of a track
     * @return {list} Ids of all existing references in this state whith a type compatible with the trackType
     */
    StateManager.prototype.getAvailableReferencesForTrackType = function( trackType ){

      if( !trackType ){ return []; }

      var conf = this.registry.getTrackConf(trackType);
      if( !conf ){ return []; }

      return _.filter(this.state.references, function(r){
        return _.some( conf.supportedReferences, function(crt){return crt && r.refType && crt == r.refType;});
      });

    };


    /**
     * @desc Morphs an entity into a track.
     * When the user clicks on 'Morph', a new track (and if necessary reference)
     * of type morphsTo defined in the current track registry declaration, is/are added.
     * Note that the morphing may necessitate to change the reference too.
     * By default the new track/reference is added, not replacing the old one
     * @example Morph a gene into a transcript. Morph a transcript into a protein.
     * @param {object} track Current track that will be morphed
     * @param {string} entityId Id of the entity that will define new track/reference
     * @param {string} morphTarget Type of the track into which this entity should morph
     * @todo rename to morph
     */
    StateManager.prototype.morphToEntity = function( track, entityId, morphTarget ){

      var self = this;

      // 1. Get the type of the new track to create
      var trackType = morphTarget;
      if( !trackType ){ $log.warn("This track has no morphing target track declared") ; return; }
      if( !self.registry.isTrackTypeAvailable( trackType ) ){ $log.warn("The morphing target declared for this track ("+trackType+") is not declared in the registry") ; return; }

      // This entity id verification is facultative
      var entity = entityId && track.getEntity( entityId );
      if( !entity ){ $log.warn("Invalid entity id "+entityId) ; return; }

      var previousRef = track.getReference();
      var previousRefType = previousRef && previousRef.refType;
      var refId = previousRef.refId;

      var supportedReferences = self.registry.getSupportedReferences( trackType );
      var newRefType = supportedReferences && !_.isEmpty(supportedReferences) && supportedReferences[0]; // by default we take the first supported reference
      if( !newRefType ){ $log.warn("No supported references declared in the registry for ("+trackType+")") ; return; }

      $log.debug("We will morph to track type "+trackType+" and load it with "+newRefType+" reference declared on "+entityId);
      this.stackState();

      var data = _.cloneDeep( previousRef.getIntervalParams() );
      data.query = entityId;

      if( newRefType !== previousRefType ){
        $log.debug("We must replace the previous reference, morph the track, and bring compatible tracks from previous reference");
        var newRef = self.addReference( newRefType, data,
          function( ref ){
            ref.scrollToRef();
            //toastr.success("Old reference replaced");
            // Replacing morphed track
            self.addTrack( ref, trackType, undefined, function(){ toastr.success("Track morphed"); });

            // Transfering other tracks from reference
            _.each( previousRef.getTracks(), function( trackFromOldRef ){
                if( track !== trackFromOldRef ){ // Ignoring track that initiated the morphing
                 // track type by which this track should be replaced in a new reference
                var morphReplaceTrackType = trackFromOldRef.getMorphReplacement( newRefType );
                if( morphReplaceTrackType ){
                  $log.debug("The track "+trackFromOldRef.trackType+" fill be transfered");

                  // Transfering transferable track. ex: genotype -> peptype
                  self.addTrack( ref,
                                 morphReplaceTrackType,
                                 trackFromOldRef.filter, function(){ toastr.success("Track transfered"); },
                                 {label: trackFromOldRef.label});
                }
              }
            });
          }) ;

          // We make sure the new reference is displayed just after the reference that created it
          self.insertAfter( self.getReferences(), previousRef, newRef );

      } else {
        $log.debug("Juste adding the track");
        self.addTrack( previousRef, trackType, undefined, function(){ toastr.success("Morphing done"); });
      }

    };

    /**
     * @desc Unmorphs a track into an entity.
     * When the user clicks on 'Unmorph' button fo a track,
     * a new track (and if necessary reference)
     * of type unmorphsTo defined in the current track registry declaration, is/are added.
     * Note that the unmorphing may necessitate to change the reference too.
     * By default the new track/reference is added, not replacing the old one.
     * @example Unmorph a protein into a transcript. Unmorph a transcript into a gene.
     * @param {object} track Current track that will be unmorphed
     * @param {string} morphTarget Type of the track into which this track should unmorph
     * @param {}
     * @return {}
     * @todo Implement
     */
    StateManager.prototype.unmorph = function( track, morphTarget ){

      // TODO First we need to retrieve the entity id to which we will unmorph.
      // For a protein, we can declare in the registry : entityId : transcript_id and
      // then retrieve it from the trackParams or refParams
      // Then we just call morph

    };



    /**
     * @desc Forks a track into a new track, using an entity id.
     * The idea of forking is basically to recreate the same reference in a different species
     * The species aspect is to genome/protein specific, and should probably be generalised, using fields
     * declared for the reference in the registry

     Example registry configuration :

     "forksTo":[
       {
         "name": "orthologs",
         "legend": "'epiro'",
         "urlTemplate": "api/autocomplete/ortholog/gene?entityId={{params.entityId}}&species={{params.species}}&query={{text}}"
       }
     ],

     One of the main differences between forking and moprhing, is that forking will ask user for additional
     information.

     * @param {object} track : Track containing the entity that will be forked
     * @param {object} forkValue : object containing all the necessary information (key/value couples)
     * to create the new reference, retrieved from the user autosuggest
     * @param {object} forkTargetConf : forking configuration
     */
    StateManager.prototype.fork = function( track, forkValue, forkTargetConf ){

      var self = this;
      if( !forkTargetConf ){ $log.warn("The forking configuration object is mandatory for forking") ; return; }
      if( !forkValue ){ $log.warn("The forking value object is mandatory for forking") ; return; }
      if( !track ){ $log.warn("The track is mandatory for forking") ; return; }

      toastr.info("Adding and resolving new reference...", "New "+forkTargetConf.name+" Reference");

      self.stackState();

      // TODO this must be generalised using fields declared for the reference, inseatd of explicitely using terms species and query
      // map identifier to query in forkTargetConf
      var query = forkValue && forkValue.identifier;
      var species = forkValue && forkValue.species;
      var intervalParams = { species: species, query: query };
      var previousRef = track.getReference();

      var newRef = self.addReferenceWithDefaults(
        previousRef.refType, // we recreate a new reference of the same type
        intervalParams,
        function( ref ){
          ref.scrollToRef();
          toastr.success("Reference "+ref.name+" added.", "New "+forkTargetConf.name+" Reference");
        }
      );

      // We make sure the new reference is displayed just after the reference that created it
      self.insertAfter( self.getReferences(), previousRef, newRef );
    };

    /**
     * @return {integer} The number of tracks in this state
     * (sums the number of tracks for each reference of this state)
     */
    StateManager.prototype.getTracksCount = function(){
      return _.reduce( this.state.references, function( sum, ref ){
        return sum + _.size( ref.tracks );
      }, 0);
    };


    /**
     * @return {integer} The number of references in this state
     */
    StateManager.prototype.getReferencesCount = function(){
      return this.state && _.size(this.state.references);
    };


    /**
     * @desc Returns true only if no reference is currently being resolved
     * @return {boolean}
     */
    StateManager.prototype.allReferencesResolved = function(){
      return Reference.resolvingCount === 0;
    };

    /**
     * @desc Returns true only if no track is currently being updated
     * @return {boolean}
     */
    StateManager.prototype.allTracksUpdated = function(){
      return DataEngine.updatingCount === 0;
    };

    /**
     * @desc Returns true only if all references and tracks are resolved and updated
     * @return {boolean}
     */
    StateManager.prototype.isStandBy = function(){
      return this.allReferencesResolved() && this.allTracksUpdated();
    };

    /**
     * @desc Returns true only if no export query is currently runiing on none of the tracks
     * @return {boolean}
     */
    StateManager.prototype.noExportsRunning = function(){
      return Track.runningExports === 0;
    };

    StateManager.prototype.getRegistry = function(){
      return this.registry;
    };

    /**
    * @desc Generates a reference id
    * @param {string} refType
    * @return {number} id
    */
    StateManager.getNextRefId = function( refType ){
      return ++StateManager.refIdCounter;
    };

    /**
     * @desc Changes the order of all elements in list such as otherEl is positioned just after el
     * @param {list} list Unordered list
     * @param {object} el Element after which we want to move otherEl
     * @param {object} otherEl Element that we want to place after el
     * @return {boolean} true if the operation succeeded
     */
    StateManager.prototype.insertAfter = function( list, el, otherEl ){
      if( !list || !el || !otherEl || !el.order || !otherEl.order ){ return false; }

      otherEl.order = el.order + 1;
      _.each( list, function(oel){
        // if the list does not already have order duplicates, this will not create duplicates
        if( oel !== otherEl && oel.order >= otherEl.order ){
          if(oel && oel.order){ oel.order++; }
        }
      });
      return true;
    };

    return new StateManager();
  }

})();

// 1. Decide wheter data should be retrieved from server
// track api provider sends data about thresholds (list of integers) with entities and meta
// if user zooms in/out then see if one of the thresholds is in [oldIntervalSize;newIntervalSize]

// if filters or master data has changed, must get data from server again

// Contains a pool of entities for that track, larger than the entities shown in the track for an interval
// This pool of entities can never contain holes (entities can only be removed by the edges)
// The DataEngine maintains the pool of entities to keep the following equilibrium :


// WHEN NOT TO MAKE SERVER REQUESTS :
// 1. TODO REMOVED entity filters
// 2. New zoom/browse interval inside current virtual interval


(function() {
  'use strict';

    DataEngineFactory.$inject = ["$http", "$q", "toastr", "$log", "StatusEnum", "UpdateModeEnum"];
    angular.module('lera').constant('StatusEnum',
      {
        PENDING: 'pending', // Track just created, no update performed
        OK: 'ok', // Last update successfully performed
        ERROR: 'error', // Last update was a failure
        WARNING: 'warn' // Incorrect parameters or need user action
      });

    angular.module('lera').constant('UpdateModeEnum',
      {
        /**
         * Moving to another genomic location, chromosome genome...
         * 1. Force clean entities pool
         * 2. Force server update
         */
        JUMP: 'jump',

        /**
         * Sliding the window when dragging / zooming, adjacent entities
         * Updates the virtual pool of entities with result of server request
         */
        MOVE: 'move',

        /**
         * Returns immediately after recalculating lanes and overview
         */
        VIRT: 'virtual_update'

      });


    angular.module('lera').factory('DataEngine', DataEngineFactory);

    function DataEngineFactory( $http, $q, toastr, $log, StatusEnum, UpdateModeEnum ){

      /**
      * @desc Each track is associated with a Data Engine whcih will retrive entities for given parameters.
      * making server calls when necessary.
       * @class
       * @memberOf lera.factories
       */
      var DataEngine = function( url ){

        /**
         * @desc Last stable data status for this data engine. This is
         * the status obtained after the last update.
         */
        this.status = StatusEnum.PENDING;

        this.lastUpdateMode = undefined;

        /**
         * @desc Latest parameters received from the track to do an update.
         * Not necesserally parameters sent to the server.
         */
        this.lastRequestedParams = undefined;

        /**
         * @desc Allows to cancel the current server request
         */
        this.canceller = $q.defer();

        /**
         * @desc hash of entities. Entities are the highest level objects displayed on a track.
         * Usually entities are displayed one per lane.
         * registry->reference->track->entity->component is just a tree of relationships. Biological nature has no importance.
         *
         * Entities are generally composed of lower level objects called components. Components can be genotypes of a sample entity or
         * exons and introns from a transcript. Components are allways drawn on the same lane.
         *
         * This is a virtual pool of entities. It contains all entities retrieved with server updates.
         * It should contain >= of entities displayed in the browser.
         *
         * NOTE: Currently this pool of entities is reseted only on JUMP. So if the user keeps MOVEing and MOVEing,
         * the pool will grow bigger and bigger. We may consider a threshold based on number of entities or interval size
         * and remove some entities/components from the virtual pool when it becomes too big.
         *
         */
        this.virtualEntities = {};

        /**
         * @desc URL of the REST API endpoint to retrieve data for this track
         */
        this.url = url;

        this.virtualParams = undefined;

        /**
         * @desc Number of server calls this data engine is currently running
         * This does not count the preload server calls.
         */
        this.serverCalls = 0;
      };

      /**
       * @desc Number of updating requests being currently run for all tracks by all
       * created data engines
       */
      DataEngine.updatingCount = 0;

      /**
       * @desc HTTP error code used when there is no real server/client error,
       * but the server wants to communicate
       * some information to the user (generally asking to specify some data)
       */
      DataEngine.USER_ERROR_CODE = 418;

      /**
       * @desc Updates the pool of entities if necessary
       * @param {object} intervalParams : new parameters for the track, sent by the reference
       * @param {object} trackParams : parameters specific to the track, including entity filters
       * @param {function} success : called if server responded with 200
       * @param {function} error : called if server responded with something else than 200
       * @param {string} rawMode : JUMP or MOVE. Helps the system to determine whether a server update is mandatory
       */
      DataEngine.prototype.update = function( intervalParams, trackParams, success, error, rawMode ){

        var self = this;
        var mode = self.getCorrectedMode( intervalParams, rawMode );
        this.lastUpdateMode = mode;

        switch( mode ){

          case UpdateModeEnum.VIRT: self._virt( intervalParams, trackParams, success, error ); break;
          case UpdateModeEnum.MOVE: self._move( intervalParams, trackParams, success, error ); break;
          case UpdateModeEnum.JUMP: self._jump( intervalParams, trackParams, success, error ); break;
        }

      };

      /**
       * @desc This update is executed when the user navigate inside the pool of virtual entities,
       * and no new entities/components must be retrieved from the server.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._virt = function( intervalParams, trackParams, success, error ){

          var self = this;
          $log.debug("Performing VIRT update for "+self.url+"("+self.status+")");
          self.setLastRequestedParams(intervalParams);

          self.cancelCurrentRequest();

          self.status = StatusEnum.OK;
          DataEngine._logParams(UpdateModeEnum.VIRT, self.virtualParams, intervalParams);

          return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories );
      };

      /**
       * @desc Appends new entities to the existing virtual pool of entities,
       * by making a server request.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._move = function( intervalParams, trackParams, success, error ){

        var self = this;

        $log.debug("Performing MOVE server update for "+self.url);
        self.setLastRequestedParams(intervalParams);

        // Optimising size of interval to query for browsing left/right
        // Not yet adapted for the two queries required to zoom-out
        var adaptedIntervals = DataEngine.adaptQueryInterval( self.lastRequestedParams, self.virtualParams );
        if( adaptedIntervals.length === 1){
          intervalParams = adaptedIntervals[0];
        }

        return self.serverUpdate( intervalParams, trackParams, function( responseParams, entities, overviewCategories ){

          // When a MOVE request is finished, the big quest is how relevant is the retrieved data to the current
          // state of the virtual pool, as many others VIRT, MOVE or JUMP requests may have finished before this one.
          // The simpliest would be to ignore all data retrieved for all but the latest request.

            // The callback is called only if the server request returns data for the last asked parameters,

              // use _.merge instead to give priority to newly retrieved objects on existing objects
              _.defaultsDeep( self.virtualEntities, entities );
              _.defaultsDeep( self.overviewCategories, overviewCategories ); // Merging with previous categories
              self.virtualParams = {
                start: (self.virtualParams.start < responseParams.start) ? self.virtualParams.start : responseParams.start,
                end: (self.virtualParams.end > responseParams.end) ? self.virtualParams.end : responseParams.end
              };

              self.status = StatusEnum.OK;
              DataEngine._logParams(UpdateModeEnum.MOVE, self.virtualParams, responseParams);
              return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories );

          }, function( responseParams, errorMessage, status ){

                self.status = status;
                return error && error( self.lastRequestedParams, errorMessage );
          }  );
      };

      /**
       * @desc This update replaces th current virtual pool of entities with data retrieved from the server.
       * @param {object} intervalParams
       * @param {object} trackParams
       * @param {function} success
       * @param {function} error
       */
      DataEngine.prototype._jump = function( intervalParams, trackParams, success, error ){

        var self = this;
        $log.debug("Performing JUMP server update for "+self.url);
        self.setLastRequestedParams(intervalParams);

        return self.serverUpdate( intervalParams, trackParams, function( responseParams, entities, overviewCategories, filtersStatus ){

              self.status = StatusEnum.OK;
              self.virtualEntities = entities;
              self.overviewCategories = overviewCategories; // Replacing all previous categories
              self.virtualParams = {
                start: responseParams.start,
                end: responseParams.end
              };

              return success && success( self.lastRequestedParams, self.virtualEntities, self.overviewCategories, filtersStatus );

          }, function( responseParams, errorMessage, status ){

                self.status = status;
                self.virtualParams = undefined;
                $log.debug("Invalidating virtual interval after unsuccessful jump");
                return error && error( self.lastRequestedParams, errorMessage );
          } );

      };

      /**
       * @desc Decides which king of update should be performed
       * -  By default we JUMP, which means that the current virtual pool of entities is completely renewed with
       *    entities retrieved from the server.
       * -  If the new intervalParams intersect with the current pool of entities, we add new entities from
       *    the server to the current pool.
       * -  If the new intervalParams are inside the current virtual pool of entities, no server call is necessary,
       *    so we perforam a VIRTUAL update
       *
       * @param {string} mode Requested MODE. This function will determine which
       * is the best mode to use, given the requested mode.
       */
      DataEngine.prototype.getCorrectedMode = function( intervalParams, mode ){
        var self = this;

        if( mode == UpdateModeEnum.MOVE && self.isIntersect( intervalParams ) ){

          if( self.isInside( intervalParams ) ){
            return UpdateModeEnum.VIRT;
          }
          return UpdateModeEnum.MOVE;

        } else {
          return UpdateModeEnum.JUMP;
        }
        return UpdateModeEnum.JUMP;
      };


       /**
        * @desc We retrieve from the rest api endpoint declared for this track in the registry, alle entities corresponding
        * to the current intervalParams from the reference, and entity filters declared for this track.
        *
        * The data engine associated to a track should run only one server request at a given time.
        *
        *
        * @param {object} newIntervalParams new intervalParams describing the interval for which the track must be updated
        * @param {object} newTrackParams object containing the filter with the filtered entities
        * that must be displayed in this track.
        * @param {function} success Function executed if the server call is successfull.
        *    This function will be called with following parameters : requestParams, requestEntities, requestCategories, filtersStatus
        * @param {function} error Function executed if the server call resulted in an error.
        *    This can be a server error or a user error (missing information)
             Called with parameters : requestParams, errorMessage, status
        * @returns {object} promise The promise used to query the data from the server
        * @todo Server can decide to send a different interval from the interval asked.
        *       Ex: peptype api will send all the data for a protein (as its simplier for VCF retrieval)
        *       So it needs a way to say to the data engine that it returns more data than requested to avoid
        *       unecessary MOVE queries
        * @todo It could have a parallel mechanism for automatic PREL requests.
        */
      DataEngine.prototype.serverUpdate = function( newIntervalParams, newTrackParams, success, error ){

        var self = this;

        // https://stackoverflow.com/questions/35375120/cancelling-ongoing-angular-http-requests-when-theres-a-new-request
        // http://www.frontcoded.com/angular-cancel-$http-request.html
        self.cancelCurrentRequest();

        var promise = DataEngine.getPromise( self.url, newIntervalParams, newTrackParams, self.canceller );

        DataEngine.updatingCount++;
        self.serverCalls++;

        promise.then(
          function(res){
            //console.log("OK: "+res.config.url+" : "+JSON.stringify(res.config.data)+" : "+JSON.stringify(_.keys(res.data.entities)));

            // The reference data that was used to retrieve this track
            var intervalParams = res && res.config && res.config.data && res.config.data.intervalParams;
            var requestEntities = res.data.entities;
            var requestCategories = res.data.categories;
            var filtersStatus = res && res.data && res.data.about && res.data.about.filtersStatus;

            DataEngine.updatingCount--;
            self.serverCalls--;
            return success && success( intervalParams, requestEntities, requestCategories, filtersStatus );
          },
          function(res){

            var status = ( res.status == DataEngine.USER_ERROR_CODE ) ? StatusEnum.WARNING : StatusEnum.ERROR;
            var intervalParams = res && res.config && res.config.data && res.config.data.intervalParams;
            var errorMessage = ( res.data && res.data.message ) || res.statusText || 'Can not connect to server';

            DataEngine.updatingCount--;
            self.serverCalls--;

            // Differentiate between canceled by us or server not accessible
            if( res && res.config && res.config.timeout && res.config.timeout.leraDataEngineCancel ){
               $log.debug("Cancelled unfinished request.");
               return;
            }

            return error && error( intervalParams, errorMessage, status );
          }
        );

        return promise;
      };

      /**
       * @desc Cancels the current server request. Should be executed when a new request is executed,
       * to cancel any previous requests.
       * Each canceller promise is associated to a single request.
       * @private
       */
      DataEngine.prototype.cancelCurrentRequest = function(){
        if(this.canceller){
          // To differentiate between when we cancel a request and when the server does not responds
          this.canceller.promise.leraDataEngineCancel = true;
          this.canceller.resolve("cancelled");
          this.canceller = $q.defer();
        }
      };


      /**
       * @desc Stores a copy of the parameters used for the last update
       * @param {object} params
       */
      DataEngine.prototype.setLastRequestedParams = function( params ){
        this.lastRequestedParams = _.cloneDeep( params );
      };

      /**
       * @desc Returns all entities from the current virtual pool.
       * Those are all entities retrieved from the server since the last JUMP,
       * and correspond to an interval which is smaller or equal to the current interval
       * @return {object} entities
       */
      DataEngine.prototype.getVirtualEntities = function(){
        return this.virtualEntities;
      };

      /**
       * @param {object} requestInterval Interval (start,end) for which entities must be displayed on the track
       * @return {boolean} true if the requested interval intersects with current virtual interval
       */
      DataEngine.prototype.isIntersect = function( requestInterval ){
        return requestInterval && this.virtualParams && this.virtualParams.start && this.virtualParams.end &&
          ! ( requestInterval.end < this.virtualParams.start || requestInterval.start > this.virtualParams.end );
      };

      /**
       * @param {object} requestInterval Interval (start,end) for which entities must be displayed on the track
       * @return {boolean} true if the requested interval is inside the current virtual interval
       */
      DataEngine.prototype.isInside = function( requestInterval ){
        return requestInterval && this.virtualParams && this.virtualParams.start && this.virtualParams.end &&
          requestInterval.start >= this.virtualParams.start && requestInterval.end <= this.virtualParams.end;
      };

      /**
       * @desc Returns the intervalParams defining the current virtual pool of entities
       * @return {object} virtualParams
       */
      DataEngine.prototype.getVirtualParams = function(){
        return this.virtualParams;
      };

      /**
       * @return {object} Last parameters requested by the track
       */
      DataEngine.prototype.getRealParams = function(){
        return this.lastRequestedParams;
      };

      /**
       * @desc Returns true if data for this track is currently retrieved from the server
       * @return {boolean} isLoading
       */
      DataEngine.prototype.isLoading = function(){
        return this.serverCalls !== 0;
      };



      /**
       * @desc prints to the console log the virtual and real intervals
       * @param {}
       */
      DataEngine._logParams = function(mode, virt, real, prefix){
        // $log.debug( "\t"+(prefix || "")+" AFTER "+mode+" VIRT("+virt.start+"-"+virt.end+") REAL("+real.start+"-"+real.end+")");
      };

      /**
       * @param {object} position : the object for which we want to know if it is outside the given interval
       * @param {object} interval
       */
      DataEngine.isOutsideInterval = function( position, interval ){

        if( !interval || _.isUndefined(interval.start) || _.isUndefined(interval.end) ||
                 !position || _.isUndefined(position.start) ){ return false; }

        return (
          ( position.start < interval.start && ( _.isUndefined(position.end) || position.end < interval.start )) ||
          ( position.start > interval.end && ( _.isUndefined(position.end) || position.end > interval.end ))
        );
      };

      /**
       * @desc When possible reduces the interval sent to the server to retrieve only missing entities/components
       * To use only for a MOVE update.
       * @param {object} queryInterval Parameters (start, end, etc) for the new interval to retrieve
       * @return {list} virtualInterval Interval (start, end, etc) corresponding to the current virualInterval
       */
      DataEngine.adaptQueryInterval = function( queryInterval, virtualInterval ){

        // We do not need to check if both intervals intersect, because the MOVE update is only called if the do.
        var adaptedIntervals = [];

        if( queryInterval.start < virtualInterval.start ){
          var newLeftInterval = _.cloneDeep( queryInterval );
          newLeftInterval.end = virtualInterval.start;
          adaptedIntervals.push( newLeftInterval );
        }

        if( queryInterval.end > virtualInterval.end ){
          var newRightInterval = _.cloneDeep( queryInterval );
          newRightInterval.start = virtualInterval.end;
          adaptedIntervals.push( newRightInterval );
        }

        return adaptedIntervals;
      };

      /**
       * @desc Created a $http promise, allowing to the intervalParams from the reference and filters from the track
       * @param {string} url URL to which send the reference and track params
       * @param {object} intervalParams data object
       * @param {object} trackParams object
       * @param {object} canceller (optional) Used to cancel a previous request
       * @return {object} The promise to the server call to retrieve track data
       */
      DataEngine.getPromise = function( url, intervalParams, trackParams, canceller ){

        var options = {
          method: 'POST',
          url   : url,
          data  : _.cloneDeep({
            intervalParams: intervalParams,
            trackParams: trackParams // ex: samples, ignore homo_ref, experience...
          })
        };

        if( canceller ){ options.timeout = canceller.promise; }

        return $http( options );
      };


      /**
       * @desc Returns the extreme bounds of visible entities in the current view
       * @param {object} entities
       * @param {object} filter The filter contains category filters, used to decide wheter considered objects
       * are visible or not. If they are not visible, their bounds are not taken into consideration.
       * @param {object} interval (optional) Objects outside the interval are not taken into consideration
       * @return {object} (start, end) of bounds of submitted entities (min start and max end from all entities)
       */
      DataEngine.getEntitiesBounds = function( entities, filter, interval ){
        var self = this;
        var starts = _.map( _.filter( entities, function(e){
          return filter.isEntityVisible(e) && ! DataEngine.isOutsideInterval( e, interval);
        } ), 'start');
        var ends = _.map( _.filter( entities, function(e){
          return filter.isEntityVisible(e) && ! DataEngine.isOutsideInterval( e, interval);
        } ), 'end');
        return {
          start: _.min( starts ),
          end  : _.max( ends )
        };
      };

      /**
       * @desc Returns true if no track is corrently using his dataEngine to update its data.
       * @return {boolean} allTracksStandby
       */
      DataEngine.allTracksStandby = function(){
        return DataEngine.updatingCount === 0;
      };


      return DataEngine;
    }

})();

(function() {
  'use strict';

    FilterFactory.$inject = ["$http", "$q", "$sce", "toastr", "$log", "$interpolate"];
    angular.module('lera').factory('Filter', FilterFactory);

    function FilterFactory( $http, $q, $sce, toastr, $log, $interpolate ){

      /**
       * @constructor
       * @memberOf lera.factories
       * @desc Filters decide which entities and components should be displayed.
       *
       * ENTITY FILTERS : are a list of entities ids sent to the server, to retrieve only requested entities.
       *
       * CATEGORY FILTERS : Are applied after the date has been retrieved from the server, and before it is displayed.
       * The category filters store key/value pairs characterising the entities or components that should not be displayed.
       *
       * How this works :

       1. The server send entities and components, each containing a data object with key/value pairs, ex : { biotype: protein_coding }

       2. The server also sends a hash of categories which indicates which keys from data should be displayed on the overview.
       For example an overview categories hash { biotype => "entity" } means that we should build an overview chart using biotype keys
       in data objects from entities. The resulting overview object will look like
        { biotype: [{ name: protein_coding, y: 6, visible: true, color: red}]}

       3. Finally, when the user click on the overview legend, this adds a category filters, meaning that entities or components
       with a given key/values in their data object should not be displayed. The resulting object is like :
       { biotype: [{ name: protein_coding, y: 6, visible: true, color: red}]} ---(click)--> { biotype: { protein_coding: true } }

       To decide if an entity or component is visible, we sill compare

       { biotype: protein_coding } AND { biotype: { protein_coding: true } }

       *
       */
      var Filter = function( filters ){

        /**
         * @desc For categories the order does not matter but we want
         * to be able to rapidly check if a filter exists in a given category +> hash
         * Note that we put in categories the categories we want to HIDE, not to show !
         * ex: this.categories = {"AARA007746-RA":{"missense_variant":true}};
         */
        this.categories = {};

        /**
         * @desc For entities order matters, as we want to display entities
         * in the order they were asked for => list

         * @todo For each sample found by autosuggest, store the keywords that allowd to find it
         * @todo add properties : found/not found. entities become a list of objects instead of strings, can complicate save/load
         */
        this.entities = [];


        if( filters ){
          _.merge(this, filters);
        }

      };

      Filter.prototype.hasCategoryFilters = function(){
        return _.some( this.categories, function(cat){ return !_.isEmpty(cat);} );
      };

      Filter.prototype.hasEntityFilters = function(){
        return ! _.isEmpty( this.entities );
      };

      /**
       * @return {object} Returns ALL declared entity filters. (ex: sample_id:[sam1, sam2, sam3]})
       */
      Filter.prototype.getEntityFilters = function(){
        return this.entities;
      };


      /**
       * @desc An entity should not be drawn only if one of its data fields is in the filters and the value for this data field corresponds
       * to the filter value
       * @return {boolean} visible
       */
      Filter.prototype.isEntityVisible = function( entity ){
        return Filter.isVisible( this.categories, entity.data );
      };

      /**
       * @desc A component should not be drawn only if one of its data fields is in the filters and the value for this data field corresponds
       * to the filter value
       * @return {boolean} visible
       */
      Filter.prototype.isComponentVisible = function( component ){
        return Filter.isVisible( this.categories, component.data );
      };


      /**
       * @static
       * @desc An entity/component is usually hidden if a value from data exists in the filter
       * or if it has no value for a given entity
       * @param catFilter {object} Ex : {biotype:{protein_coding:1}}
       * @param data {object} Ex : {biotype:'protein_coding'}
       * @returns {boolean} True only if no category filter was declared for any data key,
       * or if the corresponding data value corresponds to filter values
       * @todo return ! _.some(categories, function(cat, catName){ return _.isUndefined(data && data[catName]) && cat['NONE'] === level });
       * @todo use the level (component/entity) instead of 1 in categories, ex: {biotype:{protein_coding:'entity'}}
       */
      Filter.isVisible = function(catFilter, data){
        return ! _.some(data, function(value,category){ return catFilter[category] && catFilter[category][value]; });
      };

      /**
       * @desc Updates the filters that should be shown for a category
       * @param {string} category ex: biotype
       * @param {object} filters ex: {"protein_coding":1}
       */
      Filter.prototype.setCategoryFilters = function(category, filters){
        if(category && filters){
          if(_.isEmpty(filters)){
            filters = {};
          }
          this.categories[category] = filters;
        }
      };

      /**
       * @desc Returns a hash of filters corresponding to active legend items after user click
       * @static
       * @param {object} point Clicked point
       * @param {list} points List of point objects used by highcharts to model chart data
       * @todo category[point.name] = point.level (entity/component) instead of category[point.name] = true;
       */
      Filter.legendAsFilters = function( clickedPoint, points ){

        return _.transform( points, function( category, point ){
          // visibility of current legend item will be trigerred AFTER the call to this function
          var hidden = (clickedPoint === point) ? point.visible : !point.visible;
          if( hidden ){
            category[point.name] = true;
          } else{
            delete category[point.name];
          }

        }, {});
      };

      /**
       * @desc Removes all category filters. Categories will still be displayed in the overview,
       * but no entities and components will be hidden
       */
      Filter.prototype.resetCategoryFilters = function(){
        var self = this;
        _.each( self.categories, function(cat, catName){
          self.categories[catName] = {};
        });
      };

      /**
       * @desc Wheter we should hide entities and components corresponding to this category
       * @return {boolean} true if no filters are declared for this category,
       * or if filters are declared and meta_name is NOT in declared filters
       */
      Filter.prototype.isCategoryFiltered = function( category, meta_name ){

        return !!this.categories[category] && !!this.categories[category][meta_name];
      };

      /**
       * @desc Updates the entity filters with the new value and updates the track, but only if new filters are different form the old ones
       * @param {list} entities
       * @todo return true if new filters different from old ones
       */
      Filter.prototype.updateEntityFilters = function(entities){
        this.entities = entities;
      };

      /**
       * @desc Stores a list of entities ids that were requested but not found during last server request.
       * This list in only updated when the server returns information about notFound entities (it can be an empty list)
       * @param {object} filtersStatus Object received from the server listing found and not found entities
       */
      Filter.prototype.setNotFoundFilters = function( filtersStatus ){

        if( filtersStatus && filtersStatus.notFound ){
          this.entitiesNotFound = filtersStatus.notFound;
        }
      };

      /**
       * @desc Returns the a string of requested entities that were not found, separated by commas,
       * or undefined if all requested entities were found.
       * @return {string} notFoundEntities
       */
      Filter.prototype.getNotFoundEntities = function(){
        if( this.entitiesNotFound && _.size(this.entitiesNotFound) ){
          return _.join(this.entitiesNotFound, ", ");
        }
      };

      return Filter;
    }

})();

(function() {
    'use strict';

        TrackCard.$inject = ["$mdDialog", "toastr", "$log", "$sce", "StateManager", "Overview", "Dialogs"];
    angular.module('lera').directive('trackCard', TrackCard);

    function TrackCard($mdDialog, toastr, $log, $sce, StateManager, Overview, Dialogs) {
        return {
            restrict: 'EA',
            templateUrl: 'track/track.template.html',
            scope: {
              track:'='
            },
            link: function(scope, element){

              $(element).on('click', '.message .fa-edit', function(){
                Dialogs.editEntityFiltersDialog( scope.track );
              });

            },
            controller: ["$scope", function($scope) {

                $scope.StateManager = StateManager;
                $scope.Overview = Overview;
                $scope.Dialogs = Dialogs;

                // TODO watch for reference.cursorPosition and change coordinated of the red line div accordingly
                // absolute coordinated for the page, given track start and end height, and width = track.start + cursorPosition
                // TODO line draw in very transparent red, to see what is behind

                $scope.setView = function() {
                    $scope.track.optimiseView();
                };

                $scope.remove = function() {
                    $scope.track.removeFromReference();
                };

                $scope.changeDisplayOrder = function( direction ){
                    $scope.track.shiftTrack( direction );
                };

                $scope.canMoveUp = function(){
                    return ! $scope.track.getReference().isFirstTrack( $scope.track );
                };

                $scope.canMoveDown = function(){
                    return ! $scope.track.getReference().isLastTrack( $scope.track );
                };

                $scope.export = function() {
                    $scope.track.exportTrack();
                };

                $scope.getHeader = function() {
                  var header = '';

                  var headerTemp = $scope.track.headerTemplate;
                  var params = $scope.track.getIntervalParams();
                  if( headerTemp && ! _.isEmpty( params ) ){
                    header += headerTemp({ ref: params, track: $scope.track });
                  }

                  var cp = $scope.track.getReference().getCursorPosition();
                  if( !_.isUndefined(cp)){ header += " : " + cp; }

                  return header;
                };

                $scope.getLegend = function(){
                  if( $scope.track.legendTemplate ){
                    return $sce.trustAsHtml( $scope.track.legendTemplate({ ref: $scope.track.getIntervalParams(), track: $scope.track }) );
                  }
                };

            }]
          };
        }
})();

(function() {
  'use strict';

  TrackFactory.$inject = ["$window", "toastr", "$log", "$interpolate", "UpdateModeEnum", "Filter", "DataEngine", "Overview"];
    angular.module('lera').factory('Track', TrackFactory);

    function TrackFactory( $window, toastr, $log, $interpolate, UpdateModeEnum, Filter, DataEngine, Overview ){

      /**
       * @class
       * @memberOf lera.factories
       * @desc A track displays entities (composed of components) in a coordinate system defined by
       * a reference. When the interval in the coordinate system is updated, the track calls
       * a REST API URL to retrieve the data corresponding to the new interval.
       *
       * Note that you should not use this constructor directly, instead use the addTrack method from the
       * StateManager
       *
       * @param {string} trackId The (arbitrary) autogenerated id
       * @param {string} trackType Type of the track declared in the registry (ex: ensTranscript)
       * @param {object} trackConf  JSON object from the registry describing a track. It is structured as :
       * @example
       * {
       *  name : 'transcripts'
       *  description : "This track shows transcript entities"
       *  urlTemplate : "template to call the rest api url"
       *  reference : "name of the track that serves as x-axix reference, for example the genome of a species"
       *  morphsTo : "name of the track into which should replace this track after double-click on an entity"
       * }
       * @param {object} reference Reference object this track depends on
       * @param {object} filters object used to restrict the entities shown for this track
       * @throws Exception if no url or no reference supplied
       */
    	var Track = function( trackId, trackType, trackConf, reference, filters ){

        this.trackId = trackId;

        /**
         * @desc Number used to determine in which order we should display the tracks.
         * In difference of the trackId, this value can be changed to change the order.
         */
        this.order = trackId;

        /**
         * @desc Optional label that the user may add to a specific track
         * to differenciate it from others tracks of the same type
         */
        this.label = undefined;


        /**
         * @desc Hash associating a comment to an entity/component, whcih is shown and can be edited
         * on the contextmenu, and can be saved and loaded with the save file, or sent by GET/POST config
         * @todo NotYetImplemented
         */
        this.comments = {};

        /**
         * @desc Hash of selected entities and components for this track
         */
        this.selected = {
          entities: {},
          components: {}
        };

        /**
         * @example ensTranscript, ensGenotype
         */
        this.trackType = trackType;

        /**
         * @desc A copy of reference data used to retrieve entities for this track, for the last successful server request
         */
        this.intervalParams = {};

        if( !reference ){ throw "Undefined reference for track "+trackId; }

        /**
         * @desc A reference is mandatory to position track entities
         */
        this.reference = reference;

        /**
         * @desc The corresponding registry json object basically serves as prototype for each Track object created
         */
        _.defaultsDeep(this, trackConf);

        if( !this.url ){
          throw "A REST API URL is mandatory for each track, but was not declared in the registy for track type"+trackType;
        }

        /**
         * @desc We transform declared links into template objects. These links will be shown inside the contextmenu
         */
        if( this.links ){
          _.forOwn(this.links, function( url, name, o ){
            if(url){
              o[name] = $interpolate(url);
            }
          });
        }


        /**
         * @desc The legend will explain colors and symbols used for entities and components of the track
         */
        this.legendTemplate = this.legendTemplate && $interpolate( this.legendTemplate );

        /**
         * @desc The header will briefly present the track (what kind of entities it displays, for which species, etc)
         */
        this.headerTemplate = this.headerTemplate && $interpolate(this.headerTemplate);

        if( this.export && this.export.fileNameTemplate ){
          /**
           * @desc File name generated for the file with exported data from the track
           */
          this.export.fileNameTemplate = $interpolate(this.export.fileNameTemplate);
        }

        /**
         * @desc Number of updates ran for this track since it was created
         */
        this.lastUpdate = 0;

        /**
         * @desc Default dimensions of the track visualisation. Can be overriden in the registry.
         */
        this.height = this.height || 200;

        // this.minDistanceBetweenEntities = this.minDistanceBetweenEntities || 0;

        /**
         * @desc Filters are used to hide components or entities based on their attributes,
         * or select entities that must be displayed on the track.
         */
        this.filter = new Filter( filters  );

        /**
         * @desc The data engine is responsible for updating data for this track.
         * It decided for example if a server request is necessary, and retrieves data from the server if needed.
         */
        this.dataEngine = new DataEngine( this.url );

        /**
         * @desc Overview charts are shown on the sidebar as statistics about the data,
         * and is used to filter the entities/components that must be displayed.
         */
        this.overview = new Overview();
    	};

      Track.SELECTED_ENTITY_COLOR = "#FF1744";
      Track.INACTIVE_ENTITY_COLOR = "#B0BEC5";
      Track.ADAPTATIVE_HEIGHT_FACTOR = 20;
      Track.ENTITY_HEIGHT_FACTOR = 1.5;

      /**
       * @desc Number of export queries currently running (eg: number of files currently downloaded)
       */
      Track.runningExports = 0;

      /**
       * @return Statistics about retrieved entities in a format compatible with highcharts
       */
      Track.prototype.getOverview = function(){
        return this.overview.get();
      };

      /**
       * @param {string} entityId It can be transcript stable ids, sample ids, etc...
       * @returns {object} entity The entity object corresponding to the entity id.
       */
      Track.prototype.getEntity = function( entityId ){
        return this.getEntities()[entityId];
      };

      /**
       * @desc An entity is real if it has a start and an end defined.
       * For example transcripts are real entities, while samples are not.
       * @param {object} entity
       * @return {boolean} isReal
       */
      Track.isRealEntity = function( entity ){
        return entity && entity.start && entity.end;
      };

      /**
       * @desc An entity is virtual if it has no start and an end defined.
       * For example transcripts are real entities, while samples are virtual.
       * @param {object} entity
       * @return {boolean} isVirtual
       */
      Track.isVirtualEntity = function( entity ){
        return ! Track.isRealEntity( entity );
      };

      /**
       * @desc Returns the official/public properties of the entity as a hash.
       * Components are not part of the properties
       * @param {string} entityId
       * @return {hash} properties
       */
      Track.prototype.getEntityProperties = function( entityId ){
        var e = this.getEntity(entityId);
        return e && e.data;
      };


      /**
       * @return {array} lanes Returns the lanes containing current entities
       * filtered by current category filters.
       * Each lane contains non overlapping entities
       * @todo Possibility to create groups of lanes... We will have a list of lists of lists
       * @deprecated Replaced by getLaneGroups
       */
      Track.prototype.getLanes = function(){

        var self = this;
        return Track.createLanes( self.getEntities(), function(e){
          return self.filter.isEntityVisible(e) &&
            ! DataEngine.isOutsideInterval( e, self.getIntervalParams() );
        });
      };


      /**
       * @desc Intermediary step towards implementing groups of lanes
       * @param {}
       * @return {}
       */
      Track.prototype.getLaneGroups = function(){
        var self = this;
        return Track.createGroupedLanes( self.getEntities(), function(e){
          return self.filter.isEntityVisible(e) &&
            ! DataEngine.isOutsideInterval( e, self.getIntervalParams() );
        }, self.lanesGroupConfig);
      };

      Track.countLanes = function( groups ){
        return _.sumBy( groups, function(group){ return _.size(group && group.lanes);} );
      };

      /**
       * @param {string} entityId
       * @param {string} componentId It can be the variation id if the entityId is the sample id.
       * The component id must only be unique for an entity
       * @returns {object} component The component matching componentId from the entity entityId
       */
      Track.prototype.getComponent = function( entityId, componentId ){
        return this.getEntity(entityId) && this.getEntity(entityId).components[componentId];
      };

      /**
       * @desc Returns the official/public properties of the entity as a hash.
       * Components are not part of the properties
       * @param {string} entityId
       * @return {hash} properties
       */
      Track.prototype.getComponentProperties = function( entityId, componentId ){
        var c = this.getComponent( entityId, componentId );
        return c && c.data;
      };



      /**
       * @desc Uses the Data Engine to update the pool of entities for the new parameters of this track.
       * When several update requests are run in the same time,
       * only the data from the latest request is displayed
       *
       * @param {object} intervalParams New parameters used to update the track
       * @param {function} callback Function that will be executed after the update is complete.
       * Note that the function will be executed whether the update is successfull or not.
       * @param {string} mode : JUMP or MOVE. Used by data.engine to decide if a server call is necessary
       * Note that this is the SUGGESTED mode, and the Data Engine will decide which is the best mode to use.
       * Asking for a JUMP bascially tells the Data Engine that you want to clean the existing pool of entities.
       * @returns {object} promise The promise used to query the data from the server
       */
      Track.prototype.update = function( intervalParams, callback, mode ){
        var self = this;
        var trackParams = self.getTrackParams();

        return self.dataEngine.update( intervalParams, trackParams,

          function( params, entities, overviewCategories, filtersStatus ){

            self.intervalParams = params;
            self.entities = entities;
            self.overview.set( entities, overviewCategories, self.filter, params );
            self.errorMessage = "";
            self.redraw(); // To trigger rendering update
            self.filter.setNotFoundFilters( filtersStatus );

            return callback && callback();
          },
          function ( params, errorMessage ){

            // Even if server errored, we still synchronise track interval state with ref interval state
            self.entities = {};
            self.intervalParams = params;
            self.errorMessage = errorMessage;
            self.overview.reset();
            self.redraw();
            return callback && callback();
          },
          mode
        );

      };


      /**
       * @desc Exports data from this track.
       * Feature available only if track declaration has an export url
       * Note: exports with all entity filters, not only the active page...
       */
      Track.prototype.exportTrack = function(){

        if( ! (this.export && this.export.url) ){
          $log.warn('This track has no export capabilities');
          return;
        }

        var fileName = this._createExportFileName( this.export.fileNameTemplate );

        Track.runningExports++;
        var promise = DataEngine.getPromise( this.export.url, this.getIntervalParams(), this.getTrackParams() );
        promise.then(
          function(res){
            Track.runningExports--;
            try{
              var export_data = res.data;
              var blob = new Blob([ export_data ], {type: "text/plain;charset=utf-8"});
              saveAs(blob, fileName);

            } catch(err){
              toastr.error("Could not save exported track data: "+err);
            }
          },
          function(res){
            Track.runningExports--;
            toastr.error( (res.data && res.data.message), "Could not export track data", {allowHtml: true});
          }
        );
      };

      /**
       * @desc Supplies all track attributes to a template defined in the registry,
       * to generate the name for the export file for this track.
       * @private
       * @param {object} template
       * @return {string} fileName
       */
      Track.prototype._createExportFileName = function( template ){

        if( template ){
          var fileName = template( this );
          return _.replace( fileName, / +/g, '_');
        }

        return "myExportedTrack.txt";
      };

      /**
       * @return {boolean} True if this track can be exported as a file (BED/CSV/etc...)
       */
      Track.prototype.hasExportCapability = function(){
        return this.export && !!this.export.url;
      };

      /**
       * @return {boolean} True if the entity has a start and end declared
       */
      Track.prototype.canCenterToEntity = function( entityId ){
        var entity = entityId && this.getEntity( entityId );
        return entity && entity.start && entity.end;
      };

      /**
       * @desc Updates the interval parameters of the reference to the start/end of this entity
       * We can obvisouly only center to entities that have a start/end declared.
       * @param {string} entityId Id of the entity on which the track should center
       */
      Track.prototype.centerToEntity = function( entityId ){
        var entity = entityId && this.getEntity( entityId );
        if( !entity ){ throw "Could not center to entity because no entity found for id "+entityId; }
        this._centerTo( entity );
      };

      /**
       * @desc Updates the interval parameters of the reference to the start/end of this component
       * We can obvisouly only center to components that have a start/end declared.
       * @param {string} entityId Id of the entity containing the component on which the track should center
       * @param {string} componentId Id of the component on which the track should center
       */
      Track.prototype.centerToComponent = function( entityId, componentId ){

        var component = componentId && this.getComponent( entityId, componentId );
        if( !component ){ throw "Could not center to component because no component found for ids "+entityId+","+componentId; }
        this._centerTo( component );
      };

      /**
       * @desc Updates the interval parameters of the reference to the start/end of this feature (entity or component)
       * We can obvisouly only center to features that have a start/end declared.
       * @param {object} feature : entity or component
       */
      Track.prototype._centerTo = function( feature ){

        var self = this;
        if( !feature ){ return; }
        if( !feature.start || !feature.end ){
          $log.warn("Could not center to "+feature.name+" because it has no start/end declared");
          return;
        }

        if( self.getReference().intervalMatch(feature.start, feature.end) ){
          $log.info("Already centered.");
          return;
        }

        $log.debug("Centering to "+feature.name);
        self.getReference().updateInterval( feature.start, feature.end, UpdateModeEnum.MOVE );
      };

      /**
       * @desc Updates the filters that should be shown for a category
       * @param {string} category ex: biotype
       * @param {object} filters ex: {"protein_coding":1}
       */
      Track.prototype.setCategoryFilters = function(category, filters){
        this.filter.setCategoryFilters(category, filters);
        this.redraw();
      };

      /**
       * @desc Removes all the category filters set by the user
       * @todo Only if category filters not empty
       * @todo DO NOT USE YET. legend labels are not yet consistently updated
       */
      Track.prototype.resetCategoryFilters = function(){
        this.filter.resetCategoryFilters();
        this.overview.setAllVisible();
        this.redraw();
      };

      /**
       * @desc Updates the entity filters with the new value and updates the track,
       * @todo update only if new filters are different form the old ones
       * @param {list} entities
       * @param {function} Called when track update has finished
       */
      Track.prototype.updateEntityFilters = function(entities, callback){
        this.filter.updateEntityFilters( entities );
        this.update( this.getIntervalParams(), callback, UpdateModeEnum.JUMP );
      };


      /**
       * @return {object} Category filters (ex: biotype:{protein_coding:1}})
       */
      Track.prototype.getCategoryFilters = function(){
        return this.filter.categories;
      };


      /**
       * @desc Returns all entities from the virtual pool of entities.
       * Overview and Display must filter the entities/components to keep only those located in the current interval
       *
       * @todo Filter by interval (entities with no start/end declarecd AND entities with start/end intersectiong with interval).
       */
      Track.prototype.getEntities = function(){
        return this.entities;
      };


      /**
       * @param {integer} start
       * @param {integer} end
       * @return {boolean} True if given start and end match current track interval
       */
      Track.prototype.intervalMatch = function( start, end ){
        return start == this.getIntervalParams().start && end == this.getIntervalParams().end;
      };


      /**
       * @desc Fully shows all entities currently visible onscrren.
       */
      Track.prototype.optimiseView = function(){
        var newBounds = this.getEntitiesBounds();
        this.getReference().updateInterval( newBounds.start, newBounds.end, UpdateModeEnum.MOVE );
      };


      /**
       * @desc Jumps to the location of the given entity
       * @param {object} entity
       * @todo Implement
       */
      Track.prototype.jumpToEntity = function( entity ){
        if( entity.start && entity.end ){
          this.getReference().updateInterval( entity.start, entity.end, UpdateModeEnum.MOVE );
        } else {
          $log.info("Can not jump to a virtual entity");
        }
      };

      /**
       * @return {object} Start and end which corresponds to
       * the bounds of currently loaded entities (min start and max end from all entities)
       */
      Track.prototype.getEntitiesBounds = function(){
        return DataEngine.getEntitiesBounds( this.getEntities(), this.filter, this.getIntervalParams() );
      };


      /**
       * @returns {object} reference The reference of this track.
       */
      Track.prototype.getReference = function(){
        return this.reference;
      };

      /**
       * @desc Returns the type of track by which the current track should be replaced in the new reference
       * For egample the genotype track is replaced by the peptype track
       * when switching from genome to protein reference.
       * @param {string} refType
       * @return {string} trackType
       */
      Track.prototype.getMorphReplacement = function( refType ){
        return this.morphing && this.morphing.replacement && this.morphing.replacement[refType];
      };

      /**
       * @desc Returns all morph targets declared for this track
       * @return {list} targets
       */
      Track.prototype.getAllMorphTargets = function(){
        return this.morphing && this.morphing.morphsTo;
      };

      /**
       * @desc Removes this track from its reference
       */
      Track.prototype.removeFromReference = function(){
        this.reference.removeTrack( this.trackId );
      };

      /**
       * @desc Changes the display order of this track, by adding 'direction' to its index
       * @param {integer} direction
       * @return {boolean} true if track was shifted
       */
      Track.prototype.shiftTrack = function( direction ){
        return this.getReference().shiftTrack( this, direction );
      };

      /**
       * @desc Changes count of the last update
       * @deprecated Use redraw() instead
       */
      Track.prototype.setLastUpdate = function(){
        this.redraw();
      };

      /**
       * @desc Forces the redraw of the track.
       * We do this by changing the count of the last update.
       * This allows to avoid checking the complete complex and big structure
       * of entities to see if we need to update the display of the track.
       */
      Track.prototype.redraw = function(){
        // $log.debug("Redrawing track");
        this.lastUpdate++;
      };

      /**
       * @return {string} error message returned by the last (unsuccessful) server call
       */
      Track.prototype.getErrorMessage = function(){
        return this.errorMessage;
      };

      /**
       * @param {object} feature Any object that has start and and properties
       * @return {boolean} true if both start and end of the feature are outside interval bounds
       */
      Track.prototype.isOutsideInterval = function( feature ){
        return DataEngine.isOutsideInterval( feature, this.getIntervalParams() );
      };

      /**
       * @return {object} Parameters (such as start/end) used for the last successful update. These
       * parameters correspond to the interval and entities shown on the screen for this track.
       */
      Track.prototype.getIntervalParams = function(){
        return this.intervalParams;
      };

      /**
       * @desc Builds an object containing track specific parameters (ex: filters)
       * that should be sent to the server.
       * @return {object} trackParams
       */
      Track.prototype.getTrackParams = function(){
        // TODO entities undefined if list empty ??
        // TODO selected entities in addition to filters
        return {
          filters : { entities: this.filter && this.filter.getEntityFilters() }
        };
      };

      /**
       * @desc Changes the current interval parameters for this track
       * @param {object} params
       */
      Track.prototype.setIntervalParams = function( params ){
        this.intervalParams = params;
      };

      /**
       * @desc Retrieves a given interval parameter
       * @example getIntervalParam('start') retrieves the current start of the track
       * @param {string} param
       * @return {variable} paramValue
       */
      Track.prototype.getIntervalParam = function(param){
        return this.intervalParams && this.intervalParams[param];
      };

      /**
       * @returns {StatusEnum} last stable (before loading) status of track data : ok, warning, or error
       */
      Track.prototype.getStatus = function(){
        return this.dataEngine.status;
      };

      /**
       * @return {boolean} isLoading True if the data engine for this track is currently making a
       * server request
       */
      Track.prototype.isLoading = function(){
        return this.dataEngine.isLoading();
      };

      /**
       * @return {string} size of the current interval in the coordinate system
       */
      Track.prototype.getDisplayableIntervalSize = function(){
          var intervalSize = this.getIntervalSize();
          return Track.convertSize(intervalSize);
      };


      /**
       * @return {string} size of a tick (drawn to help visualise the iseze of objects drawn in the track)
       * for the current interval in the coordinate system
       */
      Track.prototype.getDisplayableTickSize = function(){
        var tickSize  = this.getIntervalSize() / this.getReference().getTicksCount();
        return Track.convertSize(tickSize);
      };

      /**
       * @desc Transforms a number into an easy to understand approximation.
       * Code adapted from https://gist.github.com/lanqy/5193417
       * @param {float} interval psotitive number
       * @return {string} approximation K bp, M bp, etc..
       */
      Track.convertSize = function( interval ){
        var sizes = ['', 'K', 'M', 'G'];
        // TODO better rounding, better precision after comma if interval is between 1 and 2 to avoid having a
        interval = _.floor(interval);
        if (interval === 0) return 'less than 1';
        var i = Math.floor(Math.log(interval) * Math.LOG10E / 3);
        if (i === 0){ return interval; }
        if (i > sizes.length ){ return interval; }
        return _.round( (interval / Math.pow(1000, i)) ) + sizes[i];
      };

      /**
       * @return {boolean} minReached True only if min exists and start === min
       */
      Track.prototype.minReached = function(){
        return this.getIntervalParam('start') === this.getIntervalParam('min');
      };

      /**
       * @return {boolean} minReached True only if min exists and start === min
       */
      Track.prototype.maxReached = function(){
        return this.getIntervalParam('end') === this.getIntervalParam('max');
      };

      /**
       * @desc Returns the size of the interval as end - start + 1
       * @return {interger}
       */
      Track.prototype.getIntervalSize = function(){
        return this.getIntervalParam('end') - this.getIntervalParam('start') + 1;
      };

      /**
       * @desc Returns the color that will be used to paint a given component.
       * Note that for efficiency reasons to handle cases with thousands of components to display,
       * if a component has a color declared this color will be used, and no other checks will be performed.
       * @param {object} component
       * @param {string} entityId
       * @param {object} entity
       * @return {string} hex color used to paint the component
       */
      Track.prototype.getColor = function( component, entityId, entity ){

        return component && component.color ||
          this.getSelectedEntityColor( entityId ) ||
          this.getEntityOverviewColor( entity) ||
          entity && entity.color ||
          this.color ||
          this.reference.color;
      };

      /**
       * @desc Returns the color of selected entities if the given entity is selected, undefined otherwise
       * @param {string} entityId
       * @return {string} colr
       */
      Track.prototype.getSelectedEntityColor = function( entityId ){
        return this.isSelected( entityId ) && Track.SELECTED_ENTITY_COLOR;
      };

      /**
       * @desc Uses the color of the category in overview to which the entity belongs, to paint the entity,
       * return undefined if useOverviewForEntityColor is not declared in the registry for this track
       * or no color in the overview corresponds to the v
       * @param {object} entity
       * @return {string} color
       * @todo EXPERIMENTAL !!
       */
      Track.prototype.getEntityOverviewColor = function( entity ){
        var field = this.useOverviewForEntityColor;
        var value = field && entity && entity.data &&
          entity.data[field];
        return value && Overview.getColor( value );
      };

      /**
       * @desc Returns true if a feature with the given id was selected
       * @param {string} id of an entity or component
       * @return {boolean} isSelected
       */
      Track.prototype.isSelected = function( id ){
        return this.selected.entities[id] || this.selected.components[id];
      };

      /**
       * @return {list} Returns the list of entities corresponding to selected ids. If an id does not
       * correspond to any entity in the current entities pool, it is ignored.
       * @TODO Need testing
       */
      Track.prototype.getSelectedEntities = function(){
        var self = this;
        return self.selected && self.selected.entities && _.transform( self.selected.entities, function(res, val, id){
          var e = self.entities[id];
          return e && res.push(e);
        }, []);
      };

      /**
       * @desc Empties the hash of selected entities
       */
      Track.prototype.deselectAll = function(){
        if( self.selected && self.selected.entities ){
          self.selected.entities = {};
        }
      };

      /**
       * @desc Adds an entity to the hash of selected entities
       * @param {string} entityId
       */
      Track.prototype.selectEntity = function( entityId ){
        this.selected.entities[entityId] = 1;
      };

      /**
       * @desc Selects all loaded entities which names start with 'query'
       * @param {string} query
       */
      Track.prototype.autoselectEntity = function( query ){

        var self = this;
        _.each( self.getEntities(), function( entity, entityId ){
          if( entity && entity.name && _.startsWith(entity.name, query) ){
            self.selectEntity( entityId );
          }
        });
      };

      /**
       * @desc Adds or removes an entity from the hash of selected entities
       * @param {string} entityId
       */
      Track.prototype.triggerSelectedEntity = function( entityId ){
        if( ! entityId ) return;

        if( !this.selected.entities[entityId] ){
          this.selected.entities[entityId] = true;
        } else {
          delete this.selected.entities[entityId];
        }

        this.redraw();
      };

      /**
       * @desc Jumps to the location containing all the selected entitites
       */
      Track.prototype.jumpToSelected = function(){

        if ( this.hasSelectedEntities() ){

          var selected = this.getSelectedEntities();
          var bounds = DataEngine.getEntitiesBounds( selected, this.filter );
          if( bounds.start && bounds.end ){
            this.getReference().updateInterval( bounds.start, bounds.end, UpdateModeEnum.MOVE );
          } else {
            $log.warn("All selected entities are virtual (i.e.  do not have proper start/end)");
          }

        } else {
          $log.warn("Selected entities no more in reach");
          toastr.warn("Selected entities no more in reach");
        }
      };

      /**
       * @return {boolean} true if at least one id of the hash of the selected entities corresponds
       * to an entity of the current pool of entities.
       */
      Track.prototype.hasSelectedEntities = function(){
        var self = this;
        var selected = this.selected && this.selected.entities;
        return _.some( selected, function( v, eId){ return self.entities && self.entities[eId]; } );
      };


     /**
       * @desc Transforms the REAL x coordinate of an entity in the reference, to a VIEW coordinate
       * Adjusts positions to make sure that juxtaposing elements stay juxtaposed (no holes between)
       */
      Track.prototype.toViewAdjusted = function( start, end ){
        var adj = this._getAdjustment();
        var aStart = this.toView( start ) - adj;
        var aEnd = this.toView( end ) + adj;
        var refWidth = this.reference.getWidth();

        // Artificially extending coordinates if the view is less than three pixels
        if( aEnd - aStart < 3 ){
          aStart -= 1.5;
          aEnd += 1.5;
        }

        var newstart = aStart < 0 ? 0 : aStart;
        var newEnd = aEnd > refWidth ? refWidth : aEnd;

        return {start: newstart, end: newEnd};
      };


      /**
       * @desc Transforms the REAL x coordinate of an entity in the reference,
       * to VIEW coordinate. The minimum coordinate is 0.
       * @param {interger} x coordinate to transform
       * @return {float} The coordinate inside the reference transformed to a coordinate inside the view. Returns 0 if x < start
       * @deprecated When view interval is much bigger than reference interval, there are holes between juxtaposing elements. Use toViewAdjusted instead.
       */
      Track.prototype.toView = function( x ){

        if( _.isUndefined(x) ){
          throw("toView needs valid x parameter");
        }

        return Track.transform( this.getIntervalParam('start'), this.getIntervalParam('end'), 0, this.reference.getWidth(), x );
      };

      /**
       * @private
       * @desc Transforms a position from an interval into another. If the position is out of bounds of the first interval,
       * it is set to the bounds of the second interval
       * @param {integer} x Position to transform
       * @return {float} position form one interval transformed into another.
       */
      Track.transform = function( oldStart, oldEnd, newStart, newEnd, x ){

        if( x < oldStart ){ return newStart; }
        if( x > oldEnd ){ return newEnd; }
        return newStart + ( x - oldStart ) / ( oldEnd - oldStart ) * ( newEnd - newStart );
      };


      /**
       * @desc
       * Calculates the adjustment necessary to keep juxtaposing elements still juxtaposing after the transformation
       * The main idea of qdjustment is to keep elements that are adjacent in reference, still adjecent in the view
       * @private
       * @return {integer}
       */
      Track.prototype._getAdjustment = function(){
        return _.round(this.reference.getWidth() / ( (this.getIntervalParam('end') - this.getIntervalParam('start')) * 2 ));
      };


      /**
       * @desc Opens in a new tab the first link declared for the track, for the given entityId
       * @param {string} entityId
       */
      Track.prototype.openLink = function( entityId ){
        if( entityId ){
          var link = this.links && _.values(this.links)[0];
          if( link ){
            var url = link({
              ref       : this.getIntervalParams(),
              entity_id : encodeURIComponent( entityId )
            });
            $window.open( url );
          } else {
            toastr.warning("This track does not supports external links");
          }
        }
      };


      /**
       * @desc Returns the height of this track. Given the value of the adaptativeHeight flag,
       * the height can be fixed (entities height change given their number to fill the available space)
       * or dynamic (depending on the number of entities)
       * @param {integer} nbLanes Number of lanes that will be displayed in the track
       * @return {float} height
       */
      Track.prototype.getHeight = function( nbLanes ){

        nbLanes = nbLanes || 0;

        if( this.adaptativeHeight ){
          return ( nbLanes ) * Track.ADAPTATIVE_HEIGHT_FACTOR;
        } else {
          return this.height;
        }
      };

      /**
       * @desc Returns the filter declared for this track
       * @return {object} filter
       */
      Track.prototype.getFilter = function(){
        return this.filter;
      };


      /**
       * Calculates the height of lanes displayed on the track and the vertical
       * space between lanes
       * @param {float} height
       * @param {integer} nbLanes
       * @returns {object} {entityHeight:..., separationHeight:...}
       * @todo Should be ( nbLanes - 1 ) to not draw margins
       */
      Track.getVerticalInterval = function( height, nbLanes ){

        var separationHeight = height / ( nbLanes );
        var entityHeight = _.round( separationHeight / Track.ENTITY_HEIGHT_FACTOR );
        return { entityHeight: entityHeight, separationHeight: separationHeight };
      };


      /**
       * @desc Creates a list of groups, each group containing a list of lanes.
       * Groups are ordered by value of the field by which they were grouped
       * @param {object} entities
       * @param {function} entityFilter (optional) Function that will decide if an entity should be displayed
       * @param {string} lanesGroupConfig (optional) Property used to create groups of lanes
       * @return {}
       */
      Track.createGroupedLanes = function( entities, entityFilter, lanesGroupConfig ){

        var groups = [];

        if( !entities ){ return groups; }
        var objects = _.map( entities, function(entity, entityId){ return { entity: entity, entityId: entityId }; });

        _.each( Track.createGroups( entities, lanesGroupConfig ), function( group, groupName ){

          groups.push( {
            name: groupName,
            conf: lanesGroupConfig,
            lanes: Track.createLanes( group, entityFilter )
          } );
        });

        groups = _.orderBy( groups, 'name' );
        return groups;
      };

      /**
       * @desc Groups together entities with the same value for the data property defined by lanesGroupConfig.field
       * @example
       * { e1: {data:{strand:'+'}}, e2: {data:{strand:'-'}} } => { '+' : { e1: {data:{strand:'+'}} }, '-' : {e2: {data:{strand:'-'}} }
       * @param {hash} entities Hash of entities as { entityId : entity }
       * @param {object} lanesGroupConfig Configuration for grouping together entities
        EX: { "field": "strand" }
       * @return {hash} groups
       */
      Track.createGroups = function( entities, lanesGroupConfig ){
        var groupField = lanesGroupConfig && lanesGroupConfig.field;
        return _.transform( entities, function(groups, entity, entityId){
          var groupName = entity && entity.data && entity.data[groupField];
          if( ! _.has(groups, groupName) ){ groups[groupName] = {}; }
          groups[groupName][entityId] = entity;
        }, {});
      };

      /**
       * @desc Creates the minimum number of lanes with non intersecting entities.
       * Virtual entities ( which have no start/stop declared ) are added each on a separate lane)
       * Logic inspired by https://github.com/chmille4/Scribl/blob/master/src/Scribl.track.js
       * @param {object} entities
       * @param {function} entityFilter (optional) Function that will decide if an entity should be displayed
       * @return {list} lanes List of lanes. each lane is a list of objects. ex: [ [{}, {}], [{}] ]
       * @todo TEST
       */
      Track.createLanes = function( entities, entityFilter ){

        var lanes = [];
        if( !entities ){ return lanes; }
        var objects = _.map( entities, function(entity, entityId){ return { entity: entity, entityId: entityId }; });
        // FILTER
        if( entityFilter){ objects = _.filter( objects, function(o){ return entityFilter( o.entity );}); }
        // SORT
        objects = _.sortBy( objects, 'entity.end' );

        _.each( objects, function( object ){
          var lane = Track.laneForEntity( lanes, object.entity );
          lane.push( object );
        });

        return lanes;
      };

      /**
       * @desc Finds the best lane to which add this entity. If no existing lane is compatible,
       * adds a new lane to lanes and returns it.
       * @param {list} lanes
       * @param {object} entity Entity
       * @param {integer} separator (optional) Minimal required space between two entities so they appear on the same lane
       * @return {list} lane
       */
      Track.laneForEntity = function( lanes, entity, separator ){

        separator = separator || 0;
        // Can the entity be added to an existing lane ?
        var lane = _.find( lanes, function(lane){
          var lastLaneObject = _.last(lane);
          var lastLaneEntity = lastLaneObject && lastLaneObject.entity;
          return ! _.isUndefined( entity.start ) && ! _.isUndefined(lastLaneEntity.end) && entity.start > (lastLaneEntity.end + separator);
        });

        if( lane ){ return lane; } else {
          lane = [];
          lanes.push(lane);
          return lane;
        }
      };


      /**
       * @desc Returns the list of entity ids corresponding to current interval and filters.
       * The list of entities to display will be either the entities inside the interval or
       * the list of entity filters if submitted
       * @param {array} filter (optional) The filter contains
       * an ordered list of entity filters, used to render entities is specific order, and a hash of category filters,
       * used to show/hide entities and components given the content of their data hash
       * @param {object} interval (optinal) If provided, only entities that are not outside the interval are added to lanes
       * @return {list} entityIds
       * @todo Is currently not compatible with entity synonyms !
       * @deprecated Using createLanes now
       */
      Track._getOrderedFilteredEntityIds = function( entities, filter, interval ){

        if( !entities ){ return []; }

        var entityFilter = filter && filter.getEntityFilters();
        if( _.isEmpty(entityFilter) ){
          return _.transform( entities, function(res, entity, entityId){
            if( ! DataEngine.isOutsideInterval( entity, interval) ){
              res.push(entityId);
            }
          }, []);
        } else {
          return entityFilter;
        }
      };

      return Track;
  }

})();

(function() {
    'use strict';

    angular.module('lera').directive('tutorial', Tutorial);

    function Tutorial() {
        return {
            restrict: 'E',
            templateUrl: 'tutorial/tutorial.template.html',
            controller: ["$scope", "$timeout", function( $scope, $timeout ) {

                $scope.clickLoad = function(){
                  $timeout(function(){
                    $('#load-session').click();
                  },0);
                };

                $scope.clickAddRef = function(){
                  $timeout(function(){
                    $('#add-ref').click();
                  },0);
                };

            }]
        };
    }

})();

(function() {
  'use strict';

    EaseJsRendererFactory.$inject = ["$log", "Track", "Reference", "ContextMenuState"];
    angular.module('lera').factory('Renderer', EaseJsRendererFactory);

    function EaseJsRendererFactory($log, Track, Reference, ContextMenuState){


      var MAX_FONT_SIZE = 20;

      /**
       * @class
       * @memberOf lera.factories
       * @desc using easeJS library to render tracks

       This renderer bascially draw a table with rows and columns. Each lane/entity corresponds
       to a row, then components are drawn on each column.
       The difference with a real table is that columns (their position, number and width)
       is different/independant for each row.

       @todo Show a cursor:pointer when cursor over clickable element, on mouse-move event.
       Can not use directly EaselJS functionality for this as it is too exact and too expensive.
       Some alternative solutions : https://stackoverflow.com/questions/20350199/easeljs-best-way-to-detect-collision
                                    http://www.createjs.com/tutorials/HitTest/

      @todo EaseJs may add non negligeable weight in terms of memory (lots of Shape objects for genotypes)
      The problem is that to allow interactions we must store the bounding rectangle of each genotype...


       * @param browserElement {object} DOM node into which we will draw the 'genome' browser svg
       * @param browserElement {object} DOM node into which we will draw the entities names
       * @param track {object} Track from which we will draw the entities
       */
      var EaseJsRenderer = function( browserElement, entitiesElement, track){

        this.track = track;
        this.fontColor = 'black';

        $(browserElement).html("<canvas></canvas>");
        $(entitiesElement).html("<canvas></canvas>");

        this.browserStage = new createjs.Stage( $(browserElement).find('canvas')[0] );

        this.entitiesStage = new createjs.Stage( $(entitiesElement).find('canvas')[0] );
        this.entitiesStage.enableMouseOver();

      };

      /**
       * @desc Number of currently executed renderings
       */
      EaseJsRenderer.renderingsCount = 0;

      /**
       * @desc Links this renderer with a new track
       */
      EaseJsRenderer.prototype.switchTrack =function( track ){
        this.track = track;
      };

      /**
       * @desc Creates and adds the HTML for the browser and enames divs
       */
    	EaseJsRenderer.prototype.update = function(){
    		// Creates levels of non overlapping entities (calculated on real positions), then sends the y position to each entity
        // If there is only one entity, it is shown in the vertical center and takes all the height/width of the this.view
        var self = this;
        EaseJsRenderer.renderingsCount++;

        var laneGroups = self.track.getLaneGroups();
        var nbLanes = Track.countLanes( laneGroups );
        var trackHeight = this.track.getHeight( nbLanes );
        var verticalInterval = Track.getVerticalInterval( trackHeight, nbLanes );

        this.browserStage.removeAllChildren();
        this.browserStage.update();
        this.entitiesStage.removeAllChildren();
        this.entitiesStage.update();

        this.browserStage.canvas.width = Reference.width;
        this.browserStage.canvas.height = trackHeight;

        this.entitiesStage.canvas.width = 200; // TODO put all constant sized in lera conf
        this.entitiesStage.canvas.height = trackHeight;

        // Note that lanes have already been filtered to keep only entities/components corresponding
        // to the current reference interval

        var y = verticalInterval.separationHeight / 2;

        // DRAWING GROUPS
        _.each( laneGroups, function( group ){

          self.drawGroupBackground( group, y, verticalInterval.separationHeight );

          var lanes = group && group.lanes || [];

          // DRAWING LANES
      		_.each(lanes, function(lane, i){

              // DRAWING ENTITIES
              _.each(lane, function( o ){
                  self._renderEntity( o.entityId, o.entity, y, verticalInterval.entityHeight );

                  // Entity name is rendered in the legend only if there is one entity per lane
                  if(_.size(lane) === 1){
                    self._renderEntityName( o.entityId, o.entity, y, verticalInterval.entityHeight );
                  }
              });

              y += verticalInterval.separationHeight;
          });

        });

        this.browserStage.update();
        this.entitiesStage.update();
        EaseJsRenderer.renderingsCount--;
    	};

      /**
       * @todo If number of characters > limit, elipse the entity name (or use both lines ? or change lineHeight ?)
       */
      EaseJsRenderer.prototype._renderEntityName = function( entityId, entity, y, lineHeight ){

        // TODO Displaying entity ID only if one entity by lane !!

        // return if entity.name is empty
         if( !entity || !entity.name ){ return; }

         var self = this;
         var fontSize = lineHeight < MAX_FONT_SIZE ?_.round( lineHeight ) : MAX_FONT_SIZE;
         var legendEntityName = new createjs.Text( entity.name, fontSize+"px Arial", this.fontColor );

         legendEntityName.x = 10;
         legendEntityName.y = y+(fontSize/2);
         legendEntityName.textBaseline = "alphabetic";
         legendEntityName.cursor = "pointer";

         var hit = new createjs.Shape();
         var hw = legendEntityName.getMeasuredWidth();
         var hh = legendEntityName.getMeasuredHeight();
  			 hit.graphics.beginFill("#000").drawRect( 0, -hh, hw, hh );
  			 legendEntityName.hitArea = hit;

         legendEntityName.on("click", function(ejsEvent) {
           self.track.openLink( entityId );
         });

         this.entitiesStage.addChild(legendEntityName);
      };


      /**
       * @desc renders the components of the entity. If the entity has no components (ex: gene) then treats the
       * entity as a component.
       * @param {string} entityId
       * @param {object} entity
       * @param {integer} y : Height at which the components for this entity will be drawn
       * @param {integer} entityHeight : height of the entity to draw and all included components
       * @todo Display name next to entity ?
       */
      EaseJsRenderer.prototype._renderEntity = function( entityId, entity, y, entityHeight ){

        var self = this;
        // TODO Warning if entity has no components and no start
        var components = _.isEmpty(entity.components) ? [entity] : entity.components;

        _.each(components, function(component, componentId){
          if( !self.track.isOutsideInterval( component ) && self.track.filter.isComponentVisible(component)){
            var color = self.track.getColor( component, entityId, entity );
            self._renderComponent( entityId, componentId, component, y, entityHeight, color );
          }
        });
      };



       /**
        * @desc This function decides of the shape of each component, and transforms real positions of components into view coordinates
        * @param {string} entityId
        * @param {string} componentId
        * @param {object} component
        * @param {float} y
        * @param {float} entityHeight : maximum height allowed for this entity
        * @param {string} color
        */
      EaseJsRenderer.prototype._renderComponent = function( entityId, componentId, component, y, entityHeight, color ){

        var refStart = component.start;

        // Start is the only mandatory parameter
        if( _.isUndefined( refStart ) ){
          throw("start is mandatory for all components. It is missing from component "+componentId+" of entity "+entityId);
        }

        var refEnd =  component.end || refStart; // If not end declared for component, we use start as end

        // TODO Maybe special simple calculations for a unique point ? (start == end) Would be faster ??
        var pos   = this.track.toViewAdjusted( refStart, refEnd );
        var start = pos.start;
        var end   = pos.end;
        var shape = component.shape || this.track.defaultComponentShape;

        // TODO don't render component if something already drawn at the exact same position(s)

        if( !shape ){
          $log.warn("No shape declared for component");
          return;
        }

        var canvasComponent = new createjs.Shape();

        canvasComponent.on("click", function(ejsEvent) {
          var e = ejsEvent && ejsEvent.nativeEvent;
          ContextMenuState.showInfoMenu(e, entityId, componentId);
        });

        // Height can be dynamic, to create histograms for example
        if( ! _.isUndefined( component.heightFactor ) ){
          entityHeight *= component.heightFactor;
        }

        // We can set max height for entities
        if( this.track.maxEntityHeight && entityHeight > this.track.maxEntityHeight ){
          entityHeight = this.track.maxEntityHeight;
        }

        var canvasComponentGraphics = canvasComponent.graphics.beginFill(color);

        switch( shape ){

          case "thick-full" : canvasComponentGraphics.s(color).ss(entityHeight).mt(start, y).lt(end, y);break;
          case "thin"       : canvasComponentGraphics.s(color).mt(start, y).lt(end, y);break;
          default           : $log.warn("Invalid shape "+shape);
        }

        this.browserStage.addChild(canvasComponent);
      };


      /**
       * @desc Draws the ticks directly on the canvas to separate the groups
       * @param {float} y Vertical position where to draw the line
       */
      EaseJsRenderer.prototype.drawSeparationLine = function( y ){

        var separationLine = new createjs.Shape();
        var tickCount = this.track.getReference().getTicksCount();
        var tickWidth = Reference.width / tickCount;
        var ticks = _.range( 0, Reference.width, tickWidth );

        _.each( ticks, function( w, i ){
          var color = i % 2 ? '#0097A7' : '#80DEEA';
          var separationLineGraphics =
            separationLine.graphics
            .ss(2)
            .s(color)
            .mt(w, y)
            .lt(w+tickWidth, y);
        });

        this.browserStage.addChild( separationLine );
      };

      /**
       * @desc Draws a background rectangle for a group
       * @param {object} group Configuration object for this group
       * @param {float} y Horizontal position on which entities are drawn
       * @param {float} separationHeight Maximum height that an entity can occupy
       */
      EaseJsRenderer.prototype.drawGroupBackground = function( group, y, separationHeight){
        // Draw background ONLY if more than 1 non empty groups AND if color declared for value colors: {'+':'red', '-':'blue'}
        var groupRect = EaseJsRenderer.calculateGroupRect( group, y, separationHeight, Reference.width );
        if( groupRect ){
          var background = new createjs.Shape();
          background.graphics
            .beginFill( groupRect.c )//group.conf.color )
            .drawRect( groupRect.x, groupRect.y, groupRect.w, groupRect.h );
          this.browserStage.addChild( background );

          this.drawSeparationLine( groupRect.y +1 );
        }
      };

      /**
       * @desc Creates the parameters for a background rectangle for a group
       * @param {}
       * @return {object} A {x,y,w,h,c} object describing the background rectangle to draw, or undefined if
       * no configuration available to draw a background for this group
       */
      EaseJsRenderer.calculateGroupRect = function( group, y, separationHeight, canvasWidth ){
        if( _.isEmpty(group && group.lanes)){ return; }
        var color = group.name && group.conf && group.conf.colors && group.conf.colors[group.name];
        if( color ){
          var backgroundStart = y - separationHeight / 2;
          var backgroundHeight = _.size( group.lanes ) * separationHeight;
          return { x:0, y: backgroundStart, w: canvasWidth, h: backgroundHeight, c: color };
        }
      };

      return EaseJsRenderer;
    }

})();

(function() {
  'use strict';

      TrackRenderer.$inject = ["$sce", "Renderer", "$rootScope", "$log"];
  angular.module('lera').directive('trackRenderer', TrackRenderer);

  function TrackRenderer( $sce, Renderer, $rootScope, $log ){

      return {
          restrict: 'EA',
          templateUrl: 'track/renderer/track.renderer.template.html',

          link: function(scope, element){

            /**
             * @desc The renderer is expected to implemet following method :
             * - update() : recreate/update the DOM
             */
            scope.renderer = createNewRenderer();

            scope.ticks = _.range(scope.track.getReference().getTicksCount() );

            scope.$watch("track.lastUpdate", updateView, true);

            // If a track replaces another track at the same position (id),
            // this directive becomes associated with a different track.
            // We need to recreate a renderer with a pointer to this news track.
            scope.$watch("track", function(newValue, oldValue){
              if(newValue !== oldValue){
                scope.renderer = createNewRenderer();
              }
            });

            /**
             * @desc Rerenders the canvas
             */
            function updateView(newValue, oldValue){

              if(newValue!==oldValue){
                  var startTime = performance.now();

                  scope.renderer.update();

                  var renderingTime = performance.now() - startTime;
                  if( renderingTime > 100 ){ $log.warn("Rendering calculations done in "+renderingTime+" ms."); }
              }
            }


            /**
             * @return {object} A new Renderer instance with the current scope view and track.
             */
            function createNewRenderer(){
              return new Renderer(
                $(element).find(".browser")[0],
                $(element).find(".enames")[0],
                scope.track
              );
            }

          }

        };
      }
})();

angular.module('templates').run(['$templateCache', function($templateCache) {$templateCache.put('autosuggest/autosuggest.template.html','<div layout="row">\n  <md-input-container>\n      <label>{{field.name}}</label>\n      <input\n        type="text"\n        autocomplete="off"\n        autocorrect="off"\n        spellcheck="false"\n        ng-model="rawValue"\n        placeholder="{{field.name}}"\n        uib-typeahead="item as item.identifier for item in getSuggestions($viewValue)"\n        typeahead-append-to-body="true"\n        typeahead-editable="false"\n        typeahead-wait-ms="100"\n        typeahead-min-length="triggerLength"\n\n        typeahead-template-url="autosuggest/autosuggest_menu.template.html"\n        typeahead-on-select="onSelect($item, $model, $label, $event)"\n        typeahead-no-results="noResults"\n        typeahead-loading="isLoading"\n        />\n        <div class="hint" ng-bind-html="getHint()"></div>\n  </md-input-container>\n</div>\n\n<!-- Does not works great woth genome location, as only one match :\n    typeahead-select-on-exact="true"\n-->\n');
$templateCache.put('autosuggest/autosuggest_menu.template.html','<a href tabindex="-1" class="autosuggest-item">\n  <span class="sa-command">\n    {{match.model.commandName}}\n  </span>\n  <span class="sa-identifier">\n    {{match.model.identifier}}\n  </span>\n  <div class="sa-detail" ng-bind-html="match.model.description"></div>\n </a>\n');
$templateCache.put('contextmenu/contextMenu.template.html','\n<md-card ng-if="ContextMenuState.isDisplayable()"\n\t\t\t\t class="card context-menu"\n\t\t\t\t ng-style="{top:ContextMenuState.getTopPos()+\'px\', left:ContextMenuState.getLeftPos()+\'px\'}">\n\t <md-card-title>\n\t\t <md-card-title-text>\n\t\t\t <span class="md-headline context-menu-name">{{ContextMenuState.getTitle()}}\n\t\t\t\t <i class="fa fa-map-pin selected"\n\t\t\t\t \tng-class="ContextMenuState.isEntitySelected() ? \'is-selected\' : \'\'"\n\t\t\t\t\tng-show="canSelectEntity()"\n\t\t\t\t\tng-click="ContextMenuState.triggerSelectedEntity()"\n\t\t\t\t\ttitle="Click to select/unselect this entity"></i>\n\t\t\t </span>\n\t\t\t <span class="description">{{ContextMenuState.getEntity().description}}</span> <!-- When available... -->\n\t\t </md-card-title-text>\n\t </md-card-title>\n\t <md-card-content>\n\t\t <ul>\n\t\t\t <li ng-repeat="p in ContextMenuState.getEntityProperties()"><span class="info-key">{{p.field}} :</span> {{p.value}}</li>\n\t\t </ul>\n\n\t\t <div class="component-info">\n\t\t\t <span\n\t\t\t \t\tng-if="!!ContextMenuState.getComponent().name"\n\t\t\t \t\tng-click="centerToComponent()" class="md-button"\n\t\t\t\t\ttitle="Click to zoom into this component">\n\t\t\t\t\t{{ContextMenuState.getComponent().name}}\n\t\t\t </span>\n\t\t\t <ul>\n\t\t\t\t <li ng-repeat="p in ContextMenuState.getComponentProperties()"><span class="info-key">{{p.field}} :</span> {{p.value}}</li>\n\t\t\t </ul>\n\t\t </div>\n\n\t </md-card-content>\n\t <md-card-actions layout="row" layout-align="end center">\n\n\t\t <a\n\t\t \t\tng-repeat="(name,url) in ContextMenuState.getTrack().links"\n\t\t \t\tclass="md-button link"\n\t\t \t\ttarget="_blank"\n\t\t    ng-href="{{createUrl(url)}}"\n\t\t\t\tng-attr-title="View in {{name}}">\n\t\t \t\t{{name}}</a>\n\n\t\t <md-button\n\t\t \t\tclass="morph-button"\n\t\t \t\tng-repeat="morphTarget in ContextMenuState.getMorphTargets()"\n\t\t\t\tng-click="morphToEntity(morphTarget.trackType)"\n\t\t\t\t>\n\t\t\t\t{{morphTarget.name}}</md-button>\n\n\t\t<md-button\n\t\t\tclass="fork-button"\n\t\t\tng-repeat="forkTargetConf in ContextMenuState.getTrack().forksTo"\n\t\t\ttitle="Fork to..."\n\t\t\tng-click="forkToEntity(forkTargetConf)"\n\t\t\t>{{forkTargetConf.name}}</md-button>\n\n\t\t <md-button\n\t\t \t\tng-show="canCenterToEntity()"\n\t\t \t\tng-click="centerToEntity()"\n\t\t\t\tng-attr-title="Center to {{ContextMenuState.getEntity().name}}">\n\t\t \t\tZoom</md-button>\n\n\t </md-card-actions>\n</md-card>\n');
$templateCache.put('dialogs/add_reference.template.html','<md-dialog class="add-ref-dialog">\n  <md-toolbar>\n        <div class="md-toolbar-tools">\n          <h2>Add a new reference</h2>\n          <span flex></span>\n          <md-button class="md-icon-button fa fa-close" ng-click="close()" aria-label="close"></md-button>\n        </div>\n      </md-toolbar>\n\n  <md-dialog-content>\n    <md-content layout-gt-sm="row" layout-padding>\n\n      <md-radio-group ng-model="refType">\n        <md-radio-button\n          ng-repeat="(rt, conf) in availableRefTypes"\n          ng-value="rt"\n          class="md-primary select-ref-type">\n          {{conf.name}}\n        </md-radio-button>\n      </md-radio-group>\n\n      <autosuggest\n        ng-repeat="field in getReferenceDataFields(refType)"\n        field="field"\n        value="intervalParams[field.name]"\n        params="intervalParams"\n      ></autosuggest>\n\n   </md-content>\n   <md-content>\n     <div class="dialog-description">{{getSelectedConfProperty(\'description\')}}</div>\n   </md-content>\n\n  </md-dialog-content>\n\n  <md-dialog-actions layout="row">\n\n    <md-button class="confirm-add-ref" ng-click="addReference()" ng-show="refType">\n      Add\n    </md-button>\n  </md-dialog-actions>\n\n</md-dialog>\n');
$templateCache.put('dialogs/add_track.template.html','<md-dialog>\n  <md-toolbar>\n    <div class="md-toolbar-tools">\n      <h2>Add a new track</h2>\n      <span flex></span>\n      <md-button class="md-icon-button fa fa-close" ng-click="close()" aria-label="close"></md-button>\n    </div>\n  </md-toolbar>\n\n  <md-dialog-content>\n    <md-content layout-gt-sm="row" layout-padding>\n\n      <md-radio-group ng-model="trackType">\n        <md-radio-button\n          ng-repeat="(trackType, conf) in StateManager.registry.getTrackTypesForRefType(ref.refType)"\n          ng-value="trackType">\n          {{conf.name}}\n        </md-radio-button>\n      </md-radio-group>\n\n   </md-content>\n   <md-content>\n     <div class="dialog-description">\n       <img ng-if="!!getSelectedConfProperty(\'image\')" ng-src="{{getSelectedConfProperty(\'image\')}}" alt="track image" />\n       {{getSelectedConfProperty(\'description\')}}\n     </div>\n   </md-content>\n\n  </md-dialog-content>\n\n  <md-dialog-actions layout="row">\n\n    <md-button ng-show="trackType" ng-click="addTrack()" >\n      Add\n    </md-button>\n  </md-dialog-actions>\n\n</md-dialog>\n');
$templateCache.put('dialogs/edit_filters.template.html','<md-dialog class="edit-filters-dialog">\n  <md-toolbar>\n        <div class="md-toolbar-tools">\n          <h2>{{getTitle()}}</h2>\n          <span flex></span>\n          <md-button class="md-icon-button fa fa-close" ng-click="close()" aria-label="close"></md-button>\n        </div>\n      </md-toolbar>\n\n  <md-dialog-content style="max-width:600px;">\n\n    <md-content layout-gt-sm="row" layout-padding layout-wrap>\n\n      <div class="tags">\n        <div class="tag" ng-repeat="entityFilter in filtersToEdit">\n          <div class="tag-content">\n            <strong>{{entityFilter}}</strong>\n          </div>\n          <i class="fa fa-close tag-remove" ng-click="removeTag($index)"></i>\n        </div>\n\n        <div ng-show="hasMultipleTags()"\n          ng-click="removeAllTags()"\n          title="Remove all added filters"\n          class="fa fa-trash remove-all-tags">\n        </div>\n      </div>\n\n      <autosuggest\n        field="track.filtersAutocomplete"\n        value="selectedFilter"\n        params="intervalParams"\n        values="filtersToEdit"\n      ></autosuggest>\n\n   </md-content>\n  </md-dialog-content>\n\n  <md-dialog-actions layout="row">\n\n    <md-button class="confirm-edit-filters" ng-click="updateEntityFilters()" >\n      View {{track.filtersAutocomplete.name}}\n    </md-button>\n  </md-dialog-actions>\n\n</md-dialog>\n');
$templateCache.put('dialogs/fork.template.html','<md-dialog class="add-fork-dialog">\n  <md-toolbar>\n    <div class="md-toolbar-tools">\n      <h2>Add {{forkTargetConf.name}}</h2>\n      <span flex></span>\n      <md-button class="md-icon-button fa fa-close" ng-click="close()" aria-label="close"></md-button>\n    </div>\n  </md-toolbar>\n\n  <md-dialog-content>\n    <md-content layout-gt-sm="row" layout-padding>\n\n      <autosuggest\n        field="forkTargetConf"\n        value="forkValue"\n        force-object-value="true"\n        params="params"\n        show-example="true"\n      ></autosuggest>\n\n    </md-content>\n  </md-dialog-content>\n\n  <md-dialog-actions layout="row">\n\n    <md-button class="confirm-add-fork" ng-click="fork()" >\n      Add\n    </md-button>\n  </md-dialog-actions>\n\n</md-dialog>\n');
$templateCache.put('dialogs/search.template.html','<md-dialog>\n  <md-toolbar>\n    <div class="md-toolbar-tools">\n      <h2>Change location</h2>\n      <span flex></span>\n      <md-button class="md-icon-button fa fa-close" ng-click="close()" aria-label="close"></md-button>\n    </div>\n  </md-toolbar>\n\n  <md-dialog-content>\n    <md-content layout-gt-sm="row" layout-padding>\n\n      <autosuggest\n        field="searchableConf"\n        value="dataToEdit[searchable]"\n        params="dataToEdit"\n      ></autosuggest>\n\n    </md-content>\n  </md-dialog-content>\n\n  <md-dialog-actions layout="row">\n\n    <md-button ng-click="updateReferenceData()" >\n      Jump\n    </md-button>\n  </md-dialog-actions>\n\n</md-dialog>\n');
$templateCache.put('lera/lera.template.html','<div class="lera">\n\n  <reference-card\n    ng-repeat="ref in StateManager.getOrderedReferences()"\n    ref="ref"\n    data-ref-id="{{ref.refId}}"\n    ng-attr-id="{{ref.refId}}"\n    ></reference-card>\n\n</div>\n\n<tutorial ng-if="isAppEmpty()"></tutorial>\n');
$templateCache.put('overview/overview.template.html','<div class="side-menu slide-{{Overview.getSidebarState()}}" >\n\n      <div class="filters-title">\n          <span>{{StateManager.getActiveTrack().name}} OVERVIEW</span>\n\n           <!-- <i class="fa fa-eraser" ng-click="StateManager.getActiveTrack().resetCategoryFilters()"></i> -->\n\n          <i aria-label="Close overview" class="fa fa-close close-overview" ng-click="Overview.closeSidebar()"></i>\n      </div>\n      <span class="overview-tips">Click on categories to show/hide them in the browser</span>\n\n      <div layout="flex">\n\n      </div>\n\n      <md-divider></md-divider>\n\n      <!-- Showing data from meta category of the currently selected track -->\n      <md-content class="charts" >\n\n          <!-- A hichart chart for each category in meta of the track\n               When selecting legend items, this updates the related track by sending selected filters to the rest api\n         -->\n         <div ng-repeat="track in StateManager.getAllTracks()"\n              ng-show="track === StateManager.getActiveTrack()"\n              class="track-overview">\n           <highcharts ng-repeat="(category,data) in track.getOverview()"\n                       data="data"\n                       category="category"\n                       track="track">\n           </highcharts>\n         </div>\n\n      </md-content>\n\n</div>\n');
$templateCache.put('reference/reference.template.html','<div class="reference-card" layout="row">\n\n <div class="ref-control" layout="column"\n ng-style="{\'background-color\':ref.color}"\n ng-attr-title="Reference {{ref.refId}}">\n\n  <i ng-click="Dialogs.showAddTrackDialog(ref)" class="fa fa-plus" aria-label="Add track" title="Add track"></i>\n  <span flex></span>\n  <i ng-click="removeReference(refId)" class="fa fa-close" aria-label="Remove reference" title="Remove reference"></i>\n </div>\n\n <!-- TODO : "Inverse reference" button to inverse the rendering of track components (start on the right, end on the left)\n      to make it easier to compare proteins with - strand transcripts, and orthologous transcripts that are on different strands\n  -->\n\n <div class="tracks" layout="column">\n\n  <div class="ref-tracks-controls-wrapper" layout="row">\n    <div class="ref-tracks-controls-line" ng-style="{\'border-color\' : ref.color}" flex></div>\n     <div class="ref-tracks-controls" ng-style="{\'border-color\':ref.color, \'color\': ref.color}">\n\n       <a class="fa fa-minus" ng-click="ref.zoom(-200)" title="Zoom out"></a>\n       <span>Zoom</span>\n       <a class="fa fa-plus" ng-click="ref.zoom(200)" title="Zoom in"></a>\n\n       <i ng-click="Dialogs.showSearchDialog(ref)" ng-if="ref.searchable" class="fa fa-crosshairs" aria-label="Change location" title="Change location"></i>\n     </div>\n     <div flex></div>\n   </div>\n\n   <div ng-show="isReferenceEmpty()" class="empty-ref-help">\n     This is an empty {{ref.name}} reference. Please click on <i class="fa fa-plus"></i> to add tracks.\n   </div>\n\n   <track-card\n     ng-repeat="track in ref.getOrderedTracks()"\n     track="track"\n     layout="column"\n     class="track-card"\n     ng-class="[track.getStatus(), \'loading_\'+track.isLoading()]"\n     data-track-id="{{track.trackId}}"\n     >\n   </track-card>\n\n </div>\n</div>\n');
$templateCache.put('toolbar/toolbar.template.html','<div class="md-toolbar-tools">\n     <a href="/" title="Go to VectorBase website"><img src="images/vectorbase.png"/></a>\n     <div hide-xs hide-sm style="font-size:16px;">\n       VectorBase\n       <div style="font-size: 10px; color:grey; max-width:250px; font-weight:normal;">\n         Bioinformatics Resource for Invertebrate Vectors of Human Pathogens</div>\n      </div>\n\n     <!-- <i ng-click="Overview.triggerSidebar()" class="fa fa-bars" title="Show overview"></i> -->\n\n      <!-- ADD REF -->\n      <md-button\n        id="add-ref"\n        ng-click="Dialogs.showAddReferenceDialog()"\n        aria-label="add reference"\n        title="Add a new (genomic, protein, etc) reference" >\n        Add new reference\n      </md-button>\n\n     <!-- LOAD-->\n     <i type="file" ngf-select="StateManager.load($file, $invalidFiles)"\n          accept="json" ngf-max-size="1MB" class="vb-button" id="load-session" title="Load session">\n          <i class="fa fa-upload"></i>LOAD\n      </i>\n\n     <!-- SAVE -->\n     <i ng-show="StateManager.notEmpty()" ng-click="StateManager.save()" class="vb-button" id="save-session" title="Save session">\n       <i class="fa fa-download"></i>SAVE\n     </i>\n\n      <span flex></span>\n\n      <div hide-xs hide-sm class="beta">\n        This app is currently in <span class="beta-mark">BETA</span>. Please help us improve it by sending remarks and suggestions to\n        <a href="mailto:info@vectorbase.org">info@vectorbase.org</a>\n      </div>\n\n      <span flex></span>\n\n      <i ng-show="StateManager.notEmpty()"\n         ng-click="StateManager.removeAllReferences()"\n         aria-label="Reset" class="vb-button" id="reset-session" title="Remove all references and show tutorial">\n        <i class="fa fa-eraser"></i>RESET\n      </i>\n\n      <i ng-show="StateManager.canUndo()"\n         ng-click="StateManager.undo()"\n         aria-label="Undo" class="vb-button" id="undo-session" title="Undo your last action">\n        <i class="fa fa-undo"></i>UNDO\n      </i>\n\n</div>\n<!-- References are resolved (color 1) -->\n<md-progress-linear ng-show="!StateManager.isStandBy()"  md-mode="indeterminate"></md-progress-linear>\n<md-progress-linear ng-show="!StateManager.noExportsRunning()" md-mode="indeterminate" class="md-warn"></md-progress-linear>\n');
$templateCache.put('track/track.template.html','\n<div class="track-overview"\n  ng-style="{\'color\':track.reference.color}">\n\n  <span class="status-display fa"></span>\n  <span class="loading-display fa"></span>\n  <span class="header">{{track.name}}</span>\n  <span class="sub-header">{{ getHeader() }}</span>\n  <editable value="track.label" default-value="My track"></editable>\n</div>\n\n<div layout="row"\n  class="track-body"\n  ng-mouseover="StateManager.setActiveTrack(track)"\n  ng-style="{\'border-color\':track.reference.color}">\n\n  <track-renderer></track-renderer>\n\n  <div class="message browsable" ng-bind-html="track.getErrorMessage()"></div>\n\n  <div flex></div>\n  <div class="controls" layout="column">\n\n    <a ng-click="setView()" class="fa fa-circle-o err-hide warn-hide" aria-label="Optimise view" title="Optimise view"></a>\n\n    <a ng-show="track.filtersAutocomplete"\n       ng-click="Dialogs.editEntityFiltersDialog(track)"\n       class="fa fa-edit err-hide add-filters" aria-label="Add filters" ng-attr-title="Add {{track.filtersAutocomplete.name}}"></a>\n\n    <a\n      ng-if="track.hasExportCapability()"\n      ng-click="export(track)"\n      class="export-track-btn warn-hide err-hide"\n      aria-label="Export track as file"\n      title="Export current view as...">\n      {{track.export.name}}\n    </a>\n\n    <a ng-show="track.overview.hasOverview()"\n       ng-click="Overview.triggerSidebar()"\n       ng-class="{\'filters-applied\' : track.filter.hasCategoryFilters()}"\n       class="fa fa-bars err-hide warn-hide show-overview"\n       aria-label="Show overview"\n       title="Show overview">\n     </a>\n\n     <a\n       ng-show="track.hasSelectedEntities()"\n       ng-click="track.jumpToSelected()"\n       class="fa fa-map-pin err-hide warn-hide"\n       aria-label="Jump to selected"\n       title="Jump to selected">\n     </a>\n\n    <div flex></div>\n\n    <!-- <md-button ng-click="addAnnotator()" class="fa fa-indent" aria-label="Add annotator" title="Add an anotator"></md-button> -->\n\n    <a ng-show="canMoveUp()" ng-click="changeDisplayOrder(-1)" class="fa fa-arrow-up secondary-controls" title="Move track up"></a>\n    <a ng-show="canMoveDown()" ng-click="changeDisplayOrder(1)" class="fa fa-arrow-down secondary-controls" title="Move track down"></a>\n\n    <a ng-click="remove()" class="fa fa-close" aria-label="Remove track" title="Remove track"></a>\n  </div>\n\n</div>\n');
$templateCache.put('tutorial/tutorial.template.html','<div class="wellcome-message">\n\n  <p class="page-title"><b>Genotype Explorer</b></p>\n\n  <div class="presentation-cards" layout="row" layout-wrap>\n\n    <div class="pc-red">\n      <i class="fa fa-magic"></i>\n      <span>Introduction</span>\n      <p>\n        Genotype Explorer will help you explore genetic variations within a genomic or proteomic context.\n        <p>\n          To allow you to easily <b>explore</b> new species and features, Genotype Explorer allows you to create references,\n          or add samples to a genotype track without knowing species names, transcript or sample ids.\n        </p>\n\n        <p>\n          You can simply start typing a subject you are interested in <b>(ex: \'zika\')</b> and the <b>autosuggest</b> will suggest\n          to you species linked to that subject,\n          e.g. if you are interested in mosquitos resistant to DDT, simply type \'ddt\' in samples dialog.\n        </p>\n      </p>\n    </div>\n\n    <div class="pc-blue">\n      <i class="fa fa-map-o"></i>\n      <span>References</span>\n      <img src="images/tutorial/reference.png" alt="Image showing how to add a new reference" />\n      <p>\n        A reference is used to provide a uniform system of coordinates to draw features on screen.\n        Genotype Explorer is compatible with any uni-dimentional references such as transcript, DNA or protein sequences.\n        <p>\n          Currently supported types of references are <b>Genome</b> and <b>Protein</b>.\n        </p>\n        <p>\n          You can add multiple references to allow you to easily <b>compare features between different organisms</b>,\n          intervals of the same chromosome, etc...\n        </p>\n        <i class="md-button md-raised md-primary" ng-click="clickAddRef()">Add a reference</i>\n      </p>\n    </div>\n\n    <div class="pc-green">\n      <i class="fa fa-exchange"></i>\n      <span>Tracks</span>\n      <img src="images/tutorial/track.png" alt="Image showing two tracks" />\n      <p>\n         A track groups features of a given type which are then displayed in context of a specific reference.\n         This means that you have to add a reference to your project, before you can add tracks.\n        <p>\n          Some tracks allow you to download the current interval,\n          e.g. the genotype track allows you to download a <b>VCF</b> file corresponding to the displayed interval and selected samples.\n        </p>\n        <p>\n          Some features can be <b>morphed</b> into others. For example you can click on a transcript and morph it into a protein.\n        </p>\n      </p>\n    </div>\n\n    <div>\n      <i class="fa fa-pie-chart"></i>\n      <span>Overview</span>\n      <img src="images/tutorial/overview.png" alt="Image showing the overview legend for a track" />\n      <p>\n        Most of the tracks provide an overview of the properties of currently displayed features.\n        To show/hide the overview you can click the <i class="fa fa-bars"></i> icon in the toolbar.\n        For example, for the <b>genotypes</b> track you can visualise a chart of consequences of these genotypes on each transcript.\n        By clicking items of the legend, you can hide/show genotypes with specific consequences.\n      </p>\n    </div>\n\n\n    <div class="pc-pink">\n      <i class="fa fa-folder"></i>\n      <span>Load / Save / Undo</span>\n      <p>\n        You can save the current state of your work and load it later, or <b>share</b> the\n        save file with collegues. If you are not happy with any action you have executed\n        (browsed to bad location, accidentally removed a track) you can click on the <b>UNDO</b> button\n        to return to the previous state of the application.\n        <p>If you have saved previous work, you can click on\n        <i class="fa fa-upload" ng-click="clickLoad()"></i> to load it.</p>\n\n      </p>\n    </div>\n\n  </div>\n\n</div>\n');
$templateCache.put('track/renderer/track.renderer.template.html','<div layout="row">\n  <div layout="column" class="interval">\n    <div title="Click on a feature to display the action menu" class="browser browsable"></div>\n\n    <div class="ticks" layout="row">\n      <div flex class="tick" ng-repeat="tick in ::ticks"></div>\n    </div>\n\n    <span class="track-footer" layout="row">\n\n        <a class="fa fa-arrow-left browse-left" ng-hide="track.minReached()" ng-click="track.getReference().browse(-100)" title="Browse left"></a>\n\n        <span class="track-start" ng-class="track.minReached() ? \'ref-limit\' : \'\'">{{track.getIntervalParams().start}}</span> <!-- RED if equals to min -->\n        <span flex></span>\n        <span class="interval-legend">\n          Interval size is <b>{{track.getDisplayableIntervalSize()}} {{track.getReference().unit}}</b>\n          A tick represents {{track.getDisplayableTickSize()}} {{track.getReference().unit}}\n        </span>\n        <span flex></span>\n        <span class="track-end" ng-class="track.maxReached() ? \'ref-limit\' : \'\'">{{track.getIntervalParams().end}}</span> <!-- RED if equals to max -->\n\n        <a class="fa fa-arrow-right browse-right" ng-hide="track.maxReached()" ng-click="track.getReference().browse(100)" title="Browse right"></a>\n    </span>\n    <span class="legend" ng-bind-html="getLegend()"></span>\n    <span ng-if="track.getFilter().getNotFoundEntities()" class="entities-not-found">Not found: {{track.getFilter().getNotFoundEntities()}}</span>\n  </div>\n\n  <div class="enames" title="Click to view in..."></div>\n</div>\n');}]);