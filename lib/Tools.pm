=head1 NAME

Tools - General utility functions

=head1 DESCRIPTION

General utility functions

=cut

package Tools;

use strict;
use warnings;

use Data::Dumper;

use Exporter qw(import);
use Try::Tiny;

our @EXPORT = qw( first_slice run_job run_solr_job get_params );
our @EXPORT_OK = qw( user_die task_die );


=head2 user_die
  Arg[1]      : string $msg
  Description : Dies with a special code signaling that this is a user related error, not a system error
  Status      : Stable
=cut

sub user_die {
	my ( $msg ) = @_;
	die {status => 418, message => "$msg\n" };
}

=head2 task_die
  Arg[1]      : string $msg
	Arg[2]      : string $details
  Description : Dies when a task failed, with an object containing the error summary message and more detailed information
  Exceptions  : none
  Status      : Stable
=cut

sub task_die {
	my ( $msg, $details ) = @_;
	die { message => "$msg\n$details\n" };
}

=head2 get_params
  Arg[1]      : Request $req
  Description :
  Returntype  : list ( intervalParams, trackParams )
  Exceptions  : none
  Status      : Stable
=cut

sub get_params {
	my ( $req ) = @_;
	my $intervalParams = $req->json && $req->json->{'intervalParams'};
	my $trackParams = $req->json && $req->json->{'trackParams'};
  return ( $intervalParams, $trackParams );
}

=head2 first_slice
  Arg[1]       : list $parray
	Arg[2]       : int $limit
  Description: Returns the first n entries from an array, or the whole array if limit > array size
  Returntype : list ref
  Exceptions : none
  Status     : Stable
=cut

sub first_slice {
	my($parray, $limit) = @_;
	my @array = @$parray;
	my $count = scalar @array;

	return \@array if !defined($limit) || $count < $limit;

	my @new_array = @array[0..$limit-1];
	return \@new_array;
}


=head2 run_job
  Arg[1]     : object $c
	Arg[2]     : function $sub Function to run inside a Try/Catch block
	Arg[3]     : string $context
  Description: Executes a job inside a try and renders an exception if an exception is thrown
  Returntype :
  Exceptions : none
  Status     : Stable

	TODO Use http://search.cpan.org/~patl/Time-Out-0.11/Out.pod
	to timeout (ensembl perl api) jobs that take longer than 3s
	TODO Would still be better to ask the perl api to send timeout signal to database to stop the request...
=cut

sub run_job {
  my ( $c, $sub, $context ) = @_;

  try{
    $sub->();
  } catch {
		my $status = ( $_ && $_->{status} ) || 400;
		my $message = ( $_ && $_->{message} ) || Dumper($_);

		my $full_message = $message;

		# Ensembl error messages are often big chunks of text which we have to parse to extract the error message from the stack
		my ($ens_message) = $message =~ /MSG:(.*)\n/;
		$message = $ens_message if $ens_message;
		$message =~ s/as user ensr. using.*//g;

		# Log to servers log the full ensembl message
    $c->app->log->warn("$context : $full_message");

    $c->render(
      json => {
          summary => "$context : $message",
          message => $message,
          context => $context
      },
      status => $status
    );
  }
}


=head2 run_solr_job
	Arg[1]      : object $self : Reference to the controllers
	Arg[2]      : string $url
  Arg[3]      : function $process_sub : This function is called with ( $docs (list), $hl(hash) ),
								and the result of this function will be rendered to the client.
  Description : Calls a given URL, and renders the resolut processed with the function process_sub.
								Note that because exception in asynchronous code are note send to the application,
								all exceptions in process_sub will result in a 500 exception json sent to the client.
								This means that no user parameters testing should be done in process_sub.
	Arg[4]      : string $context : text adding more clarity to the error messages
  Returntype  : none
  Exceptions  : none
  Status      : Stable
=cut

sub run_solr_job {
  my ( $self, $url, $process_sub, $context ) = @_;

    $self->app->log->debug("$context URL : $url");

    $self->render_later;
    $self->ua->get( $url => {Accept => 'application/json'} => sub {
      my ($ua, $result) = @_;

			try {

      	if( $result->success ){

          my $json = $result->res->json;

          if( $json && ref($json) eq 'HASH' && defined $json->{response} ){

						# Fixing for the difference between Solr json structure and json retrieved from vbsearch/query entrypoint
						my $docs = [];
						my $hl = $json->{highlighting};

						if ( ref($json->{response} ) eq 'ARRAY' ){ # vbsearch/query
							$docs = $json->{response};

						} elsif ( ref($json->{response} ) eq 'HASH' ){ # json retrieved from Solr directly
							$docs = $json->{response}{docs};
						}

            my $processed = $process_sub->( $docs, $hl );
            $self->render( json => $processed );

          } else {
						# vbsearch can sometimes send invalid JSON with some text added to the end
            my $content = $result->res->body;
            $self->app->log->warn("Invalid JSON for autocomplete : ".($content || Dumper($result->res)));
            $self->render( json => {
              message => "Invalid JSON from Solr",
              context => "$context : $url"},
              status => '400');
          }

	      }	else {

					my $conn_err_msg = $result->error && $result->error->{message};
					my $error_message = "Can not connect to Solr: $conn_err_msg";

					# If it is Solr that responded with an error message, we retrieve the message
					if($result && $result->res && $result->res->json && $result->res->json->{error}){
						$error_message = $result->res->json->{error}{msg};
					}

	        $self->app->log->warn( $error_message );
					my $content = $result->res->body;
					$self->app->log->warn("Error page received from VB Solr api : ".($content || Dumper($result->res)));

	        $self->render( json => {
	          message => $error_message,
	          context => "$context : $url"},
	          status => '400');
	      }

			} catch {
					$self->reply->exception($_) if $_;
			};

    });

}


1;
