package Lera;

use strict;
use warnings;

use Mojo::Base 'Mojolicious';
use Mojo::Log;
use IO::Compress::Gzip 'gzip';

use Data::Dumper;
$Data::Dumper::Terse = 1;

use Conf;
#use FindBin qw($Bin);
#use lib "$Bin/lib";

=head2 startup
  Description : Starts the server
  Exceptions  : none
  Status      : Stable
=cut

sub startup {
  my $self = shift;

  ### CONF ###

  $self->secrets(['Java']);

  # TODO Mojolicious does not manage rolling logs... Use logrotate ? https://github.com/kraih/mojo/wiki/logrotate
  $self->log->level('debug');

  $self->config(hypnotoad => {
  	proxy => 1,
  	listen => [ get_config('conf/server.ini', 'url') ],
  	workers => 20, # 10
  	accepts => 100, # Maximum number of connections a worker can accept before restarting
  	clients => 1 # Default is 10. Trying 1 to prevent perl api genotypes from blocking everyone
  });

  ### URL PREFIX ###
  $self->hook(before_dispatch => sub {
  	# The prefix "/lera" is removed from all requests
  	my $c    = shift @_;
  	my $path = $c->req->url->path->to_abs_string;
  	$path    =~ s{^/lera(/|$)}{/};
  	$c->req->url->path($path);
  });

  ### EXCEPTIONS ###
  $self->hook(before_render => sub {
    my ($c, $args) = @_;

    return unless my $template = $args->{template};
    return unless $template eq 'exception';
    $args->{json} = {error => $template, message => $args->{exception}, context => 'Mojolicious server'};
  });


  ### COMPRESSION ###
  # Compress the output with gzip : http://mojolicious.org/perldoc/Mojolicious/Guides/Rendering#Post-processing_dynamic_content
  # http://search.cpan.org/~ams/Mojolicious-4.26/lib/Mojolicious/Guides/Rendering.pod
  $self->hook(after_render => sub {
    my ($c, $output, $format) = @_;

    # Check if "gzip => 1" has been set in the stash
    return unless $c->stash->{gzip};

    # Check if user agent accepts gzip compression
    return unless ($c->req->headers->accept_encoding // '') =~ /gzip/i;

    $c->res->headers->append(Vary => 'Accept-Encoding');
    $c->res->headers->content_encoding('gzip');

    gzip $output, \my $compressed;
    $$output = $compressed;
  });



  ### ROUTES ###
  my $r = $self->routes;


  $r->any('/' => sub {
    my $c = shift;
    # 1. get conf parameter from get or post
    my $conf = $c->req->param('conf');
    $c->app->log->debug("Using configuration supplied by GET/POST : $conf") if $conf;
    # 2. add configuration to template as data-conf
    $c->render(template => 'index', conf => $conf);
  });


  # ENSEMBL
  # TODO add ensembl prefix
  $r->any('/api/track/transcript')->to('ensembl_adapter#track_transcript');
  $r->any('/api/track/protein')->to('ensembl_adapter#track_protein');
  $r->any('/api/track/protein_genotype')->to('ensembl_adapter#track_protein_genotype');
  $r->any('/api/track/variations_density')->to('ensembl_adapter#track_variations_density');

  $r->any('/api/reference/genome')->to('ensembl_adapter#ref_genome');
  $r->any('/api/reference/protein')->to('ensembl_adapter#ref_protein');

  # BCF_TOOLS
  $r->any('/api/commandline/track/genotype')->to('bcftools_adapter#track_genotype');
  $r->any('/api/commandline/track/genotype/export')->to('bcftools_adapter#export_track_genotype');

  # SOLR
  $r->any('/api/autocomplete/species')->to('solr_adapter#species_autocomplete');
  $r->any('/api/autocomplete/genome_feature')->to('solr_adapter#gene_transcript_autocomplete');
  $r->any('/api/autocomplete/protein_feature')->to('solr_adapter#protein_transcript_autocomplete');
  $r->any('/api/autocomplete/sample')->to('solr_adapter#sample_autocomplete');
  $r->any('/api/autocomplete/ortholog/gene')->to('solr_adapter#gene_orthologs_autocomplete');
  $r->any('/api/autocomplete/ortholog/protein')->to('solr_adapter#protein_orthologs_autocomplete');

  # REDIRECT
  $r->any('/api/redirect/variation/by/location')->to('redirect#redirect_variation');

  # MAPPERS
  $r->any('/api/map/samples')->to('mapper#map_sample_names');

  # LOG
  $r->any('/api/log')->to('log#log_client_error');

  # STATS
  $r->any('/api/stats/species')->to('stats#available_species');

  # Preloading of the vcf config
  hot_load_vcf_config($self);

  hot_load_ensembl_registry($self);

}


=head2 hot_load_vcf_config
  Description : Makes sure that each worker reads and checks the vcf configuration
  Exceptions  : none
  Status      : Stable
=cut

sub hot_load_vcf_config {
  my $self = shift;
  use Lera::Model::BcftoolsAdapters::VcfConf;
  $self->log->info("Started hot loading VCF config");
  my $vcf_conf = get_vcf_config();
  my $nb_species = keys %$vcf_conf;
  $self->log->info("Finished hot loading VCF config for $nb_species species");
}

=head2 hot_load_ensembl_registry
  Description : Makes sure that each worker initialises the Ensembl Registry
  Exceptions  : none
  Status      : Stable
=cut

sub hot_load_ensembl_registry {
  my $self = shift;
  use Lera::EnsemblRegistry;
  $self->log->info("Started hot loading ensembl registry");
  my $species = get_registry()->get_all_species();
  die "No species could be loaded into Ensembl registry\n" unless $species && @$species;
  $self->log->info("Finished hot loading ensembl registry for ".(scalar @$species)." species");
}

1;
