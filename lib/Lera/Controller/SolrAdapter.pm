=head1 NAME

Lera::Controller::SolrAdapter - Adaptor for querying Solr instances

=head1 DESCRIPTION

Adaptor for querying Solr instances

=cut

package Lera::Controller::SolrAdapter;

use strict;
use warnings;

use Tools;
use Mojo::Base 'Mojolicious::Controller';

use Lera::Model::SolrAdapters::Autosuggest;

=head2 species_autocomplete
 Arg [1]     : query
 Description : Returns species suggestions for a given query
 Status      : Stable
=cut

sub species_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $query = $params->{query};
  die "query is a mandatory parameter.\n" unless $query;

  my $url = get_species_autocomplete_url( $query );

  run_solr_job( $self, $url, sub {
    my ( $docs, $hl ) = @_;
    return process_species_autocomplete( $docs, $hl, $query );
  }, "species autocomplete");

}


=head2 gene_orthologs_autocomplete
 Arg [1]     : entityId : gene or transcript
 Arg [2]     : query
 Description : Returns orthologs for a given gene or transcript id, matching the query
 Status      : Stable

=cut

sub gene_orthologs_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $entity_id = $params->{entityId};
  my $query = $params->{query};
  my $species = $params->{species};
  $species = lc( $species ); # species name in ensembl is like 'Anopheles_gambiae'

  $entity_id =~ s/-[A-Z]{2}$//; # transform transcript_id to gene_id
  return _orthologs_autocomplete( $self, $query, $entity_id, 'gene', $species);
}

=head2 protein_orthologs_autocomplete
 Arg [1]     : entityId
 Arg [2]     : query
 Description : Returns orthologs for a given protein id, matching the query
 Status      : Stable
=cut

sub protein_orthologs_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $entity_id = $params->{entityId};
  my $query = $params->{query};

  return _orthologs_autocomplete( $self, $query, $entity_id, 'peptide');
}

=head2 _orthologs_autocomplete
 Description : Returns orthologous genes and proteins suggestions for a given species and query.
               Should not be called directly.
 Status      : Stable
=cut

sub _orthologs_autocomplete {
  my ( $self, $query, $entity_id, $entity_type, $species ) = @_;

  # To be ablle to show some example hits, query is no more a mandatory parameter

  die "entityId is a mandatory parameter.\n" unless $entity_id;

  my $url = get_orthologs_url( $query, $entity_id, $entity_type, $species );

  run_solr_job( $self, $url, sub {
    my ( $docs, $hl ) = @_;
    return process_orthologs_autocomplete( $docs, $hl, $entity_id, $entity_type, $query );
  }, "orthologs autocomplete");

}

=head2 gene_transcript_autocomplete
 Arg [1]     : query
 Arg [2]     : species
 Arg [3]     : transcriptOnly (optional)
 Description : Returns gene and transcript suggestions for a given species and query
 Status      : Stable
=cut

sub gene_transcript_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $query = $params->{query};
  my $species = $params->{species};
  my $transcriptOnly = $params->{transcriptOnly}; # Does not allow genes. Only transcripts are returned.

  # FIXME Allow only gunpowder and vectorbase origins ??
  # $self->res->headers->header('Access-Control-Allow-Origin' => '*');

  my $location = get_location_autocomplete($query);
  if( $location ){ # Case where user directly entered a genomic location
    $self->render( json => $location );
    return;
  }

  my $features = $transcriptOnly ? ['transcript'] :  ['transcript', 'gene'];

  return _feature_autocomplete( $self, $query, $species, $features);
}


=head2 protein_transcript_autocomplete
 Arg [1]     : query
 Arg [2]     : species
 Description : Returns protein and transcript suggestions for a given species and query
 Status      : Stable
 TODO Find a way to use descriptions from genes associated to transcripts/proteins https://wiki.apache.org/solr/Join
=cut

sub protein_transcript_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $query = $params->{query};
  my $species = $params->{species};
  return _feature_autocomplete( $self, $query, $species, ['transcript', 'protein'], 'biotype:protein_coding');
}

=head2 _feature_autocomplete
  Arg[1]      : object $self
  Arg[2]      : string $query
  Arg[3]      : string $species
  Arg[4]      : list $feature_types ex: ['gene', 'transcript']
  Arg[5]      : string $filter Additional fq filter query
  Description : Retrieves suggestions for genomic features for a given species and query
  Exceptions  : query is a mandatory parameter
                feature_types is a mandatory paramater
  Status      : Stable
=cut

sub _feature_autocomplete {
  my ($self, $query, $species, $feature_types, $filter) = @_;

  die "query is a mandatory parameter.\n" unless $query;
  die "feature_types is a mandatory paramater\n" unless $feature_types;

  my $url = get_feature_autocomplete_url( $query, $feature_types, $species, $filter );

  run_solr_job( $self, $url, sub {
    my ( $docs, $hl ) = @_;
    return process_feature_autocomplete( $docs, $hl, $query );
  }, "genomic features autocomplete");

}


=head2 sample_autocomplete
 Arg[1]      : query
 Arg[2]      : species
 Arg[3]      : exclude (optional) List of samples ids to exclude from the suggested (ex: already selected ids)
 Description : Returns samples suggestions for a given species and query
 Status      : Stable
 TODO Servlet container limit for GET calls length ?? Switch to POST for calling Solr ??
=cut
sub sample_autocomplete {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $query = $params->{query};
  my $species = $params->{species};
  my $exclude = $self->req->json && $self->req->json->{exclude};

  die "query is a mandatory parameter.\n" unless $query;
  die "species is a mandatory paramater\n" unless $species;

  my $url = get_sample_autocomplete_url( $query, $species, $exclude );

  run_solr_job( $self, $url, sub {
    my ( $docs, $hl ) = @_;
    return process_sample_autocomplete( $docs, $hl );
  }, "samples autocomplete");

}



1;
