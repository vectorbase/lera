=head1 NAME

Lera::Controller::BcftoolsAdapter - Allows to retrieve and export genotypes from BCf/VCF files

=head1 DESCRIPTION

Allows to retrieve and export genotypes from BCF/VCF files

=cut

package Lera::Controller::BcftoolsAdapter;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';
use Tools;

use Lera::Model::BcftoolsAdapters::Tracks::Genotype qw( get_genotypes export_genotypes );

=head2 track_genotype
  Arg[1]      : (object) $intervalParams
  Arg[2]      : (object) $trackParams
  Description : Renders a genotype track
  Exceptions  : none
  Status      : Stable
=cut

sub track_genotype {
  my $self = shift;
  my ( $intervalParams, $trackParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_genotypes( $intervalParams, $trackParams ) );
  }, "Genotype Track API");
}

=head2 export_track_genotype
  Arg[1]      : (object) $intervalParams
  Arg[2]      : (object) $trackParams
  Description : Renders a VCF file containing the requested region with the requested samples
  Exceptions  : none
  Status      : Stable
=cut

sub export_track_genotype {
  my $self = shift;
  my ( $intervalParams, $trackParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( text => export_genotypes( $intervalParams, $trackParams ) );
  }, "Genotypes VCF Exporter API");
}


1;
