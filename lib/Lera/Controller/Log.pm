=head1 NAME

Lera::Controller::Log - Prints to the log files errors and exceptions received from the client app

=head1 DESCRIPTION

Prints to the log files errors and exceptions received from the client app

=cut

package Lera::Controller::Log;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';


=head2 log_client_error
  Arg[1]      : string $msg
  Description : Prints to server log an error message received from the client
  Exceptions  : none
  Status      : Stable
=cut

sub log_client_error {
  my $self = shift;
  my $msg = $self->req->query_params->to_hash->{msg};

  $self->app->log->warn("[client] $msg");
  $self->render( json => { message => "Message added to server log" } );

}

1;
