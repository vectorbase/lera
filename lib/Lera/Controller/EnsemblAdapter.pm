=head1 NAME

Lera::Controller::EnsemblAdapter - Retrieving data from VB Ensembl Database Using Perl API or directly quering the database

=head1 DESCRIPTION

Retrieving data from VB Ensembl Database Using Perl API or directly quering the database

=cut

package Lera::Controller::EnsemblAdapter;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';

use Tools;

use Lera::Model::EnsemblAdapters::Tracks::Peptype qw( get_protein_genotypes );
use Lera::Model::EnsemblAdapters::Tracks::Transcript;
use Lera::Model::EnsemblAdapters::Tracks::Protein;
use Lera::Model::EnsemblAdapters::Tracks::VariationsDensity qw( get_variations_density );

use Lera::Model::EnsemblAdapters::References::GenomeReference;
use Lera::Model::EnsemblAdapters::References::ProteinReference;

=head2 track_transcript
  Arg[1]      : (object) $intervalParams
  Description : Renders a track displaying transcripts (with exons and introns)
  Exceptions  : none
  Status      : Stable
=cut

sub track_transcript {
  my $self = shift;
  my ( $intervalParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_transcripts( $intervalParams ) );
  }, "Ensembl Transcript track API");
}

=head2 track_variations_density
  Arg[1]      : (object) $intervalParams
  Description : Renders a variations density track,
                showing the density of variations in adjacent genomic
                regions of same size (buckets)
  Exceptions  : none
  Status      : Stable
=cut

sub track_variations_density {
  my $self = shift;
  my ( $intervalParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_variations_density( $intervalParams ) );
  }, "Ensembl Variations Density track API");
}

=head2 track_protein
  Arg[1]      : (object) $intervalParams
  Description : Renders a track displaying protein features, including domains
  Exceptions  : none
  Status      : Stable
=cut

sub track_protein {
  my $self = shift;
  my ( $intervalParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_protein_features( $intervalParams ) );
  }, "Ensembl Protein track API");
}

=head2 track_protein_genotype
  Arg[1]      : (object) $intervalParams
  Arg[2]      : (object) $trackParams
  Description : Renders a track displaying protein genotypes, or peptypes,
                which are the projection of the genotypes associated with a transcripts
                into the resulting protein. Genotypes associated with base pairs corresponding
                to a given amino acid are combined to deduce the effect on the amino acid.
  Exceptions  : none
  Status      : Stable
=cut

sub track_protein_genotype {
  my $self = shift;
  my ( $intervalParams, $trackParams ) = get_params( $self->req );

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_protein_genotypes( $intervalParams, $trackParams ) );
  }, "Ensembl Peptype track API");
}

=head2 ref_genome
  Arg[1]      : (object) $intervalParams
  Arg[2]      : (object) $trackParams
  Description : Renders a resolved genome reference
  Exceptions  : none
  Status      : Stable
=cut

sub ref_genome {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_genomic_reference( $params ) );
  }, "Ensembl Genome reference API");
}

=head2 ref_protein
  Arg[1]      : (object) $intervalParams
  Arg[2]      : (object) $trackParams
  Description : Renders a resolved protein reference
  Exceptions  : none
  Status      : Stable
=cut

sub ref_protein {
	my $self = shift;
	my $params = $self->req->query_params->to_hash;

  run_job($self, sub {
    $self->stash( gzip => 1);
    $self->render( json => get_protein_reference( $params ) );
  }, "Ensembl Protein reference API");
}


1;
