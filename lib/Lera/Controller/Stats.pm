=head1 NAME

Lera::Controller::Stats - Returns various statistics about available data

=head1 DESCRIPTION


=cut

package Lera::Controller::Stats;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';

use Lera::Model::BcftoolsAdapters::VcfConf qw( get_available_species_names );

=head2 available_species
  Description : Returns a JSON array of available species
  Exceptions  : none
  Status      : Stable
=cut

sub available_species {

  my $self = shift;
  my @species = get_available_species_names();
  $self->res->headers->header('Access-Control-Allow-Origin' => '*');
  $self->render( json => \@species );
}

1;
