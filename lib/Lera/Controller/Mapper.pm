=head1 NAME

Lera::Controller::Mapper - Maps ids to other ids

=head1 DESCRIPTION

Maps ids to other ids

=cut

package Lera::Controller::Mapper;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';

use Lera::Model::EnsemblAdapters::Mappers::SamplesMapper qw( to_popbio_ids );
use Tools;

=head2 map_sample_names
  Arg[1]      : string $species
  Arg[2]      : string or list $samples
  Description : Show a page with a JSON array of sample ids, retrieved from the submitted sample names
  Exceptions  :
    species is a mandatory parameter
    samples is a mandatory parameter
  Status      : Stable
=cut

sub map_sample_names {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $species = $params->{species};
  my $samples = $params->{samples};

  die "species is a mandatory parameter\n" unless $species;
  die "samples is a mandatory parameter\n" unless $samples;

  run_job($self, sub {
    $samples = [$samples] if ref($samples) ne 'ARRAY';
    $self->render( json => to_popbio_ids( $species, $samples ) );
  }, "Samples synonyms mapping API");

}

1;
