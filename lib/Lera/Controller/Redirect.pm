=head1 NAME

Lera::Controller::Redirect - Easy redirection to VB Genome Browser pages

=head1 DESCRIPTION

Easy redirection to VB Genome Browser pages, allowing to access these pages
with parameters that are not natively supported.

=cut

package Lera::Controller::Redirect;

use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';

use Tools;
use Lera::EnsemblRegistry;
use Conf;


=head2 redirect_variation
  Arg [1] : string $species
  Arg [2] : string $chr
  Arg [3] : string $start
  Description: Redirects towards the ensembl genome browser variation page corresponding
  to the first variation_id matching the given location. If there are multiple matches, shows a page
  with links to these matches.
  Exceptions :
    species is a mandatory parameter
    chr is a mandatory parameter
    start is a mandatory parameter
    Can not connect to variation database
    Couldn't execute statement
  Status     : Stable
=cut

sub redirect_variation {
  my $self = shift;
  my $params = $self->req->query_params->to_hash;
  my $species = $params->{species};
  my $chr = $params->{chr};
  my $start = $params->{start};

  die "species is a mandatory parameter\n" unless $species;
  die "chr is a mandatory parameter\n" unless $chr;
  die "start is a mandatory parameter\n" unless $start;

  my $registry = get_registry();
  my $dba = $registry->get_DBAdaptor( $species, 'variation');
  my $dbc = $dba && $dba->dbc();

  die "Can not connect to variation database for $species\n" unless $dbc;

  my $sth = $dbc->prepare("
    SELECT vf.variation_name as var_name, vf.seq_region_start, sr.name
    FROM variation_feature vf
    JOIN seq_region sr ON vf.seq_region_id = sr.seq_region_id
    where sr.name = ? AND vf.seq_region_start = ?
  ");

  $sth->execute( $chr, $start ) or die "Couldn't execute statement: " . $sth->errstr;
  my $rows = $sth->rows;

  if( $rows eq 0 ){
    $self->render( text => "No matching variation for $chr:$start\n" );
    return;
  }

  my @links = ();

  while (my $data = $sth->fetchrow_hashref()) {
    my $var_name = $data->{'var_name'};
    my $base = get_config( 'conf/ensembl.ini', 'genome_browser_base');
    my $link = "$base$species/Variation/Explore?v=$var_name";
    push( @links, { name => $var_name, url => $link } );
  }
  $sth->finish();

  if( (scalar @links) > 1 ){

    my $links_html = join('<br/>', map { "<a href=".$_->{url}.">".$_->{name}."</a>" } @links);
    $self->render( text => "VARIATIONS REDIRECTION SERVICE<br/>Multiple matching variations for $chr:$start<br/>$links_html" );
    return;

  } else {
    $self = $self->redirect_to( $links[0]{url} );
    return;
  }


}

1;
