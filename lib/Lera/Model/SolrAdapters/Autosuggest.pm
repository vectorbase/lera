=head1 NAME

Lera::Model::SolrAdapters::Autosuggest - Layer allowing to retrieve autosuggest data from Solr instances

=head1 DESCRIPTION

Layer allowing to retrieve autosuggest data from Solr instances

=cut

package Lera::Model::SolrAdapters::Autosuggest;

use strict;
use warnings;

use Exporter qw(import);

use Tools;
use Conf;
use Lera::Model::EnsemblAdapters::EnsemblTools;
use Lera::Model::BcftoolsAdapters::VcfConf qw( get_available_species_names is_species_available );

use Mojo::Util qw(b64_encode url_escape url_unescape);
use Data::Dumper;

our @EXPORT = qw( get_species_autocomplete_url get_feature_autocomplete_url get_sample_autocomplete_url get_solr_error_message species_ens_to_vb process_species_autocomplete process_feature_autocomplete process_sample_autocomplete create_description get_location_autocomplete get_orthologs_url process_orthologs_autocomplete);
our @EXPORT_OK = qw( __get_available_species_filter process_query );

## CONF

# Sample Explorer Solr
my $SAMPLE_NAME_FIELD = 'UUID';

# VB Solr
my $SPECIES_FIELD = 'reference_genome_s';

## END CONF

=head2 get_species_autocomplete_url
 Arg [1]     : string $query
 Description : Creates the URL allowing to query species from Solr
 Returntype  : string
 Status      : Stable

 TODO Use reference_genome_ss field instead of label (https://jira.vectorbase.org/browse/VB-6644)
 TODO WARNING: Will be quite difficult to have backwards compatibility
=cut
sub get_species_autocomplete_url {
  my ( $query ) = @_;
  $query = process_query( $query );

  my $fq ='entity_type:organism';
  my $qf = "label,content";
  my $fl = "id,label,content,reference_genome_ss";
  my $hlfl = "content";

  # Filtering by species available in the VCF files
  my $available_species_filter = _get_available_species_filter();
  $fq .= ",reference_genome_ss:$available_species_filter";

  return get_config( 'conf/solr.ini', 'vb_autocomplete', {query => $query, fq => $fq, qf => $qf, fl => $fl, hlfl => $hlfl} );
}

=head2 process_species_autocomplete
 Arg [1]     : list $docs
 Arg [2]     : string $highlighted
 Arg [3]     : string $rawQuery
 Description :
 Returntype  : array ref
 Status      : Stable
=cut
sub process_species_autocomplete {
  my ( $docs, $highlighted, $rawQuery ) = @_;
  return unless $docs;
  my $processed = [];

  foreach my $suggestion (@$docs){

    my @species_names = map { species_vb_to_ens( $_ ) } @{$suggestion->{reference_genome_ss} || []};
    # Keep only species available in VCF
    @species_names = map { is_species_available( $_ ) ? $_ : () } @species_names;

    foreach my $species_name (@species_names){

      my $processed_suggestion = {
        identifier => $species_name,
        description => create_description( $suggestion->{id}, 'content', $highlighted, $suggestion, $rawQuery )
      };
      push( @$processed, $processed_suggestion);
    }
  }

  return $processed;
}


=head2 get_feature_autocomplete_url
 Arg [1]     : string $query
 Arg [2]     : list $feature_types list of allowed feature types (transcript/gene/protein)
 Arg [3]     : string $species (optional) If no species is provided, we will filter using a list of available species
 Arg [4]     : string $filter (optional) Additional filter, ex: biotype:protein_coding
 Description : Creates the URL allowing to query genomic features from Solr
 Returntype  : string
 Status      : Stable
=cut
sub get_feature_autocomplete_url {
  my ( $query, $feature_types, $species, $filter) = @_;

  $species = species_ens_to_vb( $species );
  $query = process_query( $query );

  my $fq ='entity_type:('.join(' OR ', @$feature_types).')';

  $species ||= _get_available_species_filter();
  $fq.=",$SPECIES_FIELD:$species" if $species;
  $fq.=",$filter" if $filter;

  my $qf = "accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype";
  my $fl = "id,accession,$SPECIES_FIELD,entity_type";

  my $hlfl = "accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype";

  return get_config( 'conf/solr.ini', 'vb_autocomplete', {query => $query, fq => $fq, qf => $qf, fl => $fl, hlfl => $hlfl} );
}

=head2 process_feature_autocomplete
 Arg [1]     : list $docs
 Arg [2]     : string $highlighted
 Arg [3]     : string $rawQuery
 Description :
 Returntype  : array ref
 Status      : Stable
=cut
sub process_feature_autocomplete {
  my ( $docs, $highlighted, $rawQuery) = @_;
  return unless $docs;

  my @processed = map {

    {
      identifier => $_->{accession},
      description => create_description( $_->{id}, 'text', $highlighted, $_, $rawQuery ),
      species => species_vb_to_ens( $_->{$SPECIES_FIELD} && $_->{$SPECIES_FIELD} )
    }

  } @$docs;

  return \@processed;
}


=head2 get_sample_autocomplete_url
 Arg [1]     : string $query
 Arg [2]     : string $species
 Arg [3]     : list $exclude : List of already selected samples that must be excluded from the results
 Description : Creates the URL allowing to query samples from Solr. Only non deprecated samples are retrieved.
 Returntype  : string
 Status      : Stable

=cut
sub get_sample_autocomplete_url {
  my ( $query, $species, $exclude ) = @_;

  $species = ucfirst($species) if $species; # Transforming species from database format to PopBio ref genome format
  $query = process_query( $query, '+' );
  my $fq = "fq=m_reference_genome:$species&fq=is_deprecated:false";
  $fq .= "&fq=-$SAMPLE_NAME_FIELD:(".(join(' OR ', map { sanitize_for_solr( $_ ) } @$exclude )).')' if $exclude && @$exclude; # array exists and is not empty

  return get_config( 'conf/solr.ini', 'sample_explorer_autocomplete', {query => $query, fq => $fq} )
}


=head2 process_sample_autocomplete
 Arg [1]     : hash $res
 Description :
 Returntype  :
 Status      : Stable
=cut
sub process_sample_autocomplete {
  my ( $docs, $highlighted ) = @_;
  return unless $docs;
  my $processed = ();

  foreach my $suggestion (@$docs){

    my $processed_suggestion = {
      identifier => $suggestion->{$SAMPLE_NAME_FIELD},
      description => create_description( $suggestion->{UUID}, '_text_', $highlighted )
    };
    push( @$processed, $processed_suggestion);
  }
  return $processed;
}

=head2 get_orthologs_url
 Arg [1]     : string $query : text used to find the best ortholog
 Arg [2]     : string $entity_id : id of a gene or a protein
 Arg [3]     : string $entity_type : gene or protein
 Description : Creates the URL allowing to query Solr for orthologs_one2one for a given gene,
 only from species for which we have VCF files
 Returntype  :
 Status      : Stable
=cut
sub get_orthologs_url {
  my ( $query, $entity_id, $entity_type, $species ) = @_;

  $query = process_query( $query );
  
  my $SPECIES_FIELD = 'species';  # HOTFIX: Replace the species field here

  my $fq ="entity_type:Homology,$entity_type:$entity_id,label:ortholog*";
  my $qf = "text";
  my $fl = "id,gene,$SPECIES_FIELD,text";
  my $hlfl = "text";

  # Filtering by species available in the VCF files
  my $available_species_filter = _get_available_species_filter( $species ); # removing request species from filter species
  $fq.=",$SPECIES_FIELD:$available_species_filter";

  return get_config( 'conf/solr.ini', 'vb_autocomplete', {query => $query, fq => $fq, qf => $qf, fl => $fl, hlfl => $hlfl} );
}

=head2 process_orthologs_autocomplete
 Arg [1]     : list $docs
 Arg [2]     : string $highlighted
 Arg [3]     : string $entity_id
 Arg [4]     : string $entity_type (only: gene OR peptide)
 Arg [5]     : string $rawQuery
 Description :
 Returntype  : array ref
 Status      : Stable
=cut
sub process_orthologs_autocomplete {
  my ( $docs, $highlighted, $entity_id, $entity_type, $rawQuery ) = @_;
  return unless $docs;
  my $processed = ();
  my $SPECIES_FIELD = 'species';  # HOTFIX: Replace the species field here

  foreach my $suggestion (@$docs){
    # The target gene/protein/species can be either on first or second position... they are not ordered...
    # Each couple member is at a given position in the pair... But the query member can be randomly on the first or second position.
    my $features_couple = $suggestion->{$entity_type};
    my $species_couple = $suggestion->{$SPECIES_FIELD};

    # The target orthologue can be anywhere...
    my $index = $features_couple->[0] ne $entity_id ? 0 : 1;

    my $target_entity = $features_couple->[$index];
    my $target_species = $species_couple->[$index];

    my $processed_suggestion = {
      identifier => $target_entity,
      species => species_vb_to_ens( $target_species ),
      description => create_description( $suggestion->{id}, 'text', $highlighted, $suggestion, $rawQuery )
    };
    push( @$processed, $processed_suggestion);
  }
  return $processed;
}

=head2 get_solr_error_message
  Arg[1]      : Result $result
  Description : Creates an error message string by parsing available data from result
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub get_solr_error_message {
  my ( $result ) = @_;
  my $conn_err_msg = $result->error && $result->error->{message};
  my $error_message = "Can not connect to Solr: $conn_err_msg";

  # If the proxy responded with error message
  if($result && $result->res && $result->res->json && $result->res->json){
    $error_message = $result->res->json;
  }

  # If it is Solr that responded with an error message, we retrieve the message
  if($result && $result->res && $result->res->json && $result->res->json->{error}){
    $error_message = $result->res->json->{error}{msg};
  }
  return $error_message;
}


=head2 process_query
 Arg [1]     : string $query
 Arg [2]     : string $prefix (optional)
 Description : Changes the query to :
               1. Escape Solr special characters
               2. Set each word as mandatory
               3. Set each word as prefix
 Returntype  : string
 Status      : Stable
=cut
sub process_query {
  my ($query, $prefix) = @_;

  # * encoded is %2A
  return '%2A' unless length( $query //= '' );

  $prefix //= '';

  # IGNORING '/'
  $query =~ s/\//_/g;

  # ESCAPING
  # https://lucene.apache.org/core/2_9_4/queryparsersyntax.html
	# + - && || ! ( ) { } [ ] ^ " ~ * ? : \
  $query =~ s/([\+\-\&\|\!\(\)\{\}\[\]\^\"\~\*\?\:\\])/\\$1/g;
  $query =~ s/(\S+)/$prefix$1*/g;
  my @jquery = split(/ +/, $query);
  $query = join(' AND ', @jquery);
  my $url_encoded = url_escape( $query );
  return $url_encoded;
}


=head2 sanitize_for_solr
  Arg[1]      : string $text
  Description :
	Returns a string in whitch :
	* all Solr special characters are escaped
	* all special characters are URL encoded
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub sanitize_for_solr{
	my $text = shift @_;
	# https://lucene.apache.org/core/2_9_4/queryparsersyntax.html
	# + - && || ! ( ) { } [ ] ^ " ~ * ? : \
	$text =~ s/([\+\-\&\|\!\(\)\{\}\[\]\^\"\~\*\?\:\\])/\\$1/g;

	my $url_encoded = url_escape( $text );
	return $url_encoded;
}


=head2 create_description
  Arg[1]      : string $id
  Arg[2]      : string $d_field
  Arg[3]      : hash $highlighted : hash returned from solr
  Arg[4]      : hash $fields : hash returned from solr
  Arg[5]      : string $query
  Description : Highligting of prefixes from $query in the description fields,
                and retrieval of snippets
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub create_description {
  my ($id, $d_field, $highlighted, $fields, $query ) = @_;

  my $SNIPPET_SIZE = 20; # max snippet size in words

  # If Solr returned highlights, we use them
  if( defined $id && defined $highlighted ){
    my $highlighted_fields = $highlighted->{$id};
    my @h_descriptions = values %$highlighted_fields;
    if( @h_descriptions ){
      # Here we suppose that we allways have a list of snippets for a field in highlight
       my @flatened = map { @$_ } @h_descriptions;
        my $description = join( '...', @flatened );
        return $description;
    }
  }

  # If not, manually creating description
  my $f_description = $fields && $fields->{$d_field};
  if( $f_description && $query ){
    if (ref($f_description) eq 'ARRAY') {
      $f_description = join('...', @$f_description);
    }
    # Manual highlighting of matches
    # Return a match for each of the query words
    # 1. Get snippet (match)
    # 2. Put highlight tags around snippet (replace)
    my $snippets = [];
    my @prefixes = split(' ', $query );
    foreach my $prefix (@prefixes){
      # First match for each snippet
      if ($f_description =~ /(\b.{0,$SNIPPET_SIZE}\b$prefix\w*.{0,$SNIPPET_SIZE}\b)/i) {
        my $snippet = $1;
        $snippet =~ s/(\b$prefix\w*)/<strong>$1<\/strong>/i;
        push(@$snippets, $snippet);
      }
    }
    return join('...', @$snippets);
  }
  # If the field is a list, we concatenate itwit '...'

  return ''; # Could not create description
}


=head2 species_ens_to_vb
 Arg [1]     : string $ens_species
 Description :  anopheles_gambiae => Anopheles_gambiae
 Returntype  : string
 Status      : Stable
=cut
sub species_ens_to_vb {
  my ($ens_species) = @_;
  $ens_species = ucfirst($ens_species) if $ens_species;
  $ens_species = '"'.$ens_species.'"' if $ens_species;
  return $ens_species;
}


=head2 species_vb_to_ens
 Arg [1]     : string $vb_species
 Description :  Anopheles gambiae => anopheles_gambiae
 Returntype  : string
 Status      : Stable
=cut
sub species_vb_to_ens {
  my ($vb_species) = @_;
  $vb_species =~ s/[ -]/_/g if $vb_species;
  $vb_species = lc($vb_species) if $vb_species;
  return $vb_species;
}

=head2 _get_available_species_filter
 Arg[1]      : string $ignore_species (optional) Species to ignore (in ensembl format) when processing list of species
 Description : Returns a string that can be used in Solr query URL to restrict species in results
 to those for which we have VCF files
 Returntype  : string
 Status      : Stable
=cut
sub _get_available_species_filter {
  my ( $ignore_species ) = @_;
  my @raw_vcf_species = get_available_species_names();
  return __get_available_species_filter( \@raw_vcf_species, $ignore_species );
}

=head2 __get_available_species_filter
  Arg[1]      : list $ens_species List of available species in ensembl format
  Arg[2]      : string $ignore_species (optional) Species to ignore (in ensembl format) when processing list of species
  Description : Test friendly version of _get_available_species_filter
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub __get_available_species_filter {
  my ( $ens_species, $ignore_species ) = @_;
  my @vb_species = map { ($ignore_species && $_ eq $ignore_species) ? () : species_ens_to_vb( $_ ) } @$ens_species;
  return '('.join(' OR ', @vb_species).')';
}

=head2 get_location_autocomplete
 Arg [1]     : string $query
 Description : Creates an autosuggest results if the query is a valid genomic location
 Returntype  : list
 Status      : Stable
=cut
sub get_location_autocomplete {
  my ($query) = @_;
  my $location = execute_on_feature_type($query, sub {
    return [{identifier => $query, description => 'User submitted location inside the genome'}];
  });
}

1;
