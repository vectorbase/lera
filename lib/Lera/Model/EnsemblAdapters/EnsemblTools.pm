=head1 NAME

Lera::Model::EnsemblAdapters::EnsemblTools - Utilities to retrieve data from Vb perl API/ DB

=head1 DESCRIPTION

Utilitiesto retrieve data from Vb perl API/ DB

=cut

package Lera::Model::EnsemblAdapters::EnsemblTools;

use strict;
use warnings;

use Exporter qw(import);
use Config::General;

our @EXPORT = qw( execute_on_feature_type get_feature_type_from_id );
our @EXPORT_OK =  qw( get_chr_id );


=head2 execute_on_feature_type
  Arg[1]      : string $text
  Arg[2]      : function $if_coord_sub
  Arg[3]      : function $if_transcript_sub
  Arg[4]      : function $if_protein_sub
  Arg[5]      : function $if_gene_sub
  Description : Executes the sub corresponding to the identified feature type.
                Recognised features are : ( coordinated, transcript, proteinm gene )
  Returntype  : variable
  Exceptions  : none
  Status      : Stable

  TODO recognise location as : species:chr:start-stop
  TODO add support for protein locations, ex: AGAP004707-PA:1014
=cut

sub execute_on_feature_type {
  my ( $text, $if_coord_sub, $if_transcript_sub, $if_protein_sub, $if_gene_sub ) = @_;

  return unless $text;

  # TODO accept SNP coordinates too (CHR:START)
  if( $text =~ /^[A-Z0-9]+:\d+-\d+$/i ){
    my ($chr, $pos) = split(/:/, $text);
    my ($start, $end) = split(/-/, $pos);
    if( $end >= $start){ # valid location
        return $if_coord_sub && $if_coord_sub->($chr, $start, $end);
    }

  } elsif( $text =~ /^[A-Z0-9]+-R.$/ ){

    return $if_transcript_sub && $if_transcript_sub->();

  } elsif ( $text =~ /^[A-Z0-9]+-P.$/ ){

    return $if_protein_sub && $if_protein_sub->();

  } elsif ( $text =~ /^[A-Z0-9]+$/ ){

    return $if_gene_sub && $if_gene_sub->();

  } else {
    return;
  }

}


=head2 get_feature_type_from_id
  Arg[1]      : string $id
  Description : Returns the type of the feature (COORDINATES, TRANSCRIPT, PROTEIN, GENE) given its id
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub get_feature_type_from_id {
  my ( $id ) = @_;
  return execute_on_feature_type( $id,
    sub { return 'COORDINATES';},
    sub { return 'TRANSCRIPT';},
    sub { return 'PROTEIN';},
    sub { return 'GENE';}
  );
}


=head2 _get_chr_id
  Arg[1]      : DBConnection $dbc
  Arg[2]      : string $species
  Arg[3]      : string $chr
  Description : Retrieve the chromosome id in VB database, associated with a chromosome name
  Returntype  :
  Exceptions  : none
  Status      : Stable

=cut

sub get_chr_id {
  my ( $dbc, $species, $chr ) = @_;

  my $sth = $dbc->prepare("SELECT seq_region_id FROM seq_region WHERE name = ?", {});
  $sth->execute($chr) or die "Couldn't execute statement: " . $sth->errstr;

  my $rows = $sth->rows;
  die "Multiple matches for chromosome name\n" unless $rows eq 1;
  my $data = $sth->fetchrow_hashref();
  $sth->finish();
  return $data && $data->{'seq_region_id'};
}


1;
