=head1 DESCRIPTION

This library returns abstract data representations of Ensembl GENOME reference objects.
The goal is to have the same rendering code on the web browser side that can render any track with
Ensembl objects (transcripts, genes, proteins, genotypes, etc...)

DEPRECATED (use function from SolrAdapter instead)

=cut


package Lera::Model::EnsemblAdapters::Annotators::OrthologsAnnotator;

use strict;
use warnings;

use Exporter qw(import);
use Config::General;
use Data::Dumper;

use Lera::EnsemblRegistry;

our @EXPORT = qw( get_orthologs );

=head2 get_orthologs ()
 Arg [1]     : string $gene
 Description : Returns orthologs for a given gene
 Returntype  :
 Status      : Stable
=cut
sub get_orthologs {
  my ( $gene ) = @_;
  my $registry = get_registry();

  my $query_stable_id = 'AGAP004707';

  my $gene_member_adaptor = $registry->get_adaptor('Multi', 'compara', 'GeneMember');
  my $gene_member = $gene_member_adaptor->fetch_by_stable_id( $query_stable_id );

  # then you get the homologies where the member is involved

  my $homology_adaptor = $registry->get_adaptor('Multi', 'compara', 'Homology');
  my $homologies = $homology_adaptor->fetch_all_by_Member($gene_member);

  print "\nLooking for orthologs of $query_stable_id\n\n";

  foreach my $homology (@{$homologies}) {

    my ($g1, $g2) = @{$homology->gene_list()};

    my $target = $g1->stable_id() ne $query_stable_id ? $g1 : $g2;

    print $query_stable_id, "\t", $target->stable_id(), "\t", $target->taxon()->species(), "\t", $target->get_Slice()->name(), "\n";
    #print " slice ", $homology->get_Slice(),"\n";
  }

}

1;
