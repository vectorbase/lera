=head1 NAME

Lera::Model::EnsemblAdapters::Annotators::ConsequencesAnnotator - Retrieves consequences of genomic variations

=head1 DESCRIPTION

Retrieves consequences of genomic variations

=cut

package Lera::Model::EnsemblAdapters::Annotators::ConsequencesAnnotator;

use strict;
use warnings;

use Exporter qw(import);
use Config::General;
use Data::Dumper;

use Lera::EnsemblRegistry;
use Lera::Model::EnsemblAdapters::EnsemblTools qw( get_chr_id );

our @EXPORT = qw( create_consequences_cache get_transcript_consequences get_transcript_consequences_with_hash_marge );


# Declare consequences to ignore
my $ignore_consequences = {
  downstream_gene_variant => 1,
  upstream_gene_variant => 1,
  intron_variant => 1
};


=head2 create_consequences_cache
  Arg[1]      : string $species
  Arg[2]      : string $chr
  Arg[3]      : int $interval_start
  Arg[4]      : int $interval_end
  Description : Creates a hash allowing to retrieve consequences for each transcript,
                corresponding to a key consisting of chromosome:position:allele
  Returntype  : list (cache, transcripts)
  Exceptions  : species must be declared
                chromosome must be declared
                start must be declared
                end must be declared
                Can not connect to variation database for species
                Couldn't execute statement
  Status      : Stable
=cut

sub create_consequences_cache {
  my ( $species, $chr, $interval_start, $interval_end ) = @_;

  die "species must be declared\n" unless $species;
  die "chromosome must be declared\n" unless $chr;
  die "start must be declared\n" unless $interval_start;
  die "end must be declared\n" unless $interval_end;

  my $registry = get_registry();
  my $dba = $registry->get_DBAdaptor( $species, 'variation');
  my $dbc = $dba && $dba->dbc();
  die "Can not connect to variation database for $species\n" unless $dbc;

  my $chr_id = get_chr_id( $dbc, $species, $chr );

  # 0 : variation_name
  # 1 : seq_region_start
  # 2 : seq_region_end
  # 3 : feature_stable_id
  # 4 : allele_string
  # 5 : consequence_types
  my $sth = $dbc->prepare("
  SELECT vf.variation_name, vf.seq_region_start, vf.seq_region_end, tv.feature_stable_id, tv.allele_string, tv.consequence_types

  FROM transcript_variation tv
  JOIN variation_feature vf on vf.variation_feature_id = tv.variation_feature_id

  WHERE vf.seq_region_id = ?
  AND vf.seq_region_start >= ?
  AND vf.seq_region_start <= ?
  AND vf.seq_region_end >= ?
  AND vf.seq_region_end <= ?

  ");

  $sth->execute( $chr_id, $interval_start, $interval_end, $interval_start, $interval_end ) or die "Couldn't execute statement: " . $sth->errstr;
  my $rows = $sth->rows;
  #print("$rows transcript-variation relationships retrieved.\n");

  my $cache = {};
  my $transcripts = {};

  # http://stackoverflow.com/questions/8605322/speeding-up-perl-dbi-fetchrow-hashref

  while (my $rows = $sth->fetchall_arrayref(undef, 10000)) {
    for my $data( @{$rows} ) {

      my @alleles = split( /[\/|]/ ,$data->[4] );
      my $alt_allele = $alleles[1];
      my $transcript_id = $data->[3];
      my $start = $data->[1];
      my $end = $data->[2];

      my $pos = $start;
      my $key = "$chr:$pos:$alt_allele";
      my @consequences = split( /,/ , $data->[5] );
      # Filtering consequences
      my @filtered_consequences = grep { ! $ignore_consequences->{$_}  } @consequences;
      my %consequences_set = map { $_ => 1 } @filtered_consequences;

      if( %consequences_set ){ # only if hash not empty after consequnces filtering
        $cache->{$key}{$transcript_id} = \%consequences_set;
        $transcripts->{$transcript_id}++;
      }
    }
  }

  $sth->finish();

  return ($cache, $transcripts);
}


=head2 get_transcript_consequences
  Arg[1]      : hash $consequences_cache
  Arg[2]      : string $chr
  Arg[3]      : int $pos
  Arg[4]      : list $alleles
  Description : Uses the created consequences_cache to retrieve all consequences and transcripts
                for a specific allele in a variation
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub get_transcript_consequences {
  my ( $consequences_cache, $chr, $pos, $alleles ) = @_;

  my $genotype_consequences = {}; # We assume the possibility of multiple matching consequences

  foreach my $allele (@$alleles){
    my $key = "$chr:$pos:$allele";
    my $allele_consequences = $consequences_cache->{$key};
    if( $allele_consequences ){
      while ( my ($transcript_id, $consequences_hash) = each %$allele_consequences ){
        $genotype_consequences->{$transcript_id} //= {};
        $genotype_consequences->{$transcript_id} = { %{$genotype_consequences->{$transcript_id}}, %$consequences_hash };
      }
    }

  }

  # TODO takes almost half of the time of the sub
  while ( my ($transcript_id, $consequences_hash) = each %$genotype_consequences )
  {
    my @consequences = sort keys %$consequences_hash;
    $genotype_consequences->{$transcript_id} = join( ', ', @consequences);
  }

  return $genotype_consequences;
}


1;
