=head1 NAME

Lera::Model::EnsemblAdapters::Tracks::Peptype

=head1 DESCRIPTION

This library returns abstract data representations of PROTEIN GENOTYPE objects.
The goal is to have the same rendering code on the web browser side that can render any track with
Ensembl objects (transcripts, genes, proteins, genotypes, etc...)

=cut

package Lera::Model::EnsemblAdapters::Tracks::Peptype;

use strict;
use warnings;

use Exporter qw(import);
use List::Util qw( any );

use Tools qw( user_die );
use Conf;
use Lera::Model::BcftoolsAdapters::BcfTools qw( execute_bcftools );
use Lera::Model::BcftoolsAdapters::Tracks::Genotype qw ( _get_alleles _get_genotype is_missing );

use Data::Dumper;

use Lera::EnsemblRegistry;

our @EXPORT_OK = qw( get_protein_genotypes );

my $MAX_SAMPLES = get_bloc_config( 'conf/tracks.ini', 'peptype', 'MAX_SAMPLES') || 20;

my $PEPTYPE_COLOR = {
  COMPLEX => '#01579B',
  REF     => 'rgb(76,175,80)',
  ALT     => 'rgb(244,67,54)'
};

=head2 _get_all_colors_hash
  Description : Returns the hash associating colors to peptype type
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _get_all_colors_hash {
  return $PEPTYPE_COLOR;
}

=head2 get_protein_genotypes
 Arg [1]     : object $registry Ensembl registry object
 Arg [2]     : hash $params Parameters of the interval on the reference
 Arg [3]     : list $filters Samples for which we want the peptypes
 Description : Returns the track containing peptypes for a given interval.
 We have to start by transcript-consequences because we start with a protein/transcript id.
 Based on admission of UNIQUE Protein-Transcript mapping
 Returntype  : hash
 Status      : Stable
 TODO Store exons in protein reference, and use them as multiple intervals asked to bcftools query
=cut
sub get_protein_genotypes{
  my ( $params, $trackParams ) = @_;

  my $samples = $trackParams && $trackParams->{filters} && $trackParams->{filters}{entities};

  user_die("Please click on <i class='fa fa-edit'></i> to add samples to this display") unless $samples && scalar @$samples;
  user_die("You can not add more than $MAX_SAMPLES samples to a track") if ( scalar @$samples ) > $MAX_SAMPLES;

  my $track = { entities => {} };

  my $species = $params->{'species'};
  my $transcript_id = $params->{'transcript_id'};

  die "species must be declared\n" unless $species;
  die "transcript_id must be declared\n" unless $transcript_id;

  my ( $vars_hash, $chr ) = _get_vcf_vars_hash( $species, $transcript_id, $samples );

  # LOOP OVER VARIATIONS
  _process_potein_variations( $species, $transcript_id, sub {
    my ( $t_start, $p_start, $ref, $alt, $p_ref, $p_alt, $sift ) = @_;

    my $var_hash = $vars_hash->{"$chr:$t_start"};
    die "Variation from transcript_variation table was not found in the VCF : $chr:$t_start\n" unless $var_hash;
    # We parse samples/genotypes only for variations found in the protein
    my $genotypes = _parse_sg_string_to_hash($var_hash->{sglist}, $var_hash->{ref}, $var_hash->{alt} );

    # LOOP OVER SAMPLES
    foreach my $sample (@$samples) {
      # Create entity if it does not exists yet
      my $components = ( $track->{entities}{$sample} ||= { name => $sample, components => {} } )->{components};
      my $genotype = $genotypes->{$sample};
      _add_peptype( $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start );
    }
  });

  # Remove entities with no components
  foreach my $sample_name (keys %{$track->{entities}} ) {
    delete $track->{entities}{$sample_name} unless scalar %{$track->{entities}{$sample_name}{components}};
  }

  return $track;
}


=head2 _add_peptype
  Arg[1]      :
  Description : Adds or updates a peptype given the variation and the genotype.
  Returntype  : none
  Exceptions  : none
  Status      : Stable
=cut

sub _add_peptype {
  my ( $peptypes, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $genomic_position ) = @_;

  return unless $genotype;
  my $peptype_id = "$p_start#$sample";

  # True if the peptide is derived from a homozygous ref genotype
  my $genotype_homo_ref = ( @$genotype == grep { $_ eq $ref } @$genotype );

  foreach my $gen_allele (@$genotype){

    # The genotype allele can be :
    # 1. alt, not corresponding to this alt_pep (ignore)
    next unless $gen_allele eq $alt || $gen_allele eq $ref;

    # 2. alt, corresponding to alt_pep (add component with alt_pep)
    my $gen_pep_alt = $gen_allele eq $alt ? $p_alt : undef;

    # 3. ref (add component with no alt_pep)

    # Using three letter codes
    # TODO Maybe we should but this in get_protein_genotypes before calling _add_peptype ?
    my $ref_code = get_three_letter_code( $p_ref );
    my $alt_code = get_three_letter_code( $gen_pep_alt );

    my $peptype = $peptypes->{$peptype_id} ||= { start => $p_start, shape => 'thick-full',
      data => { position => $p_start, ref => $ref_code }};

    merge_components( $peptype, $alt_code, $genomic_position, $genotype_homo_ref );
  }
}

=head2 merge_components ()
 Arg [1]     : hash $peptype
 Arg [2]     : string $alt_pep_allele
 Arg [3]     : integer $genomic_position (optional) Position of the variation associated to this peptide
 Arg [4]     : string $genotype_homo_ref (optional) True if the peptide is derived from a homozygous ref genotype
 Description : Merges an existing peptype (genotype mapped to protein) with alt allele and sift prediction.
 We need this merge because alt alleles for the same peptide are on different rows in the transcript_variation table,
 so when looping on these rows we need to merge together information concerning the same peptide.
 from another peptype
 Status      : Stable
=cut
sub merge_components {
  my ( $peptype, $alt_pep_allele, $genomic_position, $genotype_homo_ref ) = @_;

  if( $alt_pep_allele ){
    $peptype->{data}{alt} ||= [];
    push( @{$peptype->{data}{alt}}, $alt_pep_allele )
      unless any { $_ eq $alt_pep_allele } @{$peptype->{data}{alt}}
  }

  if( defined $genomic_position){
    $peptype->{data}{genotypes} ||= {};
    $peptype->{data}{genotypes}{$genomic_position} = $genotype_homo_ref ? 'HOMO_REF' : 'NON_HOMO_REF';
  }

  # Recalculate color
  $peptype->{color} = get_color( $peptype->{data}{alt}, $peptype->{data}{genotypes} );

  # Recalculate custom title
  my @residues = ($peptype->{data}{ref}, @{$peptype->{data}{alt} || []});
  $peptype->{title} = join(", ", @residues);
}


=head2 get_color ()
 Arg [1]     : list $_[0] Array of alt alleles (pep)
 Arg [2]     : hash $_[1] Hash of genotype types for each genomic position of variations linked to the peptide
               ex: { 155556 => 'HOMO_REF' } means for example a G/G genotype on position 155556 on the genome
 Description : Returns red if alternative a.a. axists for this position, or green of not.
               Returns gray if there is least one alt allele on BOTH positions
 Returntype  : string
 Status      : Stable
 TODO Even if multiple genomic positions, if all genotypes are ref/ref, we are OK
=cut
sub get_color {
  return $PEPTYPE_COLOR->{COMPLEX} if scalar (grep { $_ ne 'HOMO_REF' } values %{$_[1]} ) > 1;
  return $_[0] && @{$_[0]} ? $PEPTYPE_COLOR->{ALT} : $PEPTYPE_COLOR->{REF};
}


=head2 _process_potein_variations
  Arg[1]      : string $species
  Arg[2]      : string $transcript_id
  Arg[3]      : function $processor ( $p_start, $p_end, $ref_allele, $alt_allele, $ref_pep_allele, $alt_pep_allele, $sift )
  Description : For each row of transcript variation, executes the 'processor' sub
  Exceptions  : Can not connect to variation database
                Couldn't execute statement
  Status      : Stable
=cut

sub _process_potein_variations {
  my ( $species, $transcript_id, $processor ) = @_;

  my $registry = get_registry();
  my $dba = $registry->get_DBAdaptor( $species, 'variation');
  my $dbc = $dba && $dba->dbc();
  die "Can not connect to variation database for $species\n" unless $dbc;

  my $sth = $dbc->prepare("
  SELECT tv.allele_string, tv.translation_start, tv.translation_end, tv.pep_allele_string,
  tv.polyphen_prediction, tv.sift_prediction, vf.seq_region_start

  FROM transcript_variation tv
  JOIN variation_feature vf on vf.variation_feature_id = tv.variation_feature_id

  WHERE tv.feature_stable_id = ?
  AND tv.pep_allele_string is not null
  AND tv.translation_start is not null
  AND tv.translation_end is not null
  ");

  $sth->execute( $transcript_id ) or die "Couldn't execute statement: " . $sth->errstr;
  my $rows = $sth->rows;

  while (my $data = $sth->fetchrow_hashref()) {
    my @alleles = split( /[\/|]/ , uc( $data->{'allele_string'} ) ); # ref allele can sometimes be in lowercase
    my @pep_alleles = split( /[\/|]/ ,$data->{'pep_allele_string'} );
    my $start = $data->{'seq_region_start'};
    my $p_start = $data->{'translation_start'};
    my $sift = $data->{'sift_prediction'} || undef;

    my $ref_allele = $alleles[0];
    my $alt_allele = $alleles[1];
    my $ref_pep_allele = $pep_alleles[0];
    my $alt_pep_allele = $pep_alleles[1];

    $processor->( $start, $p_start, $ref_allele, $alt_allele, $ref_pep_allele, $alt_pep_allele, $sift );
  }

  $sth->finish();
}


=head2 _get_vcf_vars_hash
  Arg[1]      :
  Description : Returns a hash of variations corresponding to a given transcript
  Returntype  : list ( { chr:start => { ref => ..., alt => ..., sglist => ...} }, $chr )
  Exceptions  : Could not retrieve genomic interval for transcript id
  Status      : Stable
  TODO  Alternatively get list of exon coordinates
=cut

sub _get_vcf_vars_hash {
  my ( $species, $transcript_id, $samples ) = @_;
  my $registry = get_registry();
  #  Retrieve transcript to have (chr, start, end, seq_region_name, seq_region_name_syn)
  my $slice_adaptor = $registry->get_adaptor( $species, 'Core', 'Slice' );
  my $slice = $slice_adaptor->fetch_by_transcript_stable_id( $transcript_id );
  die "Could not retrieve genomic interval for transcript id $transcript_id\n" unless $slice;

  my $chr      = $slice->seq_region_name();
  my @chr_synonyms = map { $_->{name} } @{$slice->get_all_synonyms()};
  my $start    = $slice->start();
  my $end      = $slice->end();

  my $format = "%CHROM\t%POS\t%ID\t%REF\t%ALT\t[#%SAMPLE=%GT]\n";

  my ( $output, $about ) = execute_bcftools( "query -f '$format'", $species, $chr, $start, $end, $samples, \@chr_synonyms);

  return ( _parse_output_to_var_hash( $output, $chr ), $chr );
}

=head2 _parse_output_to_var_hash
  Arg[1]      : string $output the stdout from bcftools
  Arg[2]      : hash $about Various statistics about the parsing of data will be stored inside
  Arg[3]      : string $def_chr
  Description : Parses the output from bcftools to create a hash allowing to quickly access the genotype string (not splitted)
                for each given variation position (in coordinated where indels are adapted for ensembl)
  Returntype  : hash { chr:start => { ref => ..., alt => ..., sglist => ...} }
  Exceptions  : none
  Status      : Stable
=cut

sub _parse_output_to_var_hash {
  my ( $output, $def_chr ) = @_;
  my $vars_hash = {};
  my $parse_start = time();

  my @variations = split( /\n/ , $output );
  my $nb_variations = scalar @variations;

  foreach my $variation (@variations) {

    # Follows the order declared in $format
    my ($chr, $pos, $variation_name, $raw_ref, $alleles_str, $sg_list) = split( /\t/, $variation );

    # If a list of chromosomes is provided, we replace the chromosome from the vcf (which can be a synonym)
    # by the official chromosome name
    $chr = $def_chr if defined $def_chr;
    my ( $ref, $alt_alleles, $start ) = _get_alleles( $raw_ref, $alleles_str, $pos );

    $vars_hash->{"$chr:$start"} = { ref => $ref, alt => $alt_alleles, sglist => $sg_list};
  }

  return $vars_hash;
}


=head2 _parse_sg_string_to_hash
  Arg[1]      : string $sg_list serialized couples of samples and genotypes
  Arg[2]      : string $ref reference allele
  Arg[3]      : string $alleles_string alternative alleles
  Description : Returns a hash associating a genotype array for a given sample name.
                Note that missing genotypes are ignored.
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _parse_sg_string_to_hash {
  my ( $sg_list, $ref, $alt_alleles ) = @_;
  my $sg_hash = {};

  foreach my $sg (split( /#/, $sg_list )){
    if( $sg ){
      my ($sample_name, $genotype_str) = split( /=/, $sg );
      next if is_missing( $genotype_str );
      $sg_hash->{$sample_name} = _get_genotype( $ref, $alt_alleles, $genotype_str );
    }
  }

  return $sg_hash;
}

# From http://cpansearch.perl.org/src/CJFIELDS/BioPerl-1.007001/Bio/SeqUtils.pm
our %THREECODE = (
    'A' => 'Ala',
    'B' => 'Asx',
    'C' => 'Cys',
    'D' => 'Asp',
    'E' => 'Glu',
    'F' => 'Phe',
    'G' => 'Gly',
    'H' => 'His',
    'I' => 'Ile',
    'K' => 'Lys',
    'L' => 'Leu',
    'M' => 'Met',
    'N' => 'Asn',
    'P' => 'Pro',
    'Q' => 'Gln',
    'R' => 'Arg',
    'S' => 'Ser',
    'T' => 'Thr',
    'V' => 'Val',
    'W' => 'Trp',
    'Y' => 'Tyr',
    'Z' => 'Glx',
    'X' => 'Xaa',
    '*' => 'Ter',
    'U' => 'Sec',
    'O' => 'Pyl',
    'J' => 'Xle'
);

=head2 get_three_letter_code
  Arg[1]      : string $one_letter_code : One letter code of the amino acid
  Description : Returns the three letter code of the amino acid. Returns undefined if undefined submitted
  Note that Bio::SeqUtils allows only to translate a sequence of one letter codes to three letter codes,
  it would bu a waste to use this function with each individual letter.
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub get_three_letter_code {
  my ( $one_letter_code ) = @_;
  return unless $one_letter_code; # GIGO
  return $THREECODE{$one_letter_code} || '?';
}


1;
