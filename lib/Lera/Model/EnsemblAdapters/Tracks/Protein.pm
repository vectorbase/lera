=head1 DESCRIPTION

This library returns abstract data representations of Ensembl PROTEIN objects.
The goal is to have the same rendering code on the web browser side that can render any track with
Ensembl objects (transcripts, genes, proteins, genotypes, etc...)

=head1 METHODS

=cut


package Lera::Model::EnsemblAdapters::Tracks::Protein;

use strict;
use warnings;

use Exporter qw(import);
use Term::ANSIColor qw(:constants);
use List::Util qw(max min);

use Lera::EnsemblRegistry;

use Data::Dumper;

our @EXPORT = qw( get_protein_features );


=head2 get_protein_features
  Arg[1]      : hash $params
  Description : Returns the track for a protein for a given transcript or protein id
  Returntype  : hash
  Exceptions  : transcript_id and species are mandatory parameter
  Status      : Stable
=cut

sub get_protein_features{
   my ( $params ) = @_;

	 die( "transcript_id and species are mandatory parameter.\n" ) unless defined $params->{transcript_id} && defined $params->{species};

   my $track = {
     entities => {},
     categories => { source => "entity" }
   };

   my $only_domains = 0; # track parameter ?

   my $registry = get_registry();

   my $transcript_adaptor = $registry->get_adaptor( $params->{species}, 'Core', 'Transcript' );
   my $transcript = $transcript_adaptor->fetch_by_stable_id( $params->{transcript_id} );

   die "Could not retrieve transcript ".$params->{transcript_id}." for species ".$params->{species}."\n" unless $transcript;

   my $translation = $transcript->translation();

   my $pfeatures = $only_domains ? $translation->get_all_DomainFeatures() : $translation->get_all_ProteinFeatures();

   while ( my $pfeature = shift @{$pfeatures} ) {

     my $e_id = $pfeature->hseqname().'-'.$pfeature->start().'-'.$pfeature->end();
     next unless $e_id;

     #TODO Many components with different start/stop can be part of an entity with the same $pfeature->hseqname() ??
     $track->{entities}{$e_id} = {
        name        => $pfeature->hseqname(),
        description => get_description( $pfeature ),
        components  => {},
        start       => $pfeature->start(),
        end         => $pfeature->end(),
        shape       => 'thick-full',
        data        => {
          source    => $pfeature->analysis()->db()
        }
     } unless $track->{entities}{$e_id} ;

     my $entity = $track->{entities}{$e_id};

     set($entity, "interpro", $pfeature->interpro_ac() );
   }

  return $track;
}

=head2 get_description
  Arg[1]      :
  Description : WSe choose between the hit_description and the interpro description.
                Most of the time the longest description is the best...
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub get_description {
  my ( $pfeature ) = @_;
  my $ipd = $pfeature->idesc() || '';
  my $hd = $pfeature->hdescription() || '';
  return length($ipd) > length($hd) ? $ipd : $hd;
}

=head2 set
  Inspired by http://stackoverflow.com/questions/16849830/only-create-perl-hash-key-value-pair-if-value-is-defined
  Arg[1]      : (hash) $feature : entity or component
  Arg[2]      : (string) $key
  Arg[3]      : (string) $value
  Description : Adds the key/value to component data only if value not undefined
  Exceptions  : none
  Status      : Stable
=cut

sub set {
    my $feature = shift;
    my $key  = shift;
    my $val  = shift;

    $feature->{data}{$key} = $val if $val;
}


1;
