=head1 DESCRIPTION

This library returns abstract data representations of Ensembl TRANSCRIPT objects.
The goal is to have the same rendering code on the web browser side that can render any track with
Ensembl objects (transcripts, genes, proteins, genotypes, etc...)

=head1 METHODS

=cut


package Lera::Model::EnsemblAdapters::Tracks::Transcript;

use strict;
use warnings;

use Exporter qw(import);
use Term::ANSIColor qw(:constants);

use Tools qw( user_die );
use Conf;
use Lera::EnsemblRegistry;

use Data::Dumper;
$Data::Dumper::Terse = 1;

our @EXPORT = qw( get_transcripts );


my $MAX_INTERVAL = get_bloc_config( 'conf/tracks.ini', 'transcript', 'MAX_INTERVAL') || 1e7;

=head2 get_transcripts
  Arg[1]      : hash $params
  Description : Returns the track of transcripts centered on a given genomic interval
  Returntype  : hash
  Exceptions  : species, start and end are mandatory parameters
                Refusing to serve transcripts for an intervalbigger than THRESHOLD_INTERVAL
                Could not resolve genomic region for params
  Status      : Stable
=cut

sub get_transcripts{
   my ( $params ) = @_;

   die( "species, start and end are mandatory parameters.\n" ) unless defined $params->{start} && defined $params->{end} && defined $params->{species};

   my $track = {
     entities => {},
     categories => { biotype => "entity" }
   };

   my $registry = get_registry();

   my $start                         = $params->{'start'};
   my $end                           = $params->{'end'};
   my $interval                      = $end - $start;

   user_die("Refusing to serve transcripts for an interval ($interval) bigger than $MAX_INTERVAL") unless $interval <= $MAX_INTERVAL;

   my $slice_adaptor                 = $registry->get_adaptor( $params->{'species'}, 'Core', 'Slice' );
   my $slice                         = $slice_adaptor && $slice_adaptor->fetch_by_region( $params->{'coord_system_name'}, $params->{'seq_region_name'}, $start, $end);

   die "Could not resolve genomic region for ".Dumper($params)."\n" unless $slice;

   my $load_exons                    = 1;
   my $transcripts                   = $slice->get_all_Transcripts( $load_exons, undef, undef, undef, undef );

   for my $tr (@{ $transcripts }){
     my $tr_id = $tr->stable_id();
     my $gene = $tr->get_Gene();

     # main entity object
     my $entity   = {
       name        => $tr_id,
       # SAD the 'description' field is empty for all gambiae transcripts, and retrieving the description from a gene makes
       # additional DB requests...
       description => $gene && $gene->description(),
       start       => $tr->seq_region_start(),
       end         => $tr->seq_region_end(),
       components  => {},
       data        => {
         strand    => ($tr->strand() < 0) ? '-' : '+',
         biotype   => $tr->biotype()
       }
     };

     # TODO Implement better method to get all exons for all transcripts in the same time
     my $exons = $tr->get_all_Exons();
     for my $exon ( @{ $exons } ) {
        my $exon_id = $exon->stable_id();
        my $component = {
          data => {
            feature => 'Exon',
            pos => $exon->seq_region_start().'-'.$exon->seq_region_end()
          },
          name => $exon_id,
          start => $exon->seq_region_start(),
          end => $exon->seq_region_end(),
          shape => 'thick-full'
        };
        $entity->{components}{$exon_id} = $component;

        #TODO UTR (from CDS coordinates)
     }
     my $introns = $tr->get_all_Introns();
     for my $intron ( @{ $introns } ) {
        my $intron_id = $tr_id.$intron->seq_region_start().'-'.$intron->seq_region_end();
        my $component = {
          start => $intron->seq_region_start(),
          end => $intron->seq_region_end(),
          shape => 'thin'
        };
        $entity->{components}{$intron_id} = $component;

        #TODO UTR (from CDS coordinates)
     }

     $track->{entities}{$tr_id} = $entity;

   }

   return $track;
}


1;
