=head1 NAME

Lera::Model::EnsemblAdapters::Tracks::VariationsDensity : Shows the variation density in buckets of given size

=head1 DESCRIPTION

Shows the variation density in buckets of given size

=cut

package Lera::Model::EnsemblAdapters::Tracks::VariationsDensity;

use strict;
use warnings;

use Exporter qw( import );
use Term::ANSIColor qw(:constants);
use List::Util qw( min max );
use Number::Format qw( round );

use Tools qw( user_die );
use Conf;
use Lera::EnsemblRegistry;
use Lera::Model::EnsemblAdapters::EnsemblTools qw( get_chr_id );

use Data::Dumper;
$Data::Dumper::Terse = 1;

our @EXPORT_OK = qw( get_variations_density );

my $MAX_INTERVAL = get_bloc_config( 'conf/tracks.ini', 'variations_density', 'MAX_INTERVAL') || 1e7;
my $BUCKET_SIZE = get_bloc_config( 'conf/tracks.ini', 'variations_density', 'BUCKET_SIZE') || 4;


=head2 get_variations_density
  Arg[1]      :
  Description :
  Returntype  :
  Exceptions  : none
  Status      : Stable

-----------------232700--------------------------232800-------------------
--------232650---------------232749-|-232750-------------------232849-----
-----------------BUCKET_1-----------------------BUCKET_2------------------

=cut

sub get_variations_density{
   my ( $params ) = @_;

   my $species  = $params->{'species'};
   my $chr      = $params->{'seq_region_name'};
   my $interval_start    = $params->{'start'};
   my $interval_end      = $params->{'end'};
   my $interval = $interval_end - $interval_start + 1;

   die "species must be declared\n" unless $species;
   die "chromosome must be declared\n" unless $chr;
   die "start must be declared\n" unless $interval_start;
   die "end must be declared\n" unless $interval_end;
   user_die("Refusing to show variations density for an interval ($interval) bigger than $MAX_INTERVAL") unless $interval <= $MAX_INTERVAL;

   my $registry = get_registry();
   my $dba = $registry->get_DBAdaptor( $species, 'variation');
   my $dbc = $dba && $dba->dbc();
   die "Can not connect to variation database for $species\n" unless $dbc;

   my $chr_id = get_chr_id( $dbc, $species, $chr );

   my $track = {
     entities => { density => { components => {} } },
   };


   my $sth = $dbc->prepare("
     SELECT ROUND(seq_region_start, - $BUCKET_SIZE) AS bucket, COUNT(*) AS count

     FROM variation_feature

     WHERE seq_region_id = ?

     AND seq_region_start >= ?
     AND seq_region_start <= ?
     AND seq_region_end >= ?
     AND seq_region_end <= ?

     GROUP  BY bucket
   ");

   # Correcting intervals to match bucket limits
   ( $interval_start, $interval_end ) = _get_interval_start_end( $interval_start, $interval_end, $BUCKET_SIZE );

   $sth->execute( $chr_id, $interval_start, $interval_end, $interval_start, $interval_end ) or die "Couldn't execute statement: " . $sth->errstr;

   while (my $row = $sth->fetchrow_hashref) {
        my $bucket_id = $row->{bucket};
        my $count = $row->{count};

        my ($start, $end) = _get_bucket_start_end( $bucket_id, $BUCKET_SIZE );

        # Avoid going over chromosomes limits
        $start = max( $start, $params->{'min'} );
        $end = min( $end, $params->{'max'} );

        my $density = _get_bucket_density( $start, $end, $count );

        $track->{entities}{density}{components}{$bucket_id} = {
          start => $start,
          end => $end,
          name => "$chr:$start-$end",
          color => _get_color( $density ),
          heightFactor => $density,
          data => {
            bucket_size => $end - $start + 1,
            density => sprintf( "%.2f", $density ),
            variations_count => $count
          }
        }
   }

   $sth->finish();

   return $track;
}

=head2 _get_bucket_start_end
  Arg[1]      : string $bucket_id The number to which genomic positions are rounded
  Arg[2]      : integer $round_factor The decimal precision to which a number is (negatively) rounded
  Description : Given a bucket id (which is the rounded form of all numbers rounded to that form),
                We return the limits of the interval containing all numbers that will be rounded to that bucket id.
                All numbers smaller than the round factor (for example trying to round 8 to the closest hundred) will
                go in the bucket 0.

  Example     : If the bucket id is 106700, and round factor is 2 (which means we round all numbers to the closest hundred)
                the the limits are [106650, 106749]. This means that all numbers between 106650 and 106749 will be rounded to
                106700 with the gicen round factor.
  Returntype  : list ( bucket_start, bucket_end )
  Exceptions  : none
  Status      : Stable

=cut

sub _get_bucket_start_end {
  my ( $bucket_id, $round_factor ) = @_;
  my $half_size = ( 10**$round_factor ) / 2;
  my $start = $bucket_id - $half_size;
  # start is allways >=1
  $start = $start >= 1 ? $start : 1;

  my $end = $bucket_id + $half_size - 1;
  return ( $start, $end );
}


=head2 _get_interval_start_end
  Arg[1]      :
  Description :
  Returntype  :
  Exceptions  : none
  Status      : Stable

=cut

sub _get_interval_start_end {
  my ( $interval_start, $interval_end, $round_factor ) = @_;

  my $start_bucket_id = round( $interval_start, - $round_factor );
  my $buckets_start = (_get_bucket_start_end( $start_bucket_id, $round_factor ))[0];

  my $end_bucket_id = round( $interval_end, - $round_factor );
  my $buckets_end = (_get_bucket_start_end( $end_bucket_id, $round_factor ))[1];

  return ( $buckets_start, $buckets_end );
}


=head2 _get_color
  Arg[1]      : float $density : [0;1]
  Description : Associates a given color string to a density value.
  The color is between blue (low density) and red (high density)
  Returntype  : string
  Exceptions  : none
  Status      : Stable

  TODO We should set a limit after which the density is height, for example over 1/2 is estimated heigh
=cut


sub _get_color {
  my ( $density ) = @_;
  $density = 1 if $density > 1;
  $density = 0 if $density < 0;
  my $h = 240 + int( 120 * $density ); # h is between 245 and 360 (max h)
  return "hsl($h,50%,50%)";
}

=head2 _get_bucket_density
  Arg[1]      : integer $bucket_start
  Arg[2]      : integer $bucket_end
  Arg[3]      : integer $variants_count Number of variations in the bucket
  Description : Returns a float value [0;1] corresponding to the proportion of variations in a
  genomic interval, calculated as log(count of variations + 1) / log( end - start + 1 )
  Returntype  : float
  Exceptions  : none
  Status      : Stable
  TODO Use log ???
=cut

sub _get_bucket_density {
  my ( $bucket_start, $bucket_end, $variants_count ) = @_;
  my $interval_size = $bucket_end - $bucket_start + 1;
  
  if ($variants_count > 0 and $interval_size > 0) {
    return log($variants_count + 1) / log($interval_size + 1);
  } else {
    return 0;
  }
}

1;
