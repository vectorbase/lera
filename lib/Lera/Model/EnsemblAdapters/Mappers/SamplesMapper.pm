
=head1 NAME

Lera::Controller::Mapper - Maps sample names to popbio ids

=head1 DESCRIPTION

Maps sample names to popbio ids

=cut

package Lera::Model::EnsemblAdapters::Mappers::SamplesMapper;

use strict;
use warnings;

use Exporter qw(import);
use List::Util qw( uniq );

use Tools;
use Lera::EnsemblRegistry;
use Conf;

our @EXPORT_OK = qw( to_popbio_ids build_synonyms_hash );

=head2 create_synonyms_hash
  Arg[1]      : string $species
  Arg[2]      : list $samples
  Description : Creates a dictionnary of samples names synonyms
  Returntype  : hash
  Exceptions  : none
  Status      : New

  TODO Cache the synonyms hash for each species
=cut

sub to_popbio_ids {
  my ( $species, $samples ) = @_;

  my $synonyms = build_synonyms_hash( $species );

  # If no id is found for the synonym, we keep the synonym as it is. If an id is found, the synonym is replaced by the id.
  my @matches = map { $synonyms->{$_} || $_ } @$samples;

  # Multiple synonyms can point to the same sample, so we need to remove duplicates
  @matches = uniq @matches;

  return \@matches;
}

=head2 build_synonyms_hash
  Arg[1]      : string $species
  Description : Builds a hash mapping sample synonyms to popbio ids, for a given species
  Returntype  : hash
  Exceptions  : none
  Status      : New
=cut

sub build_synonyms_hash {
  my ( $species ) = @_;

  my $registry = get_registry();
  my $dba = $registry->get_DBAdaptor( $species, 'variation');
  my $dbc = $dba && $dba->dbc();

  die "Can not connect to variation database to retrieve ids for samples names for $species\n" unless $dbc;

  my $sth = $dbc->prepare("
    SELECT s.name as synonym, t.name as popBioId FROM individual_synonym s
    JOIN individual_synonym t on s.individual_id = t.individual_id
    JOIN source ts ON ts.source_id = t.source_id
    WHERE s.synonym_id != t.synonym_id AND ts.name = \"VBPopBio\"
  ");

  $sth->execute( ) or die "Couldn't retrieve PopBio ids for samples names: " . $sth->errstr . "\n";
  my $rows = $sth->rows;

  my $synonyms = {};

  while (my $data = $sth->fetchrow_hashref()) {
    my $source = $data->{'synonym'};
    my $target = $data->{'popBioId'};
    die "Multiple popBio ids found for sample $source\n" if defined $synonyms->{$source};
    $synonyms->{$source} = $target;
  }
  $sth->finish();

  return $synonyms;
}

1;
