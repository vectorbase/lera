=head1 NAME

Lera::Model::EnsemblAdapters::References::ProteinReference - Reference allowing to represent features on a Protein

=head1 DESCRIPTION

This reference expects following parameters :

species
query ( protein id or transcript id )

Any track depending on this reference can expect to receive following parameters on update :

species
start         : position on the protein
end           : position on the protein
protein_id
transcript_id : transcript that was translated into this protein
min           : first a.a. position, usually 1
max           : last a.a. position
sequence      : sequence of a.a.

=cut

package Lera::Model::EnsemblAdapters::References::ProteinReference;

use strict;
use warnings;

use Exporter qw(import);
use Term::ANSIColor qw(:constants);

use Tools;
use Lera::Model::EnsemblAdapters::EnsemblTools;
use Lera::EnsemblRegistry;

use Data::Dumper;

our @EXPORT = qw( get_protein_reference autocomplete );


=head2 get_protein_reference
  Arg[1]      : hash $params ( species, query )
  Description : Resolves the proteic references. This means retrieving all information that tracks may need
                from the parameters supplied. For example when supplied with species and protein id, transcript id,
                we sill find the corresponding protein id, transcript id, protein bounds
  Returntype  : hash
  Exceptions  : Species is a mandatory parameter
                Could not retrieve reference for transcript for species
                Could not find protein for transcript
                Gene identifiers are not supported for this reference
                Could not identify the feature type for query
  Status      : Stable
=cut

sub get_protein_reference{
  my ( $params ) = @_;

  my $registry = get_registry();
  my $query = $params->{'query'};
  my $species = $params->{'species'};

  die "Species is a mandatory parameter\n" unless $species;

  my $translation = execute_on_feature_type( $query, undef,
    sub {

      my $transcript_adaptor = $registry->get_adaptor( $species, 'Core', 'Transcript' );
      my $transcript = $transcript_adaptor->fetch_by_stable_id( $query );
      die "Could not retrieve reference for transcript $query for species $species\n" unless $transcript;
      my $translation = $transcript->translation();
      die "Could not find protein for transcript $query\n" unless $translation;
      return $translation;

    }, sub {

      my $translation_adaptor = $registry->get_adaptor( $species, "Core", "Translation" );
      my $translation = $translation_adaptor->fetch_by_stable_id( $query );
      return $translation;

    }, sub {
      die "Gene identifiers are not supported for this reference\n";
    }
  );

 # If not enough data was submitted to resolve the reference, we simply return an empty object.
 # Each reference will try to update its data even if this is not necessary ( as we don't know how much.whcich data each track will need)
 die "Could not identify the feature type for $query\n" if $query and !$translation;

 # We return the params we received if no resolva was possible
 return $params unless $translation;

 return {
   species       => $params->{species},
   start         => 1,
   end           => $translation->length(),
   protein_id    => $translation->display_id(),
   transcript_id => $translation->transcript()->display_id(),
   min           => 1,
   max           => $translation->length(),
   sequence      => $translation->seq()
 };
}


1;
