=head1 NAME

Lera::Model::EnsemblAdapters::References::GenomeReference - Reference allowing to represent features on a Genome

=head1 DESCRIPTION

This reference expects following parameters :

species
query ( gene id or transcript id )

Any track depending on this reference can expect to receive following parameters on update :

species
coord_system_name   : DEPRECATED
seq_region_name     : name of the chromosome
seq_region_name_syn : array of synonyms of the chromosome name
start               : position on the chromosome
end                 : position on the chromosome
min                 : position of the first base of the chromosome
max                 : position of the last base of the chromosome

=cut

package Lera::Model::EnsemblAdapters::References::GenomeReference;

use strict;
use warnings;

use Exporter qw(import);
use Term::ANSIColor qw(:constants);

use Tools;
use Lera::Model::EnsemblAdapters::EnsemblTools;
use Lera::EnsemblRegistry;

use Data::Dumper;

our @EXPORT = qw( get_genomic_reference species_autocomplete );


=head2 get_genomic_reference
  Arg[1]      : hash $params ( species, query )
  Description : Resolves the genomic references. This means retrieving all information that tracks may need
                from the parameters supplied. For example when supplied with species and gene, transcript id,
                we sill find the corresponding chromosome, genomic location, bounds of the chromosome, etc...
  Returntype  : hash
  Exceptions  : Species is a mandatory parameter
                Protein identifiers are not supported for this reference
                Could not identify the feature type for
  Status      : Stable
=cut

sub get_genomic_reference{
   my ( $params ) = @_;

   my $registry = get_registry();
   my $species = $params->{'species'};
   my $query = $params->{'query'};
   die "Species is a mandatory parameter\n" unless $species;

   my $slice_adaptor = $registry->get_adaptor( $species, 'Core', 'Slice' );

   # Parsing the query in priority, to allow easily changing the location
   my $data = execute_on_feature_type( $query,

     sub {
       # Coordinates
       my ($chr, $start, $end) = @_;
       my $slice = $slice_adaptor->fetch_by_region( undef, $chr, $start, $end);
       return slice_as_reference($slice, $species) if $slice;
     },

     sub {
       # Transcript
       my $slice = $slice_adaptor->fetch_by_transcript_stable_id( $query );
       return slice_as_reference($slice, $species) if $slice;
     },

     sub { die "Protein identifiers are not supported for this reference\n"; },

     sub {
       # Gene
       my $slice = $slice_adaptor->fetch_by_gene_stable_id( $query );
       return slice_as_reference($slice, $species) if $slice;
     }
   );

   # Checking of chr:start-stop were submitted
   if( !$query && $params->{start} && $params->{end} && $params->{seq_region_name} ){
     my $slice = $slice_adaptor->fetch_by_region( undef, $params->{seq_region_name}, $params->{start}, $params->{end});

     return slice_as_reference($slice, $species) if $slice;
   }

   # If not enough data was submitted to resolve the reference, we simply return an empty object.
   # Each reference will try to update its data even if this is not necessary ( as we don't know how much.whcich data each track will need)
   die "Could not identify the feature type for $query\n" if $query and !$data;

   return $data || {};
}


=head2 slice_as_reference
  Arg[1]      : Slice $slice
  Arg[2]      : string $species
  Description : Returns a hash cont5aining the intervalParams, from the Slice object
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub slice_as_reference{
  my ($slice, $species) = @_;

  my @synonyms = map { $_->{name} } @{$slice->get_all_synonyms()};

  return {
    species             => $species,
    coord_system_name   => $slice->coord_system_name(), # TODO Remove ??
    seq_region_name     => $slice->seq_region_name(),
    seq_region_name_syn => \@synonyms,
    start               => $slice->start(),
    end                 => $slice->end(),
    min                 => 1,
    max                 => $slice->seq_region_length()
  };
}


=head2 species_autocomplete
  Status      : DEPRECATED : Use SolrAdapter instead
=cut

sub species_autocomplete{
  my ( $registry, $params ) = @_;
  my $text = $params->{"text"};

  my @species_names = grep { $_ =~ /.*$text.*/ } @{ $registry->get_all_species() };

  my @matches = ();
  foreach my $raw_s (@{first_slice(\@species_names, 10)}){
    push @matches, {identifier => $raw_s , description => "some species description from VB"};
  }

  return \@matches;
}

1;
