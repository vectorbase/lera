=head1 NAME

Lera::Model::BcftoolsAdapters::Tracks::Genotype -  Abstract data representations of GENOTYPE objects

=head1 DESCRIPTION

This library returns abstract data representations of GENOTYPE objects retrieved from VCF files by vcf-query.
The goal is to have the same rendering code on the web browser side that can render any track with
Ensembl objects (transcripts, genes, proteins, genotypes, etc...)

=cut

package Lera::Model::BcftoolsAdapters::Tracks::Genotype;

use strict;
use warnings;

use Exporter qw(import);
use Term::ANSIColor qw(:constants);
use Data::Dumper;
use List::Util qw( any sum );;
use Time::HiRes qw( time );

use Tools qw( user_die );
use Conf;
use Lera::Model::BcftoolsAdapters::BcfTools qw( execute_bcftools );
use Lera::Model::EnsemblAdapters::Annotators::ConsequencesAnnotator;

our @EXPORT_OK = qw( get_genotypes export_genotypes _get_alleles _get_genotype is_missing _parse_output );


my $MAX_INTERVAL = get_bloc_config( 'conf/tracks.ini', 'genotype', 'MAX_INTERVAL') || 80000;
my $MAX_VARIANTS = get_bloc_config( 'conf/tracks.ini', 'genotype', 'MAX_VARIANTS') || 2000;
my $MAX_GENOTYPES = get_bloc_config( 'conf/tracks.ini', 'genotype', 'MAX_GENOTYPES') || 30000;
my $MAX_SAMPLES = get_bloc_config( 'conf/tracks.ini', 'genotype', 'MAX_SAMPLES') || 20;


=head2 get_genotypes
  Arg[1]     : hash $params ( species, start, end, seq_region_name, seq_region_name_syn )
  Arg[2]     : list $samples Samples for which we want to retrieve genotypes
  Description: Returns the track of genotypes for a given genomic interval and given samples
  Returntype : hash ref
  Exceptions : none
  Status     : Stable
=cut

sub get_genotypes {
  my ( $params, $trackParams ) = @_;

  my $samples = $trackParams && $trackParams->{filters} && $trackParams->{filters}{entities};

  my $format = "%CHROM\t%POS\t%ID\t%REF\t%ALT\t[#%SAMPLE=%GT]\n";

  my ( $species, $chr, $start, $end, $chr_synonyms ) = _sanity_check( $params, $samples );
  my ( $output, $about ) = execute_bcftools( "query -f '$format'", $species, $chr, $start, $end, $samples, $chr_synonyms);
  my ($consequences_cache, $matched_transcripts) = _get_consequences_cache( $species, $chr, $start, $end, $about );

  return _parse_output( $output, $about, $consequences_cache, $matched_transcripts, $chr );
}


=head2 export_genotypes
  Arg[1]     : hash $params
  Arg[2]     : list $samples
  Description: Returns text in VCF format for the given interval 9specified in $params) and list of samples
  Returntype : string
  Exceptions : none
  Status     : Stable
=cut

sub export_genotypes {
  my ( $params, $trackParams ) = @_;

  my $samples = $trackParams && $trackParams->{filters} && $trackParams->{filters}{entities};

  my ( $species, $chr, $start, $end, $chr_synonyms ) = _sanity_check( $params, $samples );
  return (execute_bcftools( "view", $species, $chr, $start, $end, $samples, $chr_synonyms))[0];
}




=head2 _sanity_check
  Arg[1]       : hash $params ( species, start, end, etc... )
  Arg[2]       : list $samples
  Description: Checks that all requested fields are present in params, and returns these fields as list
  Returntype : list
  Exceptions : 'species' is a mandatory paramater
               Start must be a strictly positive integer
               End must be a strictly positive integer
               Start must be smaller or equal to end
  Status     : Stable


=cut

sub _sanity_check {

  my ( $params, $samples ) = @_;

  my $species = $params->{'species'};
  my $start = $params->{'start'};
  my $end = $params->{'end'};
  my $interval = $end - $start + 1;
  my $chr = $params->{'seq_region_name'};
  my $chr_synonyms = $params->{'seq_region_name_syn'}; # Can be string OR array

  die "'seq_region_name' is a mandatory parameter\n" unless $chr;
  die "'species' is a mandatory paramater\n" unless $species;
  die "start must be a strictly positive integer\n" unless $start > 0;
  die "end must be a strictly positive integer\n" unless $end > 0;
  die "start must be smaller or equal to end\n" unless $start <= $end;

  user_die("Please click on <i class='fa fa-edit'></i> to add samples to this display") unless $samples && scalar @$samples;
  user_die("You can not add more than $MAX_SAMPLES samples to a track") if ( scalar @$samples ) > $MAX_SAMPLES;
  user_die("We do not show genotypes for regions larger than $MAX_INTERVAL b.p. Please zoom in to see genotypes") unless $interval <= $MAX_INTERVAL;

  return ( $species, $chr, $start, $end, $chr_synonyms );
}



=head2 _get_consequences_cache
  Arg[1]       : string $species
  Arg[2]       : string $chr
  Arg[3]       : integer $start
  Arg[4]       : integer $end
  Arg[5]      : hash $about
  Description : Returns a hash of all consequences related to variations in the given interval, and a list of
                transcripts linked to these variations.
  Returntype  : list ($cache, $transcripts)
  Exceptions  : species is a mandatory paramater
                seq_region_name is a mandatory parameter
                start must be a strictly positive integer
                end must be a strictly positive integer
                start must be smaller or equal to end
  Status      : Stable
=cut

sub _get_consequences_cache {
  my ( $species, $chr, $start, $end, $about ) = @_;

  my $db_start = time();
  my ($cache, $transcripts) = create_consequences_cache( $species, $chr, $start, $end );
  my $db_end = time();
  $about->{'db_conseq_exec_time (s)'} = ($db_end - $db_start);
  return ($cache, $transcripts);
}


=head2 _parse_output
  Arg[1]      : string $output the stdout from bcftools
  Arg[2]      : hash $about Various statistics about the parsing of data will be stored inside
  Arg[3]      : hash $consequences_cache
  Arg[4]      : list $matched_transcripts
  Arg[5]      : list $def_chr
  Arg[6]      : boolean $ignore_homo_ref (optionl) Homologous ref genotypes are ignored if set to true (about 4 times performance increase)
  Description : Transforms the output from "bcftools query" into a json structure used bu LERA
  Returntype  : hash
  Exceptions  : Zoom in to see genotypes (too many variations)
  Status      : Stable

=cut

sub _parse_output {
  my ( $output, $about, $consequences_cache, $matched_transcripts, $def_chr, $ignore_homo_ref ) = @_;

  my $track = {
    entities => {}
  };

  my $parse_start = time();

  # TODO We could stream from pipe http://alvinalexander.com/perl/edu/articles/pl010004.shtml
  my @variations = split( /\n/ , $output ); # Takes about 1ms to split 1763 lines

  my $nb_variations = scalar @variations;
  user_die("We do not show genotypes for more than $MAX_VARIANTS variations. Please zoom in to see genotypes") unless $nb_variations <= $MAX_VARIANTS;

  $about->{'variants_count'} = $nb_variations;

  my $genotypes_count = 0;

  foreach my $variation (@variations) {

    # Follows the order declared in $format
    my ($chr, $pos, $variation_name, $raw_ref, $alleles_str, $sg_list) = split( /\t/, $variation );

    # If a default chromosome is provided, we replace the chromosome from the vcf (which can be a synonym)
    # by the official chromosome name
    $chr = $def_chr if defined $def_chr;

    if( $variation_name eq '.' ){ $variation_name = "$chr:$pos"; }

    my ( $ref, $alt_alleles, $start ) = _get_alleles( $raw_ref, $alleles_str, $pos );

    foreach my $sg (split( /#/, $sg_list )){
      if( $sg ){

        my ($sample_name, $genotype_str) = split( /=/, $sg );
        next if is_missing( $genotype_str );
        next if $ignore_homo_ref && _is_homo_ref( $genotype_str );

        my $genotype_id = "$chr-$pos-$sample_name";

        if( !$track->{entities}{$sample_name} ){
          $track->{entities}{$sample_name} = {
            name       => $sample_name,
            components => {}
          };
        }

        $track->{entities}{$sample_name}{components}{$genotype_id} =
        _create_component($chr, $start, $variation_name, $ref, $alt_alleles, $genotype_str, $consequences_cache);
        $genotypes_count++;
      }
    }
  }

  user_die("We do not show more than $MAX_GENOTYPES genotypes. Please zoom in to see genotypes.") unless $genotypes_count <= $MAX_GENOTYPES;

  my $parse_end = time();
  $about->{'output_parse_time (s)'} = ($parse_end - $parse_start);
  $about->{'genotypes_count'} = $genotypes_count;
  $track->{'about'} = $about;
  my @transcripts = keys %$matched_transcripts;
  my %categories = map { $_ => 'component' } @transcripts;
  $track->{'categories'} = \%categories;

  return $track;
}


=head2 _create_component
  Arg[1]      :
  Description :
  Returntype  : hash
  Exceptions  : none
  Status      : Stable

  TODO Avoid copying paramaters into array ??
=cut

sub _create_component {
  my ($chr, $start, $variation_name, $ref, $alt_alleles, $genotype_str, $consequences_cache) = @_;

  my $genotype = _get_genotype( $ref, $alt_alleles, $genotype_str );

  my $color = get_color( $genotype_str );
  my $consequences = get_transcript_consequences( $consequences_cache, $chr, $start, $genotype);

  my $data = {
    reference => $ref,
    genotype => $genotype,
    variation => $variation_name,
    position => $start
  };

  $data = { %$data, %$consequences };
  
  # Define a custom title
  my $title = join("/", @$genotype);

  return {
    start => $start, # There is not really an end for a genotype, because each allele can have a different size
    color => $color,
    data => $data,
    title => $title,
  };
}




my $GENOTYPE_COLOR = {
  REF_HOMO => 'rgb(76,175,80)',
  REF_ALT => '#FF9800',
  ALT_HETERO => '#01579B',
  ALT_HOMO => 'rgb(244,67,54)',
};


=head2 _get_all_colors_hash
  Description : Returns the hash associating colors to different allele configurations
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _get_all_colors_hash {
  return $GENOTYPE_COLOR;
}


=head2 _get_color
  Arg[1]      : string $genotype_str (ex: 0/1)
  Description : Returns the color given the proportion of ref/alt alleles in the genotype
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub _get_color {
  my ($genotype_str) = @_;
  return unless defined $genotype_str;
  my @raw_genotypes = split(/[\/|]/, $genotype_str);
  my $sum = sum @raw_genotypes;
  return $GENOTYPE_COLOR->{REF_HOMO} if $sum eq 0; # sum can be 0 only if we have only ref alleles
  my %alleles_hash = map { $_, 1 } @raw_genotypes;
  my $alleles_count = keys %alleles_hash;
  return $GENOTYPE_COLOR->{ALT_HOMO} if $alleles_count eq 1; # we have only one unique allele and it is not ref
  return $GENOTYPE_COLOR->{ALT_HETERO} unless defined $alleles_hash{0}; # different alt alleles, not containing the ref
  return $GENOTYPE_COLOR->{REF_ALT}; # The only remaining possibility is to have both alt and ref alleles
}

my $genotype_color_patterns = {};

=head2 get_color
  Arg[1]      : string $genotype_str (ex: 0/1)
  Description : Returns the color given the proportion of ref/alt alleles in the genotype.
                Uses a hash to memoize previously calculated genotype/color couples
  Returntype  : string
  Exceptions  : none
  Status      : Stable
=cut

sub get_color {
  return unless @_;
  $genotype_color_patterns->{ $_[0] } ||= _get_color( $_[0] );
  return $genotype_color_patterns->{ $_[0] };
}


=head2 _is_missing
  Arg[1]      : string $alleles_string
  Description : Returns true if there is no missing alleles in the genotype
  Returntype  : boolean
  Exceptions  : none
  Status      : Stable
=cut

sub _is_missing {
  return $_[0] !~ /[0-9]+/;
}

=head2 _is_homo_ref
  Arg[1]      :
  Description : Returns true only if the genotype contains only ref alleles ( only 0 and / or | characters)
  NOTE: Should be applied only if is_missing is false
  Returntype  : boolean
  Exceptions  : none
  Status      : Stable
=cut

sub _is_homo_ref {
  return $_[0] !~ /[1-9]+/;
}

# Custom memoization for recognition of missing genotype patterns
my $missing_genotype_patterns = {};

=head2 is_missing
  Arg[1]      : string $alleles_string
  Description : Returns true if there is no missing alleles in the genotype
                Uses a hash to memoize previously calculated alleles_string/boolean couples
  Returntype  : boolean
  Exceptions  : none
  Status      : Stable
=cut

sub is_missing {
  $missing_genotype_patterns->{ $_[0] } ||= _is_missing( $_[0] );
  return $missing_genotype_patterns->{ $_[0] };
}


=head2 _get_alleles
  Arg[1]      : string $ref reference allele
  Arg[2]      : string $alleles_string alternative alleles
  Arg[3]      : int $pos position of the allele on the chromosome in the VCF
  Description : Adapts the INDEL alleles from the VCF format to the Ensembl format
  Returntype  : list
  Exceptions  : none
  Status      : Stable

  TODO Try memoize (but need to remove pos parameter. instead return a boolean saying whether we should decrement the position or not)
=cut

sub _get_alleles {
  my ($ref, $alleles_string, $pos) = @_;

  my @alleles = split( /,/, $alleles_string );

  # START From Ensembl VCFCollection::get_all_Alleles_by_VariationFeature code
  # Removed support for '*' for now as context is unclear
  my %first_bases = map {substr($_, 0, 1) => 1} ($ref, @alleles);
  if(scalar keys %first_bases == 1) {
     $ref = substr($ref, 1) || '-';
     $pos++;

     my @new_alts;

     foreach my $alt_allele(@alleles) {
       $alt_allele = substr($alt_allele, 1) || '-';
       push @new_alts, $alt_allele;
     }

     @alleles = @new_alts;
   }
  # END From Ensembl VCFCollection::get_all_Alleles_by_VariationFeature code

  return ($ref, \@alleles, $pos);
}

=head1

alt_alleles: array of alternative alleles, ex: ['A', '-']
genotype_str: indexes of alleles, ex: 0/1 or 1|1
TODO Try memoize with normalisation of alt alleles

=cut

=head2 _get_genotype
  Arg[1]      : string $ref reference allele
  Arg[2]      : list $alt_alleles alternative alleles
  Arg[3]      : string $genotypes_string
  Description : Returns a list of alleles corresponding to a given genotype
  Returntype  : list
  Exceptions  : none
  Status      : Stable
=cut

sub _get_genotype {
  my ($ref, $alt_alleles, $genotype_str) = @_;

  return [] unless $genotype_str && $alt_alleles && $ref;
  my @raw_genotype = split(/[\/|]/, $genotype_str);
  my @genotype = map { if ($_ eq 0) { $ref } elsif ( $_ ne '.' ){ $alt_alleles->[$_ - 1] } else { () } } @raw_genotype;
  return \@genotype;
}


1;
