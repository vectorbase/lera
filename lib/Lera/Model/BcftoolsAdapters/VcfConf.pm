=head1 NAME

Lera::Model::BcftoolsAdapters::VcfConf - A VCF configuration file processor

=head1 DESCRIPTION

This library returns data representation of the VCF configuration file conf/vcf_config.json,
enhanced with a list of available samples and chronmosomes for each file (calculated on first call to get_vcf_config() )

=cut

package Lera::Model::BcftoolsAdapters::VcfConf;

use strict;
use warnings;

use Exporter qw(import);
use List::Util qw( any sum );;
use Data::Dumper;

use Conf;
use Tools qw( user_die );

our @EXPORT = qw(  get_vcf_config get_vcf _get_vcf get_matching_samples get_vcf_species );
our @EXPORT_OK = qw( get_available_species_names is_species_available );

# If set to 1 (by a test script for example), the function get_vcf_config() will return a dummy vcf configuration
# instead of parsing the vcf_conf.json file to create and return the real configuration.
our $TEST_MODE = 0;


=head2 get_vcf
 Arg [1]     : string $species
 Arg [2]     : list $chromosomes
 Arg [3]     : list $samples
 Description : This sub returns a path to the VCF file containing genotypes for given species and first matching chromosome
 from submitted chromosomes. In addition to the vcf path, the sub returns for which chromosome a VCf file was found
 and which samples were found and not found in this vcf file.
 Returntype  : list
 Status      : Stable
=cut
sub get_vcf {
  my ( $species, $chromosomes, $samples ) = @_;
  my $vcf_config = get_vcf_config();
  return _get_vcf( $vcf_config, $species, $chromosomes, $samples );
}

=head2 _get_vcf
  Arg [1]     : hash $vcf_config
  Arg [2]     : string $species
  Arg [3]     : list $chromosomes
  Arg [4]     : list $samples
  Description: Private function that should be used only for tests. Use get_vcf instead.
  Returntype : list
  Exceptions : VCF configuration file could not be loaded
               Species is a mandatory parameter
               Chromosomes is a mandatory parameter
               No genotype data is currently available for species
               Please click on <i class='fa fa-edit'></i> to add samples to this display
               No declared VCF path for species and chr
               No registered VCF file for chromosomes for species
  Status     : Stable
=cut

sub _get_vcf {
  my ( $vcf_config, $species, $chromosomes, $samples ) = @_;

  $species = lc( $species );

  die "VCF configuration file could not be loaded\n" unless $vcf_config;
  die "Species is a mandatory parameter\n" unless defined $species;
  die "Chromosomes is a mandatory parameter\n" unless $chromosomes && @$chromosomes;

  my $vcf_species =  get_vcf_species($vcf_config, $species);
  user_die("No genotype data is currently available for $species") unless $vcf_species;

  die "No samples submitted\n" unless $samples && scalar @$samples;

  # For each chromosome declared for the reference,
  foreach my $chr (@$chromosomes){

    # try to get a vcf object for the chromosome or a default vcf object for the species
    my $vcf_obj = $vcf_species->{$chr} || $vcf_species->{'default'};
    if( $vcf_obj ){
        my $vcf_chromosomes = $vcf_obj->{chromosomes}; # all chromosomes associated with this vcf object

        if( defined $vcf_chromosomes->{$chr} ){ # check if the chromosome is defined for this vcf object

          my $vcf_path = $vcf_obj && $vcf_obj->{path};
          # We do not check if the file exists, as this is done when the server starts and loads the vcf config
          die "No declared VCF path for $species and $chr\n" unless $vcf_path;
          my $matching = get_matching_samples($vcf_obj, $samples);
          return ( $vcf_path, $chr, $matching ); #TODO return hash instead of list ??
        }
    }
  }

  die "No registered VCF file for chromosomes ".(join(', ', @$chromosomes))." for species $species\n";
}


=head2 get_matching_samples
 Arg [1]     : hash $vcf_obj
 Arg [2]     : list $samples
 Description : Compares the list of submitted samples to the samples in the VCf object.
 Classify found and notFound samples and returns the hash { found => [...], notFound => [...] }
 Returntype  : hash
 Exceptions  : Invalid chromosome VCf object
               No samples submitted
 Status      : Stable

=cut
sub get_matching_samples {
  my ( $vcf_obj, $samples ) = @_;
  my $matching = { found => [], notFound => [] };

  die "Invalid chromosome VCf object\n" unless $vcf_obj;
  die "No samples submitted\n" unless $samples && @$samples;

  my $vcf_samples = $vcf_obj->{samples};
  foreach my $sample (@$samples){
    if( defined $vcf_samples->{$sample} ){
      push( @{$matching->{found}}, $sample );
    } else {
      push( @{$matching->{notFound}}, $sample );
    }
  }

  return $matching;
}

=head2 get_vcf_species
 Arg [1]     : object $vcf_config
 Arg [2]     : string $species
 Description : Returns the VCF object for a given species
 Returntype  : hash
 Status      : Stable
=cut
sub get_vcf_species {
  my ( $vcf_config, $species ) = @_;
  return $vcf_config && $vcf_config->{$species};
}

=head2 get_available_species_names
  Description : Returns a list of species with VCF/BCF data, that are not ignored
  Returntype  : array
  Exceptions  : none
  Status      : Stable
=cut

sub get_available_species_names {
  my $vcf_config = get_vcf_config();
  return $vcf_config && sort keys %$vcf_config;
}

=head2 is_species_available
  Arg[1]      : string $species
  Description : Returns true if the specified species is available in the VCF
  Returntype  : boolean
  Exceptions  : none
  Status      : Stable
=cut

sub is_species_available {
  my ( $species ) = @_;
  my $vcf_config = get_vcf_config();
  return $species && $vcf_config && defined $vcf_config->{$species}
}


my $vcf_config;


=head2 get_vcf_config
 Description : Returns the vcf_config object, enhanced from the original vcf_config file.
 This object contains the species for which we have VCF/BCF data.
 For each species, for each chromosome (or a 'default' chromosome if there is only one VCF for the species) we
 have the path to the VCF file, the list of available samples and chromosomes.
 -> Returns a test confing if $TEST_MODE is enambled
 Returntype  : hash
 Status      : Stable
=cut
sub get_vcf_config {

  return {test_species_one => {}, test_species_two => {}} if $TEST_MODE;

  $vcf_config ||= _create_vcf_config();
  return $vcf_config;
}



=head2 _create_vcf_config
 Description : Loads the vcf_config file, and appends to each file object a hash of chromosomes and a hash of samples
 Returntype  : hash ref
 Exceptions  : Can not reach VCF configuration file
               Stopping loading VCF config. Registered VCF file not found
 Status      : Stable

 TODO Create a hash for samples synonyms ?

=cut
sub _create_vcf_config {
  my $vcf_config_obj = {};
  die "Can not reach VCF configuration file\n" unless has_config('conf/vcf_config.json');
  my $vcf_config_raw = get_config('conf/vcf_config.json');

  while (my ($species, $species_data) = each %$vcf_config_raw) {

    if( $species_data->{ignore} ){ next; }

    while (my ($chr, $path) = each %$species_data) {

      die "Stopping loading VCF config. Registered VCF file not found : $path\n" unless -f $path;
      my $available_chromosomes = _get_chromosomes_hash($path);
      die "Stopping loading VCF config. No chromosomes declared in : $path\n" unless %{$available_chromosomes};
      my $available_samples = _get_samples_hash($path);
      die "Stopping loading VCF config. No samples declared in : $path\n" unless %{$available_samples};
      $vcf_config_obj->{$species}{$chr} = {
        path        => $path,
        chromosomes => $available_chromosomes,
        samples     => $available_samples
      };

    }
  }
  return $vcf_config_obj;
}



=head2 _get_chromosomes_hash
 Arg [1]     : string $vcf_path
 Description : Creates a hash with all chromosomes contained in this VCF file
 Returntype  :
 Status      : Stable

 We use bcftools index -s 'file_name'
 # We need at least bcftools version 1.4 as lower versions such as 1.2 does not supports missing contigs in the header
 # bcftools index -s anopheles_arabiensis/anopheles_arabiensis.bcf 2>/dev/null | awk -F '[\t ]+' '{print $1}'
=cut
sub _get_chromosomes_hash {
  my ( $vcf_path ) = @_;
  my $command = "bcftools index -s $vcf_path 2>/dev/null | awk -F '[\\t ]+' '{print \$1}'";
  my $output = `$command`;
  my @contig_lines = split("\n", $output);
  my %chromosomes_hash = map { $_ => 1 } @contig_lines;
  return \%chromosomes_hash;
}


=head2 _get_samples_hash
 Arg [1]     : string $vcf_path
 Description : Creates a hash with all samples contained in this VCF file
 Returntype  :
 Status      : Stable
=cut
sub _get_samples_hash {
  my ( $vcf_path ) = @_;
  my $command = "bcftools query -l $vcf_path 2>/dev/null";
  my $output = `$command`;
  my @samples = split("\n", $output);
  my %samples_hash = map { $_ => 1 } @samples;
  return \%samples_hash;
}



1;
