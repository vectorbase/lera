
package Lera::Model::BcftoolsAdapters::BcfTools;

use strict;
use warnings;

use Exporter qw(import);
use List::Util qw( any sum uniq );
use Time::HiRes qw( time );
use Data::Dumper;

use Lera::Model::BcftoolsAdapters::VcfConf;
use Tools qw( user_die task_die );

our @EXPORT_OK = qw( execute_bcftools );


=head2 execute_bcftools

  Arg[1]       : string $cmd_options ex: 'query' or 'view'
  Arg[2]       : string $species
  Arg[3]       : string $chr
  Arg[4]       : integer $start
  Arg[5]       : integer $end
  Arg[6]       : string $samples
  Arg[7]       : string $chr_synonyms (optional) Alternative chromosome names to try if the default name is not found in the VCF
  Description : Executes "bcftools" and returns the raw output from the command
                Currently supported subcommands are 'query' and 'view'
  Returntype  : list ( output, about )
  Exceptions  : none
  Status      : Stable
=cut

sub execute_bcftools {
  my ( $cmd_options, $species, $chr, $start, $end, $samples, $chr_synonyms ) = @_;

  # TODO Need protection against duplicates once synonyms are removed
  # $samples = \(uniq @$samples);

  my $chromosomes = _get_chromosomes( $chr, $chr_synonyms );
  my ( $vcf_path, $selected_chr, $matching_samples ) = get_vcf( $species, $chromosomes, $samples );
  return _execute_bcftools( $selected_chr, $start, $end, $vcf_path, $matching_samples, $cmd_options );
}


=head2 _get_chromosomes
  Arg[1]      : string  $def_chr
  Arg[2]      : string  $chrom_synonyms (optional)
  Description : Returns a list of chromosomes corresponding to the given interval (the official chromosome is first, followed
                by optional synonyms)
  Returntype  : list
  Exceptions  : seq_region_name' is a mandatory parameter
  Status      : Stable
=cut

sub _get_chromosomes {
  my ( $def_chr, $chrom_synonyms ) = @_;

  die "The default chromosome is a mandatory parameter\n" unless $def_chr;
  $chrom_synonyms = [$chrom_synonyms] if $chrom_synonyms && ref($chrom_synonyms) ne 'ARRAY';

  my $chromosomes = [];
  push( @$chromosomes, $def_chr);
  push( @$chromosomes, @$chrom_synonyms ) if $chrom_synonyms;

  return $chromosomes;
}

=head2 _execute_bcftools
  Arg[1]       : string $chrom
  Arg[2]       : integer $start
  Arg[3]       : integer $end
  Arg[4]       : string $vcf_path
  Arg[5]       : hash $matching_samples {found => [], not_found => []}
  Arg[6]       : string $cmd_options ex: 'view'
  Description: Executes "bcftools" and returns the raw output from the command
               Currently supported subcommands are 'query' and 'view'
  Returntype : list
  Exceptions : None of the submitted samples were found in the VCF
  Status     : Stable

  TODO Accept a list of regions [{start => ..., end => ... }] that will be used to ask only exonic data
=cut

sub _execute_bcftools {

  my ( $chrom, $start, $end, $vcf_path, $matching_samples, $cmd_options ) = @_;

  my $found_samples = $matching_samples && $matching_samples->{found};
  user_die("None of the submitted samples were found in the VCF") unless $found_samples && @$found_samples;
  my $sample_ids_string = join( ',', @$found_samples );

  my $command = "bcftools $cmd_options -r $chrom:$start-$end -s $sample_ids_string $vcf_path 2>/dev/null";
  my $cmd_start = time();
  my $res = `$command`;
  my $exit_code = $?;
  my $cmd_end = time();

  # The fact that bcftools returns no results does not mean there was an error. We can have no genotypes in the interval.

  task_die("bcftools errored when executing your command. This problem was logged and will soon be fixed.",
    "bcftools errored ($exit_code) when executing the command $command\n") if $exit_code;

  my $about = {
    interval => ($end - $start + 1),
    cmdExecTimeS => ($cmd_end - $cmd_start),
    cmd => $command,
    exitCode => $exit_code,
    filtersStatus => $matching_samples
  };

  return ( $res, $about );
}




1;
