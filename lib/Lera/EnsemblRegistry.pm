=head1 NAME

Lera::EnsemblRegistry - Utility to configure and return the Ensembl Registry object

=head1 DESCRIPTION

Utility to configure and return the Ensembl Registry object

=cut

package Lera::EnsemblRegistry;

use strict;
use warnings;

use Tools;
use Conf;

use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Variation::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;

use Exporter qw(import);
our @EXPORT = qw( get_registry );

my $VERBOSE = 0;

my $registry = 'Bio::EnsEMBL::Registry';

# This is the mandatory registry file
my $registry_file = get_config('conf/ensembl.ini', 'registry_path');
$registry->load_all( $registry_file , $VERBOSE ) if $registry_file && -f $registry_file;

# Sometimes we need to add organisms not listed officially in the production registry file (ex: add support for ag1k)
my $addentum_registry = get_config('conf/ensembl.ini', 'addentum_registry_path');
$registry->load_all( $addentum_registry, $VERBOSE, 1 ) if $addentum_registry && -f $addentum_registry;

# When running perl unit tests, it is ok if the registry file is not found. This is why we show a warning instead of dying
warn "Incorrect path to the Ensembl Registry file in ensembl.ini configuration file\n" unless $registry_file && -f $registry_file;


# To avoid 'MySQL server has gone away' error
$registry->set_disconnect_when_inactive();
$registry->set_reconnect_when_lost();

# TODO Use connection sharing as in  _intern_db_connections
# https://github.com/Ensembl/ensembl-rest/blob/f263937c530ea9fb450e20e1c9ee1fd1816167a5/lib/EnsEMBL/REST/Model/Registry.pm


=head2 get_registry
 Description : Returns the Ensembl Registry object
 Returntype  : Registry
 Status      : Stable
=cut
sub get_registry {
  return $registry;
}



1;
