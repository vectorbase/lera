=head1 NAME

Conf - Configuration manager

=head1 DESCRIPTION

Manages configuration split between different files and different formats (ini, json)
Allows to access the whole configuration hash or to retrieve specific properties.
Treats file paths as namespaces.

=cut

package Conf;

use strict;
use warnings;

use Exporter qw(import);
use Config::General;
use JSON::Parse 'parse_json';
use JSON::Parse 'json_file_to_perl';
use JSON::Parse 'valid_json';
use Mojo::Util qw(b64_encode url_escape url_unescape);

our @EXPORT = qw( has_config get_config get_bloc_config _interpolate );


=head2 $config
  Description: Stores configs loaded from files
=cut

my $config = {};


=head2 _get_ini_config
 Arg [1]     : string $path
 Arg [2]     : boolean $ignore_empty
 Description : Parses INI configuration files and returns a perl HASH object.
 The configuration files can use interpolation within the file (reusing file variables)
 and interpolation allowing to use configuration values as templates.
 The configuration values can also be environment variables.
 Returntype  : hash ref
 Status      : Stable

 #TODO test that file exists with nice exception
=cut
sub _get_ini_config {
	# Loads configuration from a given file and checks that each paramater (is defined OR declared as possibly undefined)
	my ($path, $ignore_empty) = @_;
	my %conf = Config::General->new(
				-ConfigFile => "$path",
				-InterPolateVars => 1,
				-InterPolateEnv => 1)->getall;

	foreach my $key (keys %conf) {
		unless ($conf{$key} || $ignore_empty->{$key}) {
			die "Parameter '$key' should be defined in configuration file '$path' (If this parameter is associated to an ENV variable, check that this variable is set)";
		}
	}

	return %conf;
}


=head2 _get_json_config
 Arg [1]     : string $path
 Description : Parses JSON configuration files and returns a perl HASH object.
 Modified from json_file_to_perl (http://cpansearch.perl.org/src/BKB/JSON-Parse-0.41/lib/JSON/Parse.pm)
 to validate file before parsing and showing nice error message
 Returntype  : hash ref
 Status      : Stable
=cut
sub _get_json_config {
	my ($path) = @_;

	my $json = '';
	open my $in, "<:encoding(utf8)", $path
			or die "Error opening JSON configuration file $path: $!\n";
	while (<$in>) {
			$json .= $_;
	}
	close $in or die "Error manipulating JSON configuration file $path: $!\n";
	die "JSON configuration file $path does not contain a valid JSON.\n" unless valid_json($json);
	my $conf = parse_json ($json);
	return $conf;
}


=head2 _interpolate
 Arg [1]     : hash ref $conf
 Arg [2]     : hash %mapping
 Description : Replace {{key}} in the configuration string by the value associated to the key in the mapping hash
 Returntype  : string
 Status      : Stable
=cut
sub _interpolate {
	my $conf = shift;
	my %mapping = %{shift()};

	foreach my $parameter_name ( keys %mapping ){
		my $parameter_value = $mapping{$parameter_name};
		die "Can not interpolate configuration because value for $parameter_name is undefined" unless defined $parameter_value;
		$conf =~ s/\{\{$parameter_name}}/$parameter_value/;
	}

	return $conf;
}

=head2 has_config
  Arg[1]      : string $path
  Description : Returns true if there is a configuration hash for a given path/namespace
  Returntype  : boolean
  Status      : Stable
=cut

sub has_config {
	my ($path) = @_;
	return defined get_config( $path );
}



=head2 get_config
 Arg [1]     : string $path
 Arg [2]     : string $key (optional)
 Arg [3]     : hash ref $mapping (optional)
 Description : Returns the full hash ref corresponding to a configuration file,
 or a specific value if a key is submitted, If mapping is submitted, the value will be interpolated with mapping keys/values
 Returntype  : hash or string
 Status      : Stable

 TODO Support not found config files and keys
=cut
sub get_config {
  my ($path, $key, $mapping) = @_;

  $config->{$path} ||= _load_config($path);

	if ($key){
		my $selected_config = $config->{$path} && $config->{$path}{$key};
		if( $mapping ){
				return _interpolate( $selected_config, $mapping );
		} else {
			return $selected_config;
		}

	} else {
			return $config->{$path};
	}
}

=head2 get_bloc_config
	Arg [1]     : string $path
	Arg [2]     : string $bloc_key
	Arg [3]     : string $key
  Description : Returns configuration nested in a bloc
  Returntype  : scalar
  Status      : Stable
=cut

sub get_bloc_config {
	my ($path, $bloc_key, $key) = @_;
	my $bloc = get_config($path, $bloc_key);
	return $bloc && ref($bloc) eq 'HASH' && $bloc->{$key};

}


=head2 _load_config
  Arg[1]      : string $path
  Description : uses aini or json parser to retrieve a configuration hash from a given file
  Returntype  : hash ref
  Exceptions  : Can not handle configuration format for file
  Status      : Stable
=cut

sub _load_config {
  my ($path) = @_;
  if( $path=~ /\.ini$/ ){

    my %ini_conf = _get_ini_config($path);
    return \%ini_conf;

  } elsif ( $path=~ /\.json$/ ) {

    return _get_json_config($path);

  } else {

    die "Can not handle configuration format for file $path\n";
  }

}

1;
