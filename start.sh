#!/bin/bash

# 1. Testing that bcftools exists and is of version >= 4...
command -v bcftools >/dev/null 2>&1 || { echo >&2 "bcftools is required but is not installed. Aborting."; exit 1; }

MIN_VERSION=1.4
BCFTOOLS_VERSION=$(bcftools -v 2>/dev/null | head -1 | awk -F ' +' '{print $2}' | bc)

if (( $( echo "$BCFTOOLS_VERSION < $MIN_VERSION " | bc -l ) ))
  then
    echo "Your bcftools version $BCFTOOLS_VERSION is lower than the minimum requirement of $MIN_VERSION !";
    exit 1;
fi

# 2. Ensembl perl API (Links for charlie)
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl/modules/
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-compara/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-variation/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-funcgen/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-io/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-tools/modules
export PERL5LIB

# 3. Test that all necessary Ensembl modules are available
for LIB in Bio::EnsEMBL::Compara::DBSQL::DBAdaptor Bio::EnsEMBL::DBSQL::DBAdaptor Bio::EnsEMBL::Registry Bio::EnsEMBL::Variation::DBSQL::DBAdaptor
do
  LIB_ERROR=$(perl -M$LIB -e '' 2>&1);
  if [[ ! -z $LIB_ERROR ]]
    then
      echo "$LIB was not found. Please install the necessary Ensembl Perl API library (and its dependencies!).";
      exit 1;
  fi
done

# 4. Starting the server
hypnotoad app.pl
