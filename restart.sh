# 3 tries to stop
LIMIT=3

a=0
while [ $a -lt "$LIMIT" ]; do
  a=$(($a+1))
  
  # Try to stop
  msg=$(./stop.sh)
  echo $msg
  
  # Break when the server is stopped
  stop=$(echo $msg | grep "Hypnotoad server not running")
  if [ -n "$stop" ]; then
    ./start.sh
    break
  fi
done

if [ $a -ge "$LIMIT" ]; then
  echo "Could not stop the server. Not starting."
fi
