Each Genotype Explorer instance on a different server has its own configuration.
This is an example configuration which should be adapted for each server.

If this is your first deployment of the Genotype Explorer on a new server,
the conf folder should be empty. You should copy the content of this folder to the
conf folder, then edit the configuration files.

The Genotype Explorer uses following configuration files :

- server.ini : Configuring the server for this app
- tracks.ini : Optional parametersfor different tracks

- vcf_config.json : Paths to VCF/BCF files used to display genotypes for samples

- ensembl.ini : Details connection to ensembl DB
- solr.ini : Configuring URLs to Solr used for autosuggest
