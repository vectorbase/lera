#!/bin/bash
# Use only for continuous integration of dev branch
# Deploying release branches should be done manually

echo "Stopping hypnotoad server..."
./stop.sh

echo "Updating project dev code from bitbucket..."
git pull

echo "Starting hypnotoad server..."
./start.sh

perl test.pl
