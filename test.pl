=head1 DESCRIPTION

 This script will test all the REST API endpoints of the LERA server for each of the declared organisms.
 Only tests for organisms currently available on the target server will be run.

 To use this script in a cron / automated checking pipeline you can watch the STDERR. If one of the tests fails,
 a message will be written to STDERR.

=cut

use strict;
use warnings;

use Term::ANSIColor qw(:constants);
use Mojo::UserAgent;
use Data::Dumper;
use FindBin;
use JSON::Parse 'json_file_to_perl';

use lib "$FindBin::Bin/lib";
use Conf;

my $ua                = Mojo::UserAgent->new; # User agent used to call different URLs

my $baseUrl           = get_config('conf/server.ini', 'url'); # LERA instance you want to test
my $tests             = get_tests('tests.json'); # Loading declared tests
my $speciesToTest     = get_available_species( $baseUrl ); # Get available species on target server


run_tests( $baseUrl, $speciesToTest, $tests );


=head2 run_tests
  Arg[1]      : string $baseUrl
  Arg[2]      : hash $speciesToTest
  Arg[3]      : hash $tests
  Description : Runs for all species available on the target server, the tests declared in tests.json
  Exceptions  : none
  Status      : Stable
=cut

sub run_tests {
  my ( $baseUrl, $speciesToTest, $tests ) = @_;

  my $not_working_count = 0; # Number of tests that were not successfull
  my $not_tested        = 0; # Number of species for which no tests were declared

  print "\nTesting Genotype Explorer REST API endpoints...\n";

  foreach my $species (@$speciesToTest){
    my $conf = $tests->{$species};

    if (!$conf){
      print YELLOW, "\nNo test data declared for species $species\n", RESET;
      $not_tested++;
      next;
    }

    print BLUE, "$species\n", RESET;

    while (my ($cat, $links) = each %$conf) {
      print "  $cat\n";
      while (my ($name, $link) = each %$links) {

          my $url = $link->{url};
          my $data = $link->{data};
          my $res = $data ? $ua->post( $url => {} => json => $data )->res : $ua->get($link->{url})->res ;
          print "    $name: ";
          if ( $res->code && $res->code eq "200" ) {

            if( $cat eq "TRACKS" ){
              # For tracks, testing that entities list is not empty
              my $entities = $res->json->{entities};
              my $entities_count = scalar keys %$entities;
              if( !$entities_count){
                print RED, "REST endpoint returned empty hash of entities\n", RESET;
                $not_working_count++;
                next;
              }
            }

            if( $cat eq "AUTOSUGGEST" ){
              my $suggestions = $res->json;
              my $suggestions_count = $suggestions && scalar @$suggestions;
              if( !$suggestions_count){
                print RED, "REST endpoint returned empty list of suggestions\n", RESET;
                $not_working_count++;
                next;
              }
            }

            print GREEN, "OK", RESET;
          }
          else {
            print RED, ( $res->json && $res->json->{message} ) || $res->body || ($res && $res->{error} && $res->{error}{message}), RESET;
            $not_working_count++;
          }

          print "\n";
        }
      }
  }

  die "$not_working_count tests were not successful.\n" if $not_working_count ne 0;
  print GREEN, "\nALL TESTS SUCCESSFULL\n", RESET;
  print YELLOW, "\nNo tests declared in tests.json for $not_tested species. Please add these species.\n", RESET if $not_tested ne 0;

}


=head2 get_available_species
  Arg[1]      : string $baseUrl
  Description : Returns the list of available species on the server that will be tested
  Returntype  : list
  Exceptions  : none
  Status      : Stable
=cut

sub get_available_species {
  my ( $baseURL ) = @_;

  my $res = $ua->get("$baseURL/api/stats/species")->res;
  my $error_msg = $res->{error} && $res->{error}{message};
  die "Can not connect to $baseUrl : $error_msg\n" if $error_msg;

  my $speciesToTest = $res->json;
  die "Could not retrieve list of available species from the server\n" unless $speciesToTest && scalar @$speciesToTest;
  return $speciesToTest;
}

=head2 get_tests
  Arg[1]      : string $file Path to the file containing test specs for different species
  Description : Returns a hash containing for each species the URLs that must be tested
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub get_tests {
  my ( $file ) = @_;
  my $declared_tests = json_file_to_perl ( $file );
  my $tests = {};
  while ( my ($species, $conf) = each %$declared_tests ){
    $tests->{$species} = _buildSpeciesConf(
      $species, $conf->{chr},
      $conf->{chr_syn},
      $conf->{start},
      $conf->{end},
      $conf->{transcript_id},
      $conf->{samples}
    );
  }
  return $tests;
}


=head2 _buildReferenceLink
  Arg[1]      : string $type
  Arg[2]      : string $species
  Arg[3]      : string $query
  Description : Creates the test hash for a given reference
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _buildReferenceLink {
  my ( $type, $species, $query ) = @_;
  return {url => "$baseUrl/api/reference/$type?species=$species&query=$query"};
}


=head2 _buildTrackLink
  Arg[1]      : string $url
  Arg[2]      : hash $intervalParams
  Arg[3]      : hash $trackParams
  Description : Creates the test hash for a given track
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _buildTrackLink {
  my ( $url, $intervalParams, $trackParams ) = @_;
  return {
    data => { intervalParams => $intervalParams, trackParams => $trackParams },
    url => "$baseUrl/$url"
  }
}


=head2 _buildSpeciesConf
  Arg[1]      : string $species
  Arg[2]      : string $chr
  Arg[3]      : list $chr_syn
  Arg[4]      : integer $start
  Arg[5]      : integer $end
  Arg[6]      : string $transcript_id
  Arg[7]      : list $samples
  Description : Builds a hash with all the tests to run for a given species
  Returntype  : hash
  Exceptions  : none
  Status      : Stable
=cut

sub _buildSpeciesConf {
  my ( $species, $chr, $chr_syn, $start, $end, $transcript_id, $samples ) = @_;

  my $intervalParams = {
    species => $species,
    transcript_id => $transcript_id,
    start => $start,
    end => $end,
    seq_region_name => $chr,
    seq_region_name_syn => $chr_syn
  };

  my $trackParams = {
     filters => { entities => $samples }
  };

  # A query string containing some part of the species name, used to test the autocomplete for that species
  my $species_query = (split(/_/, $species))[-1];

  return {
    AUTOSUGGEST => {
      species => {url => "$baseUrl/api/autocomplete/species?query=$species_query"},
      gene_transcript => {url => "$baseUrl/api/autocomplete/genome_feature?query=sodi&species=$species"},
      transcript_protein => {url => "$baseUrl/api/autocomplete/protein_feature?query=$transcript_id&species=$species"},
      samples => {url => "$baseUrl/api/autocomplete/sample?species=$species&query=s"},
      orthologs => {url => "$baseUrl/lera/api/autocomplete/ortholog/gene?entityId=$transcript_id&query=epi"}
    },
    REFERENCES => {
      genome => _buildReferenceLink("genome", $species, $transcript_id),
      protein => _buildReferenceLink("protein", $species, $transcript_id)
    },
    TRACKS => {
      genotype => _buildTrackLink( 'api/commandline/track/genotype', $intervalParams, $trackParams ),
      peptype => _buildTrackLink( 'api/track/protein_genotype', $intervalParams, $trackParams ),
      transcript => _buildTrackLink( 'api/track/transcript', $intervalParams, $trackParams ),
      protein => _buildTrackLink( 'api/track/protein', $intervalParams, $trackParams ),
      variations_density => _buildTrackLink( 'api/track/variations_density', $intervalParams, $trackParams )
    }
  };
}
