\hypertarget{classConf}{\section{Conf Class Reference}
\label{classConf}\index{Conf@{Conf}}
}


\subsection{Class Summary}
\hypertarget{classLera_1_1Model_1_1BcftoolsAdapters_1_1VcfConf_Description}{}\subsection{Description}\label{classLera_1_1Model_1_1BcftoolsAdapters_1_1VcfConf_Description}
 \subsection*{Available Methods}
\begin{DoxyCompactItemize}
\item 
protected Hash \hyperlink{classConf_abad62bc45fc8337db097ec730b320438}{\-\_\-get\-\_\-ini\-\_\-config} ()
\item 
protected Hash \hyperlink{classConf_ad2b7dc6caefb6a290b115ae4486b4491}{\-\_\-get\-\_\-json\-\_\-config} ()
\item 
protected String \hyperlink{classConf_a79b2e039289f66e4d4bb7fd63df30b81}{\-\_\-interpolate} ()
\item 
public \hyperlink{classConf_a77fb531512c0962ac98804481990b313}{has\-\_\-config} ()
\item 
public Hash \hyperlink{classConf_a236923e7ef9fba4e0e08181df405b772}{get\-\_\-config} ()
\item 
protected \hyperlink{classConf_abf3091528ce1c850afe361d655c89f96}{\-\_\-load\-\_\-config} ()
\end{DoxyCompactItemize}


\subsection{Method Documentation}
\hypertarget{classConf_abad62bc45fc8337db097ec730b320438}{\index{Conf@{Conf}!\-\_\-get\-\_\-ini\-\_\-config@{\-\_\-get\-\_\-ini\-\_\-config}}
\index{\-\_\-get\-\_\-ini\-\_\-config@{\-\_\-get\-\_\-ini\-\_\-config}!Conf@{Conf}}
\subsubsection[{\-\_\-get\-\_\-ini\-\_\-config}]{\setlength{\rightskip}{0pt plus 5cm}protected Hash Conf\-::\-\_\-get\-\_\-ini\-\_\-config (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_abad62bc45fc8337db097ec730b320438}

\begin{DoxyPre} Arg [1]     : string $path
 Arg [2]     : boolean $ignore\_empty
 Description : Parses INI configuration files and returns a perl HASH object.
 The configuration files can use interpolation within the file (reusing file variables)
 and interpolation allowing to use configuration values as templates.
 The configuration values can also be environment variables.
 Returntype  : hash ref
 Status      : Stable
 \end{DoxyPre}


  
\begin{DoxyCode}
sub \hyperlink{classConf_abad62bc45fc8337db097ec730b320438}{\_get\_ini\_config} \{
\textcolor{preprocessor}{    # Loads configuration from a given file and checks that each paramater (is defined OR declared as
       possibly undefined)}
\textcolor{preprocessor}{}    my ($path, $ignore\_empty) = @\_;
    my %conf = Config::General->new(
                -ConfigFile => \textcolor{stringliteral}{"$path"},
                -InterPolateVars => 1,
                -InterPolateEnv => 1)->getall;

    \textcolor{keywordflow}{foreach} my $key (keys %conf) \{
        unless ($conf\{$key\} || $ignore\_empty->\{$key\}) \{
            die \textcolor{stringliteral}{"Parameter '$key' should be defined in configuration file '$path' (If this parameter is
       associated to an ENV variable, check that this variable is set)"};
        \}
    \}

    \textcolor{keywordflow}{return} %conf;
\}
\end{DoxyCode}
  \hypertarget{classConf_ad2b7dc6caefb6a290b115ae4486b4491}{\index{Conf@{Conf}!\-\_\-get\-\_\-json\-\_\-config@{\-\_\-get\-\_\-json\-\_\-config}}
\index{\-\_\-get\-\_\-json\-\_\-config@{\-\_\-get\-\_\-json\-\_\-config}!Conf@{Conf}}
\subsubsection[{\-\_\-get\-\_\-json\-\_\-config}]{\setlength{\rightskip}{0pt plus 5cm}protected Hash Conf\-::\-\_\-get\-\_\-json\-\_\-config (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_ad2b7dc6caefb6a290b115ae4486b4491}

\begin{DoxyPre} Arg [1]     : string $path
 Description : Parses JSON configuration files and returns a perl HASH object.
 Modified from json\_file\_to\_perl (\href{http://cpansearch.perl.org/src/BKB/JSON-Parse-0.41/lib/JSON/Parse.pm}{\tt http://cpansearch.perl.org/src/BKB/JSON-Parse-0.41/lib/JSON/Parse.pm})
 to validate file before parsing and showing nice error message
 Returntype  : hash ref
 Status      : Stable
 \end{DoxyPre}


  
\begin{DoxyCode}
sub \hyperlink{classConf_ad2b7dc6caefb6a290b115ae4486b4491}{\_get\_json\_config} \{
    my ($path) = @\_;

    my $json = \textcolor{stringliteral}{''};
    open my $in, \textcolor{stringliteral}{"<:encoding(utf8)"}, $path
            or die \textcolor{stringliteral}{"Error opening JSON configuration file $path: $!\(\backslash\)n"};
    \textcolor{keywordflow}{while} (<$in>) \{
            $json .= $\_;
    \}
    close $in or die \textcolor{stringliteral}{"Error manipulating JSON configuration file $path: $!\(\backslash\)n"};
    die \textcolor{stringliteral}{"JSON configuration file $path does not contain a valid JSON.\(\backslash\)n"} unless valid\_json($json);
    my $conf = parse\_json ($json);
    \textcolor{keywordflow}{return} $conf;
\}
\end{DoxyCode}
  \hypertarget{classConf_a79b2e039289f66e4d4bb7fd63df30b81}{\index{Conf@{Conf}!\-\_\-interpolate@{\-\_\-interpolate}}
\index{\-\_\-interpolate@{\-\_\-interpolate}!Conf@{Conf}}
\subsubsection[{\-\_\-interpolate}]{\setlength{\rightskip}{0pt plus 5cm}protected String Conf\-::\-\_\-interpolate (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_a79b2e039289f66e4d4bb7fd63df30b81}

\begin{DoxyPre} Arg [1]     : hash ref $conf
 Arg [2]     : hash mapping
 Description : Replace \{\{key\}\} in the configuration string by the value associated to the key in the mapping hash
 Returntype  : string
 Status      : Stable
 \end{DoxyPre}


  
\begin{DoxyCode}
sub \hyperlink{classConf_a79b2e039289f66e4d4bb7fd63df30b81}{\_interpolate} \{
    my $conf = shift;
    my %mapping = %\{shift()\};

    \textcolor{keywordflow}{foreach} my $parameter\_name ( keys %mapping )\{
        my $parameter\_value = $mapping\{$parameter\_name\};
        die \textcolor{stringliteral}{"Can not interpolate configuration because value for $parameter\_name is undefined"} unless 
      defined $parameter\_value;
        $conf =~ s/\{\{$parameter\_name\}\}/$parameter\_value/;
    \}

    \textcolor{keywordflow}{return} $conf;
\}
\end{DoxyCode}
  \hypertarget{classConf_abf3091528ce1c850afe361d655c89f96}{\index{Conf@{Conf}!\-\_\-load\-\_\-config@{\-\_\-load\-\_\-config}}
\index{\-\_\-load\-\_\-config@{\-\_\-load\-\_\-config}!Conf@{Conf}}
\subsubsection[{\-\_\-load\-\_\-config}]{\setlength{\rightskip}{0pt plus 5cm}protected Conf\-::\-\_\-load\-\_\-config (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_abf3091528ce1c850afe361d655c89f96}
Undocumented method

  
\begin{DoxyCode}
sub \hyperlink{classConf_abf3091528ce1c850afe361d655c89f96}{\_load\_config} \{
  my ($path) = @\_;
  \textcolor{keywordflow}{if}( $path=~ /\(\backslash\).ini$/ )\{

    my %ini\_conf = \hyperlink{classConf_abad62bc45fc8337db097ec730b320438}{\_get\_ini\_config}($path);
    \textcolor{keywordflow}{return} \(\backslash\)%ini\_conf;

  \} elsif ( $path=~ /\(\backslash\).json$/ ) \{

    \textcolor{keywordflow}{return} \hyperlink{classConf_ad2b7dc6caefb6a290b115ae4486b4491}{\_get\_json\_config}($path);

  \} \textcolor{keywordflow}{else} \{

    die \textcolor{stringliteral}{"Can not handle configuration format for file $path\(\backslash\)n"};
  \}

\}
\end{DoxyCode}
  \hypertarget{classConf_a236923e7ef9fba4e0e08181df405b772}{\index{Conf@{Conf}!get\-\_\-config@{get\-\_\-config}}
\index{get\-\_\-config@{get\-\_\-config}!Conf@{Conf}}
\subsubsection[{get\-\_\-config}]{\setlength{\rightskip}{0pt plus 5cm}public Hash Conf\-::get\-\_\-config (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_a236923e7ef9fba4e0e08181df405b772}

\begin{DoxyPre} Arg [1]     : string $path
 Arg [2]     : string $key (optional)
 Arg [3]     : hash ref $mapping (optional)
 Description : Returns the full hash ref corresponding to a configuration file,
 or a specific value if a key is submitted, If mapping is submitted, the value will be interpolated with mapping keys/values
 Returntype  : hash or string
 Status      : Stable\end{DoxyPre}



\begin{DoxyPre} TODO Support not found config files and keys
 \end{DoxyPre}


  
\begin{DoxyCode}
sub \hyperlink{classConf_a236923e7ef9fba4e0e08181df405b772}{get\_config} \{
  my ($path, $key, $mapping) = @\_;

  $config->\{$path\} ||= \hyperlink{classConf_abf3091528ce1c850afe361d655c89f96}{\_load\_config}($path);

    \textcolor{keywordflow}{if} ($key)\{
        my $selected\_config = $config->\{$path\} && $config->\{$path\}\{$key\};
        \textcolor{keywordflow}{if}( $mapping )\{
                \textcolor{keywordflow}{return} \hyperlink{classConf_a79b2e039289f66e4d4bb7fd63df30b81}{\_interpolate}( $selected\_config, $mapping );
        \} \textcolor{keywordflow}{else} \{
            \textcolor{keywordflow}{return} $selected\_config;
        \}

    \} \textcolor{keywordflow}{else} \{
            \textcolor{keywordflow}{return} $config->\{$path\};
    \}
\}
\end{DoxyCode}
  \hypertarget{classConf_a77fb531512c0962ac98804481990b313}{\index{Conf@{Conf}!has\-\_\-config@{has\-\_\-config}}
\index{has\-\_\-config@{has\-\_\-config}!Conf@{Conf}}
\subsubsection[{has\-\_\-config}]{\setlength{\rightskip}{0pt plus 5cm}public Conf\-::has\-\_\-config (
\begin{DoxyParamCaption}
{}
\end{DoxyParamCaption}
)}}\label{classConf_a77fb531512c0962ac98804481990b313}
Undocumented method

  
\begin{DoxyCode}
sub \hyperlink{classConf_a77fb531512c0962ac98804481990b313}{has\_config} \{
    my ($path) = @\_;
    \textcolor{keywordflow}{return} defined \hyperlink{classConf_a236923e7ef9fba4e0e08181df405b772}{get\_config}( $path );
\}
\end{DoxyCode}
  

The documentation for this class was generated from the following file\-:\begin{DoxyCompactItemize}
\item 
/mnt/myebihome/\-P\-R\-O\-J\-E\-C\-T\-S/lera/lib/\hyperlink{Conf_8pm}{Conf.\-pm}\end{DoxyCompactItemize}
