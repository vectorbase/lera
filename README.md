***Genotype Explorer***

**Layered Extensible Research Assistant**

**Main principles**

*Abstract data structure for ease of extensibility to new biological entities/data.*

We keep the browser code independent of any biological meaning. The browser simply manipulate abstract concepts : references on which depend tracks, tracks composed of entities, entities composed of components. It is the task of the rest api data provider to translate specific biological data into an abstract format.

- Reference : Determines the x-axis for entities and components on dependant tracks. When a track modifies the reference (by scrolling left or right for example), all tracks that depend on that reference are updated.
- Track : Groups entities of same type (ex: gene) that are drawn with respect to the same reference (ex: a.gambiae genome or proteic a.a. sequence)
- Entity : It may be a transcript, gene, protein, sample
- Component : It may be an exon, genotype, proteic domain

*Distributed computation/memory instead of distributed data*

The browser does very little processing, delegating the task to retrieve entities for a given reference location to different rest api. The rest api urls can target any server in the world, which allows massive distribution of processing/memory requirements.

All the available rest APIs are registered on a “Registry” file (which could become a website) which allows humans to add new tracks, and allows all browser clients to retrieve the most up to date list of available tracks.

*Support for multiple references*

The app can deal with multiple types of references, ex :
- genomic (species, chrom, start, end)
- protein (species, protein_id, start, end)
- geographic ? (distance from a given position (ex: sample location ?) ?)

To update the dependant tracks, the app calls the rest api url declared for each track, and sends all data from the reference and track.

All tracks are linked to a reference. When the user scrolls left/right/zooms on one of the tracks, this changes the view interval of the reference on which multiple tracks depends, which triggers the update of all tracks that depend on this reference.
This allows us to have in the same browser multiple tracks for different species, or even multiple tracks at different positions of the same species. We can use orthology data to synchronise tracks between species.

*Support for incomplete configuration*

The browser needs every reference to have at least start/stop properties. If a reference was declared without these properties, the app will send all available data about this reference to the URL declared for this reference, to update the reference with missing data.
For example if a genomic reference is declared with only species and gene_id properties, the app will send this data to the rest api url associated with the genomic reference type to retrieve the start/end and other properties for this reference.

Ex:

Genomic reference declared with data :
{“species”:”anopheles_funestus”, “query”:”AFUN005325”}

Will be resolved after a call to the rest api declared for the reference, into :
{"seq_region_name":"KB668725","coord_system_name":"supercontig","species":"anopheles_funestus","end":2655466,"start":2617657}

*Enjoyable user experience*

* As few buttons and controls as possible
* History of actions and possibility to undo/redo
