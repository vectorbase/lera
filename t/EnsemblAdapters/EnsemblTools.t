#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::EnsemblAdapters::EnsemblTools;


##
# execute_on_feature_type
##
is( get_feature_type_from_id('AGAP004707-RC'), 'TRANSCRIPT', 'Can detect a transcript');
is( get_feature_type_from_id('AGAP004707-PC'), 'PROTEIN', 'Can detect a protein');
is( get_feature_type_from_id('AGAP004707'), 'GENE', 'Can detect a gene');
is( get_feature_type_from_id('AGAP004707-KKK-1'), undef, 'Returns undefined if feature type is unknown');
is_deeply( execute_on_feature_type('2L:12-14', sub {
  return { chromosome => $_[0], start => $_[1], end => $_[2] }
}), {chromosome => '2L', start => 12, end => 14}, 'Can parse coordinates' );


done_testing();
