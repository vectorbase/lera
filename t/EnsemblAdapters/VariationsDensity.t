#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::EnsemblAdapters::Tracks::VariationsDensity;

subtest '_get_bucket_start_end' => sub {

  is_deeply( [Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_start_end(106700, 2)], [106650, 106749],
  '' );

  # SQL ROUND rules :
  # 106649 -> 106600
  # 106650 -> 106700
  # 106749 -> 106700
  # 106750 -> 106800

};

subtest '_get_interval_start_end' => sub {

 is_deeply( [Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_interval_start_end( 7612415, 14686906, 6 )],
  ['7500000', '15499999'], 'gives the interval limits allowing to have full buckets' );

 is_deeply( [Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_interval_start_end( 76, 14686906, 6 )],
  ['1', '15499999'], 'puts in bucket 0 values smaller than 10^decimal_precision, and the smallest authorized bucket start is 1' );

};

subtest '_get_color' => sub {
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_color( 0 ), 'hsl(240,50%,50%)', 'smallest density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_color( 0.5 ), 'hsl(300,50%,50%)', 'medium density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_color( 1 ), 'hsl(360,50%,50%)', 'highest density');

  # Bad arguments
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_color( -1 ), 'hsl(240,50%,50%)', 'density smaller than 0');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_color( 2 ), 'hsl(360,50%,50%)', ' density bigger than 1');
};

subtest '_get_bucket_density' => sub {
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1000, 1000 ), 1, 'saturated density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1001, 0 ), 0, 'no density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1001, 1 ), 0.100314316923765, 'light density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1001, 500 ), 0.899685683076235, 'half density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1001, 950 ), 0.99243978518912, 'high density');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1, 1 ), 1, 'One length, one variant');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, 1, 0 ), 0, 'One length, no variant');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 0, 0, 1 ), 1, 'Zero length, one variant');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 0, 0, 0 ), 0, 'Zero length, no variant');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, -10, 1 ), 0, 'Negative length, one variant');
  is( Lera::Model::EnsemblAdapters::Tracks::VariationsDensity::_get_bucket_density( 1, -10, 0 ), 0, 'Negative length, no variant');
};

done_testing();
