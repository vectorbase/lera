
#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::EnsemblAdapters::Tracks::Peptype;


my $COLORS = Lera::Model::EnsemblAdapters::Tracks::Peptype::_get_all_colors_hash();

subtest '_add_peptype' => sub {

  {
    local $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, undef, 1, 's1', 'T', 'K', 'M');
    is_deeply( $c, {}, 'entries for not found genotypes are ignored' );
  }

};

subtest 'One variation' => sub {

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T', 'T'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{ALT}, # alt color
          data => { position => 1, ref => 'Lys', alt => ['Met'], genotypes => {} },
          title => 'Lys, Met'
        } },
     'alt/alt genotype : one single alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['A', 'T'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{ALT}, # alt color
          data => { position => 1, ref => 'Lys', alt => ['Met'], genotypes => {} },
          title => 'Lys, Met',
        } },
      'ref/alt genotype : one single alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['A', 'A'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => { start => 1,
          shape => 'thick-full',
          color => $COLORS->{REF}, # alt color
          data => { position => 1, ref => 'Lys', genotypes => {} },
          title => 'Lys',
        } },
      'ref/ref genotype : no alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T', 'C'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{ALT}, # alt color
          data => { position => 1, ref => 'Lys', alt => ['Met'], genotypes => {} },
          title => 'Lys, Met',
        } },
      'alt/altBad genotype : the alt_pep_allele corresponding to alt is added, altBad is ignored' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['C', 'C'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, {},
     'altBad/altBad genotype : no component is created' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['A', 'C'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{REF}, # alt color
          data => { position => 1, ref => 'Lys', genotypes => {} },
          title => 'Lys',
        } },
      'ref/altBad genotype : no alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['A'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => { start => 1,
          shape => 'thick-full',
          color => $COLORS->{REF}, # alt color
          data => { position => 1, ref => 'Lys', genotypes => {} },
          title => 'Lys',
        } },
      'ref genotype : no alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T'], 1, 's1', 'A', 'T', 'K', 'M');
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{ALT}, # alt color
          data => { position => 1, ref => 'Lys', alt => ['Met'], genotypes => {} },
          title => 'Lys, Met',
        } },
      'alt genotype : no alt_pep_allele is added to component' );
  }

  {
    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    # pep_allele_string is like 'K', which makes split('/', pep_allele_string)[1] undefined
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T'], 1, 's1', 'A', 'T', 'K', undef);
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{REF}, # alt color
          data => { position => 1, ref => 'Lys', genotypes => {} },
          title => 'Lys',
        } },
     'alt/alt genotype on synonymous variant' );
  }

};


subtest 'Two variations on two different amino acids' => sub {

  my $c = {};
  #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
  Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T'], 1, 's1', 'A', 'T', 'K', 'M');
  Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['C'], 2, 's1', 'A', 'C', 'R', 'W');
  is_deeply( $c, {
      '1#s1' => {
        start => 1,
        shape => 'thick-full',
        color => $COLORS->{ALT}, # alt color
        data => { position => 1, ref => 'Lys', alt => ['Met'], genotypes => {} },
        title => "Lys, Met",
      },
      '2#s1' => {
        start => 2,
        shape => 'thick-full',
        color => $COLORS->{ALT}, # alt color
        data => { position => 2, ref => 'Arg', alt => ['Trp'], genotypes => {} },
        title => "Arg, Trp"
      },
   },
   'two genotypes on different amino acids' );

};


subtest 'Two variations on DIFFERENT genomic positions on the SAME amino acid' => sub {

    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    # 1. ref/altBad
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['A', 'C'], 1, 's1', 'A', 'T', 'K', 'M', 'sift', 10);
    # 2. alt/alt
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['T', 'T'], 1, 's1', 'A', 'T', 'K', 'M', 'sift', 11);
    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{COMPLEX},
          data => { position => 1, genotypes => {10 => 'NON_HOMO_REF', 11 => 'NON_HOMO_REF'}, ref => 'Lys', alt => ['Met']},
          title => 'Lys, Met',
      } },
     'genotype alleles (ref, altbad, alt1, alt1) => one alt_pep_allele added' );

};

subtest 'Two variations on SAME genomic position but different effect on the SAME amino acid' => sub {

    my $c = {};
    #  $components, $genotype, $p_start, $sample, $ref, $alt, $p_ref, $p_alt, $sift, $t_start
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['C', 'T'], 1, 's1', 'A', 'T', 'K', 'M', 'sift', 10);
    Lera::Model::EnsemblAdapters::Tracks::Peptype::_add_peptype($c, ['C', 'T'], 1, 's1', 'A', 'C', 'K', 'W', 'sift',10);

    is_deeply( $c, { '1#s1' => {
          start => 1,
          shape => 'thick-full',
          color => $COLORS->{ALT}, # alt color
          data => { position => 1, genotypes => {10 => 'NON_HOMO_REF'}, ref => 'Lys', alt => ['Met', 'Trp'] },
          title => 'Lys, Met, Trp',
      } },
     'genotype alleles (ref, alt1, ref, alt2) : two alt_pep_allele added' );

};

subtest 'get_color' => sub {

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ),
  $COLORS->{REF}, 'color is red if alt_pep_alleles list is undef' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( [] ),
  $COLORS->{REF}, 'color is green if alt_pep_alleles list is empty' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'] ),
  $COLORS->{ALT}, 'color is red if alt_pep_alleles list is not empty' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'], {'pos1' => 'NON_HOMO_REF'} ),
  $COLORS->{ALT}, 'color is red if one NON_HOMO_REF genotype and alt list is not empty' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'], {'pos1'=>'NON_HOMO_REF', 'pos2' => 'NON_HOMO_REF'} ),
  $COLORS->{COMPLEX}, 'color is COMPLEX because two non homo ref genotypes' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'], {'pos1' => 'HOMO_REF', 'pos2' => 'NON_HOMO_REF'} ),
  $COLORS->{ALT}, 'color is RED if there are no more than one non HOMO_REF genomes' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( [], {'pos1' => 'HOMO_REF', 'pos2' => 'HOMO_REF'} ),
  $COLORS->{REF}, 'color is green because all genotypes are homo ref' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( [], {'pos1' => 'HOMO_REF', 'pos2' => 'HOMO_REF', 'pos3' => 'HOMO_REF'} ),
  $COLORS->{REF}, 'color is green because all genotypes are homo ref' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'], {'pos1' => 'HOMO_REF', 'pos2' => 'HOMO_REF', 'pos3' => 'NON_HOMO_REF'} ),
  $COLORS->{ALT}, 'color is red because one non homozygous ref genotype is present' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( ['alt1'], {'pos1' => 'HOMO_REF', 'pos2' => 'NON_HOMO_REF', 'pos3' => 'NON_HOMO_REF'} ),
  $COLORS->{COMPLEX}, 'color is COMPLEX because two non homozygous ref genotypes are present' );

  # conflicts
  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( [], {'pos1' => 'NON_HOMO_REF'} ),
  $COLORS->{REF}, 'It is OK to have no alt peptides and a non homo ref genotype, because some mutations are synonymous' );

  is( Lera::Model::EnsemblAdapters::Tracks::Peptype::get_color( [], {'pos1' => 'HOMO_REF'} ),
  $COLORS->{REF}, 'It is OK to have no alt peptides and a non homo ref genotype, because some mutations are synonymous' );

};


subtest 'merge_components' => sub {

  {
    local $c = { data => { ref => 'R'} }; # first peptype has no alt
    Lera::Model::EnsemblAdapters::Tracks::Peptype::merge_components( $c, undef );
    is_deeply( $c, { color => $COLORS->{REF}, data => { genotypes => {}, ref => 'R' }, title => 'R' }, 'color is green if merged component has no alt_pep_alleles' );

    Lera::Model::EnsemblAdapters::Tracks::Peptype::merge_components( $c, 'M' );
    is_deeply( $c, { color => $COLORS->{ALT}, data => { alt => ['M'], genotypes => {}, ref => 'R' }, title => 'R, M' }, 'added first alt pep allele' );

    Lera::Model::EnsemblAdapters::Tracks::Peptype::merge_components( $c, 'K' );
    is_deeply( $c, { color => $COLORS->{ALT}, data => { alt => ['M', 'K'], genotypes => {}, ref => 'R' }, title => 'R, M, K' }, 'added different alt pep allele' );

    Lera::Model::EnsemblAdapters::Tracks::Peptype::merge_components( $c, 'M' );
    is_deeply( $c, { color => $COLORS->{ALT}, data => { alt => ['M', 'K'], genotypes => {}, ref => 'R' }, title => 'R, M, K' }, 'added same pep allele again' );
  }


};

done_testing();
