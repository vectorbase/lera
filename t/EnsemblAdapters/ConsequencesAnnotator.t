#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::EnsemblAdapters::Annotators::ConsequencesAnnotator;


##
# _create_consequences_hash
##



##
# _create_key
##



##
# get_genotype_consequences
##

my $conseq_cache1 = {
 '2L:2358316:G' => { 'AGAP004707-RC' => {'intron_variant'   => 1 }, 'AGAP004707-RB' => {'intron_variant'   => 1 } },
 '2L:2358254:A' => { 'AGAP004707-RC' => {'missense_variant' => 1 }, 'AGAP004707-RB' => {'missense_variant' => 1 } },
 '2L:1:A' => { 'tr1'           => {'cons1'            => 1, 'cons1_bis' => 1} },
 '2L:1:G' => { 'tr1'          => {'cons2'            => 1} }
};


my $alleles1 = ['A', 'G'];
my $alleles2 = ['G', 'G'];
my $alleles3 = ['A', 'A'];

my $gc1 = get_transcript_consequences( $conseq_cache1, '2L', 2358316, $alleles1 );
is_deeply( $gc1, {'AGAP004707-RC' => 'intron_variant', 'AGAP004707-RB' => 'intron_variant'},
  'can retrieve single consequence for a genotype and single transcript' );

my $gc2 = get_transcript_consequences( $conseq_cache1, '2L', 2358316, $alleles2 );
is_deeply( $gc2, {'AGAP004707-RC' => 'intron_variant', 'AGAP004707-RB' => 'intron_variant'},
  'can retrieve single consequence for a genotype with repeated matching alleles' );

my $gc3 = get_transcript_consequences( $conseq_cache1, '2L', 2358316, $alleles3 );
is_deeply( $gc3, {}, 'can retrieve empty consequences for a genotype with no matching alleles' );

my $gc4 = get_transcript_consequences( $conseq_cache1, '2L', 1, $alleles1 );
my $cons4 = $gc4->{'tr1'};
is(  $cons4 , 'cons1, cons1_bis, cons2' ,
  'can combine different consequences for different alleles' );


#TODO Test complex merging of transcripts and consequences

#TODO Test INSERTIONS !


done_testing();
