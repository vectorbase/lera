#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Test::Exception;

use Data::Dumper;

use Lera::Model::BcftoolsAdapters::VcfConf;

my $vcf_conf = {

  species1 => {
    chr1 => {
      chromosomes => {
        chr1 => 1
      },
      samples => {
        s1 => 1,
        s2 => 1
      },
      path => 'vcfPath1'
    },
    chr9 => {
      chromosomes => {
        chr9 => 1
      },
      samples => {
        s1 => 1,
        s2 => 1
      },
      path => 'vcfPath19'
    }
  },

  species2 => {
    default => {
      chromosomes => {
        chr1 => 1,
        chr2 => 1
      },
      samples => {

      },
      path => 'vcfPath2'
    }
  }

};

##
# _get_vcf
##
subtest '_get_vcf' => sub {

  dies_ok ( sub { _get_vcf($vcf_conf, 'unexistingSpecies', ['unexisting Chromosome'], []) },
   'with unexisting species' );

  throws_ok( sub { _get_vcf($vcf_conf, 'species1', [], []) },
  qr/Chromosomes is a mandatory parameter/ , 'with existing species, but no submitted chromosomes' );

  # Chromosome declared explicitely in config ( ex: gambiae ag1k )

  throws_ok( sub { _get_vcf($vcf_conf, 'species1', ['chr2'], ['s1']) },
  qr/No registered VCF file for chromosomes chr2 for species species1/ , 'chromosome not found for species' );

  throws_ok( sub { _get_vcf($vcf_conf, 'species1', ['chr1'], []) },
  qr/No samples submitted/ , 'no samples submitted' );

  is_deeply(  [_get_vcf($vcf_conf, 'species1', ['chr1'], ['s3', 's4'])] ,
  ['vcfPath1', 'chr1', { found => [], notFound => ['s3', 's4'] }] , 'none of the samples is found' );

  is_deeply( [_get_vcf($vcf_conf, 'species1', ['chr1'], ['s1', 's3'])] ,
  ['vcfPath1', 'chr1', { found => ['s1'], notFound => ['s3'] }] , 'one of the samples is not found' );

  # Try to finf the chromosome that exists
  is_deeply( [_get_vcf($vcf_conf, 'species1', ['inexistingChr', 'chr1'], ['s1'])] ,
  ['vcfPath1', 'chr1', { found => ['s1'], notFound => [] }] , 'multiple chromosomes for ref' );


  # Multiple chromosomes in the same file ( ex: arabianeis )
  throws_ok( sub { _get_vcf($vcf_conf, 'species2', ['chr3'], ['s1']) },
  qr/No registered VCF file for chromosomes chr3 for species species2/ , 'chromosome not found in default vcf object' );

  throws_ok( sub { _get_vcf($vcf_conf, 'species2', ['chr1'], []) },
  qr/No samples submitted/ , 'no samples submitted' );

  is_deeply( [_get_vcf($vcf_conf, 'species2', ['chr1'], ['s1'])] ,
  ['vcfPath2', 'chr1', { found => [], notFound => ['s1'] }] , 'chromosome found in default vcf object' );

  is_deeply( [_get_vcf($vcf_conf, 'species2', ['chrUnexisting', 'chr1', 'chr2'], ['s1'])] ,
  ['vcfPath2', 'chr1', { found => [], notFound => ['s1'] }] ,
   'multiple chromosomes declared, using first found in default vcf object' );


};

##
# get_vcf_species
##


##
# get_matching_samples
##
is_deeply( get_matching_samples({samples => {s1 => 1, s4 => 1}}, ['s1', 's2', 's3']),
{found => ['s1'], notFound => ['s2', 's3'] }, 'get_matching_samples: 1 sample found and 2 missing');

is_deeply( get_matching_samples({samples => {s1 => 1, s2 => 1, s3 => 1}}, ['s1', 's2', 's3']),
{found => ['s1', 's2', 's3'], notFound => [] }, 'get_matching_samples: all found');

is_deeply( get_matching_samples({samples => {}}, ['s1', 's2', 's3']),
{found => [], notFound => ['s1', 's2', 's3'] }, 'get_matching_samples: all not found');


done_testing();
