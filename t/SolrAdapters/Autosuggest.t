#!/usr/bin/env perl
use lib "lib";

use Test::More;
use Data::Dumper;
use Conf;

use Lera::Model::SolrAdapters::Autosuggest;
use Lera::Model::SolrAdapters::Autosuggest qw( process_query __get_available_species_filter );


# The vcf_config now contains test_species_one and test_species_two
$Lera::Model::BcftoolsAdapters::VcfConf::TEST_MODE = 1;


my $vb_base_url = get_config('conf/solr.ini', 'vb_autocomplete_server');
my $se_solr_base_url = get_config('conf/solr.ini', 'sample_explorer_autocomplete_server');

##
# process_query
##
subtest 'process_query' => sub {

  is(process_query('test1', '+'), "%2Btest1%2A", 'process_query transforms terms from the query into mandatory prefixes');

  is(process_query('AGAP004707-RA', '+'), "%2BAGAP004707%5C-RA%2A", 'process_query escapes special solr character -');

  is(process_query(), "%2A", 'search for * if no query submitted');

  is(process_query(""), "%2A", 'search for * if empty string submitted');

  is(process_query(0), "0%2A", 'consider 0 as valid query');

};

##
# species_ens_to_vb
##

is( species_ens_to_vb("anopheles_gambiae"), "\"Anopheles_gambiae\"",
  "species_ens_to_vb can transform spcies name from ensembl to vb format");


##
# get_species_autocomplete_url
##
is( get_species_autocomplete_url("zika"),
  "$vb_base_url/vbsearch/query/zika%2A?fq=entity_type:organism,reference_genome_ss:(\"Test_species_one\" OR \"Test_species_two\")&qf=label,content&fl=id,label,content,reference_genome_ss&hl=true&hl.fl=content",
  "get_species_autocomplete_url can create autocomplete URL for species for one complete word" );

is( get_species_autocomplete_url("zika anoph"),
  "$vb_base_url/vbsearch/query/zika%2A%20AND%20anoph%2A?fq=entity_type:organism,reference_genome_ss:(\"Test_species_one\" OR \"Test_species_two\")&qf=label,content&fl=id,label,content,reference_genome_ss&hl=true&hl.fl=content",
  "get_species_autocomplete_url can create autocomplete URL with word prefixes" );

##
# get_feature_autocomplete_url
##

subtest 'get_feature_autocomplete_url' => sub {

  is( get_feature_autocomplete_url("sodium", ["transcript", "gene"]),
    "$vb_base_url/vbsearch/query/sodium%2A?fq=entity_type:(transcript OR gene),reference_genome_ss:(\"Test_species_one\" OR \"Test_species_two\")&qf=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype&fl=id,accession,reference_genome_ss,entity_type&hl=true&hl.fl=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype",
"get_feature_autocomplete_url can create autocomplete URL for two feature for one complete word, if no species is specified, all available species are searched");

  is( get_feature_autocomplete_url("sodium", ["transcript"], "anopheles_gambiae"),
    "$vb_base_url/vbsearch/query/sodium%2A?fq=entity_type:(transcript),reference_genome_ss:\"Anopheles_gambiae\"&qf=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype&fl=id,accession,reference_genome_ss,entity_type&hl=true&hl.fl=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype",
    "get_feature_autocomplete_url can create autocomplete URL by adapting species name no vb format");

  is( get_feature_autocomplete_url("sodium *AGAP0047", ["transcript"], "anopheles_gambiae"),
    "$vb_base_url/vbsearch/query/sodium%2A%20AND%20%5C%2AAGAP0047%2A?fq=entity_type:(transcript),reference_genome_ss:\"Anopheles_gambiae\"&qf=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype&fl=id,accession,reference_genome_ss,entity_type&hl=true&hl.fl=accession,symbol,strain,label,synonym,identifier,domain,xref,history,description,biotype",
    "get_feature_autocomplete_url can create a AND query woth two prefixes");

};

##
# get_sample_autocomplete_url
##
subtest 'get_sample_autocomplete_url' => sub {

  is( get_sample_autocomplete_url("ddt", "anopheles_gambiae"),
    "$se_solr_base_url/solr/metasearch/sample_autosuggest?q=%2Bddt%2A&fq=m_reference_genome:Anopheles_gambiae&fq=is_deprecated:false",
    "get_sample_autocomplete_url can create autocomplete URL with word prefixes");

  is( get_sample_autocomplete_url("ddt", "anopheles_gambiae", ['ex1', 'ex2']),
    "$se_solr_base_url/solr/metasearch/sample_autosuggest?q=%2Bddt%2A&fq=m_reference_genome:Anopheles_gambiae&fq=is_deprecated:false&fq=-UUID:(ex1 OR ex2)",
    "get_sample_autocomplete_url can exclude already selected samples");

  is( get_sample_autocomplete_url("ddt", "anopheles_gambiae", []),
    "$se_solr_base_url/solr/metasearch/sample_autosuggest?q=%2Bddt%2A&fq=m_reference_genome:Anopheles_gambiae&fq=is_deprecated:false",
    "get_sample_autocomplete_url ignores empty exclude list");

  is( get_sample_autocomplete_url("ddt", "anopheles_gambiae", ['ex1+', 'ex2:2']),
    "$se_solr_base_url/solr/metasearch/sample_autosuggest?q=%2Bddt%2A&fq=m_reference_genome:Anopheles_gambiae&fq=is_deprecated:false&fq=-UUID:(ex1%5C%2B OR ex2%5C%3A2)",
    "get_sample_autocomplete_url can exclude already selected samples with special characters");

};



##
# process_species_autocomplete
##

is_deeply(
  process_species_autocomplete([{id => 'id1', reference_genome_ss => ['Anopheles_gambiae', 'Test_species_one'],  content=>'c1'}],
  {id1 => {content => ['hld2']},
  'some query'
  }),
  [{identifier=>'test_species_one', description=>'hld2'}],
  'process_species_autocomplete joins all highlighted descriptions as one description field');

is_deeply(
  process_species_autocomplete([{id => 'id1', path=>'/organisms/anopheles-gambiae',  content=>'c1'}],
  {id1 => {content => ['hld2']},
  'some query'
  }),
  [],
  'process_species_autocomplete fails elegantly if field reference_genome_ss does not exists in document');

# TODO Test cases where the vb api returns unexpected format

# test cases where $res is [] // no hits


##
# create_description
##
subtest 'create_description' => sub {

  is(create_description('id1', '_text_', {id1 => {_text_ => ['t1', 't<strong>2</strong>']}} ), 't1...t<strong>2</strong>',
    'create_description when highlighting from solr is available');

  is(create_description('id1', '_text_', {}, {_text_ => ['the zika virus is related to several aedes species', 'recent outbreak of the zika virus']},
  'zik aed' ), 'the <strong>zika</strong> virus is related to... related to several <strong>aedes</strong> species...recent ',
    'create_description when highlighting must be done by lera');

  is(create_description('id1', '_text_', {}, {_text_ => ['the zika virus', 'multiple aedes species']},
  'zik aed' ), 'the <strong>zika</strong> virus...multiple ... virus...multiple <strong>aedes</strong> species',
    'create_description when highlighting must be done by lera, need several sentences to match all values');

# TODO need a way to guarantee order
#  is(create_description('id1', undef, {id1 => {
#    accession => ['t1', 't<strong>2</strong>'],
#    label => ['<strong>superLabel</strong>']
#    }
#  } ), '<strong>superLabel</strong>...t1...t<strong>2</strong>',
#    'create_description when highlighting from solr is available for multiple fields');

  is(create_description(undef, undef, {id1 => {
    accession => ['t1', 't<strong>2</strong>']
    }
  } ), '',
    'create_description when highlighting from solr, but highlighting id field undefined');

    is(create_description('id2', undef, {id1 => {
      accession => ['t1', 't<strong>2</strong>']
      }
    } ), '',
      'create_description when highlighting from solr, but id does not correspond to highlighting');


};

subtest '__get_available_species_filter' => sub {

  is( __get_available_species_filter( ['anopheles_gambiae', 'anopheles_funestus'] ), '("Anopheles_gambiae" OR "Anopheles_funestus")', '__get_available_species_filter transforms a list of ensembl species names into q Solr query of VB species names' );
  is( __get_available_species_filter( [] ), '()', '__get_available_species_filter returns () for empty list' );
  is( __get_available_species_filter( ['anopheles_gambiae', 'anopheles_funestus'], 'anopheles_gambiae' ), '("Anopheles_funestus")', '__get_available_species_filter ignores a specified species' );

};

done_testing();
