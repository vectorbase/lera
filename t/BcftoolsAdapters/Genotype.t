#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::BcftoolsAdapters::Tracks::Genotype qw( _get_genotype _get_alleles );

# getting genotypes

# "%CHROM\t%POS\t%ID\t%REF\t[|%SAMPLE=%TGT]\n";
my $output1 = "CHR1\t1\t.\tA\tG\t#s1=1/1#s2=0|1\nCHR1\t2\tmyVar1\tT\tA,G\t#s1=2/2#s2=1|1#s3=./.";
my $result1 = {
    s1 => {
      name => "s1",
      components => {
        'CHR1-1-s1' => {
          color => "rgb(244,67,54)",
          start => 1,
          data => {
            variation => "CHR1:1",
            position => 1,
            reference => "A",
            genotype => ['G', 'G']
          },
          title => 'G/G',
        },
        'CHR1-2-s1' => {
          color => "rgb(244,67,54)",
          start => 2,
          data => {
            variation => "myVar1",
            position => 2,
            reference => "T",
            genotype => ['G', 'G'],
          },
          title => 'G/G',
        }
      }
    } ,
    s2 => {
      name => "s2",
      components => {
        'CHR1-1-s2' => {
          color => "#FF9800",
          start => 1,
          data => {
              variation => "CHR1:1",
              position => 1,
              reference => "A",
              genotype => ['A', 'G']
          },
          title => 'A/G',
        },
        'CHR1-2-s2' => {
          color => "rgb(244,67,54)",
          start => 2,
          data => {
              variation => "myVar1",
              position => 2,
              reference => "T",
              genotype => ['A', 'A']
          },
          title => 'A/A',
        }
      }
    }
  };
is_deeply( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_parse_output( $output1, {} )->{'entities'}, $result1, 'parsing two lines, dealing with missng var name' );

# test with missing genotypes
my $output2 = "CHR1\t1\t.\tA\tG\t#s1=1/1#s2=0|1\nCHR1\t2\tmyVar1\tT\tG\t#s1=1/1#s2=./.";
my $result2 = {
    s1 => {
      name => "s1",
      components => {
        'CHR1-1-s1' => {
          color => "rgb(244,67,54)",
          start => 1,
          data => {
            variation => "CHR1:1",
            position => 1,
            reference => "A",
            genotype => ['G', 'G'],
          },
          title => 'G/G',
        },
        'CHR1-2-s1' => {
          color => "rgb(244,67,54)",
          start => 2,
          data => {
            variation => "myVar1",
            position => 2,
            reference => "T",
            genotype => ['G', 'G']
          },
          title => 'G/G',
        }
      }
    } ,
    s2 => {
      name => "s2",
      components => {
        'CHR1-1-s2' => {
          color => "#FF9800",
          start => 1,
          data => {
              variation => "CHR1:1",
              position => 1,
              reference => "A",
              genotype => ['A', 'G']
          },
          title => 'A/G',
        }
      }
    }
  };

is_deeply( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_parse_output( $output2, {} )->{'entities'}, $result2, 'parsing two lines, dealing with missing genotype' );


##
# is_missing
##
subtest 'is_missing' => sub {

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::is_missing( '0/1' ), '', "not missing of both alleles present in genotype" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::is_missing( '1/.' ), '', "one allele missing in genotype" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::is_missing( './.' ), 1, "both alleles missing in genotype" );

};

##
# _is_homo_ref
##

subtest '_is_homo_ref' => sub {

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '0/1' ), '', "hetero -> false" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '0|1' ), '', "hetero -> false" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '1/1' ), '', "homo alt -> false" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '1/2' ), '', "hetero alt -> false" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '0/0' ), 1, "homo ref -> true" );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::_is_homo_ref( '0|0' ), 1, "homo ref -> true" );

};


##
# _get_alleles
##

subtest '_get_alleles' => sub {

  my @get_alleles_res1 =  _get_alleles( 'A', 'T,C', 10 );
  is_deeply( \@get_alleles_res1, ['A', ['T', 'C'], 10], 'SNP');

  my @get_alleles_res2 = _get_alleles( 'G', 'GCA', 2566 );
  is_deeply( \@get_alleles_res2 , ['-', ['CA'], 2567], 'Insertion');

  # From VCF QC_arabiensis_burkina_faso.vcf chromosome KB704125
  my @get_alleles_res3 = _get_alleles( 'AAAAAT', 'A', 2420 );
  is_deeply( \@get_alleles_res3 , ['AAAAT', ['-'], 2421], 'Deletion');

  # Examples from http://www.internationalgenome.org/wiki/Analysis/vcf4.0/
  my @get_alleles_res4 = _get_alleles( 'TC', 'TG,T', 2 );
  is_deeply( \@get_alleles_res4, ['C', ['G', '-'], 3], 'Complex1');

  my @get_alleles_res5 = _get_alleles( 'TCG', 'TG,T,TCAG', 2 );
  is_deeply( \@get_alleles_res5, ['CG', ['G', '-', 'CAG'], 3], 'Complex2');

};

##
# _get_genotype
##
( _get_genotype('A', ['T', 'C'], '0/1' ), ['A', 'T'] , 'ref/alt genotype' );
is_deeply( _get_genotype('A', ['T', 'C'], '1/2' ), ['T', 'C'] , 'alt1/alt2 genotype' );
is_deeply( _get_genotype('A', ['T', 'C'], './.' ), [] , './. genotype' );
is_deeply( _get_genotype('A', ['T', 'C'] ), [] , 'undefined genotype string' );
is_deeply( _get_genotype('A', [], '0/0' ), ['A', 'A'] , 'ref genotype' );
is_deeply( _get_genotype('A' ), [] , 'undefined genotype string and alt alles list' );


##
# get_color
##

subtest 'get_color' => sub {

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color(), undef, 'get_color retuns undef if no argumants' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('0/0'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{REF_HOMO}, '0/0 is ref homo' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('0|0'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{REF_HOMO}, '0|0 is ref homo' );

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('1|0'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{REF_ALT}, '1|0 is ref alt' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('0/2'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{REF_ALT}, '0/2 is ref alt' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('0/2/3'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{REF_ALT}, '0/2/3 is ref alt' );

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('1/2'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{ALT_HETERO}, '1/2 is alt hetero' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('3|2|4'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{ALT_HETERO}, '3|2|4 is alt hetero' );

  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('1/1'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{ALT_HOMO}, '1/1 is alt homo' );
  is( Lera::Model::BcftoolsAdapters::Tracks::Genotype::get_color('2|2'), Lera::Model::BcftoolsAdapters::Tracks::Genotype::_get_all_colors_hash()->{ALT_HOMO}, '2|2 is alt homo' );

};

done_testing();
