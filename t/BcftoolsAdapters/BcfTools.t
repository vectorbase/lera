#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Lera::Model::BcftoolsAdapters::BcfTools;


##
# _get_chromosomes
##

is_deeply( Lera::Model::BcftoolsAdapters::BcfTools::_get_chromosomes('offName'),
['offName'], 'only the official chromnosome' );
is_deeply( Lera::Model::BcftoolsAdapters::BcfTools::_get_chromosomes('offName', 'syn1'),
['offName', 'syn1'], 'official chromosome and one synonym' );
is_deeply( Lera::Model::BcftoolsAdapters::BcfTools::_get_chromosomes('offName', ['syn1', 'syn2']),
['offName', 'syn1', 'syn2'], 'official chromosome and multiple synonyms' );


done_testing();
