#!/usr/bin/env perl
use lib 'lib';

use Test::More;
use Data::Dumper;

use Tools;



# first_slice
##
my @arr = (1,2,3,4,5,6);
is_deeply( first_slice(\@arr, 3), [1,2,3], 'clasic perl slice behaviour');
is_deeply( first_slice(\@arr, 6), [1,2,3,4,5,6], 'asking exactly the right number of elements');
is_deeply( first_slice(\@arr, 60), [1,2,3,4,5,6], 'asking too much elements elements');

my @empty_arr = ();
is_deeply( first_slice(\@empty_arr, 3), [], 'empty in -> empty out');



done_testing();
