#!/bin/bash

grep -roh "^use .*::[a-zA-Z]*[ ;]" app.pl lib/ t/ | grep -v "Lera::" | grep -v "Bio::EnsEMBL" | sed 's/use //g' | sed 's/;//g' | sed 's/ \+$//g' | sort | uniq | sed "s/\(.*\)/requires '\1';/g" > cpanfile
