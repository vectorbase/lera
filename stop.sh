#!/bin/bash

# Ensembl perl API (Links for charlie)
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl/modules/
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-compara/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-variation/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-funcgen/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-io/modules
PERL5LIB=${PERL5LIB}:$ENSEMBL_ROOT_DIR/ensembl-tools/modules
export PERL5LIB

hypnotoad app.pl -s
